#!/bin/bash


echo "Checking PHP files for parse errors..."

declare -i errors=0

phpfiles=$(find src -name '*.php')

for phpfile in ${phpfiles[@]} ; do
    php -l ${phpfile} 1>/dev/null
    check=$?
    if [[ $check -ne 0 ]]; then
        ((errors++))
    fi
done
if [[ $errors -ne 0 ]]; then
    echo "Found $errors errors in PHP files"
    exit 1
else
    echo "PHP files are clean"
    exit 0
fi
