#!/bin/bash

echo "Running all checks..."
echo "---------------------------"

declare -i errors=0

pushd utils
allchecks=$(find * -prune -type f)
popd

for checkfile in ${allchecks[@]} ; do
    if  [ "$checkfile" != "all_checks.sh" ]; then
        utils/$checkfile src

        check=$?
        if [[ $check -ne 0 ]]; then
            ((errors++))
        fi
    fi
done
echo "---------------------------"
if [[ $errors -ne 0 ]]; then
    echo "Checks passed with errors"
    exit 1
else
    echo "All checks are clean"
    exit 0
fi
