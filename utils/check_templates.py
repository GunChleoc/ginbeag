#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  check_includes.py
#
#  Copyright 2022 GunChleoc
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

"""Check for missing and redundant template files in PHP project."""

from collections import defaultdict
import os
import os.path
import re
import sys

IGNORE_DIRS = ['images', 'img', 'templates']

TEMPLATE_REGEX = re.compile(
    r"""^.*->addTemplate\(['"]([\/A-Za-z]+\.tpl)['"]\);""")

PHP_TEMPLATES = defaultdict(set)
TEMPLATES_PHP = defaultdict(set)
ALL_TEMPLATES = set()

# Command line colors
COLOR_GREEN = '\033[32m'
COLOR_RED = '\033[31m'
COLOR_ORANGE = '\033[33m'
COLOR_DEFAULT = '\033[39m'


def collect_from_php_files(directory_or_file):
    """Fill PHP_TEMPLATES and TEMPLATES_PHP from PHP files.

    Arguments:
        directory_or_file: If directory, walk the whole directory.
                           If .php file, process the file.
    """
    if os.path.isdir(directory_or_file):
        for root, _dirs, files in os.walk(directory_or_file):
            for filename in sorted(filter((lambda x:
                                           x.endswith('.php')),
                                          files)):
                filepath = os.path.join(root, filename)
                with open(filepath) as lines:
                    for line in lines:
                        match = TEMPLATE_REGEX.match(line)
                        if match:
                            PHP_TEMPLATES[filepath].add(match.group(1))
                            TEMPLATES_PHP[match.group(1)].add(filepath)
    else:
        with open(directory_or_file) as lines:
            for line in lines:
                match = TEMPLATE_REGEX.match(line)
                if match:
                    PHP_TEMPLATES[filepath].add(match.group(1))
                    TEMPLATES_PHP[match.group(1)].add(filepath)


def main(args):
    """Check for missing and redundant template files in PHP project."""
    errors_count = 0

    base_path = os.getcwd()

    if len(args) > 1:
        os.chdir(args[1])

    print('######################')
    print('Templates check tool')
    print('######################')

    for directory_or_file in filter((lambda x:
                                     x not in IGNORE_DIRS
                                     and (os.path.isdir(x)
                                          or x.endswith('.php'))),
                                    sorted(os.listdir('.'))):
        collect_from_php_files(directory_or_file)

    templatedir = 'templates/default'
    templatedir_length = len(templatedir) + 1

    # Check for unused templates
    for root, _dirs, files in os.walk(templatedir):
        for filename in sorted(filter((lambda x:
                                       x.endswith('.tpl')),
                                      files)):
            template = os.path.join(root, filename)[templatedir_length:]
            ALL_TEMPLATES.add(template)
            if template not in TEMPLATES_PHP.keys():
                print('%sUnused template%s  %s' %
                      (COLOR_ORANGE, COLOR_DEFAULT, template))
                errors_count += 1

    # Check for missing templates
    for php_file in PHP_TEMPLATES.keys():
        for template in PHP_TEMPLATES[php_file]:
            if template not in ALL_TEMPLATES:
                print('%sMissing template%s %s %sfor PHP file%s %s' % (
                    COLOR_RED, COLOR_DEFAULT, template,
                    COLOR_GREEN, COLOR_DEFAULT, php_file))
                errors_count += 1

    os.chdir(base_path)

    print('######################')
    print('Found %i errors' % (errors_count))
    print('######################')
    if errors_count:
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
