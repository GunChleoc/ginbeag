#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  check_includes.py
#
#  Copyright 2022 GunChleoc
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

"""Check for missing and redundant includes in PHP project."""

from collections import defaultdict
import os
import os.path
import re
import sys

IGNORE_DIRS = ['images', 'img', 'templates']

REQUIREMENT_REGEX = re.compile(
    r"^\s*require(_once){0,1} BASEDIR.'\/([\/a-zA-Z0-9_-]*\.php)'.*")
INCLUDE_REGEX = re.compile(
    r"^\s*include(_once){0,1} BASEDIR.'\/([\/a-zA-Z0-9_-]*\.php)'.*")

VARIABLE_REGEX = re.compile(r'^(\$[A-Za-z0-9_-]+)\s')
DEFINE_REGEX = re.compile(r"^\s*define\s*\('([A-Z_]+)'")
FUNCTION_REGEX = re.compile(
    r'(^|\s+([A-Za-z0-9_-]+\s+){0,1}static\s+)function\s+([A-Za-z0-9_-]+\()')
CLASS_REGEX = re.compile(r'^class\s+([A-Za-z0-9_-]+)\s')

# Symbols to be ignored in all checks
IGNORE_SYMBOLS = ['BASEDIR', 'DEBUG',
                  'trim(', '$backtrace', '$isadmin', '$sql']

# Scripts that contain global variables.
# Variable symbols will only be taken from these.
GLOBAL_SCRIPTS = ['config.php', 'functions/treefunctions.php',
                  'functions/db.php', 'functions/emailvariables.php',
                  'includes/constants.php']

# Filled by Requirements
ALL_SYMBOLS = defaultdict(set)
ALL_PHP_FILES = set()

# Command line colors
COLOR_GREEN = '\033[32m'
COLOR_RED = '\033[31m'
COLOR_ORANGE = '\033[33m'
COLOR_DEFAULT = '\033[39m'


class Requirements:
    """Container to collect Requirements for a PHP file.

    Parses itself.
    """

    def __init__(self, filename):
        self.filename = filename
        self.requirements = []
        self.includes = []
        self.symbols = set()
        with open(filename) as lines:
            # Cheap comments filter. No support for multiline comments.
            for line in filter((lambda x:
                                not (x.startswith('/') or x.startswith('#'))),
                               lines):

                self._parse_includes(line)
                self._parse_symbols(line, filename)

        self.requirements = sorted(self.requirements)
        self.symbols = sorted(self.symbols)
        for symbol in self.symbols:
            ALL_SYMBOLS[symbol].add(filename)

    def _parse_includes(self, line):
        match = REQUIREMENT_REGEX.match(line)
        if match:
            self.requirements.append(match.group(2))
        else:
            # Add includes for symbols check
            match = INCLUDE_REGEX.match(line)
            if match:
                self.includes.append(match.group(2))

    def _parse_symbols(self, line, filename):
        # Constants
        match = DEFINE_REGEX.match(line)
        if match:
            symbol = match.group(match.lastindex)
            if symbol not in IGNORE_SYMBOLS:
                for delimiter in [' ', '=', '.', ')', '[', ',']:
                    self.symbols.add(symbol + delimiter)
        # Global variables
        if filename in GLOBAL_SCRIPTS:
            match = VARIABLE_REGEX.match(line)
            if match:
                symbol = match.group(match.lastindex)
                if symbol not in IGNORE_SYMBOLS:
                    for delimiter in [' ', '=', '.', ')', '[', ',']:
                        self.symbols.add(symbol + delimiter)
        # Special treatment for class declarations
        match = CLASS_REGEX.match(line)
        if match:
            symbol = match.group(match.lastindex)
            if symbol not in IGNORE_SYMBOLS:
                self.symbols.add('new '+symbol+'(')
                self.symbols.add('extends '+symbol)
        # Functions already have an end delimiter
        match = FUNCTION_REGEX.match(line)
        if match:
            symbol = match.group(match.lastindex)
            if symbol not in IGNORE_SYMBOLS:
                self.symbols.add(symbol)

    def __repr__(self):
        return 'File %s. R: %s. S: %s' % (self.filename,
                                          ', '.join(self.requirements),
                                          ', '.join(self.symbols))

    def __str__(self):
        return 'File %s' \
            '\n     Requirements: %s' \
            '\n     Symbols:  %s' % (self.filename,
                                     '\n               '.join(
                                         self.requirements),
                                     ', '.join(self.symbols))


def collect_php_files(directory_or_file, target):
    """Collect Requirements for PHP files.

    Arguments:
        directory_or_file: If directory, walk the whole directory.
                           If .php file, process the file.
        target: Requirements collected previously.
                New Requirements will be added to this collection.

    Returns:
        target with new Requirements added.
    """
    if os.path.isdir(directory_or_file):
        for root, _dirs, files in os.walk(directory_or_file):
            for filename in sorted(filter((lambda x:
                                           x.endswith('.php')),
                                          files)):
                filepath = os.path.join(root, filename)
                print('Collecting file:', filepath)
                target[filepath] = Requirements(filepath)
                ALL_PHP_FILES.add(filepath)
    else:
        print('Collecting file:', directory_or_file)
        target[directory_or_file] = Requirements(directory_or_file)
        ALL_PHP_FILES.add(directory_or_file)
    return target


def find_unused_symbols(phpfile, all_requirements):
    """Find unused symbols from include in PHP file.

    Arguments:
        phpfile: Requirements to check
        all_requirements: All Requirements collected previously.

    Returns:
        Error count.
    """
    errors_count = 0

    unused_symbols = set()
    for requirement in phpfile.requirements:
        symbol_found = False
        if requirement not in all_requirements.keys():
            raise LookupError(('UNKNOWN requirement: %s in %s, ABORTING') % (
                requirement, phpfile.filename))
        symbols = all_requirements[requirement].symbols
        with open(phpfile.filename) as lines:
            # Cheap comments filter. No support for multiline comments.
            for line in filter((lambda x:
                                not (x.startswith('/') or x.startswith('#'))),
                               lines):
                for symbol in symbols:
                    if symbol in line:
                        symbol_found = True
                        break
                if symbol_found:
                    break
        if not symbol_found:
            unused_symbols.add(requirement)
    if unused_symbols:
        print('\nUNUSED:   ', phpfile.filename)
        for requirement in unused_symbols:
            print('           ⇦ %s' % (requirement))
        errors_count += len(unused_symbols)
    return errors_count


def find_missing_symbols(phpfile, requirements_closure):
    """Find symbols that are missing an include in PHP file.

    Arguments:
        phpfile: Requirements to check
        requirements_closure: Files that are transitively included,
                              and the file itself.

    Returns:
        Error count.
    """
    errors_count = 0

    for include in phpfile.includes:
        if include not in ALL_PHP_FILES:
            errors_count += 1
            print('%sUNKNOWN include:%s %s'
                  '\n                 '
                  '%sin %s%s' % (
                      COLOR_RED, COLOR_DEFAULT,
                      include,
                      COLOR_ORANGE, phpfile.filename, COLOR_DEFAULT))
        requirements_closure.add(include)

    needed_symbols = set()
    missing_symbols = set()
    all_symbols_keys = ALL_SYMBOLS.keys()

    with open(phpfile.filename) as lines:
        # Cheap comments filter. No support for multiline comments.
        for line in filter((lambda x:
                            not (x.startswith('/') or x.startswith('#'))),
                           lines):
            for symbol in all_symbols_keys:
                if symbol in line:
                    # Ensure we don't grap superstrings
                    symbol_regex = re.compile(
                        r'(^|.*\W)(' + re.escape(symbol) + ').*')
                    match = symbol_regex.match(line)
                    if match:
                        needed_symbols.add(symbol)

    for symbol in needed_symbols:
        symbol_found = False
        symbol_files = ALL_SYMBOLS[symbol]
        for candidate in symbol_files:
            if candidate in requirements_closure:
                symbol_found = True
                break
        if not symbol_found:
            missing_symbols.add(symbol)
    if missing_symbols:
        print('%sMISSING:%s   in %s:%s' %
              (COLOR_RED, COLOR_ORANGE, phpfile.filename, COLOR_DEFAULT))
        for symbol in missing_symbols:
            print('           %s'
                  '\n           '
                  '⇨ %s%s%s' % (symbol, COLOR_GREEN,
                                ('\n           %s%s%s ' % (
                                    COLOR_DEFAULT, '⇨', COLOR_GREEN))
                                .join(ALL_SYMBOLS[symbol]),
                                COLOR_DEFAULT))
            errors_count += 1
    return errors_count


def check_php_file(phpfile, all_requirements):
    """Check Requirements for a PHP file.

    Arguments:
        phpfile: Requirements to check
        all_requirements: All Requirements collected previously.

    Returns:
        Error count.
    """
    errors_count = 0

    # Find unused symbols
    errors_count += find_unused_symbols(phpfile, all_requirements)

    # Find duplicate requirements
    files_to_check = defaultdict(set)
    for requirement in phpfile.requirements:
        files_to_check[phpfile.filename].add(requirement)

    # We're filling this here for the missing symbols check below
    requirements_closure = set()
    requirements_closure.add(phpfile.filename)

    while files_to_check:
        keys = set(files_to_check.keys())
        for current_file in keys:
            requirements_closure.add(current_file)

            for required_file in files_to_check[current_file]:
                if required_file not in all_requirements.keys():
                    raise LookupError(
                        ('UNKNOWN requirement: %s in %s, ABORTING') % (
                            required_file, current_file))

                requirements_closure.add(required_file)
                for mentioned_requirement \
                        in all_requirements[required_file].requirements:
                    requirements_closure.add(mentioned_requirement)

                    if mentioned_requirement == phpfile.filename:
                        print('%sCIRCULAR:%s  in %s:%s %s'
                              '\n           %s'
                              ' ⇨ %s%s%s' % (COLOR_RED, COLOR_ORANGE,
                                             phpfile.filename,
                                             COLOR_DEFAULT,
                                             mentioned_requirement,
                                             current_file,
                                             COLOR_GREEN, required_file,
                                             COLOR_DEFAULT))
                        errors_count += 1
                    elif mentioned_requirement in phpfile.requirements:
                        print('%sDUPLICATE: in %s:%s %s'
                              '\n           %s'
                              ' ⇨ %s%s%s' % (COLOR_ORANGE,
                                             phpfile.filename,
                                             COLOR_DEFAULT,
                                             mentioned_requirement,
                                             current_file, COLOR_GREEN,
                                             required_file, COLOR_DEFAULT))
                        errors_count += 1
                        files_to_check[required_file].add(
                            mentioned_requirement)

            del files_to_check[current_file]

    # Missing symbols
    errors_count += find_missing_symbols(phpfile, requirements_closure)

    return errors_count


def main(args):
    """Check for missing and redundant includes in PHP project."""
    errors_count = 0

    base_path = os.getcwd()

    if len(args) > 1:
        os.chdir(args[1])

    print('######################')
    print('Duplicates check tool')
    print('######################')
    requirements = dict()
    for directory in filter((lambda x:
                             x not in IGNORE_DIRS
                             and (os.path.isdir(x) or x.endswith('.php'))),
                            sorted(os.listdir('.'))):
        requirements = collect_php_files(directory, requirements)
    print('######################')
    print('Checking files')
    print('######################')

    for filepath in sorted(requirements):
        try:
            errors_count += check_php_file(
                requirements[filepath], requirements)
        except LookupError as error:
            print(error)
            os.chdir(base_path)
            return 1
    os.chdir(base_path)

    print('######################')
    print('Found %i errors' % (errors_count))
    print('######################')
    if errors_count:
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
