<div id="{JSID}fileoperationscontents" class="contentoutline">
		<fieldset>
			<legend class="highlight">File Operations</legend>
			<fieldset>
				<legend>Image file</legend>
				<form name="replaceimageform" enctype="multipart/form-data" method="post">
					{HIDDENVARS}
					<label for="{JSID}replaceimagefile">Select a new image:</label></br>
					<input type="file" name="{JSID}replaceimagefile" id="{JSID}replaceimagefile" size="30" maxlength="255" />
					<input type="button" id="{JSID}replaceimagebutton" name="{JSID}replaceimagebutton" value="Replace Image File" class="mainoption">
					<div class="formexplain">Replace this image with a new file.</div>
				</form>
				<!-- BEGIN switch DEFAULTIMAGEWIDTH -->
				<form name="resizeimageform" enctype="multipart/form-data" method="post">
					{HIDDENVARS}
					<input type="button" id="{JSID}resizeimage" name="{JSID}resizeimage" value="Resize Image Width" class="mainoption" />
					<div class="formexplain">
						Scale the image down to fit the default width of {DEFAULTIMAGEWIDTH} pixels.
						<br />Supported filetypes: <em>gif</em>, <em>jpeg</em>,<em>png</em>, <em>wbmp</em>, <em>xbm</em>.
					</div>
				</form>
				<!-- END switch DEFAULTIMAGEWIDTH -->
			</fieldset>
			<fieldset>
				<legend>Thumbnail file</legend>
				<!-- BEGIN switch NO_THUMBNAIL -->
				<form name="addthumbform" enctype="multipart/form-data" method="post">
					{HIDDENVARS}
					<label for="{JSID}addthumbnailfile">Select a new thumbnail:</label></br>
					<input type="file" id="{JSID}addthumbnailfile" name="{JSID}addthumbnailfile" size="30" maxlength="255" />
					<input type="button" id="{JSID}addthumbnailbutton" name="{JSID}addthumbnailbutton" value="Add Thumbnail" class="mainoption" />
					<div class="formexplain">Add a thumbnail to this image.</div>
				</form>
				<!-- END switch NO_THUMBNAIL -->
				<!-- BEGIN switch THUMBNAIL -->
				<form name="replacethumbform" enctype="multipart/form-data" method="post">
						{HIDDENVARS}
				<label for="{JSID}replacethumbnailfile">Select a new thumbnail:</label></br>
					<input type="file" id="{JSID}replacethumbnailfile" name="{JSID}replacethumbnailfile" size="30" maxlength="255" />
					<input type="button" id="{JSID}replacethumbnailbutton" name="{JSID}replacethumbnailbutton" value="Replace Thumbnail File" class="mainoption" />
					<div class="formexplain">Replace the thumbnail for this image with a new file.</div>
				</form>
				<!-- END switch THUMBNAIL -->
				<!-- BEGIN switch GDAVAILABLE -->
				<form name="createthumbnailform" enctype="multipart/form-data" method="post">
					{HIDDENVARS}
					<input type="button" id="{JSID}createthumbnail" name="{JSID}createthumbnail" value="Generate Thumbnail" class="mainoption" />
					<div class="formexplain">
						Autogenerate a new thumbnail.
						<br />Supported filetypes: <em>gif</em>, <em>jpeg</em>,<em>png</em>, <em>wbmp</em>, <em>xbm</em>.
					</div>
				<!-- END switch GDAVAILABLE -->
				</form>
			</fieldset>
			<fieldset>
				<legend>Deleting</legend>
				<form name="deleteform" method="post">
					{HIDDENVARS}
					<input type="button" id="{JSID}deleteimage" name="{JSID}deleteimage" value="Delete this image from database and file system" class="mainoption" />
				</form>
				<!-- BEGIN switch THUMBNAIL -->
				<form name="deletethumbform" method="post">
					{HIDDENVARS}
					<input type="button" id="{JSID}deletethumbnail" name="{JSID}deletethumbnail" value="Delete Thumbnail File" class="mainoption" />
				</form>
				<!-- END switch THUMBNAIL -->
			</fieldset>
			&nbsp;&nbsp;
			<input type="button" id="{JSID}cancelfileoperationsbutton" name="cancelbutton" value="Cancel" />
		</fieldset>
	</div>
	<div class="newline"></div>
</div>
