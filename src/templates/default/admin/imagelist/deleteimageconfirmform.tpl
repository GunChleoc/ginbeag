<h1 class="pagetitle">Deleting image {FILENAME}</h1>
<p class="highlight">Are you sure you want to delete this image?<br>This cannot be undone!</p>
<p id="error" class="highlight"></p>
<input type="button" id="{JSID}deleteimageconfirm" name="{JSID}deleteimageconfirm" value="Yes, please delete" />
&nbsp; &nbsp;
<input type="button" id="{JSID}deleteimagecancel" name="{JSID}deleteimagecancel" value="Cancel deleting file" />
{IMAGE}
