<form method="get">
	<div id="imagesnavigator" class="leftalign">
		{HIDDENVARS}
		<label for="number">Images per page: </label>
		<input id="number" type="text" name="number" size="2" maxlength="2" value="{NUMBER}" />
		&nbsp;&nbsp;&nbsp;
		{ORDERSELECTION}
		&nbsp;
		{ASCDESCSELECTION}
		&nbsp;&nbsp;&nbsp;
		<label for="searchterm">Search: </label>
		<input id="searchterm" type="text" maxlength="255" size="20" name="searchterm" title="Leave blank if you don't want to search" value="{SEARCHTERM}" />
		&nbsp;&nbsp;&nbsp;
		<input type="submit" name="doorder" value="Go" class="mainoption" />
		&nbsp;&nbsp;&nbsp;
		<input type="button" id="searchtoggle" name="searchtoggle" value="{SEARCHTOGGLE}" />
	</div>
	<!-- BEGIN switch PAGEMENU -->
	<div class="rightalign">{PAGEMENU}</div>
	<!-- END switch PAGEMENU -->
	<div class="newline"></div>
</form>
