{JAVASCRIPT}
{HIDDENVARS}
<span id="{JSID}imageform" class="galleryimage contentsection" style="margin-top: 2em; min-height: 40em;">
	<fieldset>
		<legend class="highlight" onclick="copy('{FILENAME}')" title="Click to copy filename">Image: <em>{FILENAME}</em></legend>
		<span class="smalltext highlight">Image path: {FILEPATH}</span>
		<span id="{JSID}image">{IMAGE}</span>
		<span id="{JSID}categorylist">{CATEGORYLIST}</span>
		<fieldset>
			<legend class="highlight">Edit</legend>
		{EDITDESCRIPTIONFORM} &nbsp; &nbsp;
		{EDITCATEGORIESFORM} &nbsp; &nbsp;
		{FILEOPERATIONSFORM}
		</fieldset>
		<div id="{JSID}usage"></div><br>
		<input type="button" id="{JSID}showusagebutton" name="showusagebutton" value="Show where this image is used" class="mainoption" />
	</fieldset>
</span>
