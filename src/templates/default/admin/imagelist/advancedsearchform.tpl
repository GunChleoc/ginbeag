<!-- Button labels for the "advanced" form are used in imagelist code, so keep them consistent -->
<div class="imagecontentarea">
	<div class="contentoutline">
		<div class="contentheader">Search Images</div>
		<div class="contentsection">
			<form name="imagefilterform" method="get">
				{HIDDENVARS}
				<div class="leftalign" style="width:31% !important; padding-right:2%;">
					<fieldset>
						<legend class="highlight">Image description and filename</legend>
						<label for="filterfilename">Filename: </label>
						<input id="filterfilename" type="text" maxlength="255" size="30" name="filename" value="{FILENAME}" />
						<div class="formexplain">Search for text contained in a filename.</div>

						<br/><label for="filtercaption">Caption: </label>
						<input id="filtercaption" type="text" maxlength="255" size="30" name="caption" value="{CAPTION}" />
						<div class="formexplain">Search for text contained in a caption.</div>

						<br/><label for="filtersource">Source: </label>
						<input id="filtersource" type="text" maxlength="255" size="30" name="source" value="{SOURCE}" />
						<br />{SOURCEBLANKFORM}
						<div class="formexplain">Search for text contained in a source.</div>

						<br/><label for="filtersourcelink">Source URL: </label>
						<input id="filtersourcelink" type="text" maxlength="255" size="30" name="sourcelink" value="{SOURCELINK}" />
						<div class="formexplain">Search for text contained in a source link.</div>

						<br/><label for="filtercopyright">Copyright Holder: </label>
						<input id="filtercopyright" type="text" maxlength="255" size="30" name="copyright" value="{COPYRIGHT}" />
						<br />{COPYRIGHTBLANKFORM}
						<div class="formexplain">Search for copyright holder.</div>
					</fieldset>
					<fieldset>
						<legend class="highlight">Only images uploaded by:</legend>
				        {USERSSELECTIONFORM}
					</fieldset>
					<input type="submit" name="advanced" value="Display Selection" class="mainoption" />
				</div>
				<div class="leftalign" style="width:31% !important; padding-right:2%;">
					<fieldset>
						<legend class="highlight">Image category</legend>
				        {CATEGORYSELECTION}
				        <br />{CATEGORIESBLANKFORM}
					</fieldset>
				</div>
				<div class="leftalign" style="width:31% !important; padding-right:2%;">
					<fieldset>
						<legend class="highlight">Special Searches</legend>
						<fieldset>
							<legend>File System</legend>
							<input type="submit" name="advanced" value="Missing Image Files" class="mainoption" />
							<div class="formexplain">Images that are in the database but missing from the file system.</div>
							<br /><input type="submit" name="advanced" value="Missing Thumbnail Files" class="mainoption" />
							<div class="formexplain">Image thumbnails that are in the database but missing from the file system.</div>
							<br /><input type="submit" name="advanced" value="Unknown Image Files" class="mainoption" />
							<div class="formexplain">Image files that aren't registered in the database.</div>
						</fieldset>
						<fieldset>
							<legend>Database</legend>
							<input type="submit" name="advanced" value="Unused Images" class="mainoption" />
							<div class="formexplain">Images that have not been used in any page.<br /><span class="highlight">NOTE:</span> This does not search for images added to pages with the [img]-tag!</div>
							<br /><input type="submit" name="advanced" value="Images missing Thumbnails" class="mainoption" />
							<div class="formexplain">Images that should have a thumbnail but have none.</div>
						</fieldset>
					</fieldset>
				</div>
			</form>
			<div class="newline"></div>
			<div class="spacer">&nbsp;</div>
			<div class="highlight">
				<form name="imagefilterform" method="get">
					{HIDDENVARS}
					<input type="submit" name="advanced" value="Clear search" />
					<!-- BEGIN switch MESSAGE -->{MESSAGE}<!-- END switch MESSAGE -->
				</form>
			</div>
		</div>
	</div>
</div>
