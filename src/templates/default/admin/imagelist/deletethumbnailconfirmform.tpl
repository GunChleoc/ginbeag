<p class="pagetitle">Deleting thumbnail for image {FILENAME}</p>
<p class="highlight">Are you sure you want to delete the thumbnail for this image?<br>This cannot be undone!</p>
<p id="error" class="highlight"></p>
<input type="button" id="{JSID}deletethumbnailconfirm" name="{JSID}deletethumbnailconfirm" value="Yes, please delete" />
&nbsp; &nbsp;
<input type="button" id="{JSID}deletethumbnailcancel" name="{JSID}deletethumbnailcancel" value="Cancel deleting file" />
{IMAGE}
