<div id="{JSID}editdescriptioncontents" class="contentoutline">
	<form id="{JSID}editdescriptionform" method="post" onsubmit="event.preventDefault();">
			<fieldset>
				<legend class="highlight">Edit Image description</legend>
				<fieldset>
					<legend>Caption elements</legend>
					<label for="{JSID}caption">Caption: </label>
					<input id="{JSID}caption" type="text" name="caption" value="{CAPTION}" size="30" maxlength="200" />
					<div class="formexplain">Describe what's on the image.</div>

					<br /><label for="{JSID}source">Source Name: </label>
					<input id="{JSID}source" type="text" name="source" value="{SOURCE}" size="30" maxlength="255" />
					<div class="formexplain">Name of the website or other source you got the image from.</div>

					<br /><label for="{JSID}sourcelink">Source URL: </label>
					<input id="{JSID}sourcelink" type="text" name="sourcelink" value="{SOURCELINK}" size="30" maxlength="255" />
					<div class="formexplain">Link to the website you got the image from, starting with the protocol, e.g. <em>https://</em>.</div>

					<br /><label for="{JSID}copyright">Copyright Holder: </label>
					<input id="{JSID}copyright" type="text" name="copyright" value="{COPYRIGHT}" size="30" maxlength="255" />
					<div class="formexplain">Name of the person or organization who owns this image.</div>
				</fieldset>

				<fieldset>
					<legend>Permissions</legend>
					<span id="{JSID}permission">{PERMISSION_GRANTED} {NO_PERMISSION}</span>
					<div class="formexplain">Did the copyright owner give us permission to use this image?</div>
				</fieldset>

				<input type="button" id="{JSID}savedescriptionbutton" name="savebutton" value="Save Image Description" class="mainoption" />
				&nbsp;&nbsp;
				<input type="reset" id="{JSID}resetdescriptionbutton" name="reset" value="Reset" />
				&nbsp;&nbsp;
				<input type="button" id="{JSID}canceldescriptionbutton" name="cancelbutton" value="Cancel" />
			</fieldset>
		</form>
</div>
