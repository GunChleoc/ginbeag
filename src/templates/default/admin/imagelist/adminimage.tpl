{HIDDENVARS}
<div style="height:{ELEMENTHEIGHT}">
	<!-- BEGIN switch IMAGE -->
	<span class="smalltext"><br>{IMAGEPROPERTIES}</span>
	<!-- END switch IMAGE -->
	<!-- BEGIN switch THUMBNAILPROPERTIES -->
	<br /><span class="smalltext"><b>Thumbnail:</b>&nbsp;{THUMBNAILPROPERTIES}</span>
	<!-- END switch THUMBNAILPROPERTIES -->
	<!-- BEGIN switch RESIZED -->
	<br /><span class="smalltext highlight">Resized image for viewing.</span>
	<!-- END switch RESIZED -->
	<br />

	<!-- BEGIN switch IMAGE -->
	<!-- BEGIN switch THUMBNAIL -->
	<img id="{JSID}imagefile" src="{THUMBNAILPATH}?cache={TIMESTAMP}" title="Click to view full size" />
	<!-- END switch THUMBNAIL -->
	<!-- BEGIN switch NO_THUMBNAIL -->
	<img id="{JSID}imagefile" src="{IMAGEPATH}?cache={TIMESTAMP}" width="{WIDTH}" height="{HEIGHT}" title="Click to view full size" width="{WIDTH}" height="{HEIGHT}" />
	<!-- END switch NO_THUMBNAIL -->
	<!-- END switch IMAGE -->
	<!-- BEGIN switch NO_IMAGE -->
	<p class="highlight">File <i>{IMAGEFILE}</i> not found</p>
	<!-- BEGIN switch THUMBNAIL -->
	<br />&nbsp;<br /><img src="{THUMBNAILPATH}?cache={TIMESTAMP}"  title="Thumbnail for {IMAGEFILE}" />
	<!-- END switch THUMBNAIL -->
	<!-- END switch NO_IMAGE -->
	<!-- BEGIN switch CAPTION -->
	<div class="smalltext"><br />{CAPTION}</div>
	<!-- END switch CAPTION -->
</div>

