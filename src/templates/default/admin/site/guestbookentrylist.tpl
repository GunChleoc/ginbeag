<h1 class="headerpagetitle">Contact & Guestbook</h1>

<div class="contentoutline">
  <div class="contentheader">Contact Form Introduction</div>
  <div class="contentsection">
      {CONTACTINTRO}
  </div>
</div>
<div class="contentoutline">
  <div class="contentheader">Guestbook Introduction</div>
  <div class="contentsection">
      {GUESTBOOKINTRO}
  </div>
</div>

{ENABLEFORM}

<!-- BEGIN switch NO_ENTRIES -->
<p class="highlight">{NO_ENTRIES}</p>
<!-- END switch NO_ENTRIES -->
<!-- BEGIN switch ENTRIES -->
<div class="contentheader">Guestbook entries</div>
<div class="contentsection">
	<div align="right">{PAGEMENU}</div>
	{ENTRIES}
	<hr>
	<div align="right">{PAGEMENU}</div>
</div>
<!-- END switch ENTRIES -->
