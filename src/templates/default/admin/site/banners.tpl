<h1 class="headerpagetitle">Banners</h1>
<form name="displaybannersform" action="{DISPLAYACTIONVARS}" method="post">
{DISPLAYHIDDENVARS}
	<div class="contentheader">General Banner Settings</div>
	<div class="contentsection">
		<fieldset>
			<legend class="highlight">Display banners?</legend>
			{DISPLAYBANNERS_YES} {DISPLAYBANNERS_NO}
			<div class="formexplain">Turn all banners on or off.</div>
		</fieldset>
		<input type="submit" name="displaybanners" value="Submit" />
	</div>
</form>

<div class="contentheader">Edit Banners</div>
<div class="contentsection">
	{INSERTEDITOR}
	{EDITFORM}
</div>
