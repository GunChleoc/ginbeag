<h1 class="headerpagetitle">Redirects</h1>
<div class="highlight">Here there be dragons!</div>
<div class="contentoutline">
  <div class="contentheader">Add Redirect</div>
    <div class="contentsection">
      <form name="addform" action="{ACTIONVARS}" method="post">
          <label for="pattern">Regular Expression: </label>
          <input id="pattern" type="text" name="pattern" size="50" maxlength="255" value="" />
          <label for="replacement">Replacement: </label>
          <input id="replacement" type="text" name="replacement" size="50" maxlength="255" value="" />
          <input type="submit" name="addredirect" value="Add redirect" class="mainoption" />
          <div class="formexplain">
            Add <a href="https://regex101.com/" target="_blank">regular expression</a> replacement for text.
            The patterns must include the delimiters.
          </div>
      </form>
  </div>
</div>

<!-- BEGIN switch REDIRECT -->
<div class="contentoutline">
  <div class="contentheader">Edit Redirects</div>
  <div class="contentsection">
    {REDIRECT}
    <div class="formexplain">
      Edit <a href="https://regex101.com/" target="_blank">regular expression</a> replacements for text.
      The patterns must include the delimiters.
      Make one of the fields blank to remove an entry.
    </div>
  </div>
</div>
<!-- END switch REDIRECT -->
