<form name="permissionsform" action="{ACTIONVARS}" method="post">
	<div class="contentoutline">
		<div class="contentheader">Copyright information</div>
		<div class="contentsection">
			<fieldset>
				<legend class="highlight">Text Content</legend>
				<label for="copyright">Copyright Holder:</label>
				<input id="copyright" type="text" name="copyright" size="70" maxlength="255" value="{COPYRIGHT}" />
				<br /><label for="permission">Permissions:</label>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {PERMISSION_GRANTED} &nbsp;&nbsp;&nbsp; {NO_PERMISSION}
			</fieldset>
			<fieldset>
				<legend class="highlight">Image Content</legend>
				<label for="imagecopyright">Image Copyright Holder:</label>
				<input id="imagecopyright" type="text" name="imagecopyright" size="70" maxlength="255" value="{IMAGE_COPYRIGHT}" />
			</fieldset>
			{SUBMITROW}
		</div>
	</div>
</form>
<div class="newline"></div>
