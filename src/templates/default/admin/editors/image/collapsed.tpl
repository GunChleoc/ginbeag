<form title="Click to edit image">
	{HIDDENVARS}
	<!-- BEGIN switch DISPLAYIMAGE -->
	<div <!-- BEGIN switch HEIGHT -->style="height:{HEIGHT}"<!-- END switch HEIGHT -->>
		<div id="{JSID}displayimage" class="captionedimage" style="{HALIGN}<!-- BEGIN switch MIN-WIDTH --> min-width: {MIN-WIDTH}; <!-- END switch MIN-WIDTH --><!-- BEGIN switch WIDTH --> width: {WIDTH}; <!-- END switch WIDTH --><!-- BEGIN switch CENTER -->display: block; margin: 0 auto;<!-- END switch CENTER -->">
			{DISPLAYIMAGE}
			<div class="imagecaption">
				{CAPTION}
				<!-- BEGIN switch NO_THUMBNAIL -->
				<div class="highlight">{NO_THUMBNAIL}</div>
				<!-- END switch NO_THUMBNAIL -->
			</div>
		</div>
	</div>
	<!-- END switch DISPLAYIMAGE -->
	<!-- BEGIN switch BUTTON -->
	<input id="{JSID}addimage" type="button" value="Add Image" />
	<!-- END switch BUTTON -->
</form>
<div class="newline">&nbsp;</div>
