<form method="post">
	<fieldset>
		<legend class="highlight">Layout</legend>
		<div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto auto;">
			<fieldset>
				<legend>Horizontal Alignment</legend>
				{LEFT_ALIGN_BUTTON} {CENTER_ALIGN_BUTTON} {RIGHT_ALIGN_BUTTON}
				<div class="formexplain">Choose if the image will be aligned to left, right or center.
				Left or right alignment will make the text flow around the image, center alignment will place the image above the text.</div>
			</fieldset>
			<fieldset>
				<legend>Image Size</legend>
				{THUMBNAIL}
				<div class="formexplain">Choose if the image thumbnail will be used, or if the image will be shown at fullsize.
				If this is selected and there is no thumbnail available, the image display will be shrunk automatically.</div>
				<div class="newline">
				</div>
			</fieldset>
		</div>
		<input type="submit" id="{JSID}submitlayout" name="{SUBMITNAME}" value="Save Image Layout" class="mainoption" />
		&nbsp;&nbsp;<input id="{JSID}submitlayout"type="reset" value="Reset" />
	</fieldset>
</form>
