<div style="margin:1em">
  <p class="sectiontitle">{TITLE}</p>
  <p class="highlight">Are you sure you want to delete this item?<br>This cannot be undone!</p>
  <p id="error" class="highlight"></p>

  <div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
    <div><input type="button" id="{JSID}deleteconfirm" value="Yes, please delete" /></div>
    <div style="text-align:right"><input type="button" id="{JSID}deletecancel" value="Cancel deleting" /></div>
  </div>
</div>

<div class="contentsection" style="margin:1em">{ITEM}</div>
