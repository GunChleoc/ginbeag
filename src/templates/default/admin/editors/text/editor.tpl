{JAVASCRIPT}
<form id="{JSID}editor">
	{HIDDENVARS}
	<div lang={LANGUAGE}>
		<div id="{JSID}previewarea" class="editorpreviewarea">{PREVIEWTEXT}</div>
		<div id="{JSID}status" name="status" class="highlight"></div>
		<div id="{JSID}editorcontents">{EDITORCONTENTS}</div>
	</div>
</form>
