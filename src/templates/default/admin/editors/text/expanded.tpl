<div class="contentoutline" style="width:60em;text-align:center;">
		<div class="contentheader">Edit Text</div>
		<div class="contentsection">
			<div class="editorcodebuttonrow" style="text-align:center">
				<div class="leftalign editorcodebutton"><input id="{JSID}bold" type="button" class="button" accesskey="b" name="bold" value=" B &nbsp;" style="font-weight:bold" /></div>
				<div class="leftalign editorcodebutton"><input id="{JSID}italic" type="button" class="button" accesskey="i" name="italic" value=" i &nbsp;" style="font-style:italic" /></div>
				<div class="leftalign editorcodebutton"><input id="{JSID}underline" type="button" class="button" accesskey="u" name="underline" value=" u &nbsp;" style="text-decoration: underline" /></div>

				<div class="leftalign editorlineleft"><input id="{JSID}ul" type="button" class="button" accesskey="l" name="ul" value="List &nbsp;" /></div>
				<div class="leftalign editorcodebutton"><input id="{JSID}ol" type="button" class="button" accesskey="o" name="ol" value="List= &nbsp;" /></div>
				<div class="leftalign editorcodebutton"><input id="{JSID}li" type="button" class="button" accesskey="e" name="li" value="* &nbsp;" /></div>

				<div class="leftalign editorlineleft"><input id="{JSID}img" type="button" class="button" accesskey="p" name="img" value="Image &nbsp;" /></div>
				<div class="leftalign editorcodebutton"><input id="{JSID}url" type="button" class="button" accesskey="w" name="url" value="Link &nbsp;" style="text-decoration: underline" /></div>

				<div class="leftalign editorlineleft"><input id="{JSID}table" type="button" class="button" accesskey="t" name="table" value="Table &nbsp;" /></div>

				<div class="leftalign editorlineleft">{LANGUAGEFORM}</div>
			</div>
			<div class="newline">
				<textarea id="{JSID}edittext" name="edittext" rows="15" cols="110" class="post">{TEXT}</textarea>
			</div>

			<div class="editorbuttonrow">
				<input type="button" id="{JSID}savebutton" name="savebutton" value="Save" class="mainoption" />
				&nbsp;&nbsp;
				<input type="button"  id="{JSID}previewbutton" name="previewbutton" value="Preview" class="mainoption"  />
				&nbsp;&nbsp;
				<input type="reset" id="{JSID}resetbutton" name="reset" value="Reset" />
				&nbsp;&nbsp;
				<input type="button" id="{JSID}hideeditorbutton" name="hideeditorbutton" value="Hide Editor" />
				{HIDDENVARS}
			</div>

		</div>
	</div>
