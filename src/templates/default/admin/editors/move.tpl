<form id="{JSID}moveform" method="post">
	{JAVASCRIPT}
	{HIDDENVARS}
	<strong id="{JSID}movelabel" style="opacity:0">Move:</strong>&nbsp;
	<!-- BEGIN switch ARTICLEPAGE -->
	<input id="{JSID}movepreviouspage" type="button" value="&nbsp; 🡄 &nbsp;" title="previous page" style="opacity:0" disabled />
	&nbsp;
	<!-- END switch ARTICLEPAGE -->
	<input id="{JSID}movetop" type="button" value="&nbsp; 🡅 &nbsp;" title="top of this page" style="opacity:0" disabled />
	&nbsp;
	<input id="{JSID}moveup" type="button" value="&nbsp; ⮝ &nbsp;" title="up" style="opacity:0" disabled />
	&nbsp;
	<input id="{JSID}movepositions" type="text" size="1" maxlength="3" value="1" style="text-align:center; opacity:0" disabled />
	&nbsp;
	<input id="{JSID}movedown" type="button" value=" &nbsp; ⮟  &nbsp;" title="down" style="opacity:0" disabled />
	&nbsp;
	<input id="{JSID}movebottom" type="button" value="&nbsp; 🡇 &nbsp;" title="bottom of this page" style="opacity:0" disabled />
	<!-- BEGIN switch ARTICLEPAGE -->
	&nbsp;
	<input id="{JSID}movenextpage" type="button" value=" &nbsp; 🡆  &nbsp;" title="next page" />
	<!-- END switch ARTICLEPAGE -->
</form>
