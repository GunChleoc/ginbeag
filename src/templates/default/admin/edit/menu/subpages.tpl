<h1 class="headerpagetitle">Edit Menu</h1>
{NAVIGATIONBUTTONS}
<div class="contentheader">Sorting of subpages</div>
<div class="contentsection">
	<form name="sortsubpages" action="{ACTIONVARS}" method="post">
		<fieldset>
			<legend class="highlight">Automatic sorting</legend>
			<input type="submit" name="sortsubpages" value="Sort from A-Z" />
		</fieldset>
	</form>
</div>
<div class="contentoutline">
	<div class="contentheader">Manual ordering of subpages</div>
	<div class="contentsection">{MOVEPAGEFORM}</div>
</div>
<div><a href="#top" class="smalltext">Top of this page</a></div>
<br />
{NAVIGATIONBUTTONS}
