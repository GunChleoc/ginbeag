<h1 class="headerpagetitle">Edit Menu</h1>
{NAVIGATIONBUTTONS}
<div class="contentoutline">
  <div class="contentheader">Edit Synopsis</div>
    <div class="contentsection">
      {TEXTEDITOR}
      {IMAGEEDITOR}
    </div>
  </div>
  {MENULEVELSFORM}
  <div><a href="#top" class="smalltext">Top of this page</a></div>
  <br />
  {NAVIGATIONBUTTONS}
</div>
