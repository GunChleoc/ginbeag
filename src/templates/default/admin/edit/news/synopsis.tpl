<a name="synopsis"></a>
<div class="contentheader">Synopsis</div>
<div class="contentsection">
	{IMAGE}
	<div class="newline"></div>
	<form name="addnewsitemsynposisimageform" action="{ACTIONVARS}" method="post">
		<fieldset>
			<legend class="highlight">Add Image</legend>
			<div class="leftalign" style="margin-right:1em">
				<label for="{JSID}synopsisimagefilename">Filename:</label>
				<input id="{JSID}synopsisimagefilename" type="text" size="50" maxlength="255" name="imagefilename" />
				<div class="formexplain">A News Item Synopsis can have more than one image. Images are optional.</div>
			</div>
			<div>
				<input type="submit" name="addimage" value="Add Image"  class="leftalign" />
			</div>
		</fieldset>
	</form>
	{SYNOPSIS}
</div>
