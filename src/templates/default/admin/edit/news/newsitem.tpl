<h1 class="headerpagetitle">Edit News Item {NEWSITEM}: <span id="{JSID}itemheadertitle">{NEWSITEMTITLE}</span></h1>
{JAVASCRIPT}
{HIDDENVARS}
{NAVIGATIONBUTTONS}

<div style="display: grid; grid-template-columns: <!-- BEGIN switch HASNEWSITEMS -->auto auto <!-- END switch HASNEWSITEMS -->auto; grid-template-rows: auto;">
	<!-- BEGIN switch HASNEWSITEMS -->
	<div>{NEWSITEMSEARCHFORM}</div>
	<!-- END switch HASNEWSITEMS -->
	<div style="text-align:left">{INSERTNEWSITEMEDITOR}</div>
	<!-- BEGIN switch HASNEWSITEMS -->
	<div style="text-align:right">{JUMPTOPAGEFORM}</div>
	<!-- END switch HASNEWSITEMS -->
</div>
<!-- BEGIN switch HASNEWSITEMS -->
<div style="text-align:right">{PAGEMENU}</div>

<div class="contentheader">Title, Publishing &amp; Preview</div>
<div class="contentsection" style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
	<div>
		<div id="{JSID}titleform">{NEWSITEMTITLEFORM}</div>
		Copyright, Source information, Categorization and Date can be edited on the bottom.
	</div>
	<div style="text-align:right">{NEWSITEMPUBLISHFORM}Added by {AUTHORNAME}{DELETENEWSITEMEDITOR}</div>
</div>

{NEWSITEMSYNOPSISFORM}
<div class="contentheader">Sections</div>
<div class="contentsection">{INSERTSECTIONEDITOR}{NEWSITEMSECTIONFORM}</div>
{NEWSITEMPERMISSIONSFORM}
{NEWSITEMSOURCEFORM}
{FAKETHEDATEFORM}
{CATEGORIES}

<div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
	<div><a href="#top">Top of this page</a></div>
	<div style="text-align:right">{JUMPTOPAGEFORM}</div>
</div>
<div style="text-align:right">{PAGEMENU}</div>
{NAVIGATIONBUTTONS}
<!-- END switch HASNEWSITEMS -->
