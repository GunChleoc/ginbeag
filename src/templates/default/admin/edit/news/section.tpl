<!-- BEGIN switch QUOTE -->
<div id="{JSID}moveitem" name="moveitem" class="contentsection newsquote">
<!-- END switch QUOTE -->
<!-- BEGIN switch NOTQUOTE -->
<div id="{JSID}moveitem" name="moveitem" class="contentsection">
<!-- END switch NOTQUOTE -->
    {JAVASCRIPT}
    <div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
        <div id="{JSID}titleform">{TITLEEDITOR}</div>
        <form method="post" style="text-align:right">
            {HIDDENVARS}
            {ISQUOTEDFORM}
        </form>
    </div>
    {SECTIONEDITOR}
    {IMAGEEDITOR}
    <div class="movebuttonrow" style="display: grid; grid-template-columns: auto auto auto; grid-template-rows: auto;">
        {INSERTEDITOR}
        <div style="text-align:center">{MOVEEDITOR}</div>
        <div style="text-align:right">{DELETEEDITOR}</div>
    </div>
</div>
