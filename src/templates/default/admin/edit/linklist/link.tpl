<div id="{JSID}moveitem" name="moveitem" class="contentsection">
	<div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
		<div id="{JSID}titleform">{TITLEEDITOR}</div>
		<form id="{JSID}changelinkproperties" style="text-align:right" method="post"  onsubmit="event.preventDefault();">
				{JAVASCRIPT}
				{HIDDENVARS}
				<label for="{JSID}link">Link:</label>
				<input id="{JSID}link" type="text" name="link" size="50" maxlength="255" value="{LINK}" />
			<input type="button" id="{JSID}savepropertiesbutton" name="savepropertiesbutton" value="Save Link" class="mainoption" />
			&nbsp;&nbsp;
			<input type="reset" id="{JSID}savepropertiesreset" name="savepropertiesreset" value="Reset" />
		</form>
	</div>

	{TEXTEDITOR}
	{IMAGEEDITOR}
	<div class="movebuttonrow" style="display: grid; grid-template-columns: auto auto auto; grid-template-rows: auto;">
		{INSERTEDITOR}
		<div style="text-align:center">{MOVEEDITOR}</div>
		<div style="text-align:right">{DELETEEDITOR}</div>
	</div>
</div>
