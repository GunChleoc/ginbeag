<div id="{JSID}moveitem" name="moveitem" class="contentsection">
	<div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
		<span id="{JSID}titleform">{TITLEEDITOR}</span>
		<!-- BEGIN switch TOCFORM -->
		<form method="post" style="text-align:right">
			{JAVASCRIPT}
			{HIDDENVARS}
			{TOCFORM}
			<input id="{JSID}savetocbutton" type="button" name="savetoc" value="Save" class="Mainoption" />
		</form>
		<!-- END switch TOCFORM -->
	</div>
	{TEXTEDITOR}
	{IMAGEEDITOR}
	<div class="movebuttonrow" style="display: grid; grid-template-columns: auto auto auto; grid-template-rows: auto;">
		{INSERTEDITOR}
		<div style="text-align:center">{MOVEEDITOR}</div>
		<div style="text-align:right">{DELETEEDITOR}</div>
	</div>
</div>
