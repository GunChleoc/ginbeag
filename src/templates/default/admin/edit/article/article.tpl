<h1 class="headerpagetitle">Edit Article</h1>
{JAVASCRIPT}
{HIDDENVARS}

{NAVIGATIONBUTTONS}

<div class="contentheader">Synopsis</div>
<div class="contentsection">{TEXTEDITOR}{IMAGEEDITOR}</div>

<div class="contentheader">Source, Date & Categorization</div>
<div class="contentsection">
	<form id="{JSID}sourceform" method="post" onsubmit="event.preventDefault();">
		<fieldset>
			<legend class="highlight">Source</legend>
			<label for="{JSID}author">Author:</label>
			<input id="{JSID}author" type="text" name="author" size="80" maxlength="255" value="{AUTHOR}" />
			<br /><label for="{JSID}location">Location:</label>
			<input id="{JSID}location" type="text" name="location" size="58" maxlength="255" value="{LOCATION}" />
			<br /><label for="{JSID}source">Source name:</label>
			<input id="{JSID}source" type="text" name="source" size="58" maxlength="255" value="{SOURCE}" />
			<br /><label for="{JSID}sourcelink">Source link:</label>
			<input id="{JSID}sourcelink" type="text" name="sourcelink" size="58" maxlength="255" value="{SOURCELINK}" />
		</fieldset>
		<fieldset>
			<legend class="highlight">Date</legend>
			{DAYFORM} &nbsp;&nbsp; {MONTHFORM} &nbsp;&nbsp;
			<label for="{JSID}year">Year (4-digit):</label><!-- todo: yearform for consistency -->
			<input id="{JSID}year" type="text" name="year" size="5" maxlength="4" value="{YEAR}" />
			<div class="formexplain">You can leave the Day empty, or the Day and the Month. Set the Year to 0000 for an empty date.</div>
		</fieldset>

		<fieldset>
			<legend class="highlight">Table of Contents</legend>
			{TOCFORM}
		</fieldset>
		<input type="button" id="{JSID}savesourcebutton" name="savesourcebutton" value="Save Changes" class="mainoption" />
		&nbsp;&nbsp;
		<input id="{JSID}savesourcereset" type="reset" value="Reset" />
	</form>
</div>

{CATEGORIES}
{NAVIGATIONBUTTONS}
