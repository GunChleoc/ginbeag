<h1 class="headerpagetitle">Edit Page {ARTICLEPAGE} of Article</h1>
{NAVIGATIONBUTTONS}
<div class="contentheader">Pages</div>
<div class="contentsection" style="display: grid; grid-template-columns: auto <!-- BEGIN switch DELETEPAGE -->auto<!-- END switch DELETEPAGE -->; grid-template-rows: auto;">
    <form name="addarticlepageform" action="{ACTIONVARS}" method="post">
        <input type="submit" name="addarticlepage" value="Add Page" class="mainoption" />
    </form>

    <!-- BEGIN switch DELETEPAGE -->
    <form action="{ACTIONVARS}" method="post" style="text-align:right">
        <input type="submit" name="deletelastarticlepage" value="{DELETEPAGE}">
    </form>
    <!-- END switch DELETEPAGE -->
</div>
<div align="right">{PAGEMENU}</div>
<div class="contentheader">Sections</div>
<div class="contentsection" >
    <div>{INSERTEDITOR}</div>
    <!-- BEGIN switch ARTICLESECTIONFORM -->
    {ARTICLESECTIONFORM}
    <!-- END switch ARTICLESECTIONFORM -->
</div>
<div align="right">{PAGEMENU}</div>
{NAVIGATIONBUTTONS}
