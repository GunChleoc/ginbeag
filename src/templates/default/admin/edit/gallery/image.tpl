<div id="{JSID}moveitem" name="moveitem" class="contentsection leftalign" style="margin:2em">
	{IMAGE}
	<!-- BEGIN switch ERROR -->
	<span class="highlight">{ERROR}</span>
	<!-- END switch ERROR -->
	<div class="movebuttonrow" style="display: grid; grid-template-columns: auto auto auto; grid-template-rows: auto;">
		<div class="contentsection" style="padding-bottom:0em; margin-bottom:0.5em">{INSERTEDITOR}</div>
		<div>&nbsp;</div>
		<div class="contentsection" style="padding-bottom:0em; margin-bottom:0.5em; text-align:right">{DELETEEDITOR}</div>
	</div>
	<div style="text-align:center">{MOVEEDITOR}</div>
</div>
