<h1 class="headerpagetitle">Edit Gallery</h1>
{NAVIGATIONBUTTONS}
<div style="display: grid; grid-template-columns: auto auto; grid-template-rows: auto;">
  <div>{SHOWALLBUTTON}</div>
  <div style="text-align:right">{PAGEMENU}</div>
</div>

<div class="contentheader">Images</div>
<div class="contentsection leftalign">
  {INSERTEDITOR}
  {IMAGEFORM}
</div>

<div><a href="#top">Top of this page</a></div>
<div align="right">{PAGEMENU}</div>
{NAVIGATIONBUTTONS}
