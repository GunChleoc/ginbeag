<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="{STYLESHEETCOLORS}" type="text/css">
    <link rel="stylesheet" href="{STYLESHEET}" type="text/css">
    <link rel="stylesheet" href="{ADMINSTYLESHEET}" type="text/css">
    <!-- BEGIN switch SCRIPTLINKS -->
    {SCRIPTLINKS}
    <!-- END switch SCRIPTLINKS -->
    <!-- BEGIN switch JAVASCRIPT -->
    {JAVASCRIPT}
    <!-- END switch JAVASCRIPT -->
    <!-- BEGIN switch IS_REDIRECT -->
    <meta http-equiv="refresh" content="1;url={URL}" target="_top">
    <!-- END switch IS_REDIRECT -->

    <title>{HEADERTITLE}</title>
</head>
<body>
    <div id="messagebox" class="messagebox highlight" style="height:0px; width=0px; position:absolute;"></div>
    <div id="progressbox" class="messagebox" style="height:0px; width=0px; position:absolute;"></div>

    <div id="overlay-background">
        <div id="overlay-contents"></div>
    </div>

    <div id="wrapper">
        <div id="header">{HEADER}</div>
        <div id="main">
            <div id="adminnavigator">
                <div id="navigatorfixed">{NAVIGATORFIXED}</div>
                <div id="navigatorscroll">{NAVIGATORSCROLL}</div>
            </div>
            <div id="admincontentarea" lang={LANGUAGE}><a name="top"></a>{MESSAGE}{CONTENTS}</div>
        </div>
    </div>
</body>
