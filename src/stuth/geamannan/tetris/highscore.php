<?php

define('BASEDIR', dirname(dirname(dirname(__DIR__))));

if (!defined('LEGALVARS')) {
    define(
        'LEGALVARS',
        array(
            'checkscore',
            'name',
            'score',
            'print',
            'savescore'
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!in_array($key, LEGALVARS)) {
        header('Content-type: text/xml; charset=utf-8');
        print('<?xml version="1.0" encoding="UTF-8"?>');
        print('<highscores><noofentries>1</noofentries><rank>1</rank>');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<name>'$key' not registered with vars for highscore.</name>");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        } else {
            print('<name>No highscore found</name>');
        }
        print('<score>0</score></highscores>');
        exit;
    }
}

require BASEDIR.'/functions/db.php';
require BASEDIR.'/stuth/geamannan/tetris/config.php';

if (isset($_GET['savescore']) && isset($_COOKIE['tetris-maysavehighscore'])) {
    $cookie=$_SERVER["HTTP_COOKIE"];
    $cookie=substr($cookie, strrpos($cookie, "tetris-maysavehighscore=")+24);
    savescore($_GET['score'], rawurldecode($_GET['name']), $cookie);
} elseif (isset($_GET['checkscore'])) {
    mayaddscore($_GET['checkscore']);
} elseif (isset($_GET['print'])) {
    printscore();
}


//
// 1 if eligible for high score list, 0 otherwise
//
function mayaddscore($score)
{
    header('Content-type: text/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8"?>';

    print("<mayaddscore>");

    $sql = new SQLSelectStatement(DBTABLE, 'score');
    $sql->set_operator('count');

    if ($sql->fetch_value() < HIGHSCORES_NUMBER) {
        print("1");
    } else {
        $sql = new SQLSelectStatement(DBTABLE, 'score');
        $sql->set_operator('min');

        if ($sql->fetch_value() < $score) {
            print("1");
        } else {
            print("0");
        }
    }
    print("</mayaddscore>");
}


//
// saves a highscore
// compares with cookie against hacking
//
function savescore($score, $name, $cookie)
{
    header('Content-type: text/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8"?>';

    print("<mayaddscore>");
    //print('saving: '.$name.$score.' with cookie:'.$cookie);
    print('saving...');
    print("</mayaddscore>");
    if ($score === $cookie) {
        $sql = new SQLInsertStatement(DBTABLE, array('score', 'name'), array($score, $name), 'is');
        $sql->insert();
        prune_highscores();
    }
}


//
// Prints list of highscores and deletes superfluous entries
//
function printscore()
{
    prune_highscores();

    $sql = new SQLSelectStatement(DBTABLE, array('key', 'name', 'score'));
    $sql->set_order(array('score' => 'DESC'));
    $entries = $sql->fetch_many_rows();

    header('Content-type: text/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8"?>';

    $xml = "<highscores>";
    $xml .= "<noofentries>" . count($entries) . "</noofentries>";

    $counter = 0;
    foreach ($entries as $entry) {
        $name=$entry['name'];
        $name=str_replace("/", "", $name);

        $xml .= "<entry>";
        $xml .= "<rank>" . ++$counter . "</rank>";
        $xml .= "<name>" . $name . "</name>";
        $xml .= "<score>" . $entry['score'] . "</score>";
        $xml .= "</entry>";
    }

    $xml .= "</highscores>";
    echo $xml;
}

// delete superfluous highscores from database
function prune_highscores() {
    $sql = new SQLSelectStatement(DBTABLE, 'key');
    $sql->set_order(array('score' => 'DESC'));
    $entries = $sql->fetch_column();

    for ($i = HIGHSCORES_NUMBER; $i < count($entries); $i++) {
        $sql = new SQLDeleteStatement(DBTABLE, array('key'), array($entries[$i]), 'i');
        $sql->run();
    }
}

?>
