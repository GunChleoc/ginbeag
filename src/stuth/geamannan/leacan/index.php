﻿<?php
define('BASEDIR', dirname(dirname(dirname(__DIR__))));

include BASEDIR.'/includes/legalvars.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/includes/objects/page.php';

$meta_content = '<meta name="keywords" content="Gaelic, Scottish-Gaelic, Scots Gaelic, Schottisch-Gälisch, Gàidhlig, Fòram, bòrd-brath, forum">';
$meta_content .= '<script type="text/javascript" src="../../../includes/javascript/jquery.js"></script>';
$meta_content .= '<script type="text/javascript" src="leacan.js"></script>';
$meta_content .= '<link rel="stylesheet" href="leacan.css" type="text/css">';

$header = new PageHeader(0, "Leacan", "Leacan", $meta_content);
print($header->toHTML());
?>
<div id="contentwrapper">
        <div id="navigator" title="Clàr-taice">
<?php
$navigator = new Navigator(38, false, 1, false, false);
print($navigator->toHTML());
if (getproperty('Display Banners')) {
    $banners=new BannerList();
    print($banners->toHTML());
}
?>
        </div>
        <div id="contentarea" title="Susbaint">
<!--  Game HTML starts here  -->
                <h2 class="pagetitle">Leacan</h2>

<table cellspacing="20"  align="center">
    <tr>
        <td valign="top">
            <table cellspacing="0" cellpadding="0" class="frame">
                <tr>
                    <td class="frame">
                        <img id="0_0" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="0_1" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="0_2" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="0_3" class="frame" src="" />
                    </td>
                </tr>
                <tr>
                    <td class="frame">
                        <img id="1_0" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="1_1" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="1_2" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="1_3" class="frame" src="" />
                    </td>
                </tr>
                <tr>
                    <td class="frame">
                        <img id="2_0" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="2_1" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="2_2" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="2_3" class="frame" src="" />
                    </td>
                <tr>
                    <td class="frame">
                        <img id="3_0" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="3_1" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="3_2" class="frame" src="" />
                    </td>
                    <td class="frame">
                        <img id="3_3" class="frame" src="" />
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <img id="master" src ="" class="master" />
            <h2 align="center" id="gametitle"></h2>
            <div id="messages"></div>
        </td>
    </tr>
</table>
<hr>
<h4>Briog air dealbh gus geama ùr a thòiseachadh</h4>
<div id="games"></div>


<br /><hr>

<p class="gen">Cleachdaidh an geama seo JavaScript.</p>

<!-- End game HTML -->
</div>
</div>
    <div class="footer newline"></div>
    </div>
</body>
</html>
