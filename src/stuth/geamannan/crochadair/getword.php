<?php
define('BASEDIR', dirname(dirname(dirname(__DIR__))));

if (!defined('LEGALVARS')) {
    define(
        'LEGALVARS',
        array(
            'list',
            'mode'
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!in_array($key, LEGALVARS)) {
        header('Content-type: text/xml; charset=utf-8');
        '<?xml version="1.0" encoding="UTF-8"?>';
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<error>'$key' not registered with vars for getword.</error>");
        } else {
            print('<error>No word found</error>');
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/stuth/geamannan/crochadair/config.php';

$db->quiet_mode = true;

$mode = isset($_GET['mode']) ? $_GET['mode'] : "words";

switch ($mode) {
    case "placenames":
        makeXML("aiteachan", PLACENAMES_TABLE);
        break;
    case "wordssmall":
        makeXML("faclanbeag", WORDS_SMALL_TABLE);
        break;
    case "words":
        makeXML("faclan", WORDS_TABLE);
        break;
}

//
// get random entry form database and print as XML for AJAX
//
function makeXML($wrapper,$dbtable)
{
    global $db, $legal_tables;


    $entry = '';

    if (in_array($dbtable, $legal_tables)) {
        $query
            = "SELECT * FROM " . $dbtable
            . ", (SELECT FLOOR(MAX(".$dbtable.".id"
            . ") * RAND()) AS randId FROM " . $dbtable
            . ") AS someRandId WHERE " . $dbtable . ".id = someRandId.randId";
        $sql = new RawSQLStatement($query);

        $row = $sql->fetch_row();
        foreach ($row as $key => $element) {
            $entry .= "<$key>" . $element . "</$key>";
        }
    }

    header('Content-type: text/xml; charset=utf-8');
    '<?xml version="1.0" encoding="UTF-8"?>';

    if (empty($entry)) {
        print('<error>No word found</error>');
    }
    if (empty($db->error_report)) {
        print("<".$wrapper."><entry>" . $entry. "</entry></".$wrapper.">");
    } else     {
        print('<error>' . $db->error_report . '</error>');
    }
}
?>
