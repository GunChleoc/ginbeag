﻿<?php
define('BASEDIR', dirname(dirname(dirname(__DIR__))));

include BASEDIR.'/includes/legalvars.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/includes/objects/page.php';

$meta_content = '<meta name="keywords" content="Gaelic, Scottish-Gaelic, Scots Gaelic, Schottisch-Gälisch, Gàidhlig, Fòram, bòrd-brath, forum">';
$meta_content .= '<script type="text/javascript" src="../../../includes/javascript/jquery.js"></script>';
$meta_content .= '<script type="text/javascript" src="leumadair.js"></script>';
$meta_content .= '<link rel="stylesheet" href="leumadair.css" type="text/css">';

$header = new PageHeader(0, "Leumadairean", "Leumadairean", $meta_content);
print($header->toHTML());
?>
<div id="contentwrapper">
        <div id="navigator" title="Clàr-taice">
<?php
$navigator = new Navigator(38, false, 1, false, false);
print($navigator->toHTML());
if (getproperty('Display Banners')) {
    $banners=new BannerList();
    print($banners->toHTML());
}
?>
        </div>
        <div id="contentarea" title="Susbaint">
<!--  Game HTML starts here  -->
                <h2 class="pagetitle">Leumadairean</h2>

<table cellspacing="20"  align="center">
    <tr>
        <td id="sea" valign="top" class="sea">
            <table id="square" cellspacing="0" cellpadding="0" class="frame">
                <!-- game square goes here per JavaScript //-->
            </table>
        </td>
        <td valign="top">
            <h2 align="center" id="gametitle"></h2>
            <div id="messages"></div>
        </td>
    </tr>
</table>
<br />
<hr>
<h4>Briog air dealbh gus ìre airson geama ùr a thaghadh</h4>
<div id="games"></div>

<br /><hr>

<p class="gen">Cleachdaidh an geama seo JavaScript.</p>


<!-- End game HTML -->
</div>
</div>
    <div class="footer newline"></div>
    </div>
</body>
</html>
