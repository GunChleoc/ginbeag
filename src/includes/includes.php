<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/includes/constants.php';
require_once BASEDIR.'/functions/formatting.php';
require_once BASEDIR.'/language/languages.php';

//
// get adjusted offset for page jumped to
//
function getoffsetforjumppage($noofitems,$itemsperpage,$offset)
{
    global $_GET;

    if(isset($_GET['jumppage']) && $_GET['jumppage']>0
        && $noofitems && $_GET['jumppage']<=ceil($noofitems/$itemsperpage)
    ) {
        $offset=($_GET['jumppage']-1)*$itemsperpage;
        unset($_GET['jumppage']);
    }
    return $offset;
}

//
// makes copyright information.
//
function makecopyright($permissions)
{
    $textcopyright="";
    $imagecopyright="";
    $bypermission="";
    if(strlen($permissions['copyright'])>0) {
        $textcopyright= sprintf(getlang("footer_textcopyright"), title2html($permissions['copyright']));
    }
    if(strlen($permissions['image_copyright'])>0) {
        $imagecopyright= sprintf(getlang("footer_imagecopyright"), title2html($permissions['image_copyright']));
    }
    if(($permissions['permission'])==PERMISSION_GRANTED) {
        $bypermission=getlang("footer_bypermission");
    }
    return sprintf(getlang("footer_copyright"), $textcopyright, $imagecopyright, $bypermission);
}

?>
