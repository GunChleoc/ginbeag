<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/banners.php';
require_once BASEDIR.'/functions/formatting.php';
require_once BASEDIR.'/functions/pagecontent/externalpages.php';
require_once BASEDIR.'/functions/pagecontent/menupages.php';
require_once BASEDIR.'/functions/pagecontent/newspages.php';
require_once BASEDIR.'/functions/parameters.php';
require_once BASEDIR.'/functions/referrers.php';
require_once BASEDIR.'/functions/treefunctions.php';
require_once BASEDIR.'/includes/objects/pageelements.php';
require_once BASEDIR.'/includes/objects/template.php';
require_once BASEDIR.'/language/languages.php';

//
// Templating for Banners
//
class Banner extends Template
{

    function __construct($contents)
    {
        parent::__construct();

        if (!empty($contents['header'])) {
            $this->stringvars['header'] = title2html($contents['header']);
        }
        if (empty($contents['code'])) {
            $this->stringvars['link'] = $contents['link'];
            $this->stringvars['image'] = getprojectrootlinkpath().'img/banners/'.rawurlencode($contents['image']);
            $dimensions = getimagedimensions(BASEDIR.'/img/banners/' . $contents['image']);
            $this->stringvars['width'] = $dimensions["width"];
            $this->stringvars['height'] = $dimensions["height"];
            $this->stringvars['description'] = title2html($contents['description']);
        } else {
            $this->stringvars['complete_banner'] = stripslashes($contents['code']);
        }
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("banner.tpl");
    }
}


//
// Templating for Banners
//
class BannerList extends Template
{

    function __construct()
    {
        parent::__construct();

        $banners = getbanners();
        foreach ($banners as $id => $contents) {
            if (isbannercomplete($contents)) {
                $this->listvars['banner'][] = new Banner($contents);
            }
        }
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("bannerlist.tpl");
    }
}


//
// Templating for Navigator
//
class NavigatorLink extends Template
{

    var $style='';
    var $pagetype='';

    function __construct($page, $pageinfo, $style="simple", $level=0, $speciallink="" ,$showhidden=false)
    {
        global $_GET, $_SERVER;
        $this->style=$style;
        if ($level == 0) {
            $class="navtitle";
        } else {
            $class="navlink";
        }

        // layout parameters
        $this->stringvars['link_class']=$class;
        $this->stringvars['title_class']="";

        parent::__construct();
        if ($this->stringvars['page'] == $page) {
            $this->stringvars['title_class'] = 'navhighlight';
        } else {
            $this->stringvars['title_class'] = '';
        }

        $linkparams = array();
        if(ismobile()) { $linkparams["m"] = "on";
        }

        // for special pages like, contact, guestbook etc
        if($page==0) {
            $this->stringvars['language'] = get_site_language();
            if($speciallink==="guestbook") {
                $this->stringvars['linktooltip']=getlang("navigator_guestbook");
                $this->stringvars['title']=getlang("navigator_guestbook");
                $this->stringvars['link']=getprojectrootlinkpath()."guestbook.php".makelinkparameters($linkparams);
                $this->stringvars['link_attributes']='';
                if(basename($_SERVER['PHP_SELF'])==="guestbook.php") {
                    $this->stringvars['title_class']="navhighlight";
                } else {
                    $this->stringvars['title_class'] = '';
                }
            }
            elseif($speciallink==="contact") {
                $this->stringvars['linktooltip']=getlang("navigator_contact");
                $this->stringvars['title']=getlang("navigator_contact");
                $this->stringvars['link']=getprojectrootlinkpath()."contact.php".makelinkparameters($linkparams);
                $this->stringvars['link_attributes']='';
                if(basename($_SERVER['PHP_SELF'])==="contact.php") {
                    $this->stringvars['title_class']="navhighlight";
                } else {
                    $this->stringvars['title_class'] = '';
                }
            }
            elseif($speciallink==="sitemap") {
                $this->stringvars['linktooltip']=getlang("navigator_sitemap");
                $this->stringvars['title']=getlang("navigator_sitemap");
                $linkparams["page"] = "0";
                $linkparams["sitemap"] = "on";
                $this->stringvars['link']=getprojectrootlinkpath()."index.php".makelinkparameters($linkparams);
                $this->stringvars['link_attributes']='';
                if(isset($_GET['sitemap'])) {
                    $this->stringvars['title_class']="navhighlight";
                } else {
                    $this->stringvars['title_class'] = '';
                }
            }
            elseif($speciallink==="home") {
                $this->stringvars['linktooltip']=getlang("navigator_home");
                $this->stringvars['title']=getlang("navigator_home");
                $this->stringvars['link']=getprojectrootlinkpath().makelinkparameters($linkparams);
                $this->stringvars['link_attributes']='';
                $this->stringvars['title_class'] = '';
            }
            else
            {
                $this->stringvars['linktooltip']=getlang("navigator_notfound");
                $this->stringvars['title']=getlang("navigator_notfound");
                $this->stringvars['link']=$linkparams;
                $this->stringvars['link_class']=$class;
                $this->stringvars['link_attributes']='';
                $this->stringvars['title_class'] = '';
            }
        }
        // for normal pages
        else
        {
            $this->pagetype = $pageinfo['pagetype'];

            $this->stringvars['linktooltip'] = striptitletags($pageinfo['title_page']);

            $this->stringvars['language'] = $pageinfo['language'];

            if ($this->style=="splashpage") {
                $this->stringvars['title'] = title2html(str_replace(" ", "&nbsp;", $pageinfo['title_navigator']));
            } else {
                $this->stringvars['title'] = title2html($pageinfo['title_navigator']);
            }

            if ($showhidden) {
                if (isthisexactpagerestricted($page)) {
                    $this->stringvars['title'] .= ' (R)';
                }
                if (!ispublished($page)) {
                    $this->stringvars['title'] = '<i>' . $this->stringvars['title'] . '</i>';
                }
            }

            if($this->pagetype==="external") {
                $this->stringvars['link']=getexternallink($page);
                if(str_starts_with($this->stringvars['link'], getprojectrootlinkpath())
                    || str_starts_with($this->stringvars['link'], "?")
                    || str_starts_with($this->stringvars['link'], "index.php")
                ) {
                    $this->stringvars['link_attributes']='';
                }
                else
                {
                    $this->stringvars['link_attributes']=' target="_blank"';
                }
            }
            else
            {
                if ($showhidden) {
                    $path=getprojectrootlinkpath()."admin/pagedisplay.php";
                } else {
                    $path=getprojectrootlinkpath()."index.php";
                }
                $linkparams["page"] = $page;
                $this->stringvars['link']=$path.makelinkparameters($linkparams);
                $this->stringvars['link_attributes']="";
            }
        }
    }

    // assigns templates
    function createTemplates()
    {
        if($this->style=="splashpage") {
            if(ismobile()) { $this->addTemplate("mobile/navigatorlinksplashpage.tpl");
            } else { $this->addTemplate("navigator/navigatorlinksplashpage.tpl");
            }
        }
        elseif($this->style=="printview") {
            $this->addTemplate("navigator/navigatorlinkprintview.tpl");
        } elseif($this->stringvars['title_class']=="navhighlight") {
            $this->addTemplate("navigator/navigatornolink.tpl");
        } else {
            $this->addTemplate("navigator/navigatorlink.tpl");
        }
    }
}

//
// Templating for Navigator
// iterate over branch and create links
//
class NavigatorBranch extends Template
{

    var $style="";

    function __construct($page, $pageinfo, $style="simple",$depth=0,$level=0,$speciallink="",$showhidden=false)
    {
        $this->style=$style;
        parent::__construct();

        if($level==0) { $this->stringvars['wrapper_class'] = "navrootlinkwrapper";
        } else { $this->stringvars['wrapper_class'] = "navlinkwrapper";
        }

        if (hasaccesssession($page) || $showhidden) {
            $this->listvars['link'][]= new NavigatorLink($page, $pageinfo, $style, $level, $speciallink, $showhidden);
        }

        $this->stringvars['margin_left']=$level;

        if ($depth > 0) {
            $children = getchildren_with_navinfo($page);
            foreach ($children as $subpageid => $subpageinfo) {
                if (displaylinksforpage($subpageid) || $showhidden) {
                    $this->listvars['link'][] = new NavigatorBranch($subpageid, $subpageinfo, $style, $depth-1, $level+1, $speciallink, $showhidden);
                }
            }
        }
    }

    // assigns templates
    function createTemplates()
    {
        if($this->style=="splashpage") {
            if(ismobile()) { $this->addTemplate("mobile/navigatorbranchsplashpage.tpl");
            } else { $this->addTemplate("navigator/navigatorbranchsplashpage.tpl");
            }
        }
        elseif($this->style=="printview") {
            $this->addTemplate("navigator/navigatorbranchprintview.tpl");
        } else {
            $this->addTemplate("navigator/navigatorbranch.tpl");
        }
    }
}




//
// Templating for Navigator
// todo remove global GET?
//
class Navigator extends Template
{

    var $displaytype;

    function __construct($page,$sistersinnavigator,$depth,$displaytype="page",$showhidden=false)
    {
        global $allpages;

        $this->displaytype=$displaytype;
        parent::__construct();

        $linkparams = "";
        if(ismobile()) {
            $linkparams = makelinkparameters(array("m" => "on"));
        }

        if($displaytype=="splashpage") {

            $linksonsplashpage=explode(",", getproperty('Links on Splash Page'));
            if(!getproperty('Show All Links on Splash Page') && $linksonsplashpage[0]) {
                $rootids=$linksonsplashpage;
            }
            else
            {
                $rootids = getrootpages();
            }
            while(count($rootids))
            {
                $currentroot=array_shift($rootids);
                if(displaylinksforpage($currentroot) || $showhidden) {
                    $this->listvars['link'][] = new NavigatorBranch($currentroot, $allpages[$currentroot], $displaytype, 0, 0, "", $showhidden);
                }
            }
        }
        elseif($displaytype=="printview") {
            $this->stringvars['sitename']=title2html(getproperty("Site Name"));
            $this->stringvars['home_link']=getprojectrootlinkpath().'index.php'.$linkparams;

            // get parent chain
            $parentpages=array();
            $level=0;
            $currentpage=$page;
            while(!isrootpage($currentpage))
            {
                $parent = getparent($currentpage);
                array_push($parentpages, $parent);
                $currentpage=$parent;
                $level++;
            }
            // display parent chain
            $navdepth=count($parentpages); // for closing table tags
            for($i=0;$i<$navdepth;$i++)
            {
                $parentpage=array_pop($parentpages);
                $this->listvars['link'][] = new NavigatorBranch($parentpage, $allpages[$parentpage], "printview", 0, $i+1, "", $showhidden);
            }
            // display page
            if (array_key_exists($page, $allpages)) {
                $this->listvars['link'][] = new NavigatorBranch($page, $allpages[$page], "printview", $depth, 0, "", $showhidden);
            } else {
                $this->stringvars['link'] = '';
            }
        }
        else
        {
            $style="simple";
            $this->stringvars['home_link']=getprojectrootlinkpath().'index.php'.$linkparams;
            $this->stringvars['l_home']=getlang("navigator_home");
            $this->stringvars['language'] = get_site_language();

            // navigator
            if($page==0 || !ispageknown($page)) {
                $rootids=getrootpages();
                while(count($rootids))
                {
                    $currentroot=array_shift($rootids);
                    if(displaylinksforpage($currentroot) || $showhidden) {
                        $this->listvars['link'][] = new NavigatorBranch($currentroot, $allpages[$currentroot], $style, 0, 0, "", $showhidden);
                    }
                }
            }
            else
            {

                if(isrootpage($page)) {
                    $rootids=getrootpages();
                    $currentroot=array_shift($rootids);
                    $navposition=getnavposition($page);
                    // display upper root pages
                    while(getnavposition($currentroot)<$navposition)
                    {
                        if(displaylinksforpage($currentroot) || $showhidden) {
                            $this->listvars['link'][] = new NavigatorBranch($currentroot, $allpages[$currentroot], $style, 0, 0, "", $showhidden);
                        }
                        $currentroot=array_shift($rootids);
                    }
                    // display root page
                    $this->listvars['link'][] = new NavigatorBranch($page, $allpages[$page], $style, $depth, 0, "", $showhidden);
                }
                else
                {
                    // get parent chain
                    $parentpages=array();
                    $level=0;
                    $currentpage=$page;
                    while(!isrootpage($currentpage))
                    {
                        $parent = getparent($currentpage);
                        array_push($parentpages, $parent);
                        $currentpage=$parent;
                        $level++;
                    }
                    $parentroot=array_pop($parentpages);
                    $rootids=getrootpages();
                    $currentroot=array_shift($rootids);
                    $parentrootnavposition=getnavposition($parentroot);
                    // display upper root pages
                    while(getnavposition($currentroot)<$parentrootnavposition)
                    {
                        if(displaylinksforpage($currentroot) || $showhidden) {
                            $this->listvars['link'][] = new NavigatorBranch($currentroot, $allpages[$currentroot], $style, 0, 0, "", $showhidden);
                        }
                        $currentroot=array_shift($rootids);
                    }
                    if(displaylinksforpage($currentroot) || $showhidden) {
                        $this->listvars['link'][] = new NavigatorBranch($currentroot, $allpages[$currentroot], $style, 0, 0, "", $showhidden);
                    }

                    // display parent chain
                    $navdepth=count($parentpages); // for closing table tags
                    for($i=0;$i<$navdepth;$i++)
                    {
                        $parentpage=array_pop($parentpages);
                        $this->listvars['link'][] = new NavigatorBranch($parentpage, $allpages[$parentpage], $style, 0, $i+1, "", $showhidden);
                    }
                    // display page
                    if($sistersinnavigator) {
                        // get sisters then display 1 level only.
                        $sisterids=getsisters($page);
                        $currentsister=array_shift($sisterids);
                        $pagenavposition=getnavposition($page);
                        // display upper sister pages
                        while(getnavposition($currentsister)<$pagenavposition)
                        {
                            if(displaylinksforpage($currentsister) || $showhidden) {
                                $this->listvars['link'][] = new NavigatorBranch($currentsister, $allpages[$currentsister], $style, 0, $level, "", $showhidden);
                            }
                            $currentsister=array_shift($sisterids);
                        }
                        // display page
                        $this->listvars['link'][] = new NavigatorBranch($page, $allpages[$page], $style, $depth, $level, "", $showhidden);

                        // display lower sister pages
                        while(count($sisterids))
                        {
                            $currentsister=array_shift($sisterids);
                            if(displaylinksforpage($currentsister) || $showhidden) {
                                $this->listvars['link'][] = new NavigatorBranch($currentsister, $allpages[$currentsister], $style, 0, $level, "", $showhidden);
                            }
                        }
                    }
                    else
                    {
                        $this->listvars['link'][] = new NavigatorBranch($page, $allpages[$page], $style, $depth, 0, "", $showhidden);
                    }
                }
                // display lower root pages
                while(count($rootids))
                {
                    $currentroot=array_shift($rootids);
                    if(displaylinksforpage($currentroot) || $showhidden) {
                        $this->listvars['link'][] = new NavigatorBranch($currentroot, $allpages[$currentroot], $style, 0, 0, "", $showhidden);
                    }
                }
            }
            // special links
            if(getproperty("Enable Guestbook")) {
                $this->listvars['link'][] = new NavigatorBranch(0, array(), $style, 0, 0, "guestbook", $showhidden);
            }

            $this->listvars['link'][] = new NavigatorBranch(0, array(), $style, 0, 0, "contact", $showhidden);
            $this->listvars['link'][] = new NavigatorBranch(0, array(), $style, 0, 0, "sitemap", $showhidden);
        }
    }

    // assigns templates
    function createTemplates()
    {
        if($this->displaytype==="splashpage") {
            if(ismobile()) { $this->addTemplate("mobile/navigatorsplashpage.tpl");
            } else { $this->addTemplate("navigator/navigatorsplashpage.tpl");
            }
        }
        elseif($this->displaytype==="printview") {
            $this->addTemplate("navigator/navigatorprintview.tpl");
        } else {
            $this->addTemplate("navigator/navigator.tpl");
        }
    }
}





//
// Templating for Pictuer & Article of the Day
//
class ItemsOfTheDay extends Template
{

    function __construct($showhidden=false)
    {
        parent::__construct();

        if(getproperty('Display Picture of the Day')) {
            $potd=getpictureoftheday();
            if($potd) {
                $this->vars['potd_image']= new Image($potd, array('usethumbnail' => true), array(), $showhidden);
                $this->stringvars['l_potd']=getlang("navigator_potd");
            }
        }
        if(getproperty('Display Article of the Day')) {
            $aotd=getarticleoftheday();
            if($aotd) {
                $linkparams = array();
                $linkparams["page"] = $aotd;
                if(ismobile()) {
                    $linkparams["m"] = "on";
                }
                $this->stringvars['aotd_link']=getprojectrootlinkpath().'index.php'.makelinkparameters($linkparams);
                $this->stringvars['l_aotd']=getlang("navigator_aotd");
            }
        }
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("pages/itemsoftheday.tpl");
    }
}


//
// page footer for all pages
//
class Page extends Template
{

    private $displaytype;

    function __construct($displaytype="page",$showhidden=false)
    {
        global $_SERVER, $_GET;

        $this->displaytype=$displaytype;

        parent::__construct();


        if (isset($_SERVER['HTTP_REFERER']) && isreferrerblocked($_SERVER['HTTP_REFERER'])) {
            // todo: simple header class
            $this->stringvars['header']="<html><head></head><body>";
            $this->stringvars['navigator']="";
            $this->stringvars['banners']="";
            $this->vars['message']=new Message("Sorry, this link to our page was not authorized.");
            $this->stringvars['contents']="";
        } else {
            if(!$showhidden) {
                if(ispagerestricted($this->stringvars['page'])) {
                    checkpublicsession($this->stringvars['page']);
                }
                updatepagestats($this->stringvars['page']);
            }
            $pagecontents = getpagecontents($this->stringvars['page']);

            // contents
            if(isset($_GET['newsitem'])) {
                include_once BASEDIR.'/includes/objects/newspage.php';
                $this->vars['contents'] = new Newsitempage($_GET['newsitem'], $this->stringvars['page'], 0, false);
            } else {
                $this->makecontents($this->stringvars['page'], $pagecontents, $showhidden);
            }

            // header
            $this->makeheader($this->stringvars['page'], $pagecontents, $showhidden);

            // banners
            if (getproperty('Display Banners')) {
                $this->vars['banners']=new BannerList();
            } else {
                $this->stringvars['banners']="";
            }

            // navigator
            if (is_array($pagecontents) &&
                 ($pagecontents['pagetype'] === "menu" ||
                  $pagecontents['pagetype'] === "articlemenu" ||
                  $pagecontents['pagetype'] === "linklistmenu")) {
                $displaysisters=getsisters($this->stringvars['page']);
                $navigatordepth=getmenunavigatordepth($this->stringvars['page']);
            } else {
                $displaysisters=1;
                $navigatordepth=2;
            }
            $this->vars['navigator'] = new Navigator($this->stringvars['page'], $displaysisters, $navigatordepth-1, $displaytype, $showhidden);
        }

        // area labels for screen readers
        $this->stringvars['l_navigator']=getlang("title_navigator");
        $this->stringvars['l_content']=getlang("title_content");

        // footer
        $this->vars['footer']= new PageFooter();
    }

    //
    //
    //
    function makeheader($page, $pagecontents, $showhidden)
    {
        global $_GET;
        $title = "";
        $meta_title = "";
        $meta_content = "";

        if(!$showhidden) {
            if(ispagerestricted($page)) {
                checkpublicsession($page);
            }
            $meta_sitename = getproperty("Site Name");
            $meta_type = "website";
            $meta_description = "";

            if (is_array($pagecontents) && $pagecontents['ispublished']) {
                $title = getmaintitle($pagecontents);
                $meta_title = $pagecontents['title_navigator'];
                $meta_type = "article";
            } elseif($this->displaytype=="splashpage") {
                $meta_description .= getproperty("Site Description")." ";
                $sql = new SQLSelectStatement(SPECIALTEXTS_TABLE, 'text', array('id'), array('splashpage1'), 's');
                $meta_description .= $sql->fetch_value() . " ";
                $sql = new SQLSelectStatement(SPECIALTEXTS_TABLE, 'text', array('id'), array('splashpage2'), 's');
                $meta_description .= $sql->fetch_value();
                $imagefile = $image=getproperty("Splash Page Image");
                $meta_title = $title;
            }

            elseif(isset($_GET["sitepolicy"])) {
                $title=getproperty("Site Policy Title");
                $meta_title = $title;
            } elseif(isset($_GET["sitemap"])) {
                $title=getlang("pagetitle_sitemap");
                $meta_title = $title;
            } else {
                $title=getlang("error_pagenotfound");
            }

            // Facebook
            if (PageHeader::has_metadata('title')) {
                $meta_title .= ' - ' . striptitletags(PageHeader::$metadata['title']);
                $meta_title = str_replace('"', "'", $meta_title);
            }
            $meta_content .= "\n    " . '<meta property="og:title" content="' . striptitletags($meta_title) . '" />';

            if (PageHeader::has_metadata('description')) {
                $meta_description = substr(striptitletags(PageHeader::$metadata['description']), 0, 300);
                $meta_description = str_replace('"', "'", $meta_description);
                // Facebook
                $meta_content .= "\n    " . '<meta property="og:description" content="' . $meta_description . '" />';
                // Google
                $keywords = "";
                if ($page > 0) {
                    $keywords .= title2html(implode(', ', getcategoriesforarticle($page))) . ', ';
                }
                $keywords .= title2html(getproperty('Google Keywords'));
                if ($keywords) {
                    $meta_content .= "\n    " . '<meta name="keywords" content="'.$keywords.'">';
                    $meta_content .= "\n    " . '<meta name="description" content="'.$meta_description.' - '.$keywords.'" />';
                }
                else
                {
                    $meta_content .= "\n    " . '<meta name="description" content='.$meta_description.'" />';
                }
            }

            // Facebook
            if (PageHeader::has_metadata('image')) {
                $imagefile = PageHeader::$metadata['image'];
            } else {
                $imageurl = getproperty("Left Header Image");
                if (empty($imageurl)) {
                    $imageurl=getproperty("Right Header Image");
                }
                if (!empty($imageurl)) {
                    $imagefile = getprojectrootlinkpath() . 'img/' . $imageurl;
                }
            }
            if (!empty($imagefile)) {
                $imagedata = getimage(basename($imagefile));
                if (!empty($imagedata)) {
                    $imagefile = getimagelinkpath($imagedata['image_filename'], $imagedata['path']);
                }
                $meta_content .= "\n    " . '<meta property="og:image" content="' . $imagefile . '" />';
            }

            // Facebook
            $meta_content .= "\n    " . '<meta property="og:site_name" content="'.$meta_sitename.'" />';
            $meta_content .= "\n    " . '<meta property="og:type" content="'.$meta_type.'" />';

            $meta_url=getprojectrootlinkpath().'index.php'.makelinkparameters($_GET, false);
            $meta_content .= "\n    " . '<meta property="og:url" content="'.$meta_url.'" />';

            // Google
            $meta_content .= "\n    " . '<link rel="canonical" href="'.$meta_url.'" />';
        }
        else
        {
            if (empty($page)) {
                $title ="Welcome to the webpage editing panel";
            }
            else
            {
                $title="Displaying ".$pagecontents['pagetype']." page#".$page." - ".$pagecontents['title_navigator'];

                if($pagecontents['pagetype']==="external") {
                    $url=getexternallink($page);
                } else {
                    $url=getprojectrootlinkpath()."index.php".makelinkparameters($_GET);
                }

                $this->vars['message'] = new AdminPageDisplayMessage();
            }
        }

        $this->vars['header'] = new PageHeader($page, $title, $meta_title, $meta_content, $this->displaytype);
    }

    //
    //
    //
    function makecontents($page, $pagecontents, $showhidden)
    {
        global $_GET, $offset;

        // init
        if(isset($_GET['articlepage'])) {
            $articlepage=$_GET['articlepage'];
        } elseif(isset($_GET['offset'])) {
            $articlepage=$_GET['offset']+1;
        } elseif(!isset($_GET['articlepage']) || @strlen($_GET['articlepage'])<1) {
            $articlepage=1;
        } else { $articlepage=0;
        }

        if($this->displaytype=="splashpage") {
            $contents="";
            if(getproperty("Splash Page Font")==="italic") { $contents.='<i>';
            } elseif(getproperty("Splash Page Font")==="bold") { $contents.='<b>';
            }
            $sql = new SQLSelectStatement(SPECIALTEXTS_TABLE, 'text', array('id'), array('splashpage1'), 's');
            $text = $sql->fetch_value();

            if(strlen($text)>0) {
                $contents.='<p>'.$text.'</p><p>&nbsp;</p>';
            }
            $image=getproperty("Splash Page Image");
            if(strlen($image)>0) {
                $contents.='<p><img src="'.getprojectrootlinkpath().'img/'.$image.'" border="0" /></p><p>&nbsp;</p>';
            }
            $sql = new SQLSelectStatement(SPECIALTEXTS_TABLE, 'text', array('id'), array('splashpage2'), 's');
            $text = $sql->fetch_value();
            if(strlen($text)>0) {
                $contents.='<p>'.$text.'</p>';
            }
            if(getproperty("Splash Page Font")==="italic") { $contents.='</i>';
            } elseif(getproperty("Splash Page Font")==="bold") { $contents.='</b>';
            }
            $contents = text2html($contents);
            $this->stringvars['contents']=$contents;

            // bottom links
            $this->listvars['bottomlink'][] = new NavigatorBranch(0, array(), $this->displaytype, 0, 0, "sitemap", $showhidden);
            if(getproperty("Enable Guestbook")) {
                $this->listvars['bottomlink'][] = new NavigatorBranch(0, array(), $this->displaytype, 0, 0, "guestbook", $showhidden);
            }
            $this->listvars['bottomlink'][] = new NavigatorBranch(0, array(), $this->displaytype, 0, 0, "contact", $showhidden);
            $this->vars["itemsoftheday"] = new ItemsOfTheDay($showhidden);
        }

        // reroute to guide for webpage editors
        elseif($showhidden && @strlen($page<1) || $page<0) {
            $this->stringvars['contents'] = self::unknownpagetext();
        } else {
            // create page content
            if ($showhidden || ispublished($page)) {
                $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;

                if (!isset($this->stringvars['contents'])) {
                    switch($pagecontents['pagetype']) {
                        case "article":
                            include_once BASEDIR.'/includes/objects/articlepage.php';
                            $this->vars['contents'] = new ArticlePage($articlepage, $pagecontents, $showhidden);
                            break;
                        case "menu":
                        case "articlemenu":
                        case "linklistmenu":
                            include_once BASEDIR.'/includes/objects/menupage.php';
                            $this->vars['contents'] = new MenuPage($page, $pagecontents, $showhidden);
                            break;
                        case "external":
                            $this->stringvars['contents'] ='<a href="'.getexternallink($page).'" target="_blank">External page</a>';
                            break;
                        case "gallery":
                            include_once BASEDIR.'/includes/objects/gallerypage.php';
                            $this->vars['contents'] = new GalleryPage($pagecontents, $offset, $showhidden);
                            break;
                        case "linklist":
                            include_once BASEDIR.'/includes/objects/linklistpage.php';
                            $this->vars['contents']  = new LinklistPage($pagecontents, $showhidden);
                            break;
                        case "news":
                            include_once BASEDIR.'/includes/objects/newspage.php';
                            $this->vars['contents']  = new NewsPage($page, $pagecontents, $offset, $showhidden);
                            break;
                    }
                }
            }
            elseif(isset($_GET["sitepolicy"])) {
                $sql = new SQLSelectStatement(SPECIALTEXTS_TABLE, 'text', array('id'), array('sitepolicy'), 's');
                $this->vars['contents']  = new PageIntro(title2html(getproperty("Site Policy Title")), $sql->fetch_value(), "sectiontext");
            }
            elseif(isset($_GET["sitemap"])) {
                include_once BASEDIR.'/includes/objects/sitemap.php';
                $this->vars['contents']  = new Sitemap($showhidden);
            }
            else
            {
                $this->vars['contents']  = new PageIntro(getlang("error_pagenotfound"), sprintf(getlang("error_pagenonotfound"), $page), "highlight");
            }
        }
    }

    static function unknownpagetext() {
        $text = '<div class="contentheader">Welcome</div>
                <div class="contentsection">
                    <p>
                        Please check the
                            <a href="https://gitlab.com/GunChleoc/ginbeag/-/tree/main/docs/manual" target="_blank"> Guide</a>
                        to find your way around.
                    </p>
                    <p>This site needs JavaScript for some editing functions and cookies to keep the editing session.</p>
                    <p class="highlight">
                        Since login sessions can always be lost, it can\'t hurt to copy the texts you\'re editing to
                        your computer\'s clipboard before pressing any buttons.
                    </p>
                    <p>
                        Please stay away from the Technical Setup in the Administration section,
                        unless you know what you\'re doing ;)
                    </p>
                    <p>Please log out when you leave</p>
                 </div>';
        return $text;
    }

    // assigns templates
    function createTemplates()
    {
        if($this->displaytype=="splashpage") {
            if(ismobile()) { $this->addTemplate("mobile/splashpage.tpl");
            } else { $this->addTemplate("pages/splashpage.tpl");
            }
        }
        else {
            $this->addTemplate("pages/page.tpl");
        }
    }
}




//
//
//
class Printview extends Template
{

    function __construct($showhidden=false)
    {
        global $_SERVER, $_GET;
        parent::__construct();

        $pagecontents = getpagecontents($this->stringvars['page']);

        // header
        $this->makeheader($pagecontents);

        if(isset($_SERVER['HTTP_REFERER']) && isreferrerblocked($_SERVER['HTTP_REFERER'])) {
            // todo: simple header class
            $this->stringvars['header']='<html lang="en"><head></head><body>';
            $this->stringvars['navigator']="";
            $this->stringvars['banners']="";
            $this->vars['message']=new Message("Sorry, this link to our page was not authorized.");
            $this->stringvars['contents']="";
        } else {
            if(!$showhidden) {
                if(ispagerestricted($this->stringvars['page'])) {
                    checkpublicsession($this->stringvars['page']);
                }
            }

            // contents
            $this->makecontents($pagecontents, $showhidden);

            // navigator
            $this->vars['navigator'] = new Navigator($this->stringvars['page'], 0, 0, "printview", false);
        }

        $this->stringvars['url']=getprojectrootlinkpath().makelinkparameters(array("page" => $this->stringvars['page']));
    }

    //
    //
    //
    function makeheader($pagecontents)
    {
        $title="";
        if(ispagerestricted($this->stringvars['page'])) {
            checkpublicsession($this->stringvars['page']);
        }
        if(ispublished($this->stringvars['page'])) {
            $this->stringvars['header_title']=striptitletags($pagecontents['title_navigator']);
            $this->stringvars['title'] =  title2html(getmaintitle($pagecontents));
        }
        else
        {
            $this->stringvars['header_title']=getlang('error_pagenotfound');
            $this->stringvars['title'] = $this->stringvars['header_title'];
        }
        $this->stringvars['site_name']=title2html(getproperty("Site Name"));
        $this->stringvars['language']=$pagecontents['language'];
        $this->stringvars['stylesheet'] = getCSSPath("printview.css");
    }

    //
    //
    //
    function makecontents($pagecontents)
    {
        global $_GET;

        if (ispublished($this->stringvars['page'])) {
            switch ($pagecontents['pagetype']) {
                case "article":
                    include_once BASEDIR.'/includes/objects/articlepage.php';
                    $this->vars['contents'] = new ArticlePagePrintview($pagecontents);
                break;
                case "linklist":
                    include_once BASEDIR.'/includes/objects/linklistpage.php';
                    $this->vars['contents']  = new LinklistPagePrintview($pagecontents, false);
                break;
                case "news":
                    include_once BASEDIR.'/includes/objects/newspage.php';
                    $newsitem = get_value_or_default($_GET, 'newsitem', 0);

                    $newsitem_contents = getnewsitemcontents($newsitem);
                    if (is_array($newsitem_contents)) {
                        if (isset($_GET['offset'])) {
                            $offset = $_GET['offset'];
                        } else {
                            $offset = 0;
                        }
                        $this->vars['contents'] = new Newsitem($newsitem, $newsitem_contents, $offset, false, false);
                    } else {
                        $this->stringvars['contents'] = getlang('news_filter_nomatch');
                    }
                break;
            }
        } else {
            $this->vars['contents'] = new PageIntro(getlang('error_pagenotfound'), sprintf(getlang("error_pagenonotfound"), $this->stringvars['page']), "highlight");
        }
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("printview.tpl");
    }
}


//
// container for editdata
//
class AdminPageDisplayMessage extends Template
{

    function __construct()
    {
        global $_GET;
        parent::__construct();

        if(isset($_GET["show"])) { unset($_GET["show"]);
        }

        if(getpagetype($this->stringvars['page'])==="external") {
             $this->stringvars['publiclink']=getexternallink($this->stringvars['page']);
        }
        else
        {
            $this->stringvars['publiclink']=getprojectrootlinkpath()."index.php".makelinkparameters($_GET);
        }

        $this->stringvars['navtitle']= title2html(getnavtitle($this->stringvars['page']));
        $this->stringvars['editlink']=getprojectrootlinkpath()."admin/pageedit.php".makelinkparameters($_GET).'&page='.$this->stringvars['page'].'&action=edit';

        if(ispagerestricted($this->stringvars['page'])) {
            $this->stringvars['isrestricted']="true";
        }
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("admin/pagedisplaymessage.tpl");
    }
}

?>
