<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/categories.php';
require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/parameters.php';
require_once BASEDIR.'/includes/constants.php';
require_once BASEDIR.'/includes/objects/categories.php';
require_once BASEDIR.'/includes/objects/forms.php';
require_once BASEDIR.'/includes/objects/pageelements.php';

// HTML for quoted sections start
class NewsitemSectionQuoteStart extends Template {

    function __construct() {
        parent::__construct();
        $this->stringvars['l_quote'] = getlang('section_quote');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/news/newsitemsectionquotestart.tpl');
    }
}

// HTML for quoted sections end
class NewsitemSectionQuoteEnd extends Template {

    function __construct() {
        parent::__construct();
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/news/newsitemsectionquoteend.tpl');
    }
}

// Templating for Newsitemsections
class NewsitemSection extends Template {

    function __construct($newsitem, $newsitemsection, $contents, $showhidden = false) {
        parent::__construct();

        if (!empty($contents['sectiontitle'])) {
            if (!PageHeader::has_metadata('title')) {
                PageHeader::set_metadata('title', $contents['sectiontitle']);
            }
            $this->stringvars['title'] = title2html($contents['sectiontitle']);
        }

        if (!empty($contents['image_filename'])) {
            if (!PageHeader::has_metadata('image')) {
                PageHeader::set_metadata('image', $contents['image_filename']);
            }
            $this->vars['image'] = new CaptionedImage($contents, array('newsitem' => $newsitem), $showhidden);
        } else {
            if (!PageHeader::has_metadata('image')) {
                PageHeader::set_metadata('image', extract_image_from_text($contents['text']));
            }
            $this->stringvars['image'] = '';
        }

        if (!PageHeader::has_metadata('description')) {
            PageHeader::set_metadata('description', $contents['text']);
        }
        $this->stringvars['text'] = text2html($contents['text']);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/news/newsitemsection.tpl');
    }
}

// Templating for Newsitems
class Newsitem extends Template {

    function __construct($newsitem, $contents, $offset, $showhidden = false, $showtoplink = true) {
        global $_GET;

        parent::__construct();

        if (!isset($_GET['printview'])) {
            $linkparams = array('page' => $this->stringvars['page'], 'newsitem' => $newsitem);

            if (ismobile()) {
                $linkparams['m'] = 'on';
                $this->stringvars['itemlink']= '<a href="'.makelinkparameters($linkparams).'" title="'.getlang('news_single_link').'" class="buttonlink">'.getlang('news_single_link_short').'</a>';

                $linkparams['printview'] = 'on';
                $this->stringvars['printviewbutton'] ='<a href="'.makelinkparameters($linkparams).'" title="'.getlang('pagemenu_printview').'" class="buttonlink">'.getlang('pagemenu_printview_short').'</a>';
            } else {
                $this->vars['itemlink'] = new LinkButton(makelinkparameters($linkparams), getlang('news_single_link'), 'img/link.png');

                $linkparams['printview'] = 'on';
                $this->vars['printviewbutton'] = new LinkButton(makelinkparameters($linkparams), getlang('pagemenu_printview'), 'img/printview.png');
            }
        }

        $this->stringvars['title'] = title2html($contents['title']);

        if (!empty($contents['title'])) {
            $this->stringvars['title'] = title2html($contents['title']);
            if (!PageHeader::has_metadata('title')) {
                PageHeader::set_metadata('title', $contents['title']);
            }
        } else {
            $this->stringvars['title'] = sprintf(getlang('news_title_default'), formatdate($contents['date']));
        }

        if (!empty($contents['date']) || !empty($contents['location'])) {
            $this->stringvars['location_date'] = 'locationdate';
        }

        $this->stringvars['date'] = formatdatetime($contents['date']);
        $this->stringvars['location'] = title2html($contents['location']);


        if (!empty($contents['sourcelink'])) {
            $this->stringvars['source_link'] = $contents['sourcelink'];
        }

        if (!empty($contents['source'])) {
            $this->stringvars['source'] = title2html($contents['source']);
            $this->stringvars['l_source'] = getlang('news_source_source');
        }

        if (!empty($contents['contributor'])) {
            $this->stringvars['contributor'] = title2html($contents['contributor']);
            $this->stringvars['l_contributor'] = getlang('news_source_foundby');
        }

        $this->vars['categorylist'] = new CategorylistLinks(getcategoriesfornewsitem($newsitem), $this->stringvars['page'], CATEGORY_NEWS);

        if (!empty($contents['synopsis'])) {
            $this->stringvars['synopsis_image'] = '';
            if (!PageHeader::has_metadata('description')) {
                PageHeader::set_metadata('description', $contents['synopsis']);
            }
        }

        $this->stringvars['text'] = text2html($contents['synopsis']);
        $this->stringvars['copyright'] = makecopyright($contents);

        if ($showhidden) {
            $this->stringvars['editor'] = title2html(getdisplayname($contents['editor_id']));
        }

        if ($showtoplink) {
            $this->stringvars['show_toplink'] = '';
        }

        // synopsis
        $images = getnewsitemsynopsisimages($newsitem);

        $noofimages = count($images);

        $this->listvars['image'] = array();
        if ($noofimages > 0) {
            if (!PageHeader::has_metadata('image')) {
                PageHeader::set_metadata('image', array_values($images)[0]);
            }
            if ($noofimages == 1) {
                $contents['image_filename'] = array_shift($images);
                $this->vars['image'] = new CaptionedImage($contents, array('newsitem' => $newsitem), $showhidden);
            } else {
                $width = 0;
                $this->stringvars['multiple_images'] = "$noofimages";
                $imagedata = array('imagealign' => 'float:left; ');
                foreach ($images as $imagefilename) {
                    if (!imageexists($imagefilename)) {
                        continue;
                    }
                    $imagedata['image_filename'] = $imagefilename;
                    $imagedata['usethumbnail'] = true;
                    $imagedata = Image::make_imagedata($imagedata);

                    $this->listvars['image'][] = new Image($imagefilename, $imagedata, array('newsitem' => $newsitem), $showhidden);
                    $width += $imagedata['width'] + 20;
                }
                $this->stringvars['width'] = $width;
            }
            $this->stringvars['synopsis_image'] = 'synopsis_image';
        } elseif (!PageHeader::has_metadata('image')) {
            PageHeader::set_metadata('image', extract_image_from_text($contents['synopsis']));
        }

        // sections
        $sections = getnewsitemsectionswithcontent($newsitem);
        $this->listvars['section'] = array();
        if (!empty($sections)) {
            $wasquote = false;
            foreach ($sections as $id => $contents) {
                $isquote = isnewsitemsectionquoted($id);
                if ($isquote && !$wasquote) {
                    $this->listvars['section'][] = new NewsitemSectionQuoteStart();
                } elseif(!$isquote && $wasquote) {
                    $this->listvars['section'][] = new NewsitemSectionQuoteEnd();
                }
                $this->listvars['section'][] = new NewsitemSection($newsitem, $id, $contents, $showhidden);
                $wasquote = $isquote;
            }
            if ($wasquote) {
                $this->listvars['section'][] = new NewsitemSectionQuoteEnd();
            }
        } else {
            $this->stringvars['newsitemsectionform'] = '';
        }
        $this->stringvars['l_topofthispage'] = getlang('pagemenu_topofthispage');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/news/newsitem.tpl');
    }
}

// Templating for stand alone Newsitems
class Newsitempage extends Template {

    function __construct($newsitem, $page, $offset, $showhidden = false, $showtoplink = true) {
        global $_GET;

        parent::__construct();

        $linkparams = array('page' => $this->stringvars['page']);
        $this->stringvars['returnlink'] = makelinkparameters($linkparams);
        $this->stringvars['l_returnbutton'] = getlang('newsitem_returnbutton');

        if (!isset($_GET['printview'])) {
            $linkparams['newsitem'] = $newsitem;
            $linkparams['printview'] = 'on';
            $this->vars['printviewbutton'] = new LinkButton(makelinkparameters($linkparams), getlang('pagemenu_printview'), 'img/printview.png');
        }
        $this->stringvars['l_single'] = getlang('news_single_showing');

        $newsitem_contents = getnewsitemcontents($newsitem);
        if (is_array($newsitem_contents)) {
            $this->vars['newsitem'] = new Newsitem($newsitem, $newsitem_contents, $offset, $showhidden, false);
        } else {
            $this->stringvars['newsitem'] = getlang('news_filter_nomatch');
        }

        $this->stringvars['l_topofthispage'] = getlang('pagemenu_topofthispage');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/news/newsitempage.tpl');
    }

}

// main class for newspages
class NewsPage extends Template {

    function __construct($page, $introcontents, $offset, $showhidden) {
        global $_GET;

        parent::__construct();

        $this->vars['pageintro'] = new PageIntro($introcontents['title_page'], $introcontents['introtext'], 'introtext',  $introcontents, $showhidden);

        $linkparams = array('page' => $this->stringvars['page']);
        if (ismobile()) {
            $linkparams['m'] = 'on';
        }
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($linkparams);

        $this->stringvars['l_topofthispage'] = getlang('pagemenu_topofthispage');

        // filtering
        $filter = isset($_GET['filter']);
        $filterpage = isset($_GET['filterpage']);
        if ($filter || $filterpage) {
            $selectedcat = get_value_or_default($_GET, 'selectedcat', 0);

            $from = array(
                'day' => get_value_or_default($_GET, 'fromday', 0),
                'month' => get_value_or_default($_GET, 'frommonth', 0),
                'year' => get_value_or_default($_GET, 'fromyear', 0));

            $to = array(
                'day' => get_value_or_default($_GET, 'today', 0),
                'month' => get_value_or_default($_GET, 'tomonth', 0),
                'year' => get_value_or_default($_GET, 'toyear', 0));

            $order = get_value_or_default($_GET, 'order', 'date');
            $ascdesc = get_value_or_default($_GET, 'ascdesc', 'desc');

            $newsitemsperpage = getproperty('News Items Per Page');
            $noofnewsitems = count(getfilterednewsitems($this->stringvars['page'], $selectedcat, $from, $to, $order, $ascdesc, 0, 0));

            if (!$filterpage) {
                $offset = 0;
            } else {
                $offset = getoffsetforjumppage($noofnewsitems, $newsitemsperpage, $offset);
            }

            $newsitems = getfilterednewsitems($this->stringvars['page'], $selectedcat, $from, $to, $order, $ascdesc, $newsitemsperpage, $offset);
        } else {
            // no filtering
            $newsitemsperpage = getproperty('News Items Per Page');
            if (!($newsitemsperpage > 0)) {
                $newsitemsperpage = 5;
            }

            $noofnewsitems = countpublishednewsitems($this->stringvars['page']);
            $offset = getoffsetforjumppage($noofnewsitems, $newsitemsperpage, $offset);

            $newsitems = getpublishednewsitems($this->stringvars['page'], $newsitemsperpage, $offset);
        }
        // end filtering


        // rss
        if (hasrssfeed($this->stringvars['page'])) {
            $this->vars['rss']= new LinkButton(getprojectrootlinkpath().'rss.php'.makelinkparameters(array('page' => $this->stringvars['page'])), getlang('news_rss_feed'), 'img/rss.png');
        }

        // jumpform & pagemenu
        if ($noofnewsitems / $newsitemsperpage > 1) {
            $filterparams = array();
            if ($filter || $filterpage) {
                $filterparams['filterpage'] = 'on';
                $filterparams['selectedcat'] = $selectedcat;
                $filterparams['fromday'] = get_value_or_default($_GET, 'fromday', 0);
                $filterparams['frommonth'] =get_value_or_default($_GET, 'frommonth', 0);
                $filterparams['fromyear'] = get_value_or_default($_GET, 'fromyear', 0);
                $filterparams['today'] = get_value_or_default($_GET, 'today', 0);
                $filterparams['tomonth'] = get_value_or_default($_GET, 'tomonth', 0);
                $filterparams['toyear'] = get_value_or_default($_GET, 'toyear', 0);
                $filterparams['order'] = get_value_or_default($_GET, 'order', 'date');
                $filterparams['ascdesc'] = get_value_or_default($_GET, 'ascdesc', 'desc');
                $filterparams['page'] = $this->stringvars['page'];
                $this->vars['jumpform'] = new JumpToPageForm('', $filterparams);
            } else {
                $this->vars['jumpform'] = new JumpToPageForm('', array('page' => $this->stringvars['page']));
            }

            $this->vars['pagemenu'] = new Pagemenu($offset, $newsitemsperpage, $noofnewsitems, $filterparams);
        }

        // filter result message
        if ($filter || $filterpage) {
            if (!count($newsitems)) {
                $this->stringvars['message'] = getlang('news_filter_nomatch');
            } else {
                $this->stringvars['message'] = getlang('news_filter_result');
            }
            $this->stringvars['l_showall'] = sprintf(getlang('news_filter_showall'), title2html(getpagetitle($this->stringvars['page'])));
        }

        // get items
        $isfirst = true;
        foreach ($newsitems as $id => $contents) {
            $this->listvars['newsitem'][] = new Newsitem($id, $contents, $offset, $showhidden, $isfirst);
            $isfirst = false;
        }

        $this->stringvars['l_displayoptions'] = getlang('news_filter_displayoptions');
        $this->stringvars['l_categories'] = getlang('news_filter_categories');
        $this->stringvars['l_from'] = getlang('news_filter_from');
        $this->stringvars['l_to'] = getlang('news_filter_to');
        $this->stringvars['l_go'] = getlang('news_filter_go');
        $this->stringvars['l_orderby'] = getlang('news_filter_orderby');

        if ($filter || $filterpage) {
            $this->vars['filterform'] = $this->makenewsfilterform($page, $selectedcat, $from, $to, $order, $ascdesc);
        } else {
            $this->vars['filterform'] = $this->makenewsfilterform($page);
        }
        $this->vars['editdata'] = new Editdata($introcontents, $showhidden);
    }

    // assigns templates
    function createTemplates() {
        if (ismobile()) {
            $this->addTemplate('mobile/newspage.tpl');
        } else {
            $this->addTemplate('pages/news/newspage.tpl');
        }
    }

    function makenewsfilterform($page, $selectedcat = '', $from = array(), $to = array(), $order = 'date', $ascdesc = 'desc') {
        $oldestdate = getoldestnewsitemdate($page);
        $newestdate = getnewestnewsitemdate($page);
        if (!array_key_exists('day', $from)) {
            $from['day'] = $oldestdate['mday'];
            $from['month'] = $oldestdate['mon'];
            $from['year'] = $oldestdate['year'];
        }
        if (!array_key_exists('day', $to)) {
            $to['day'] = $newestdate['mday'];
            $to['month'] = $newestdate['mon'];
            $to['year'] = $newestdate['year'];
        }

        $this->vars['categoryselection'] = new CategorySelectionForm(false, '', CATEGORY_NEWS, 1, array($selectedcat => $selectedcat));
        $this->vars['from_day'] = new DayOptionForm($from['day'], true, '', 'fromday', getlang('news_filter_fromday'));
        $this->vars['from_month'] = new MonthOptionForm($from['month'], true, '', 'frommonth', getlang('news_filter_frommonth'));
        $this->vars['from_year'] = new YearOptionForm($from['year'], $oldestdate['year'], $newestdate['year'], '', 'fromyear', getlang('news_filter_fromyear'));
        $this->vars['to_day'] = new DayOptionForm($to['day'], true, '', 'today', getlang('news_filter_today'));
        $this->vars['to_month'] = new MonthOptionForm($to['month'], true, '', 'tomonth', getlang('news_filter_tomonth'));
        $this->vars['to_year'] = new YearOptionForm($to['year'], $oldestdate['year'], $newestdate['year'], '', 'toyear', getlang('news_filter_toyear'));
        $this->vars['order'] = new NewsOrderSelectionForm($order);
        $this->vars['ascdesc'] = new AscDescSelectionForm($ascdesc === 'asc');
    }
}

// name may contain white spaces. They will be stripped for the form name,
// but left intact for display
class NewsOrderSelectionForm  extends Template {

    function __construct($order = '') {
        parent::__construct();

        $this->stringvars['optionform_name'] = 'order';
        $this->stringvars['optionform_label'] = getlang('news_filter_property');
        $this->stringvars['optionform_id'] = 'order';
        $this->stringvars['optionform_attributes'] = '';
        $this->stringvars['jsid'] = '';

        $this->listvars['option'][] = new OptionFormOption('date', $order === 'date', getlang('news_filter_date'));
        $this->listvars['option'][] = new OptionFormOption('title', $order === 'title', getlang('news_filter_title'));
        $this->listvars['option'][] = new OptionFormOption('source', $order === 'source', getlang('news_filter_source'));
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('forms/optionform.tpl');
    }
}

?>
