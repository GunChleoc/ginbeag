<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/images.php';
require_once BASEDIR.'/functions/pages.php';
require_once BASEDIR.'/functions/publicsessions.php';
require_once BASEDIR.'/functions/users.php';
require_once BASEDIR.'/includes/includes.php';
require_once BASEDIR.'/includes/objects/images.php';

//
// container for editdata
//
class Editdata extends Template {

    function __construct($contents, $showhidden = false) {
        parent::__construct();
        $editdate = formatdatetime($contents['editdate']);

        if ($showhidden) {
            $editor = getdisplayname($contents['editor_id']);
            $this->stringvars['footerlastedited']
                = sprintf(getlang('footer_lasteditedauthor'), $editdate, $editor);
        } else {
            $this->stringvars['footerlastedited']
                = sprintf(getlang('footer_lastedited'), $editdate);
        }

        $permissions = makecopyright($contents);
        if (!empty($permissions)) {
            $this->stringvars['copyright'] = $permissions;
        }
        $this->stringvars['topofthispage'] = getlang('pagemenu_topofthispage');
    }

    // assigns templates
    function createTemplates() {
        if (ismobile()) {
            $this->addTemplate('mobile/editdata.tpl');
        } else {
            $this->addTemplate('pages/editdata.tpl');
        }
    }
}


//
// page header for all pages
//
class PageHeader extends Template {
    var $displaytype;

    // Associative array of metadata
    static $metadata = array();

    function __construct($page, $title, $browsertitle, $meta_content = '', $displaytype = 'page') {
        global $_GET, $_SERVER;

        $this->displaytype = $displaytype;
        parent::__construct();

        $linkparams = array('page' => $this->stringvars['page']);
        $linkparams['logout'] = 'on';
        if (ismobile()) {
            $linkparams['m'] = 'on';
        }

        if (ispublicloggedin()) {
            $this->stringvars['logoutlink'] = makelinkparameters($linkparams);
        }

        $this->stringvars['site_language'] = get_site_language();

        $linkparams = $_GET;
        if (ismobile()) {
            unset($linkparams['m']);
            $this->stringvars['displaytypelink'] = 'index.php'.makelinkparameters($linkparams);
            $this->stringvars['l_displaytypelink'] = getlang('header_desktopstyle');

            if ($displaytype != 'splashpage') {
                $this->stringvars['l_showmenu'] = getlang('header_showmenu');
                $this->stringvars['l_hidemenu'] = getlang('header_hidemenu');
            }
            $this->stringvars['stylesheet'] = getCSSPath('mobile.css');
        } else {
            $linkparams['m'] = 'on';
            $this->stringvars['displaytypelink'] = makelinkparameters($linkparams);
            $this->stringvars['l_displaytypelink'] = getlang('header_mobilestyle');
            $this->stringvars['stylesheet'] = getCSSPath('main.css');
        }

        $this->stringvars['meta_content'] = $meta_content;

        $this->stringvars['stylesheetcolors'] = getCSSPath('colors.css');
        $this->stringvars['sitename'] = title2html(getproperty('Site Name'));
        if ($page == 0 || empty($page)) {
            $this->stringvars['language'] = get_site_language();
        } else {
            $this->stringvars['language'] = getpagecontents($page)['language'];
        }
        $this->stringvars['browsertitle'] = striptitletags($browsertitle);
        $this->stringvars['title'] = title2html($title);

        if ($displaytype != 'splashpage') {
            $this->stringvars['site_description'] = title2html(getproperty('Site Description'));
        } elseif(getproperty('Display Site Description on Splash Page')) {
            $this->stringvars['site_description'] = title2html(getproperty('Site Description'));
        }

        $image = getproperty('Left Header Image');
        if (!empty($image)) {
            $this->stringvars['left_image'] = getprojectrootlinkpath().'img/'.$image;
            $dimensions = getimagedimensions(BASEDIR.'/img/'.$image);
            $this->stringvars['left_width'] = $dimensions['width'];
            $this->stringvars['left_height'] = $dimensions['height'];
        }

        $image = getproperty('Right Header Image');
        if (!empty($image)) {
            $this->stringvars['right_image'] = getprojectrootlinkpath().'img/'.$image;
            $dimensions = getimagedimensions(BASEDIR.'/img/'.$image);
            $this->stringvars['right_width'] = $dimensions['width'];
            $this->stringvars['right_height'] = $dimensions['height'];
        }

        $linkparams = array();
        if (ismobile()) {
            $linkparams['m'] = 'on';
        }

        $link = getproperty('Left Header Link');
        if (!empty($link)) {
            $this->stringvars['left_link'] = getprojectrootlinkpath().$link.makelinkparameters($linkparams);
        }

        $link = getproperty('Right Header Link');
        if (!empty($link)) {
            $this->stringvars['right_link'] = getprojectrootlinkpath().$link.makelinkparameters($linkparams);
        }
    }

    static function set_metadata($key, $value) {
        if (!empty($value)) {
            PageHeader::$metadata[$key] = $value;
        }
    }
    static function has_metadata($key) {
        return isset(PageHeader::$metadata[$key]);
    }

    // assigns templates
    function createTemplates() {
        // Google Chrome shenanigans
        header('Permissions-Policy: interest-cohort=()');

        if ($this->displaytype === 'splashpage') {
            if (ismobile()) {
                $this->addTemplate('mobile/splashpageheader.tpl');
            } else {
                $this->addTemplate('pages/splashpageheader.tpl');
            }
        } elseif(ismobile()) {
            $this->addTemplate('mobile/pageheader.tpl');
        } else {
            $this->addTemplate('pages/pageheader.tpl');
        }
    }
}

//
// intro/synopsis for all pages
//
class PageIntro extends Template {

    function __construct($title, $text, $class = 'introtext', $imagedata = array(), $showhidden = false) {
        parent::__construct();
        $this->stringvars['pagetitle'] = title2html($title);
        $this->stringvars['text'] = text2html($text);
        $this->stringvars['class'] = $class;

        if (!PageHeader::has_metadata('image')) {
            if (!isset($imagedata['image_filename']) || empty($imagedata['image_filename'])) {
                PageHeader::set_metadata('image', extract_image_from_text($text));
            } else {
                $this->vars['image'] = new CaptionedImage($imagedata, array('page' => $this->stringvars['page']), $showhidden);
                PageHeader::set_metadata('image', $imagedata['image_filename']);
            }
        }
        if (!PageHeader::has_metadata('description')) {
            PageHeader::set_metadata('description', $text);
        }
        if (!PageHeader::has_metadata('title')) {
            PageHeader::set_metadata('title', $title);
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/pageintro.tpl');
    }
}

//
// page footer for all pages
//
class PageFooter extends Template {

    function __construct() {
        parent::__construct();
        if (getproperty('Display Site Policy')) {
            $linkparams = array('page' => 0, 'sitepolicy' => 'on');
            if (ismobile()) {
                $linkparams['m'] = 'on';
            }
            $this->stringvars['site_policy_link'] = getprojectrootlinkpath().'index.php'.makelinkparameters($linkparams);
            $title = getproperty('Site Policy Title');
            if (!empty($title)) {
                $this->stringvars['site_policy_title'] = title2html($title);
            }
        }

        $this->stringvars['footer_message']=text2html(getproperty('Footer Message'));
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('pages/pagefooter.tpl');
    }
}

?>
