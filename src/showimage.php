<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(__FILE__));

if (!defined('LEGALVARS')) {
    define(
        'LEGALVARS',
        array(
            'fbclid', // Appended by facebook to track you with
            'image',
            'item',
            'newsitem',
            'm',
            'next',
            'page',
            'prev',
            'sid',
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!(in_array($key, LEGALVARS) || is_numeric($key))) {
        header('HTTP/1.0 404 Not Found');
        print('HTTP 404: Sorry, but this page does not exist.');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<br />'$key' not registered with vars for showimage.");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

include_once BASEDIR.'/functions/db.php';

// anti bot nonsense links
// ********************************* achtung - bot secure ist server-spezifisch!
$testpath = "/".getproperty("Local Path");
if (getproperty("Local Path") == "") {
    $testpath = "";
}

if (!((isset($_SERVER["ORIG_PATH_TRANSLATED"])
    && $_SERVER["ORIG_PATH_TRANSLATED"] == BASEDIR.'/showimage.php')
    || $_SERVER["PHP_SELF"] == $testpath."/showimage.php")
) {
    //    print("test: ".$_SERVER["PHP_SELF"]);
    header("HTTP/1.0 404 Not Found");
    print("HTTP 404: Sorry, but this page does not exist.");
    exit;
}

require_once BASEDIR.'/includes/objects/showimage.php';

$sid="";
if (isset($_GET['sid'])) {
    $sid = $_GET['sid'];
}

$nextitem=0;
$previousitem=0;
$image="";
$item=0;

if (isset($_GET['page'])) {
    $page=$_GET['page'];
} else {
    $page=0;
}

if (isset($_GET['image'])) {
    $image = $_GET['image'];
}
if (isset($_GET['item'])) {
    $item = $_GET['item'];
    // get image from item array
    if (isset($_POST[$item])) {
        $image = $_POST[$item];
        $_GET['image'] = $image;
    }
}

$showimage = new Showimage($page, $image, $item, false);

print($showimage->toHTML());
?>
