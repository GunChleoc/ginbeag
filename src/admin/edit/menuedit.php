<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(__DIR__)));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/admin/includes/objects/adminmain.php';
require_once BASEDIR.'/admin/includes/objects/edit/menupage.php';
require_once BASEDIR.'/admin/includes/objects/messages.php';

checksession();

$page = isset($_GET['page']) ? $_GET['page'] : 0;

if (!$page) {
    $editpage = noPageSelectedNotice();
    $message = 'Please select a page first';
    $error = true;
} else {
    $message = getpagelock($page);
    $error = false;
    if ($message) {
        $editpage = new pageBeingEditedNotice($message);
    } else {
        // Page content actions
        if (isset($_POST['sortsubpages'])) {
            $message = 'Sorted subpages from A-Z';
            sortsubpagesbyname($page);
            updateeditdata($page);
        }
        $editpage = new EditMenuSubpages($page);
    }
}

$content = new AdminMain($page, 'editcontents', new AdminMessage($message, $error), $editpage);
print($content->toHTML());
?>
