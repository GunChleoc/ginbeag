<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(__DIR__)));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/functions/pagesmod.php';
require_once BASEDIR.'/admin/includes/objects/adminmain.php';
require_once BASEDIR.'/admin/includes/objects/edit/newspage.php';
require_once BASEDIR.'/admin/includes/objects/forms.php';
require_once BASEDIR.'/admin/includes/objects/messages.php';
require_once BASEDIR.'/functions/imagefiles.php';

checksession();

$page = isset($_GET['page']) ? $_GET['page'] : 0;

if (!$page) {
    $editpage = noPageSelectedNotice();
    $message = 'Please select a page first';
    $error = true;
} else {
    $message = getpagelock($page);
    $error = false;
    if ($message) {
        $editpage = new pageBeingEditedNotice($message);
    } else {
        // page content actions
        if (isset($_POST['item'])) {
            $offset = getnewsitemoffset($page, 1, $_POST['item']);
        } elseif (isset($_GET['newsitem'])) {
            $offset = getnewsitemoffset($page, 1, $_GET['newsitem']);
        } elseif (isset($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 0;
        }

        // update news
        // add a newsitem
        if (isset($_POST['addimage'])) {
            // synopsis
            $filename = trim($_POST['imagefilename']);
            if (imageexists($filename)) {
                addnewsitemsynopsisimage($_GET['newsitem'], $filename);
                updateeditdata($page);
                $message = 'Added synopsis image';
                $imagedata = getimage($filename);
                if (needs_thumbnail($imagedata, getproperty('Thumbnail Size'))) {
                    $message .= ', but please create a thumbnail for this image!';
                    $error = true;
                }
            } else {
                $message = 'Failed to add synopsis image. The image <i>'.$_POST['imagefilename'].'</i> does not exist!';
            }
            $editpage = new EditNewsItemForms($page, $offset);
        } elseif(isset($_POST['removeimage'])) {
            if (isset($_POST['removeconfirm'])) {
                $message = 'Removed a synopsis image';
                removenewsitemsynopsisimage($_GET['imageid']);
            } else {
                $message = 'Failed to remove image. Please confirm when removing an image.';
                $error = true;
            }
            updateeditdata($page);
            $editpage = new EditNewsItemForms($page, $offset);
        } elseif (isset($_POST['search']) && isset($_POST['title']) && strlen($_POST['title']) > 0) {
            // searching
            $editpage = new NewsItemSearchResults($_POST['title']);
        } else {
            $editpage = new EditNewsItemForms($page, $offset);
        }
    }
}

$content = new AdminMain($page, 'editcontents', new AdminMessage($message, $error), $editpage);
print($content->toHTML());
?>
