<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(__DIR__));
include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/functions/pagecontent/externalpagesmod.php';
require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/admin/includes/objects/adminmain.php';
require_once BASEDIR.'/admin/includes/objects/messages.php';
require_once BASEDIR.'/admin/includes/objects/page.php';
require_once BASEDIR.'/functions/formatting.php';
require_once BASEDIR.'/functions/treefunctions.php';

checksession();

$page = isset($_GET['page']) ? $_GET['page'] : 0;
if (!$page) {
    $editpage = noPageSelectedNotice();
    $message = 'Please select a page first';
    $error = true;
} else {
    // Get pagelock
    $message = getpagelock($page);
    $message = '';
    $error = false;
    if ($message) {
        $editpage = new pageBeingEditedNotice($message);
    } else {
        // Perform action
        $action = isset($_GET['action']) ? $_GET['action'] : '';
        unset($_GET['action']);

        switch($action) {
            // general actions
            case 'edit':
                // update external
                if (isset($_POST['changelink']) && getpagetype($page) === 'external') {
                    updateexternallink($page, $_POST['link']);
                }
            break;
            case 'rename':
                renamepage($page, $_POST['navtitle'], $_POST['pagetitle'], trim($_POST['pagelanguageselectionform']));
                updateeditdata($page);
                unlockpage($page);
                $message = 'Renamed page to:<br /> <em>'.title2html($_POST['navtitle']).'</br />'.title2html($_POST['pagetitle']).'</em><br />Language: <em>'.$_POST['pagelanguageselectionform'].'</em>';
                $editpage = editedRedirect($page, 'Renamed page');
            break;
            case 'move':
                $direction = 'bottom';
                $positions = null;
                $positions_message = '';
                if (isset($_GET['moveup'])) {
                    $direction = 'up';
                    $positions = $_GET['positions'];
                    $positions_message = "$positions place(s) up";
                } elseif (isset($_GET['movedown'])) {
                    $direction = 'down';
                    $positions = $_GET['positions'];
                    $positions_message = "$positions place(s) down";
                } elseif(isset($_GET['movetop'])) {
                    $direction = 'top';
                    $positions_message = 'to the top';
                } elseif(isset($_GET['movebottom'])) {
                    $direction = 'bottom';
                    $positions_message = 'to the bottom';
                }
                $pagetitle = title2html(getpagetitle($page));
                if (movepage($page, $direction, $positions)) {
                    $title = "Moved page: $direction";
                    $message="Moved the page '$pagetitle' $positions_message";
                } else {
                    $title = "Failed to move page: $direction";
                    $message = "Unable to move the page '$pagetitle' $positions_message";
                    $error=true;
                }
                updateeditdata(getparent($page));
                unlockpage($page);
                $editpage = editedRedirect($page, $title);
            break;
            case 'findnewparent':
                $editpage = new SelectNewParentForm();
            break;
            case 'newparent':
                $newparent = $_POST['parentnode'];
                $message='Moved page <i>'.title2html(getpagetitle($page)).'</i> to <i>';
                if ($newparent) {
                    $message .= title2html(getpagetitle($newparent));
                } else {
                    $message .= 'Site Root';
                }
                $message .= '</i><br />' . movetonewparentpage($page, $newparent);
                updateeditdata($newparent);
                unlockpage($page);
                $editpage = editedRedirect($page, 'Moved page to a new parent page');
            break;
            case 'publish':
                publish($page);
                unlockpage($page);
                $message = 'You published the following page: ' . title2html(getpagetitle($page));
                $editpage = editedRedirect($page, 'Published a page');
            break;
            case 'unpublish':
                unpublish($page);
                unlockpage($page);
                $message = 'You removed the following page from public view: ' . title2html(getpagetitle($page));
                $editpage = editedRedirect($page, 'Hid a page');
            break;
            case 'setpublishable':
                if ($_POST['ispublishable'] === 'public') {
                    makepublishable($page);
                    $message = 'Earmarked <em>'.title2html(getpagetitle($page)).'</em> for publishing';
                    $title = 'Earmarked a page for publishing';
                } else {
                    $message = 'Marked <em>'.title2html(getpagetitle($page)).'</em> as internal';
                    $title = 'Marked a page as internal';
                    if (ispublished($page)) {
                        $message .= '<br />The page had already been published and has now been removed from public view.';
                        unpublish($page);
                    }
                    hide($page);
                }
                unlockpage($page);
                $editpage = editedRedirect($page, $title);
            break;
            case 'setpermissions':
                $permission = NO_PERMISSION;
                if (isset($_POST['permission'])) {
                    $permission=$_POST['permission'];
                }

                updatecopyright($page, $_POST['copyright'], $_POST['imagecopyright'], $permission);
                updateeditdata($page);
                $message = 'Edited copyright permissions';
            break;
            // access restriction
            case 'restrictaccess';
                if ($_POST['restrict']) {
                    restrictaccess($page);
                    $message='Restricted access';
                } else {
                    removeaccessrestriction($page);
                    $message='Removed access restriction';
                }
                $editpage = editedRedirect($page, 'Edited page restrictions');
            break;
            case 'restrictaccessusers':
                if (isset($_POST['addpublicusers'])) {
                    addpageaccess($_POST['selectusers'], $page);
                } else {
                    removepageaccess($_POST['selectusers'], $page);
                }
                $message = 'Edited user access';
            break;
            default:
                $message = "Unknown action $action";
                $error = true;
        }
    }
}

if (!isset($editpage)) {
    $editpage = new EditPage($page);
}
$content = new AdminMain($page, 'edit', new AdminMessage($message, $error), $editpage);
print($content->toHTML());
?>
