<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/bannersmod.php';
require_once BASEDIR.'/admin/functions/files.php';
require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/admin/includes/objects/adminmain.php';
require_once BASEDIR.'/admin/includes/objects/messages.php';
require_once BASEDIR.'/admin/includes/objects/site/banners.php';
require_once BASEDIR.'/functions/banners.php';

checksession();
checkadmin();

if(isset($_GET['page'])) { $page=$_GET['page'];
} else { $page=0;
}

$postaction="";
if(isset($_GET['postaction'])) { $postaction=$_GET['postaction'];
}
unset($_GET['postaction']);

$message = "";
$error = false;

//print_r($_POST);

if($postaction=='editbanner') {
    if(strlen($_POST['code'])>0) {
        $message .= 'Edited banner #'.$_POST['bannerid'].'code <i>'.$_POST['header'].'</i>';
        updatebannercode($_POST['bannerid'], $_POST['header'], $_POST['code']);
    }
    else
    {
        $id = isset($_POST['bannerid']) ? $_POST['bannerid'] : 0;
        $filename=$_FILES['image']['name'];
        if(strlen($filename)>0) {
            $filename=cleanupfilename($filename);
            $contents = getbannercontents($id);
            deletefile("img/banners", $contents['image']);
            $errorcode = replacefile("img/banners", "image", $filename);
            if($errorcode == UPLOAD_ERR_OK) {
                $success = true;
                $message .= "Replaced banner image with: ".$filename;
            }
            else
            {
                $message .= "<br />Error ".$errorcode.": ".fileerrors($errorcode)." ";
                $success = false;
                $error = true;
            }
        }
        else
        {
            $contents = getbannercontents($id);
            $filename=$contents['image'];
            $success=true;
            $message .= "Replace banner contents";
        }
        if($success) {
            $message='Edited banner #' . $id . ': <i>'.$_POST['header'].'</i>';
            updatebanner($id, $_POST['header'], $filename, $_POST['description'], $_POST['link']);

            $contents = getbannercontents($id);
            if (!isbannercomplete($contents)) {
                $message .= '. This banner is not complete and will not be displayed! Please fill out all required fields.';
                $error = true;
            }
        }
        else
        {
            $message .= 'Failed to edit banner #' . $id . ': error uploading image!';
            $error = true;
        }
    }
}

elseif($postaction=='displaybanners') {
    $newproperties = array('Display Banners' => SQLStatement::setinteger(trim($_POST['toggledisplaybanners'])));
    $message = updateproperties(SITEPROPERTIES_TABLE, $newproperties, 255);
    if (empty($message)) {
        $properties = getproperties(); // need to update global variable
        $message .= "Changed banner display options";
    } else {
        $message = "Failed to save Guestbook properties";
        $error = true;
    }
}

unset($_POST['bannerid']);

$content = new AdminMain($page, "sitebanner", new AdminMessage($message, $error), new SiteBanners());
print($content->toHTML());
?>
