<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/redirectsmod.php';
require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/admin/includes/objects/adminmain.php';
require_once BASEDIR.'/admin/includes/objects/messages.php';
require_once BASEDIR.'/admin/includes/objects/site/redirects.php';

checksession();
checkadmin();

$page = isset($_GET['page']) ? $_GET['page'] : 0;

$message = '';
$error = false;

if (isset($_POST['editredirect'])) {
    $pattern = trim($_POST['pattern']);
    $replacement = trim($_POST['replacement']);

    if (empty($pattern) || empty($replacement)) {

        $success = deleteredirect($_POST['id']);
        if ($success) {
            $message = 'Deleted redirect';
        } else {
            $message = 'Error deleting redirect';
            $error = true;
        }
    } else {
        $success = updateredirect($_POST['id'], $pattern, $replacement);
        if ($success) {
            $message = "Edited redirect <em>$pattern => $replacement</em>";
        } else {
            $message = "Error editing redirect <em>$pattern => $replacement</em>";
            $error = true;
        }
    }
} elseif (isset($_POST['addredirect'])) {
    $pattern = trim($_POST['pattern']);
    $replacement = trim($_POST['replacement']);

    if (empty($pattern) || empty($replacement)) {
        $message = 'Please fill out both fields';
        $error = true;
    } else {
        $message = 'Added redirect';
        addredirect($pattern, $replacement);
    }
}

$content = new AdminMain($page, 'siteredirects', new AdminMessage($message, $error), new SiteRedirects());
print($content->toHTML());
?>
