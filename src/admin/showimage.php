<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(__DIR__));

include BASEDIR.'/admin/includes/legaladminvars.php';

// check legal vars
if (!defined('LEGALVARS')) {
    define (
        'LEGALVARS',
        array(
            'fbclid', // Appended by facebook to track you with
            'image',
            'item',
            'next',
            'page',
            'prev'
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!is_numeric($key) && !in_array($key, LEGALVARS)) {
        header('HTTP/1.0 404 Not Found');
        print('HTTP 404: Sorry, but this page does not exist.');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<br />'$key' not registered with vars for showimage.");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/includes/objects/showimage.php';

checksession();

$nextitem=0;
$previousitem=0;
$image="";
$item=0;

if(isset($_GET['page'])) { $page=$_GET['page'];
} else { $page=0;
}

if(isset($_GET['image'])) {
    $image=$_GET['image'];
}
if(isset($_GET['item'])) {
    $item=$_GET['item'];
    // get image from item array
    $image=$_POST[$_GET['item']];
}

$showimage = new Showimage($page, $image, $item, true);
print($showimage->toHTML());
?>
