<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/functions/images.php';

//
//
//
function thumbnailexists($thumbnailfilename)
{
    if (empty($thumbnailfilename)) {
        return false;
    }
    $sql = new SQLSelectStatement(IMAGES_TABLE, 'thumbnail_filename', array('thumbnail_filename'), array($thumbnailfilename), 's');
    return $sql->fetch_value() === $thumbnailfilename;
}

// Make new path for each month to avoid directory that is too full
function makeimagesubpath() {
    $date = getdate();
    if ($date["mon"] < 10) {
        $date["mon"]= "0" . $date["mon"];
    }
    return "/" . $date["year"] . $date["mon"];
}

//
//
//
function getimagesubpath($filename)
{
    if (empty($filename)) {
        return "";
    }
    $sql = new SQLSelectStatement(IMAGES_TABLE, 'path', array('image_filename'), array($filename), 's');
    return $sql->fetch_value();
}

//
// returns false if image already exists
//
function addimage($filename, $subpath, $caption, $source, $sourcelink, $copyright, $permission)
{
    if(imageexists($filename)) {
        return false;
    }

    $columns = array('image_filename', 'uploaddate', 'imageeditor_id', 'permission');
    $values = array($filename, date(DATETIMEFORMAT, strtotime('now')), getsiduser(), $permission);
    $datatypes = 'ssii';

    if (!empty($subpath)) {
        array_push($columns, 'path');
        array_push($values, $subpath);
        $datatypes .= 's';
    }
    if (!empty($caption)) {
        array_push($columns, 'caption');
        array_push($values, $caption);
        $datatypes .= 's';
    }
    if (!empty($source)) {
        array_push($columns, 'source');
        array_push($values, $source);
        $datatypes .= 's';
    }
    if (!empty($sourcelink)) {
        array_push($columns, 'sourcelink');
        array_push($values, $sourcelink);
        $datatypes .= 's';
    }
    if (!empty($copyright)) {
        array_push($columns, 'copyright');
        array_push($values, $copyright);
        $datatypes .= 's';
    }

    $sql = new SQLInsertStatement(IMAGES_TABLE, $columns, $values, $datatypes);
    return $sql->insert();
}

//
//
//
function addthumbnail($image,$thumbnail)
{
    $sql = new SQLUpdateStatement(
        IMAGES_TABLE,
        array('thumbnail_filename'), array('image_filename'),
        array($thumbnail, $image), 'ss');
    return $sql->run();
}

?>
