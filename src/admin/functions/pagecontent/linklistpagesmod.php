<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';

// Get all columns for linklist link
function getlinkcontents($link) {
    $sql = new SQLSelectStatement(LINKS_TABLE, '*', array('link_id'), array($link), 'i');
    return $sql->fetch_row();
}

// Write alphabetic sorting of all links in a linklist page
function sortlinksbyname($page) {
    $sql = new SQLSelectStatement(LINKS_TABLE, 'link_id', array('page_id'), array($page), 'i');
    $sql->set_order(array('title' => 'ASC'));
    $items = $sql->fetch_column();

    // Bring into shape for the database call
    $values = array();
    $counter = 0;
    foreach ($items as $item) {
        array_push($values, array($counter++, $item));
    }

    // Write
    $sql = new SQLUpdateStatement(
        LINKS_TABLE,
        array('position'), array('link_id'),
        array(), 'ii'
    );
    $sql->set_values($values);
    return $sql->run();
}

?>
