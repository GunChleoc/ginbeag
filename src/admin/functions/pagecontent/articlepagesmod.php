<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';

// Get all databsase columns for articlesection
function getarticlesectioncontents($articlesection) {
    $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, '*', array('articlesection_id'), array($articlesection), 'i');
    return $sql->fetch_row();
}

// Fetch number of pages for article
function numberofarticlepages($page) {
    $sql = new SQLSelectStatement(ARTICLES_TABLE, 'numberofpages', array('page_id'), array($page), 'i');
    return $sql->fetch_value();
}

// Get last section on an article page
function getlastarticlesection($page, $pagenumber) {
    $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, 'position', array('article_id', 'pagenumber'), array($page, $pagenumber), 'ii');
    $sql->set_operator('max');
    return $sql->fetch_value();
}

// Add a page to article. Current last page must have a section in it.
function addarticlepage($page) {
    $numberofpages = numberofarticlepages($page);
    if (getlastarticlesection($page, $numberofpages)) {
        $sql = new SQLUpdateStatement(
            ARTICLES_TABLE,
            array('numberofpages'), array('page_id'),
            array($numberofpages + 1, $page), 'ii'
        );
        return $sql->run();
    }
    return false;
}

// Delete last page in article. Must gave no sections in it.
function deletelastarticlepage($page) {
    $numberofpages = numberofarticlepages($page);
    if ($numberofpages > 0 && !getlastarticlesection($page, $numberofpages)) {
        $sql = new SQLUpdateStatement(
            ARTICLES_TABLE,
            array('numberofpages'), array('page_id'),
            array($numberofpages - 1, $page), 'ii'
        );
        return $sql->run();
    }
    return false;
}

?>
