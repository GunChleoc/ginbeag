<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/pagesmod.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/functions/pagecontent/newspages.php';
require_once BASEDIR.'/functions/treefunctions.php';

// Set display order for newsitems
function setdisplaynewestnewsitemfirst($page, $shownewestfirst) {
    $sql = new SQLUpdateStatement(
        NEWS_TABLE,
        array('shownewestfirst'), array('page_id'),
        array($shownewestfirst ? 1 : 0, $page), 'ii'
    );
    return $sql->run();
}


// Get a selection of newsitems for page
function getnewsitems($page, $number, $offset) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'newsitem_id', array('page_id'), array($page), 'i');
    $sql->set_order(array('date' => 'DESC'));
    $sql->set_limit($number, $offset);
    return $sql->fetch_column();
}

// Total amount of newsitems on page
function countnewsitems($page) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'newsitem_id', array('page_id'), array($page), 'i');
    $sql->set_operator('count');
    return $sql->fetch_value();
}

// Find out which page a newsitem is on
function getpagefornewsitem($newsitem) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'page_id', array('newsitem_id'), array($newsitem), 'i');
    return $sql->fetch_value();
}

// Get pagination offset for newsitem
function getnewsitemoffset($page, $number, $newsitem)
{
    if (!$newsitem > 0) {
        return 0;
    }
    $number = max(1, $number);

    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'date', array('newsitem_id'), array($newsitem), 'i');
    $date = $sql->fetch_value();

    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'newsitem_id', array('page_id'), array($page, $date), 'is', 'date > ?');
    $sql->set_operator('count');
    $noofelements = $sql->fetch_value();

    return floor($noofelements/$number);
}

// All columns for newsitem
function getnewsitemsectioncontents($newsitemsection) {
    $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, '*', array('newsitemsection_id'), array($newsitemsection), 'i');
    return $sql->fetch_row();
}

// Get synopsis image info from its id
function getnewsitemsynopsisimage($id) {
    $sql = new SQLSelectStatement(NEWSITEMSYNIMG_TABLE, '*', array('newsitemimage_id'), array($id), 'i');
    return $sql->fetch_row();
}

// Add image to newsitem synopsis
function addnewsitemsynopsisimage($newsitem, $filename) {
    $sql = new SQLSelectStatement(NEWSITEMSYNIMG_TABLE, 'position', array('newsitem_id'), array($newsitem), 'i');
    $sql->set_operator('max');

    $sql = new SQLInsertStatement(
        NEWSITEMSYNIMG_TABLE,
        array('newsitem_id', 'image_filename', 'position'),
        array($newsitem, $filename, $sql->fetch_value() + 1),
        'isi'
    );
    return $sql->insert();
}

// Remove image from newsitem synopsis
function removenewsitemsynopsisimage($newsitemimage) {
    $sql = new SQLDeleteStatement(NEWSITEMSYNIMG_TABLE, array('newsitemimage_id'), array($newsitemimage), 's');
    $sql->run();
}

// moves all newsitems in $page that are not newer than $day, $month, $year
// to a new page below $page
// returns number of archived newsitems
function archivenewsitems($page,$day,$month,$year) {
    include_once BASEDIR.'/admin/functions/pagescreate.php';
    $maxpagetitlelength = 200;
    $maxnavtitlelength = 30;

    $date = "$day " . get_month_name($month) . " $year 23:59:59";
    $comparedate = date(DATETIMEFORMAT, strtotime($date));

    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'newsitem_id', array('page_id'), array($page, $comparedate), 'is', 'date <= ?');
    $moveids = $sql->fetch_column();

    $noofitems = count($moveids);

    if ($noofitems > 0) {
        $oldestdate = getoldestnewsitemdate($page);

        // Make pagetitle for new page
        $from = makearticledate($oldestdate['mday'], $oldestdate['mon'], $oldestdate['year']);
        $to = makearticledate($day, $month, $year);

        if ($from != $to) {
            $interval = " ($from - $to)";
        } else {
            $interval = " ($from)";
        }

        $pagetitle = getpagetitle($page);
        if (strlen($pagetitle) + strlen($interval) > $maxpagetitlelength) {
            $pagetitle = substr($pagetitle, 0, $maxpagetitlelength - strlen($interval));
            $pagetitle = substr($pagetitle, 0, strrpos($pagetitle, ' '));
        }
        $pagetitle .= $interval;

        // Make navtitle for new page
        $from = $oldestdate['mday'] . ' ' . substr(get_month_name($oldestdate['mon']), 0, 3) . ' ' . $oldestdate['year'];
        $to = "$day " . substr(get_month_name($month), 0, 3) . " $year";

        if ($from != $to) {
            $interval=" ($from - $to)";
        } else {
            $interval = " ($from)";
        }

        $navtitle = getnavtitle($page);
        if (strlen($navtitle) + strlen($interval) > $maxnavtitlelength) {
            $navtitle = substr($navtitle, 0, $maxnavtitlelength - strlen($interval));
            $navtitle = substr($navtitle, 0, strrpos($navtitle, ' '));
        }
        $navtitle .= $interval;

        // Create new page
        $newpage = createpage($page, $pagetitle, $navtitle, getpagecontents($page)['language'], 'news', getsiduser(), ispublishable($page));

        // Move items to the new page
        $values = array($newpage);
        foreach ($moveids as $value) {
            $values[] = $value;
        }

        $sql = new RawSQLStatement(
            'UPDATE ' . NEWSITEMS_TABLE .
                ' SET page_id = ? WHERE newsitem_id ' . SQLStatement::create_in_question_marks($moveids),
            $values, $datatypes = str_pad('', $noofitems + 1, 'i')
        );
        $sql->run();
    }
    return $noofitems;
}

// Set to generate RSS feed for newspage
function addrssfeed($page) {
    $sql = new SQLInsertStatement(RSS_TABLE, array('page_id'), array($page), 'i');
    return $sql->insert();
}

// Set to not generate RSS feed for newspage
function removerssfeed($page) {
    $sql = new SQLDeleteStatement(RSS_TABLE, array('page_id'), array($page), 'i');
    $sql->run();
}

// Search in newsitem titles
function searchnewsitemtitles($search, $page) {
    $query = 'SELECT DISTINCTROW newsitem_id FROM ' . NEWSITEMS_TABLE;
    $query .= ' WHERE page_id = ? AND title like ?';

    $sql = new RawSQLStatement($query, array($page, '%' . trim($search) . '%'), 'is');
    return $sql->fetch_column();
}
?>
