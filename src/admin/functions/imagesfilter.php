<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/imagesmod.php';
require_once BASEDIR.'/functions/categories.php';
require_once BASEDIR.'/functions/imagefiles.php';
require_once BASEDIR.'/includes/constants.php';

// *************************** filtering functions ************************** //

//
// $files: Images to be filtered
//
function getmissingimages($files) {
    $result=array();

    foreach ($files as $file) {
        if (!file_exists(getimagepath($file, getimagesubpath($file)))) {
            array_push($result, $file);
        }
    }
    return $result;
}


//
//
//
function getunknownimages()
{
    $result = getunknownimageshelper("");

    $dir = new DirectoryIterator(BASEDIR.'/'.getproperty("Image Upload Path"));
    foreach ($dir as $fileinfo)
    {
        if ($fileinfo->isDir() && !$fileinfo->isDot()) {
            $result = array_merge($result, getunknownimageshelper("/".$fileinfo->getFilename()));
        }
    }
    return $result;
}


//
//
//
function getunknownimageshelper($subpath)
{
    $result=array();

    $dirtolist = BASEDIR.'/'.getproperty("Image Upload Path").$subpath;

    //using the opendir function
    $dir_handle = @opendir($dirtolist)
    or die("Unable to open path");

    while($file = readdir($dir_handle)) {
        $lowercasefile = mb_strtolower($file, 'UTF-8');
        if(!is_dir($dirtolist."/".$file) && !strpos($lowercasefile, ".php") && !strpos($lowercasefile, ".htm")) {
            if (!(imageexists($file) || thumbnailexists($file))) {
                array_push($result, array("filename" => $file, "subpath" => $subpath));
            }
        }
    }
    return $result;
}

//
// $files: Images to be filtered
//
function getmissingthumbnails($files)
{
    $result = array();

    foreach ($files as $file) {
        $imagedata = getimage($file);
        $thumbnail = $imagedata['thumbnail_filename'];
        if (!empty($thumbnail) && !file_exists(BASEDIR.'/'.getproperty("Image Upload Path").$imagedata['path']."/".$thumbnail)) {
            array_push($result, $file);
        }
    }
    return $result;
}

// $files: Images to be filtered
function getimagesmissingthumbnails($files) {
    $result = array();

    foreach ($files as $file) {
        $imagedata = getimage($file);
        if (should_have_thumbnail($imagedata, getproperty("Thumbnail Size"))) {
            if (empty(getthumbnail($file))) {
                array_push($result, $file);
            } else {
                $path = BASEDIR.'/'.getproperty("Image Upload Path") . $imagedata['path'];
                // make sure a mobile thumbnail exists
                if (!file_exists("$path/" . make_thumbnail_filename($file))) {
                    array_push($result, $file);
                }
            }
        }
    }
    return $result;
}


function getimagesforsimplesearch($searchterm, $order, $ascdesc) {
    global $legal_columns;

    if ($order) {
        if ($order === 'uploader') {
            $order = 'imageeditor_id';
        } elseif($order === 'filename') {
            $order = 'image_filename';
        }
    } else {
        $order = 'uploaddate';
    }

    if (!in_array($order, $legal_columns)) {
        return array();
    }


    $values = array();
    $datatypes = '';
    $query = "SELECT DISTINCT images.image_filename, images.$order FROM " . IMAGES_TABLE . ' as images';

    $query .= ' WHERE images.image_filename LIKE ?';
    array_push($values, '%' . $searchterm . '%');
    $datatypes .= 's';

    $query .= " UNION SELECT DISTINCT images.image_filename, images.$order FROM " . IMAGES_TABLE . ' as images WHERE caption LIKE ?';
    array_push($values, '%' . $searchterm . '%');
    $datatypes .= 's';

    $query .= " UNION SELECT DISTINCT images.image_filename, images.$order FROM " . IMAGES_TABLE . ' as images WHERE source LIKE ?';
    array_push($values, '%' . $searchterm . '%');
    $datatypes .= 's';

    $query .= " UNION SELECT DISTINCT images.image_filename, images.$order FROM " . IMAGES_TABLE . ' as images WHERE sourcelink LIKE ?';
    array_push($values, '%' . $searchterm . '%');
    $datatypes .= 's';

    $query .= " UNION SELECT DISTINCT images.image_filename, images.$order FROM " . IMAGES_TABLE . ' as images WHERE copyright LIKE ?';
    array_push($values, '%' . $searchterm . '%');
    $datatypes .= 's';

    $query .= ' ORDER BY ' . $order . (mb_strtolower($ascdesc, 'UTF-8') === 'desc' ? ' DESC' : ' ASC');

    $sql = new RawSQLStatement($query, $values, $datatypes);
    return $sql->fetch_column();
}

//
//
//
function getfilteredimages(
    $filename, $caption, $source, $sourceblank, $sourcelink, $uploader,
    $copyright, $copyrightblank, $selectedcats, $categoriesblank,
    $order, $ascdesc
) {

    $filename = trim($filename);
    $caption = trim($caption);
    $source = trim($source);
    $sourcelink = trim($sourcelink);
    $copyright = trim($copyright);
    $result=array();

    // get all category children
    if(count($selectedcats)>0 && !$categoriesblank) {
        $result=getfilteredimageshelper($filename, $caption, $source, $sourceblank, $sourcelink, $uploader, $copyright, $copyrightblank, array_pop($selectedcats), $categoriesblank, $order, $ascdesc);
        while(count($selectedcats))
        {
            $filenames= getfilteredimageshelper($filename, $caption, $source, $sourceblank, $sourcelink, $uploader, $copyright, $copyrightblank, array_pop($selectedcats), $categoriesblank, $order, $ascdesc);
            $result=array_intersect($result, $filenames);
        }
    }
    else
    {
        $result=getfilteredimageshelper($filename, $caption, $source, $sourceblank, $sourcelink, $uploader, $copyright, $copyrightblank, -1, $categoriesblank, $order, $ascdesc);
    }
    return $result;
}

//
//
//
function getfilteredimageshelper($filename,$caption,$source,$sourceblank,$sourcelink,$uploader,$copyright,$copyrightblank,$selectedcat,$categoriesblank,$order,$ascdesc)
{
    global $legal_columns;

    if ($order) {
        if ($order === 'uploader') {
            $order = 'imageeditor_id';
        } elseif($order === 'filename') {
            $order = 'image_filename';
        }
    } else {
        $order = 'uploaddate';
    }

    if (!in_array($order, $legal_columns)) {
        return array();
    }

    $result=array();
    $categories=array();
    if($selectedcat>=0) {
        $pendingcategories=array(0 => $selectedcat);
        while(count($pendingcategories))
        {
            $selectedcat=array_pop($pendingcategories);
            array_push($categories, $selectedcat);
            $pendingcategories=array_merge($pendingcategories, getcategorychildren($selectedcat, CATEGORY_IMAGE));
        }
    }

    $values = array();
    $datatypes = "";

    $query="SELECT DISTINCT images.image_filename, images.$order FROM ";
    $query.=IMAGES_TABLE." as images";

    if(count($categories)>0) {
        $query.=", ".IMAGECATS_TABLE." AS cat";
        $query.=" WHERE cat.image_filename = images.image_filename";
    }
    else
    {
        $query.=" WHERE ?";
        array_push($values, 1);
        $datatypes .= 'i';
    }

    if($filename) {
        $query.=" AND images.image_filename LIKE ?";
        array_push($values, '%' . $filename . '%');
        $datatypes .= 's';
    }
    if(strlen($caption) > 0) {
        $query.=" AND caption LIKE ?";
        array_push($values, '%' . $caption . '%');
        $datatypes .= 's';
    }
    if($sourceblank) {
        $query.=" AND source = ''";
    }
    elseif(strlen($source) > 0) {
        $query.=" AND source LIKE ?";
        array_push($values, '%' . $source . '%');
        $datatypes .= 's';
    }
    if(strlen($sourcelink) > 0) {
        $query.=" AND sourcelink LIKE ?";
        array_push($values, '%' . $sourcelink . '%');
        $datatypes .= 's';
    }
    if($copyrightblank) {
        $query.=" AND copyright = ''";
    }
    elseif(strlen($copyright) >0) {
        $query.=" AND copyright LIKE ?";
        array_push($values, '%' . $copyright . '%');
        $datatypes .= 's';
    }
    if($uploader > 0) {
        $query.=" AND imageeditor_id = ?";
        array_push($values, $uploader);
        $datatypes .= 's';
    }
    if(count($categories)>0) {
        $query.= ' AND cat.category ' . SQLStatement::create_in_question_marks($categories);
        foreach ($categories as $cat) {
            $values[] = $cat;
        }
        $datatypes .= str_pad('', count($categories), 'i');
    }
    if ($order) {
        $query .= ' ORDER BY ' . $order . (mb_strtolower($ascdesc, 'UTF-8') === 'desc' ? ' DESC' : ' ASC');
    }

    $sql = new RawSQLStatement($query, $values, $datatypes);
    $result = $sql->fetch_column();


    //  print('Some debugging info: '.$query.'<p>');

    if($categoriesblank) {
        $temp=$result;
        $result=array();
        for($i=0;$i<count($temp);$i++)
        {
            $sql = new SQLSelectStatement(IMAGECATS_TABLE, 'image_filename', array('image_filename'), array($temp[$i]), 's');
            $sql->set_distinct();
            if (!$sql->fetch_value()) {
                array_push($result, $temp[$i]);
            }
        }
    }
    return $result;
}


//
//
//
function getsomefilenames($offset,$number, $order="image_filename", $ascdesc="ASC")
{
    if (empty($order)) {
        $order = 'image_filename';
    } elseif ($order === 'uploader') {
        $order = 'imageeditor_id';
    } elseif ($order === 'filename') {
        $order = 'image_filename';
    }

    $sql = new SQLSelectStatement(IMAGES_TABLE, 'image_filename');
    $sql->set_order(array($order => $ascdesc));
    $sql->set_limit($number, $offset);
    return $sql->fetch_column();
}

//
//
//
function countimages()
{
    $sql = new SQLSelectStatement(IMAGES_TABLE, 'image_filename');
    $sql->set_operator('count');
    return $sql->fetch_value();
}

?>
