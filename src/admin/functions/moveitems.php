<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';

// Change the position of an item on a page or of a page in the navigator
// $table:        the database table to update
// $primary_key:  the database column containing the primary key for the $items
// $item:         the int item to swap
// $sql:          SQLSelectStatement that will fetch all items in the same context (e.g. page) as $item
// $positions:    the number of positions that the item will be swapped
//                must be > 0; too large values will be interpreted as max position
// $direction:    'up', 'down', 'top' or 'bottom'
function move_item($table, $primary_key, $item, $sql, $positions, $direction) {
    $legaldirections = array('up', 'down', 'top', 'bottom');
    $numbereddirections = array('up', 'down');

    if (in_array($direction, $numbereddirections) && (!(@is_numeric($positions) && @ctype_digit($positions)) || $positions < 1)) {
        return false;
    }
    if (!in_array($direction, $legaldirections)) {
        return false;
    }

    $sql->set_order(array('position' => 'ASC'));
    $items = $sql->fetch_column();

    if ($direction === 'top' || $direction === 'up') {
        $items = array_reverse($items);
    }

    $itemcount = count($items);
    $itemposition = array_search($item, $items);
    unset($items[$itemposition]);

    if ($direction === 'top' || $direction === 'bottom') {
        array_push($items, $item);
    } else {
        $newposition = min($itemposition + $positions, $itemcount - 1);
        array_splice($items, $newposition, 0, $item);
    }

    // Fix keys after array change
    $items = array_values($items);

    if ($direction === 'top' || $direction === 'up') {
        $items = array_reverse($items);
    }

    // Bring into shape for the database call
    $values = array();
    for ($i = 0; $i < $itemcount; $i++) {
        array_push($values, array($i, $items[$i]));
    }

    $sql = new SQLUpdateStatement(
        $table,
        array('position'), array($primary_key),
        array(), 'ii'
    );
    $sql->set_values($values);
    return $sql->run();
}
?>
