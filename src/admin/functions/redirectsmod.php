<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';

// Add text replacement regex info to database
function addredirect($pattern, $replacement) {
    $sql = new SQLInsertStatement(REDIRECTS_TABLE, array('pattern', 'replacement'), array($pattern, $replacement), 'ss');
    return $sql->insert();
}

// Update text replacement regex record in database
function updateredirect($id, $pattern, $replacement) {
    $sql = new SQLUpdateStatement(
        REDIRECTS_TABLE,
        array('pattern', 'replacement'), array('id'),
        array($pattern, $replacement, $id),
        'ssi'
    );
    return $sql->run();
}

// Delete text replacement regex record from database
function deleteredirect($id) {
    $sql = new SQLDeleteStatement(REDIRECTS_TABLE, array('id'), array($id), 'i');
    return $sql->run();
}

?>
