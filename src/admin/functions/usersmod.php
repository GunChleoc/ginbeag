<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/sessions.php';
require_once BASEDIR.'/functions/db.php';

//
//
//
function register($user,$pass,$email)
{
    $activationkey = create_sid();

    $sql = new SQLInsertStatement(
        USERS_TABLE,
        array('user_active', 'username', 'displayname', 'password', 'email', 'userlevel', 'iscontact',
        'activationkey', 'last_login', 'retries'),
        array(0, mb_strtolower($user, 'UTF-8'), $user, md5($pass), $email, USERLEVEL_USER, 0, $activationkey, date(DATETIMEFORMAT, strtotime('now')), 0),
        'isdssiissi'
    );
    $sql->insert();
    $sql = new SQLSelectStatement(USERS_TABLE, 'activationkey', array('username'), array($user), 's');
    if ($activationkey === $sql->fetch_value()) { return $activationkey;
    }
    return false;
}

//
//
//
function changeuserpassword($userid,$oldpass,$newpass,$confirmpass)
{
    $result["message"] = "Failed to change password";
    $result["error"] = false;
    if(checkpassword(getusername($userid), md5($oldpass))) {
        if(strlen($newpass)>7) {
            if($newpass===$confirmpass) {
                $sql = new SQLUpdateStatement(
                    USERS_TABLE,
                    array('password'), array('user_id'),
                    array(md5($newpass), $userid), 'si'
                );
                if ($sql->run()) {
                    $result["message"] = "Password changed successfully";
                }
            }
            else
            {
                $result["message"] = "Passwords did not match.";
                $result["error"] = true;
            }
        }
        else
        {
            $result["message"] = "Your password must be at least 8 digits long.";
            $result["error"] = true;
        }
    }
    else
    {
        $result["message"] = "Wrong password.";
        $result["error"] = true;
    }
    return $result;
}


//
//
//
function changeuserpasswordadmin($userid,$newpass,$confirmpass)
{
    $result="Failed to change password";
    if(isadmin()) {
        if(strlen($newpass)>7) {
            if($newpass===$confirmpass) {
                $sql = new SQLUpdateStatement(
                    USERS_TABLE,
                    array('password'), array('user_id'),
                    array(md5($newpass), $userid), 'si'
                );
                if ($sql->run()) {
                    $result= "Password changed successfully";
                }
            }
            else
            {
                $result="Passwords did not match";
            }
        }
        else
        {
            $result="Your password must be at least 8 digits long";
        }
    }
    else
    {
        $result="Please hack someone else.";
    }
    return $result;
}



//
//
//
function makepassword($userid) {
    $newpass = make_random_string('.,@*#_-0123456789aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ', 32, 64);
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('password'), array('user_id'),
        array(md5($newpass), $userid), 'si'
    );
    $sql->run();
    return $newpass;
}


//
//
//
function changeuseremail($userid,$email)
{
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('email'), array('user_id'),
        array($email, $userid), 'si'
    );
    $sql->run();
}

//
//
//
function setuserlevel($userid,$userlevel)
{
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('userlevel'), array('user_id'),
        array($userlevel, $userid), 'ii'
    );
    $sql->run();
}

//
//
//
function getusername($user)
{
    $sql = new SQLSelectStatement(USERS_TABLE, 'username', array('user_id'), array($user), 'i');
    return $sql->fetch_value();
}

//
//
//
function changeiscontact($userid,$iscontact)
{
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('iscontact'), array('user_id'),
        array($iscontact, $userid), 'ii'
    );
    $sql->run();
}


//
//
//
function changecontactfunction($userid,$contactfunction)
{
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('contactfunction'), array('user_id'),
        array($contactfunction, $userid), 'si'
    );
    $sql->run();
}

//
//
//
function activateuser($userid)
{
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('user_active', 'activationkey'), array('user_id'),
        array(1, '', $userid), 'isi'
    );
    return $sql->run();
}

//
//
//
function deactivateuser($userid)
{
    $sql = new SQLUpdateStatement(
        USERS_TABLE,
        array('user_active'), array('user_id'),
        array(0, $userid), 'ii'
    );
    return $sql->run();
}

//
//
//
function hasactivationkey($username,$activationkey)
{
    $sql = new SQLSelectStatement(USERS_TABLE, 'activationkey', array('username'), array($username), 's');
    return $activationkey === $sql->fetch_value();
}

//
//
//
function userexists($username)
{
    $sql = new SQLSelectStatement(USERS_TABLE, 'username', array('username'), array(mb_strtolower($username, 'UTF-8')), 's');
    return $sql->fetch_value();
}

//
//
//
function emailexists($email,$user=false)
{
    $sql = new SQLSelectStatement(USERS_TABLE, 'user_id', array('email'), array($email), 's');
    $emailuser = $sql->fetch_value();
    if($user) {
        return ($emailuser && ($user !== $emailuser));
    }
    else
    {
        return ($emailuser);
    }
}

//
//
//
function getallusers()
{
    $sql = new SQLSelectStatement(USERS_TABLE, '*');
    $sql->set_order(array('username' => 'ASC'));
    return $sql->fetch_many_rows();
}


//
//
//
function getallusernames()
{
    $sql = new SQLSelectStatement(USERS_TABLE, array('user_id', 'username'));
    $sql->set_order(array('username' => 'ASC'));
    return $sql->fetch_two_columns();
}


//
//
//
function getuser($userid, $fields)
{
    $sql = new SQLSelectStatement(USERS_TABLE, $fields, array('user_id'), array($userid), 'i');
    return $sql->fetch_row();
}

?>
