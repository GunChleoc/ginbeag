<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

if (!defined('LEGALVARS')) {
    define (
        'LEGALVARS',
        array(
            'action',
            'addunknownfile',
            'advanced',
            'ascdesc',
            'caption',
            'categoriesblank',
            'copyright',
            'copyrightblank',
            'createthumbnail',
            'delete',
            'deletefile',
            'deletefileconfirm',
            'deletethumb',
            'dontcreatethumbnail',
            'doorder',
            'executethumbnaildelete',
            'filename',
            'image',
            'newname',
            'noofimages',
            'number',
            'offset',
            'order',
            'page',
            'permission',
            'replaceimage',
            'replacethumb',
            'resizeimage',
            'selectedcat',
            'sid',
            'searchterm',
            'source',
            'sourceblank',
            'sourcelink',
            'subpath',
            'uploader'
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!in_array($key, LEGALVARS)) {
        header('HTTP/1.0 404 Not Found');
        print('HTTP 404: Sorry, but this page does not exist.');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<br />'$key' not registered with legalimagevars.");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

?>
