<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/includes/objects/admintopframe.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/includes/objects/page.php';

//
// last parameter needs to be a Template or string when used
//  $message needs to be ot type AdminMessage
//
class AdminMain extends Template
{

    function __construct($page, $action, $message, $contentobject=null)
    {
        global $_GET;
        parent::__construct($page, array('includes/javascript/jquery.js', 'admin/includes/javascript/messageboxes.js'));

        /****************************
 * header
***************************************/

        $this->stringvars['stylesheet']=getCSSPath("main.css");
        $this->stringvars['stylesheetcolors']= getCSSPath("colors.css");
        $this->stringvars['adminstylesheet']=getCSSPath("admin.css");
        $this->stringvars['headertitle']= title2html(getproperty("Site Name")).' - Webpage building';

        // load linked Javascript
        $this->stringvars['scriptlinks'] = $this->getjspaths();
        if($contentobject instanceof Template) {
            $this->stringvars['scriptlinks'] .= $contentobject->getjspaths();

            $jscripts = $contentobject->getScripts();
            if (!empty($jscripts)) {
                $this->stringvars['javascript'] = $jscripts;
            }
        }

        if($contentobject instanceof DoneRedirect) {
            $this->stringvars['is_redirect']="redirect";
            $this->stringvars['url']=$contentobject->stringvars['url'];
        }

        $this->vars['header']= new AdminTopFrame($page, $action);


        /****************************
 * navigator
************************************/

        if(issiteaction($action)) {
            include_once BASEDIR.'/admin/includes/objects/site/navigator.php';
            $this->vars['navigatorfixed']= new SiteAdminNavigatorHeader();
            $this->vars['navigatorscroll'] = new SiteAdminNavigator();
        }
        else
        {
            include_once BASEDIR.'/admin/includes/objects/navigator.php';
            $this->vars['navigatorfixed'] = new AdminNavigatorHeader();
            $this->vars['navigatorscroll'] = new AdminNavigator($page);
        }

        /****************************
 * content
**************************************/

        $this->vars['message'] = $message;

        if(!is_null($contentobject)) {


            if($contentobject instanceof Template) {
                $this->vars['contents']= $contentobject;
            } elseif(is_string($contentobject)) {
                $this->stringvars['contents']= $contentobject;
            } else {
                $this->stringvars['contents']= "Error: illegal content in AdminMain!";
            }

        }
        else
        {
            // rerout to guide for webpage editors
            if(!isset($_GET["page"]) || strlen($_GET["page"]<1) || $_GET["page"]<1) {
                $this->stringvars['contents'] = Page::unknownpagetext();
            }
            // create page content
            else
            {

                // init
                if(isset($_GET['articlepage'])) {
                    $articlepage=$_GET['articlepage'];
                } elseif(isset($_GET['offset'])) {
                    $articlepage=$_GET['offset']+1;
                } elseif(!isset($_GET['articlepage']) || strlen($_GET['articlepage'])<1) {
                    $articlepage=1;
                } else {
                    $articlepage=0;
                }


                if(isset($_GET['offset'])) { $offset=$_GET['offset'];
                } else { $offset=0;
                }

                $this->vars['message'] = new AdminPageDisplayMessage(true);

                include_once BASEDIR.'/functions/pages.php';

                $pagecontents = getpagecontents($_GET['page']);
                $this->stringvars['language']=$pagecontents['language'];

                switch($pagecontents['pagetype']) {
                    case "article":
                        include_once BASEDIR.'/includes/objects/articlepage.php';
                        $this->vars['contents'] = new ArticlePage($articlepage, $pagecontents, true);
                        break;
                    case "menu":
                    case "articlemenu":
                    case "linklistmenu":
                        include_once BASEDIR.'/includes/objects/menupage.php';
                        $this->vars['contents'] = new MenuPage($page, $pagecontents, true);
                        break;
                    case "external":
                        $this->stringvars['contents'] ='<a href="'.getexternallink($page).'" target="_blank">External page</a>';
                        break;
                    case "gallery":
                        include_once BASEDIR.'/includes/objects/gallerypage.php';
                        $this->vars['contents'] = new GalleryPage($pagecontents, $offset, true);
                        break;
                    case "linklist":
                        include_once BASEDIR.'/includes/objects/linklistpage.php';
                        $this->vars['contents']  = new LinklistPage($pagecontents, true);
                        break;
                    case "news":
                        include_once BASEDIR.'/includes/objects/newspage.php';
                        $this->vars['contents']  = new NewsPage($page, $pagecontents, $offset, true);
                        break;
                    default:
                        $contentstring = getlang("error_pagenotfound");
                }
            }
        }
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("admin/adminmain.tpl");
    }
}

?>
