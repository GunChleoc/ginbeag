<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/includes/objects/images.php';
require_once BASEDIR.'/functions/imagefiles.php';
require_once BASEDIR.'/includes/objects/forms.php';

//
// Templating for Section Images
//
class ImageEditor extends Template
{

    function __construct($page, $elementid, $elementtype, $contents)
    {
        parent::__construct($page.'-'.$elementid, array(), array('admin/includes/javascript/editors/image.js'));
        $this->stringvars['javascript']=$this->getScripts();

        $this->vars['editor'] = new ImageEditorCollapsed($page, $elementid, $elementtype, $contents);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/image/editor.tpl');
    }
}

class ImageEditorCollapsed extends Template {

    function __construct($page, $item, $elementtype, $contents) {
        parent::__construct($page.'-'.$item);

        $hiddenvars = array('item' => $item, 'elementtype' => $elementtype);

        if (!imageexists($contents['image_filename'])) {
            $this->stringvars['button'] = '';
            $this->stringvars['header'] = '';
        } else {
            $imagedata = Image::make_imagedata($contents);
            if ($imagedata['usethumbnail'] && $imagedata['thumbnail_filename']) {
                $src = getimagelinkpath($imagedata['thumbnail_filename'], $imagedata['thumbnailpath']);
                $this->stringvars['displayimage'] = '<img src="'.$src.'" />';
            } else {
                $src = getimagelinkpath($imagedata['image_filename'], $imagedata['path']);
                $this->stringvars['displayimage'] = '<img src="'.$src.'" width="'.$imagedata['width'].'" height="'.$imagedata['height'].'" />';
                $this->stringvars['width'] = $imagedata['width'];
            }

            switch($elementtype) {
                case 'gallery':
                    $this->stringvars['height'] = getproperty('Thumbnail Size') + 3 * IMAGECAPTION_LINEHEIGHT;
                    $this->stringvars['width'] = $imagedata['width'];
                    $this->stringvars['min-width'] = getproperty('Thumbnail Size');
                    break;
                case 'newsitemsynopsis':
                    $this->stringvars['height'] = getproperty('Thumbnail Size') + 3 * IMAGECAPTION_LINEHEIGHT;
                    break;
            }

            if ($elementtype === 'gallery') {
                $this->stringvars['halign'] = '';
                $this->stringvars['center'] = 'center';
            } else {
                $imagealign = isset($imagedata['imagealign']) ? $imagedata['imagealign'] : 'left';
                switch ($imagealign) {
                    case 'right':
                        $this->stringvars['halign'] = 'float:right; ';
                    break;
                    case 'left':
                        $this->stringvars['halign'] = 'float:left; ';
                    break;
                    case 'center':
                        $this->stringvars['halign'] = '';
                        $this->stringvars['center'] = 'center';
                }
            }

            if ($elementtype === 'link' || $elementtype === 'gallery' || $elementtype === 'newsitemsynopsis') {
                if (needs_thumbnail($imagedata, getproperty('Thumbnail Size'))) {
                    $this->stringvars['no_thumbnail'] = 'This image has no thumbnail';
                }
            }
            if ($elementtype === 'link' || $elementtype === 'newsitemsynopsis') {
                $this->stringvars['caption'] = '';
            } else {
                $this->vars['caption'] = new ImageCaption($imagedata);
            }
        }

        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/image/collapsed.tpl');
    }
}

class ImageEditorExpanded extends Template {

    function __construct($page, $elementid, $elementtype, $contents)
    {
        parent::__construct($page.'-'.$elementid);

        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('item' => $elementid, 'elementtype' => $elementtype));

        $this->stringvars['image'] = isset($contents['image_filename']) ? $contents['image_filename'] : '';

        $this->stringvars['elementtype']=$elementtype;
        $this->stringvars['imagelistpath']=getprojectrootlinkpath()."admin/editimagelist.php".makelinkparameters(array("page" => $this->stringvars['page']));
        $this->vars['filenamepane'] = new ImageEditorFilenamePane($page, $elementid, $this->stringvars['image'], $elementtype);

        if ($elementtype === 'link' || $elementtype === 'gallery' || $elementtype === 'newsitemsynopsis') {
            $this->vars['imagepane'] = new ImageEditorImagePane($page, $elementtype, $contents);
            $this->stringvars['layoutpane'] = '';
        } elseif($this->stringvars['image']) {
            $this->vars['imagepane'] = new ImageEditorImagePane($page, $elementtype, $contents);
            $this->vars['layoutpane'] = new ImageEditorLayoutPane($page, $elementid, $contents['imagealign'], $contents['usethumbnail']);
        } else {
            $this->stringvars['imagepane'] = '';
            $this->stringvars['layoutpane'] = '';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/image/expanded.tpl');
    }
}

//
// Templating for assigning an image to a section
//
class ImageEditorFilenamePane extends Template
{

    function __construct($page,$elementid, $image, $elementtype)
    {
        parent::__construct($page.'-'.$elementid);

        $this->stringvars['image']=$image;
        $this->stringvars['elementtype']=$elementtype;
        $this->stringvars['imagefilename']=$image;
        if($this->stringvars['image']) { $this->stringvars['submitname']="Add / Change Image";
        } else { $this->stringvars['submitname']="Remove Image";
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/image/filenamepane.tpl');
    }
}



//
// Templating for Image alignment and whether thumbnail is used within a section.
//
class ImageEditorLayoutPane extends Template
{

    function __construct($page, $elementid, $imagealign, $usethumbnail)
    {
        parent::__construct($page.'-'.$elementid);

        $this->stringvars['submitname'] ="Save image layout";

        if(!$imagealign) { $imagealign="left";
        }
        $this->vars['left_align_button']= new RadioButtonForm($this->stringvars["jsid"], "imagealign", "left", "Left", $imagealign==="left", "right");
        $this->vars['center_align_button']= new RadioButtonForm($this->stringvars["jsid"], "imagealign", "center", "Center", $imagealign==="center", "right");
        $this->vars['right_align_button']= new RadioButtonForm($this->stringvars["jsid"], "imagealign", "right", "Right", $imagealign==="right", "right");

        $this->vars['thumbnail'] = new CheckboxForm($this->stringvars['jsid'].'usethumbnail', 'usethumbnail', 'Use Thumbnail', $usethumbnail);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/image/layoutpane.tpl');
    }
}


//
// Templating for showing the image in the form
//
class ImageEditorImagePane extends Template {

    function __construct($page, $elementtype, $imagedata) {
        parent::__construct();

        $filename = isset($imagedata['image_filename']) ? $imagedata['image_filename'] : '';

        if (!empty($filename) && imageexists($filename)) {
            $imagedata = Image::make_imagedata($imagedata);
            if ($elementtype === 'link' || $elementtype === 'gallery' || $elementtype === 'newsitemsynopsis') {
                if (needs_thumbnail($imagedata, getproperty('Thumbnail Size'))) {
                    $this->stringvars['no_thumbnail'] = 'This image has no thumbnail';
                }
            }
            $imagedata['usethumbnail'] = true;
            $this->vars['image'] = new CaptionedImageAdmin($imagedata, $page);
        } else {
            $this->stringvars['image'] = $filename;
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/image/imagepane.tpl');
    }
}

?>
