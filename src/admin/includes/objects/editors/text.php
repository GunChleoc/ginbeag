<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/pages.php';
require_once BASEDIR.'/includes/constants.php';
require_once BASEDIR.'/includes/objects/forms.php';

//
// Templating for Editor
//
class TextEditor extends Template {

    function __construct($page, $item, $elementtype)
    {
        parent::__construct($page.'-'.$item, array(), array('admin/includes/javascript/editors/text.js'));
        $this->stringvars['javascript']=$this->getScripts();

        $this->stringvars['item']=$item;
        $this->stringvars['elementtype']=$elementtype;
        $this->stringvars['language'] = $page > 0 ? getpagecontents($page)['language'] : get_site_language();

        $text = geteditortext($page, $item, $elementtype);

        $this->vars['editorcontents'] = new TextEditorCollapsed($page, $item, $elementtype, $text);

        $this->stringvars['previewtext']=text2html($text);
        $this->stringvars['hiddenvars'] = $this->makehiddenvars();
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/text/editor.tpl');
    }
}

//
// expanded editor contents
//
class TextEditorExpanded extends Template {

    function __construct($page, $item, $elementtype, $edittext=false)
    {
        parent::__construct($page.'-'.$item);

        $hiddenvars["page"] = $page;
        $hiddenvars["item"] = $item;
        $hiddenvars["elementtype"] = $elementtype;
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);

        if($edittext!=false) {
            $edittext = stripslashes(stripslashes($edittext));
            $this->stringvars['text']=$edittext;
            $this->stringvars['previewtext']=text2html($edittext);
        }
        else
        {
            $text = geteditortext($page, $item, $elementtype);
            $this->stringvars['text'] = input2html($text, true);
            $this->stringvars['previewtext'] = text2html($text);
        }

        $page_language = $page > 0 ? getpagecontents($page)['language'] : get_site_language();
        $site_languages=explode(',', getproperty('Site Languages'));
        $no_of_languages = count($site_languages);
        $dropdown_languages = array();
        foreach ($site_languages as $site_lang) {
            $lang_code_name=explode('=', $site_lang);
            $lang_code_name[0]=trim($lang_code_name[0]);
            if ($lang_code_name[0] != $page_language) {
                $dropdown_languages[$lang_code_name[0]] = (count($lang_code_name) > 1) ? trim($lang_code_name[1]) : trim($lang_code_name[0]);
            }
        }
        natcasesort($dropdown_languages);
        $no_of_languages = count($dropdown_languages);

        if ($no_of_languages > 1) {
            // Dropdown
            array_unshift($dropdown_languages, '– Language –');
            $this->vars['languageform']=new OptionForm('0', array_keys($dropdown_languages), array_values($dropdown_languages), $this->stringvars['jsid'].'languageform', '', 1);
        } else if (count($site_languages) > 1) {
            // Button. Misuse name attribute to get the value to insert, to save us a hidden element.
            $this->stringvars['languageform']='<input type="button" id="'.$this->stringvars['jsid'].'languagebutton" name="'.array_key_first($dropdown_languages).'" value="'.$dropdown_languages[array_key_first($dropdown_languages)].'" />';
        } else {
            $this->stringvars['languageform']='';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/text/expanded.tpl');
    }
}


//
// expanded editor contents
//
class TextEditorCollapsed extends Template {

    function __construct($page,$item, $elementtype, $text)
    {
        parent::__construct($page.'-'.$item);

        if (empty(trim($text))) {
            $this->stringvars['notext'] = '';
        }

        $hiddenvars["page"] = $page;
        $hiddenvars["item"] = $item;
        $hiddenvars["elementtype"] = $elementtype;
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate('admin/editors/text/collapsed.tpl');
    }
}

//
// expanded editor contents
//
class TextEditorSaveDialog extends Template {

    function __construct($page, $item, $elementtype, $edittext)
    {
        parent::__construct($page.'-'.$item);

        $hiddenvars["page"] = $page;
        $hiddenvars["item"] = $item;
        $hiddenvars["elementtype"] = $elementtype;
        $hiddenvars["edittext"] = htmlspecialchars($edittext);
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate('admin/editors/text/savedialog.tpl');
    }
}




//
// helper function to get text for editor from database
//
function geteditortext($page,$item, $elementtype)
{
    $sql = null;
    switch ($elementtype) {
        case "pageintro":
            $sql = new SQLSelectStatement(PAGES_TABLE, 'introtext', array('page_id'), array($page), 'i');
        break;
        case "articlesection":
            $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, 'text', array('articlesection_id'), array($item), 'i');
        break;
        case "link":
            $sql = new SQLSelectStatement(LINKS_TABLE, 'description', array('link_id'), array($item), 'i');
        break;
        case "newsitemsynopsis":
            $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'synopsis', array('newsitem_id'), array($item), 'i');
        break;
        case "newsitemsection":
            $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, 'text', array('newsitemsection_id'), array($item), 'i');
        break;
        case "sitepolicy":
        case "guestbook":
        case "contact":
            $sql = new SQLSelectStatement(SPECIALTEXTS_TABLE, 'text', array('id'), array($elementtype), 's');
        break;
        default:
            return "Text could not be loaded for $elementtype, page $page , item $item.";
    }
    $result = $sql->fetch_value();
    if (empty($result)) {
        return '';
    }
    return stripslashes($result);
}


?>
