<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/includes/objects/forms.php';

class DeleteEditor extends Template {
    function __construct($jsid, $page, $item, $elementtype, $offset = null, $label = 'Delete') {
        parent::__construct($jsid, array(), array('admin/includes/javascript/editors/delete.js'));
        $this->stringvars['javascript'] = $this->getScripts();

        $hiddenvars = array('item' => $item, 'elementtype' => $elementtype);

        if (isset($offset)) {
            $hiddenvars['offset'] = $offset;
        }

        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);

        $this->stringvars['label'] = $label;

        $this->vars['confirm'] = new CheckboxForm($jsid.'deleteconfirmcheckbox', '', 'Confirm', false, 'right');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/delete/delete.tpl');
    }
}

class DeleteEditorConfirm extends Template {

    function __construct($jsid, $item, $elementtype, $offset) {
        // Javascript & hiddenvars are reused from class DeleteEditor
        parent::__construct($jsid);

        switch ($elementtype) {
            case 'articlesection':
                include_once BASEDIR.'/admin/functions/pagecontent/articlepagesmod.php';
                include_once BASEDIR.'/includes/objects/articlepage.php';

                $this->stringvars['title'] = "Deleting section from Page $offset";
                // TODO text is in the HTML but does not show - see only headings
                $this->vars['item'] = new ArticleSection($item, getarticlesectioncontents($item), true);
            break;
            case 'gallery':
                include_once BASEDIR.'/includes/objects/images.php';

                $this->stringvars['title'] = 'Removing image';
                $sql = new SQLSelectStatement(GALLERYITEMS_TABLE, 'image_filename', array('galleryitem_id'), array($item), 'i');
                $this->vars['item'] = new Image($sql->fetch_value(), array('usethumbnail' => true), array('page' => $this->stringvars['page']), true);
            break;
            case 'link':
                include_once BASEDIR.'/admin/functions/pagecontent/linklistpagesmod.php';
                include_once BASEDIR.'/includes/objects/linklistpage.php';

                $this->stringvars['title'] = 'Deleting link';
                // TODO text is in the HTML but does not show - see only headings
                $this->vars['item'] = new LinklistLink($item, getlinkcontents($item), true);
            break;
            case 'newsitem':
                include_once BASEDIR.'/functions/pagecontent/newspages.php';
                include_once BASEDIR.'/includes/objects/newspage.php';

                $this->stringvars['title'] = 'Deleting newsitem';
                // TODO text is in the HTML but does not show - see only headings
                $this->vars['item'] = new Newsitem($item, getnewsitemcontents($item), $offset, true, false);
            break;
            case 'newsitemsection':
                include_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
                include_once BASEDIR.'/functions/pagecontent/newspages.php';
                include_once BASEDIR.'/includes/objects/newspage.php';

                // TODO text is in the HTML but does not show - see only headings
                $this->stringvars['title'] = 'Deleting section from: ' . title2html(getnewsitemcontents($offset)['title']);
                $this->vars['item'] = new NewsitemSection($offset, $item, getnewsitemsectioncontents($item), false, true);
            break;
            case 'banner':
                include_once BASEDIR.'/admin/functions/pagecontent/linklistpagesmod.php';
                include_once BASEDIR.'/includes/objects/page.php';

                $this->stringvars['title'] = 'Deleting banner';
                // TODO text is in the HTML but does not show - see only headings
                $sql = new SQLSelectStatement(BANNERS_TABLE, '*', array('banner_id'), array($item), 'i');
                $this->vars['item'] = new Banner($sql->fetch_row());
            break;
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/delete/deleteconfirm.tpl');
    }
}
