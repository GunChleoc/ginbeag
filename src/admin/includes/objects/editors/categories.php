<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/categories.php';
require_once BASEDIR.'/includes/constants.php';
require_once BASEDIR.'/includes/objects/categories.php';
require_once BASEDIR.'/includes/objects/forms.php';


class CategoriesEditor extends Template {
    function __construct($page, $elementtype, $item = null) {
        parent::__construct($page.'-'.$item.'categories', array(), array('admin/includes/javascript/editors/categories.js'));
        $this->stringvars['javascript'] = $this->getScripts();

        $hiddenvars = array('elementtype' => $elementtype);
        if ($item) {
            $hiddenvars['item'] = $item;
        }
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);

        switch ($elementtype) {
            case 'article':
                $this->vars['categorylist'] = new Categorylist(getcategoriesforarticle($page), CATEGORY_ARTICLE);
                $this->vars['categoryselection'] = new CategorySelectionForm(true, $this->stringvars['jsid'], CATEGORY_ARTICLE);
            break;
            case 'newsitem':
                $this->vars['categorylist'] = new Categorylist(getcategoriesfornewsitem($item), CATEGORY_NEWS);
                $this->vars['categoryselection'] = new CategorySelectionForm(true, $this->stringvars['jsid'], CATEGORY_NEWS);
            // Images have a different frontend
            default:
                $this->stringvars['categorylist'] = "Unknown category type $elementtype";
                $this->stringvars['categoryselection'] = '';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/categories.tpl');
    }
}

?>
