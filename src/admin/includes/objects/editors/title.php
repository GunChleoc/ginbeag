<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/includes/objects/forms.php';
require_once BASEDIR.'/functions/formatting.php';
require_once BASEDIR.'/functions/pages.php';
require_once BASEDIR.'/includes/objects/template.php';

//
//
//
class TitleEditorCollapsed extends Template {

    function __construct($page, $item, $elementtype, $title) {
        parent::__construct($item, array(), array('admin/includes/javascript/editors/title.js'));
        $this->stringvars['javascript'] = $this->getScripts();

        $title = trim($title);

        $hiddenvars = array('item' => $item, 'elementtype' => $elementtype);

        if (empty($title)) {
            $this->stringvars['button'] = '';
            $this->stringvars['header'] = '';
            $hiddenvars['title'] = ' '; //JavaScript barfs at empty element
        } else {
            $this->stringvars['displaytitle'] = title2html($title);
            $hiddenvars['title'] = input2html($title);
        }

        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/title/collapsed.tpl');
    }
}

//
//
//
class TitleEditorExpanded extends Template {

    function __construct($page, $item, $elementtype, $title) {
        parent::__construct($item, array(), array('admin/includes/javascript/editors/title.js'));
        $this->stringvars['javascript'] = $this->getScripts();

        $hiddenvars = array('page' => $page, 'item' => $item, 'elementtype' => $elementtype);
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);

        $this->stringvars['title'] = input2html($title);
        $this->vars['langform'] = new LanguageSelectionForm($this->stringvars['jsid'] . 'title', getpagecontents($this->stringvars['page'])['language'], false, true);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/editors/title/expanded.tpl');
    }
}

?>
