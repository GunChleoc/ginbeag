<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/includes/objects/editors/delete.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';
require_once BASEDIR.'/admin/includes/objects/editors/insert.php';
require_once BASEDIR.'/admin/includes/objects/editors/move.php';
require_once BASEDIR.'/admin/includes/objects/forms.php';
require_once BASEDIR.'/functions/images.php';
require_once BASEDIR.'/functions/pagecontent/gallerypages.php';

// Toggle to show all images or to page
class ShowAllImagesButton extends Template {
    function __construct($isshowall,$noofimages,$imagesperpage) {
        parent::__construct();

        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array(
            'page' => $this->stringvars['page'],
            'noofimages' => $noofimages,
            'showall' => $isshowall ? 1 : 0,
            'action' => 'editcontents'
        ));

        $this->stringvars['value'] =
            $isshowall ?
                "Show all images ($noofimages)" :
                "Show $imagesperpage images per page";
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/gallery/showall.tpl');
    }
}

// One gallery item
class GalleryImageForm extends Template {
    function __construct($page, $imageid, $imagedata, $offset, $highestOffset) {
        parent::__construct($page.'-'.$imageid);

        $this->vars['image'] = new ImageEditor($page, $imageid, 'gallery', $imagedata);

        if (!imageexists($imagedata['image_filename'])) {
            $this->stringvars['error'] = 'Unknown image ' . $imagedata['image_filename'];
        }

        $this->vars['moveeditor'] = new MoveEditor($this->stringvars['jsid'], $this->stringvars['page'], $imageid, 'gallery', $offset, $highestOffset);

        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], $this->stringvars['page'], $imageid, 'gallery');
        $this->vars['deleteeditor'] = new DeleteEditor($this->stringvars['jsid'], $this->stringvars['page'], $imageid, 'gallery');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/gallery/image.tpl');
    }
}

// Edit gallery items
class EditGallery extends Template {

    function __construct($page, $offset, $imagesperpage, $showall) {
        parent::__construct($page);
        $this->stringvars['javascript']=$this->getScripts();

        $noofimages = countgalleryimages($page);
        $images = array();

        if ($showall) {
            $offset = 0;
            $noofdisplayedimages = $noofimages;
            $images = getgalleryimages($page);
        } else {
            $noofdisplayedimages = $imagesperpage;
            $images = getgalleryimageslimit($page, $offset, $noofdisplayedimages);
        }

        $this->vars['showallbutton'] = new ShowAllImagesButton(!$showall, $noofimages, $imagesperpage);
        $this->vars['pagemenu'] = new PageMenu($offset, $noofdisplayedimages, $noofimages);

        if ($noofimages > 0) {
            foreach($images as $id => $image) {
                $this->listvars['imageform'][] = new GalleryImageForm($page, $id, $image, $offset, $this->vars['pagemenu']->highestOffset);
            }
        } else {
            $this->stringvars['imageform'] = 'There are no images in this gallery';
        }

        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], $this->stringvars['page'], null, 'gallery');

        $this->vars['navigationbuttons']= new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageIntroSettingsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/gallery/gallery.tpl');
    }
}

?>
