<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
require_once BASEDIR.'/admin/includes/objects/editors/categories.php';
require_once BASEDIR.'/admin/includes/objects/editors/delete.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';
require_once BASEDIR.'/admin/includes/objects/editors/insert.php';
require_once BASEDIR.'/admin/includes/objects/editors/title.php';
require_once BASEDIR.'/admin/includes/objects/editors/move.php';
require_once BASEDIR.'/admin/includes/objects/editors/text.php';
require_once BASEDIR.'/functions/images.php';
require_once BASEDIR.'/functions/users.php';
require_once BASEDIR.'/includes/includes.php';

/// TODO: Offset für alle Einträge kontrollieren!!!

// Form for a synopsis image
class NewsitemImagePropertiesForm extends Template {

    function __construct($image, $newsitem, $imageid, $offset) {
        parent::__construct();

        $linkparams = array(
            'page' => $this->stringvars['page'],
            'imageid' => $imageid,
            'offset' => $offset,
            'newsitem' => $newsitem,
            'action' => 'editcontents',
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        if (!imageexists($image)) {
            $this->stringvars['error'] = "Unknown image $image";
        }
        $this->vars['image'] = new ImageEditor($this->stringvars['page'], $imageid, 'newsitemsynopsis', array('image_filename' => $image));

        $this->vars['removeconfirmform']= new CheckboxForm('removeconfirm', 'removeconfirm', 'Confirm remove', false, 'right');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/image.tpl');
    }
}


// Edit synopsis
class NewsitemSynopsisForm extends Template {

    function __construct($newsitem) {
        global $offset;
        parent::__construct();

        $linkparams = array(
            'page' => $this->stringvars['page'],
            'offset' => $offset,
            'newsitem' => $newsitem,
            'action' => 'editcontents'
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        $images = getnewsitemsynopsisimages($newsitem);
        if (empty($images)) {
            $this->stringvars['image'] = '';
        } else {
            foreach ($images as $imageid => $imagefilename) {
                $this->listvars['image'][] = new NewsitemImagePropertiesForm($imagefilename, $newsitem, $imageid, $offset);
            }
        }

        $this->vars['synopsis'] = new TextEditor($this->stringvars['page'], $newsitem, 'newsitemsynopsis');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/synopsis.tpl');
    }
}


// Edit section
class NewsitemSectionForm extends Template {

    function __construct($newsitem, $newsitemsection, $contents) {
        parent::__construct($newsitemsection, array(), array('admin/includes/javascript/editnewsitemsection.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('newsitemsection' => $newsitemsection));

        $isquoted = isnewsitemsectionquoted($newsitemsection);
        if ($isquoted) {
            $this->stringvars['quote'] = '';
        } else {
            $this->stringvars['notquote'] = '';
        }

        $this->vars['titleeditor'] = new TitleEditorCollapsed($this->stringvars['page'], $newsitemsection, 'newsitemsection', $contents['sectiontitle']);
        $this->vars['isquotedform'] = new CheckboxForm($this->stringvars['jsid'].'isquoted', 'isquoted', 'Quoted', $isquoted, 'right');

        $this->vars['sectioneditor'] = new TextEditor($this->stringvars['page'], $newsitemsection, 'newsitemsection');
        $this->vars['imageeditor'] = new ImageEditor($this->stringvars['page'], $newsitemsection, 'newsitemsection', $contents);
        $this->vars['moveeditor'] = new MoveEditor($this->stringvars['jsid'], $this->stringvars['page'], $newsitemsection, 'newsitemsection');

        $this->vars['inserteditor'] = new InsertEditor($newsitemsection, $this->stringvars['page'], $newsitemsection, 'newsitemsection', $newsitem);
        $this->vars['deleteeditor'] = new DeleteEditor($newsitemsection, $this->stringvars['page'], $newsitemsection, 'newsitemsection', $newsitem);
    }


    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/section.tpl');
    }
}

// Button to navigate to archive newsitems form
class NewsItemArchiveForm extends Template {

    function __construct() {
        parent::__construct();

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'editcontents'));
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/archive.tpl');
    }
}


// Change displayorder
class NewsItemDisplayOrderForm extends Template {

    function __construct() {
        parent::__construct();

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'editcontents'));

        if (displaynewestnewsitemfirst($this->stringvars['page'])) {
            $this->stringvars['newestfirst'] = 'true';
        } else {
            $this->stringvars['oldestfirst'] = 'true';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/displayorder.tpl');
    }
}


// Search newsitems
class NewsItemSearchForm extends Template {

    function __construct() {
        parent::__construct();

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'editcontents'));
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/search.tpl');
    }
}


// SHow search results
class NewsItemSearchResults extends Template {

    function __construct($searchtitle) {
        parent::__construct();

        $linkparams = array(
            'page' => $this->stringvars['page'],
            'offset' => 0,
            'action' => 'editcontents'
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageIntroSettingsButton());
        $this->vars['searchform'] = new NewsItemSearchForm();
        $this->stringvars['searchtitle'] = title2html($searchtitle);

        $newsitems = searchnewsitemtitles($searchtitle, $this->stringvars['page']);
        $noofnewsitems = count($newsitems);
        if (empty($newsitems)) {
            $this->stringvars['searchresult'] = 'No newsitems found!';
        } else {
            foreach ($newsitems as $newsitem) {
                $offset = getnewsitemoffset($this->stringvars['page'], 1, $newsitem);
                $this->listvars['searchresult'][] = new NewsItemSearchResult($newsitem, $offset);
            }
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/searchresults.tpl');
    }
}

// Search result item
class NewsItemSearchResult extends Template {

    function __construct($newsitem,$offset) {
        parent::__construct();

        $linkparams = array(
            'page' => $this->stringvars['page'],
            'offset' => $offset,
            'action' => 'editcontents'
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        $contents = getnewsitemcontents($newsitem);

        $this->stringvars['sectiontitle'] = title2html($contents['title']);
        $this->stringvars['synopsis'] = text2html($contents['synopsis']);
        $this->stringvars['contributor'] = input2html($contents['contributor']);
        $this->stringvars['source'] = input2html($contents['source']);

        if (!empty($contents['sourcelink'])) {
            $this->stringvars['sourcelink'] = $contents['sourcelink'];
        }
        $this->stringvars['location'] = title2html($contents['location']);
        $this->stringvars['date'] = formatdatetime($contents['date']);
        $this->stringvars['editor'] = title2html(getdisplayname($contents['editor_id']));
        $this->stringvars['copyright'] = makecopyright($contents);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/searchresultitem.tpl');
    }
}


// Archivw newsitems options
class ArchiveNewsItemsForm extends Template {

    function __construct() {
        parent::__construct();

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'editcontents'));

        $oldestdate = getoldestnewsitemdate($this->stringvars['page']);
        $date = getnewestnewsitemdate($this->stringvars['page']);

        $hiddenvars['oldestday'] = $oldestdate['mday'];
        $hiddenvars['oldestmonth'] = $oldestdate['mon'];
        $hiddenvars['oldestyear'] = $oldestdate['year'];
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);
        $this->stringvars['olddate'] = makearticledate($oldestdate['mday'], $oldestdate['mon'], $oldestdate['year']);

        $this->vars['dayform'] = new DayOptionForm($oldestdate['mday']);
        $this->vars['monthform'] = new MonthOptionForm($oldestdate['mon']);
        $this->vars['yearform'] = new YearOptionForm($date['year'], $oldestdate['year'], $date['year']);

        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageContentsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/archiveoptions.tpl');
    }
}

// Fake the publishing date
class FakeTheDateForm extends Template {

    function __construct($newsitem, $contents, $offset) {
        parent::__construct($newsitem);

        $this->stringvars['date'] = formatdatetime($contents['date']);

        $date = @getdate(strtotime($contents['date']));

        $this->vars['dayform'] = new DayOptionForm($date['mday'], false, $this->stringvars['jsid']);
        $this->vars['monthform'] = new MonthOptionForm($date['mon'], false, $this->stringvars['jsid']);
        $this->stringvars['year'] = $date['year'];

        $this->vars['hoursform'] = new NumberOptionForm($date['hours'], 0, 23, false, $this->stringvars['jsid'], 'hours', 'hours');
        $this->vars['minutesform'] = new NumberOptionForm($date['minutes'], 0, 59, false, $this->stringvars['jsid'], 'minutes', 'minutes');
        $this->vars['secondsform'] = new NumberOptionForm($date['seconds'], 0, 59, false, $this->stringvars['jsid'], 'seconds', 'seconds');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/fakethedate.tpl');
    }
}

// Enter source info
class NewsItemSourceForm extends Template {

    function __construct($newsitem, $contents) {
        parent::__construct($newsitem);

        $this->stringvars['contributor'] = input2html($contents['contributor']);
        $this->stringvars['location'] = input2html($contents['location']);
        $this->stringvars['source'] = input2html($contents['source']);
        $this->stringvars['sourcelink'] = $contents['sourcelink'];
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/source.tpl');
    }
}

// todo: code duplication with adminpage???
// page or newsitem
class NewsItemPermissionsForm extends Template {

    function __construct($newsitem, $permissions) {
        parent::__construct($newsitem);

        $this->stringvars['copyright'] = input2html($permissions['copyright']);
        $this->stringvars['image_copyright'] = input2html($permissions['image_copyright']);
        $this->vars['permission_granted'] = new RadioButtonForm($this->stringvars['jsid'], 'permission', PERMISSION_GRANTED, 'Permission granted', $permissions['permission'] == PERMISSION_GRANTED, 'right');
        $this->vars['no_permission'] = new RadioButtonForm($this->stringvars['jsid'], 'permission', NO_PERMISSION, 'No permission', $permissions['permission'] == NO_PERMISSION, 'right');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/permissions.tpl');
    }
}

// Publish / Unpublish newsitem
class NewsItemPublishForm extends Template {

    function __construct($newsitem, $ispublished, $offset) {
        parent::__construct($newsitem);

        $linkparams = array(
            'page' => $this->stringvars['page'],
            'offset' => $offset,
            'newsitem' => $newsitem,
            'action' => 'editcontents'
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        $this->stringvars['previewlink'] = getprojectrootlinkpath() . 'admin/includes/preview.php' . makelinkparameters($linkparams);

        if ($ispublished) {
            $this->stringvars['buttontitle'] = 'Hide Newsitem';
        } else {
            $this->stringvars['buttontitle'] = 'Publish Newsitem';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/publish.tpl');
    }
}

// Edit page for a newsitem
class EditNewsItemForms extends Template {

    function __construct($page, $offset) {
        $noofnewsitems = countnewsitems($page);
        if ($noofnewsitems > 0) {
            $newsitems = getnewsitems($page, 1, $offset);
            $this->stringvars['newsitem'] = $newsitems[0];
        } else {
            $this->stringvars['newsitem'] = '';
        }

        parent::__construct($this->stringvars['newsitem'], array(), array('admin/includes/javascript/editnewsitem.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('newsitem' => $this->stringvars['newsitem']));

        $linkparams = array(
            'page' => $page,
            'offset' => $offset,
            'action' => 'editcontents'
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageIntroSettingsButton());

        $offset = getoffsetforjumppage($noofnewsitems, 1, $offset);
        if (!$offset) {
            $offset = 0;
        }

        $this->vars['pagemenu'] = new PageMenu($offset, 1, $noofnewsitems, array('action' => 'editcontents'));

        $this->vars['insertnewsitemeditor'] = new InsertEditor('newsitem-', $page, $this->stringvars['newsitem'], 'newsitem', null, 'Add Newsitem');
        $this->vars['deletenewsitemeditor'] = new DeleteEditor('newsitem-', $page, $this->stringvars['newsitem'], 'newsitem', $offset, 'Delete This Newsitem');

        if ($noofnewsitems > 0) {
            $this->stringvars['hasnewsitems'] = '';
            $this->vars['jumptopageform'] = new JumpToPageForm('', array('page' => $page, 'action' => 'editcontents'));
            $this->vars['newsitemsearchform'] = new NewsItemSearchForm();

            $contents=getnewsitemcontents($this->stringvars['newsitem']);

            if ($contents['title']) {
                $this->stringvars['newsitemtitle'] = title2html($contents['title']);
            } else {
                $this->stringvars['newsitemtitle'] = 'New Newsitem';
            }

            $this->vars['newsitemtitleform'] = new TitleEditorCollapsed($this->stringvars['page'], $this->stringvars['newsitem'], 'newsitem', $contents['title']);

            $this->stringvars['authorname']=getdisplayname($contents['editor_id']);
            $this->stringvars['title']=input2html($contents['title']);


            $this->vars['newsitempublishform'] = new NewsItemPublishForm($this->stringvars['newsitem'], $contents['ispublished'], $offset);
            $this->vars['newsitempermissionsform'] = new NewsItemPermissionsForm($this->stringvars['newsitem'], $contents);
            $this->vars['newsitemsynopsisform'] = new NewsitemSynopsisForm($this->stringvars['newsitem']);

            // sections
            $sections = getnewsitemsectionswithcontent($this->stringvars['newsitem']);
            if (!empty($sections)) {
                foreach ($sections as $id => $sectioncontents) {
                    $this->listvars['newsitemsectionform'][] = new NewsitemSectionForm($this->stringvars['newsitem'], $id, $sectioncontents);
                }
            } else {
                $this->stringvars['newsitemsectionform'] = '';
            }
            $this->vars['insertsectioneditor'] = new InsertEditor('section-', $page, null, 'newsitemsection', $this->stringvars['newsitem']);

            $this->vars['newsitemsourceform'] = new NewsItemSourceForm($this->stringvars['newsitem'], $contents);

            $this->vars['fakethedateform'] = new FakeTheDateForm($this->stringvars['newsitem'], $contents, $offset);
            $this->vars['categories'] = new CategoriesEditor($page, 'newsitem', $this->stringvars['newsitem']);
        } else {
            $this->stringvars['newsitemtitle'] = 'This page has no items';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/newsitem.tpl');
    }
}


// Archive, RSS etc.
class EditNews extends Template {

    function __construct($page) {
        parent::__construct($page, array(), array('admin/includes/javascript/editnewsitem.js'));
        $this->stringvars['javascript']=$this->getScripts();

        $this->vars['intro'] = new TextEditor($page, 0, 'pageintro');
        $this->vars['imageeditor'] = new ImageEditor($page, 0, 'pageintro', getpageintroimage($page));

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'editcontents'));

        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageContentsButton());
        $this->vars['newsitemarchiveform'] = new NewsItemArchiveForm();
        $this->vars['newsitemdisplayorderform'] = new NewsItemDisplayOrderForm();

        if (hasrssfeed($page)) {
            $this->stringvars['rssbutton'] = '<a href="'.getprojectrootlinkpath().'rss.php'.makelinkparameters(array('page' => $page)).'" target="_blank"><img src="'.getprojectrootlinkpath().'img/rss.gif"></a>';
            $this->stringvars['buttontext'] = 'Disable RSS-Feed';
            $this->stringvars['fieldname'] = 'disablerss';
        } else {
            $this->stringvars['buttontext'] = 'Enable RSS-Feed';
            $this->stringvars['fieldname'] = 'enablerss';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/news/news.tpl');
    }
}

?>
