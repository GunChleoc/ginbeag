<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/includes/objects/editors/delete.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';
require_once BASEDIR.'/admin/includes/objects/editors/insert.php';
require_once BASEDIR.'/admin/includes/objects/editors/move.php';
require_once BASEDIR.'/admin/includes/objects/editors/title.php';
require_once BASEDIR.'/admin/includes/objects/editors/text.php';
require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/pagecontent/linklistpages.php';

// Edit link on a linklist
class EditLinkListLinkForm extends Template {

    function __construct($linkid, $contents) {
        parent::__construct($linkid, array(), array('admin/includes/javascript/editlinklist.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('linkid' => $linkid));

        $this->vars['titleeditor'] = new TitleEditorCollapsed($this->stringvars['page'], $linkid, 'link', $contents['title']);

        $this->stringvars['link'] = $contents['link'];

        $this->vars['imageeditor'] = new ImageEditor($this->stringvars['page'], $linkid, 'link', $contents);
        $this->vars['moveeditor'] = new MoveEditor($this->stringvars['jsid'], $this->stringvars['page'], $linkid, 'link');

        $this->vars['texteditor'] = new TextEditor($this->stringvars['page'], $linkid, 'link');

        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], $this->stringvars['page'], $linkid, 'link');
        $this->vars['deleteeditor'] = new DeleteEditor($this->stringvars['jsid'], $this->stringvars['page'], $linkid, 'link');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/linklist/link.tpl');
    }
}


// Edit linklist
class EditLinklist extends Template {

    function __construct($page) {
        parent::__construct($page);

        $linkparams = array(
            'page' => $this->stringvars['page'],
            'action' => 'editcontents'
        );
        $this->stringvars['actionvars'] = makelinkparameters($linkparams);

        $links = getlinklistitems($page);
        if (empty($links)) {
            $this->stringvars['linkform'] = '';
        } else {
            foreach ($links as $linkid => $contents) {
                $this->listvars['linkform'][]= new EditLinkListLinkForm($linkid, $contents);
            }
        }

        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], $this->stringvars['page'], null, 'link');

        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageIntroSettingsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/linklist/linklist.tpl');
    }
}

?>
