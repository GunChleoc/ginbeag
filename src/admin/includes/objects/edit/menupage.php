<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/pagesmod.php';
require_once BASEDIR.'/admin/includes/objects/forms.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';
require_once BASEDIR.'/admin/includes/objects/editors/move.php';
require_once BASEDIR.'/admin/includes/objects/editors/text.php';
require_once BASEDIR.'/functions/formatting.php';
require_once BASEDIR.'/functions/pagecontent/menupages.php';

// Edit navigator depth for subpages
class EditMenuLevelsForm extends Template {

    function __construct($page, $sistersinnavigator, $pagelevel, $navigatorlevel) {
        parent::__construct($page, array(), array('admin/includes/javascript/editmenu.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars();

        $this->vars['pagelevelsform'] = new NumberOptionForm($pagelevel, 1, 10, false, $this->stringvars['jsid'], 'pagelevels', 'pagelevels');
        $this->vars['navigatorlevelsform'] = new NumberOptionForm($navigatorlevel, 1, 10, false, $this->stringvars['jsid'], 'navlevels', 'navlevels');
        $this->vars['sistersinnavigator'] = new CheckboxForm('sisters', '1', 'List items in same level', $sistersinnavigator, 'right');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/menu/levels.tpl');
    }
}


// Move a subpage up and down
class MenuMovePageForm extends Template {
    function __construct($page, $title, $subpageid) {
        parent::__construct($subpageid);
        $this->stringvars['title'] = title2html($title);
        $this->vars['moveeditor'] = new MoveEditor($subpageid, $page, $subpageid, 'menu');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/menu/movepage.tpl');
    }
}

// Subpage actions
class EditMenuSubpages extends Template {

    function __construct($page) {
        parent::__construct($page);

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $page, 'action' => 'editcontents'));

        $subpages = getallsubpagenavinfos($page);
        if (empty($subpages)) {
            $this->vars['movepageform'] = 'This menu has no subpages.';
        } else {
            foreach ($subpages as $subpage => $navtitle) {
                $this->listvars['movepageform'][] = new MenuMovePageForm($page, $navtitle, $subpage);
            }
        }

        $this->vars['navigationbuttons']= new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageIntroSettingsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/menu/subpages.tpl');
    }
}



// Edit menu contents
class EditMenu extends Template {

    function __construct($page) {
        parent::__construct($page);

        $this->vars['texteditor'] = new TextEditor($page, 0, 'pageintro');
        $this->vars['imageeditor'] = new ImageEditor($page, 0, 'pageintro', getpageintroimage($page));

        $contents=getmenucontents($page);

        $this->vars['menulevelsform'] = new EditMenuLevelsForm($page, $contents['sistersinnavigator'], $contents['displaydepth'], $contents['navigatordepth']);

        $this->vars['navigationbuttons']= new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageContentsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/menu/menu.tpl');
    }
}

?>
