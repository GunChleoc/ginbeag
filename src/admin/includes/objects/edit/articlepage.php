<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/pagecontent/articlepagesmod.php';
require_once BASEDIR.'/admin/functions/pagesmod.php';
require_once BASEDIR.'/admin/includes/objects/editors/categories.php';
require_once BASEDIR.'/admin/includes/objects/editors/delete.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';
require_once BASEDIR.'/admin/includes/objects/editors/insert.php';
require_once BASEDIR.'/admin/includes/objects/editors/move.php';
require_once BASEDIR.'/admin/includes/objects/editors/title.php';
require_once BASEDIR.'/admin/includes/objects/editors/text.php';
require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/pagecontent/articlepages.php';


// Article synopsis, source info & categories
class EditArticle extends Template {

    function __construct($page) {
        parent::__construct($page, array(), array('admin/includes/javascript/editarticle.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars();

        $contents = getarticlepagecontents($page);

        $this->vars['texteditor'] = new TextEditor($page, 0, 'pageintro');
        $this->vars['imageeditor'] = new ImageEditor($page, 0, 'pageintro', getpageintroimage($page));

        $this->stringvars['author'] = input2html($contents['article_author']);
        $this->stringvars['location'] = input2html($contents['location']);
        $this->stringvars['source'] = input2html($contents['source']);
        $this->stringvars['sourcelink'] = $contents['sourcelink'];
        $this->vars['dayform'] = new DayOptionForm($contents['day'], true, $this->stringvars['jsid']);
        $this->vars['monthform'] = new MonthOptionForm($contents['month'], true, $this->stringvars['jsid']);
        $this->stringvars['year'] = $contents['year'];

        $this->vars['tocform'] = new OptionForm(
            $contents['max_toc_level'],
            array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
            array('Off', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'),
            $this->stringvars['jsid'].'toc',
            'Maximum Level '
        );

        $this->vars['categories'] = new CategoriesEditor($page, 'article');
        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageContentsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/article/article.tpl');
    }
}


// Edit articlesection
class ArticleSectionForm extends Template {

    function __construct($articlepage, $articlesection, $contents, $maxtoclevel) {
        parent::__construct($articlesection, array(), array('admin/includes/javascript/editarticlesection.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('articlesection' => $articlesection));

        $this->vars['titleeditor'] = new TitleEditorCollapsed($this->stringvars['page'], $articlesection, 'articlesection', $contents['sectiontitle']);

        if ($maxtoclevel > 0 ) {
            $this->vars['tocform'] = new OptionForm(
                $contents['toc_level'],
                array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
                array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'),
                $this->stringvars['jsid'].'toc',
                "Table of Contents Level (Max used is $maxtoclevel) "
            );
        }

        $this->vars['texteditor'] = new TextEditor($this->stringvars['page'], $articlesection, 'articlesection');

        $this->vars['imageeditor'] = new ImageEditor($this->stringvars['page'], $articlesection, 'articlesection', $contents);
        $this->vars['moveeditor'] = new MoveEditor($this->stringvars['jsid'], $this->stringvars['page'], $articlesection, 'articlesection', $articlepage);

        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], $this->stringvars['page'], $articlesection, 'articlesection', $articlepage);
        $this->vars['deleteeditor'] = new DeleteEditor($this->stringvars['jsid'], $this->stringvars['page'], $articlesection, 'articlesection', $articlepage);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/article/section.tpl');
    }
}

// Edit sections in articlepage
class EditArticlePage extends Template {

    function __construct($articlepage) {
        parent::__construct($articlepage);

        $this->stringvars['actionvars'] = makelinkparameters(array(
            'page' => $this->stringvars['page'],
            'articlepage' => $articlepage,
            'action' => 'editcontents'
        ));

        $this->stringvars['articlepage'] = $articlepage;

        $numberofarticlepages = numberofarticlepages($this->stringvars['page']);
        $this->vars['pagemenu'] = new PageMenu($articlepage - 1, 1, $numberofarticlepages, array('action' => 'editcontents'));

        if ($numberofarticlepages > 1 &&
            $numberofarticlepages == $articlepage &&
            !getlastarticlesection($this->stringvars['page'], $numberofarticlepages)) {
            $this->stringvars['deletepage'] = 'Delete This Page';
        }

        $maxtoclevel = getarticlepagecontents($this->stringvars['page'])['max_toc_level'];

        $articlesections = getarticlesections($this->stringvars['page'], $articlepage);

        foreach ($articlesections as $key => $sectioncontents) {
            $this->listvars['articlesectionform'][] = new ArticleSectionForm($articlepage, $key, $sectioncontents, $maxtoclevel);
        }

        $this->vars['inserteditor'] = new InsertEditor('articlepage-', $this->stringvars['page'], null, 'articlesection', $articlepage);

        $this->vars['navigationbuttons'] = new PageEditNavigationButtons(new GeneralSettingsButton(), new EditPageIntroSettingsButton());
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/edit/article/page.tpl');
    }
}

?>
