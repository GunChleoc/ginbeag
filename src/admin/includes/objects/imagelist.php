<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/imagesfilter.php';
require_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
require_once BASEDIR.'/admin/functions/usersmod.php';
require_once BASEDIR.'/admin/includes/objects/messages.php';
require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/users.php';
require_once BASEDIR.'/includes/objects/categories.php';
require_once BASEDIR.'/includes/objects/forms.php';
require_once BASEDIR.'/includes/objects/images.php';

//
//
//
class AddImageFormCollapsed extends Template {
    function __construct() {
        parent::__construct();
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/addimageformcollapsed.tpl");
    }
}


//
//
//
class AddImageFormExpanded extends Template {
    function __construct() {
        parent::__construct('addimage');

        if (extension_loaded('gd') && function_exists('gd_info')) {
            $this->vars['createthumbnailform'] = new CheckboxForm('dontcreatethumbnail', 'dontcreatethumbnail', 'No automatic thumbnail', false, 'right');
            $this->vars['resizeimageform'] = new CheckboxForm('resizeimage', 'resizeimage', 'Resize Automatically', false, 'right');
            $this->stringvars['defaultimagewidth'] = getproperty('Image Width');
        } else {
            $this->stringvars['nogd']='nogd';
        }

        // set permissions radio buttons
        $this->vars['permission_granted'] = new RadioButtonForm('', 'permission', PERMISSION_GRANTED, 'Permission granted', false, 'right');
        $this->vars['no_permission'] = new RadioButtonForm('', 'permission', NO_PERMISSION, 'No permission', true, 'right');

        // make category selection
        $this->vars['categoryselection'] = new CategorySelectionForm(true, '', CATEGORY_IMAGE, 15, array());

        // display storage path
        $this->stringvars['imagelinkpath'] = getimagelinkpath('', makeimagesubpath());

        $this->stringvars['thumbnailsize'] = getproperty('Thumbnail Size');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/addimageformexpanded.tpl");
    }
}


//
// if deletethumbnail, then delete thumbnail
// else delete image
//
class DeleteImageConfirmForm extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));
        $image = getimage($filename);
        $this->vars['image'] = new AdminImage($filename, $image);
        $this->stringvars['filename'] = $filename;
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/deleteimageconfirmform.tpl");
    }
}

//
// if deletethumbnail, then delete thumbnail
// else delete image
//
class DeleteThumbnailConfirmForm extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));
        $image = getimage($filename);
        $this->vars['image'] = new AdminImage($filename, $image);
        $this->stringvars['filename'] = $filename;
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/deletethumbnailconfirmform.tpl");
    }
}

//
//
//
class EditImageDescriptionFormCollapsed extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));
        $this->stringvars['filename'] = $filename;
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimagedescriptioncollapsed.tpl");
    }
}

//
//
//
class EditImageDescriptionFormExpanded extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));

        $image = getimage($filename);

        $this->stringvars['filename'] = $filename;
        $this->stringvars['caption'] = input2html($image['caption']);
        $this->stringvars['source'] = input2html($image['source']);
        $this->stringvars['sourcelink'] = input2html($image['sourcelink']);
        $this->stringvars['copyright'] = input2html($image['copyright']);
        $this->stringvars['permission'] = $image['permission'];

        $this->vars['permission_granted'] = new RadioButtonForm($this->stringvars['jsid'], 'permission', PERMISSION_GRANTED, 'Permission granted', $this->stringvars['permission'] == PERMISSION_GRANTED, 'right');
        $this->vars['no_permission'] = new RadioButtonForm($this->stringvars['jsid'], 'permission', NO_PERMISSION, 'No permission', $this->stringvars['permission'] == NO_PERMISSION, 'right');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimagedescriptionexpanded.tpl");
    }
}

//
//
//
class EditImageFileOperationsFormCollapsed extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('filename' => $filename));

        $this->stringvars['filename'] = $filename;
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimagefileoperationscollapsed.tpl");
    }
}


//
//
//
class EditImageFileOperationsFormExpanded extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('filename' => $filename));

        $image = getimage($filename);

        if (extension_loaded('gd') && function_exists('gd_info')) {
            $this->stringvars['gdavailable'] = 'true';
            $dimensions = getimagedimensions(getimagepath($filename, $image['path']));

            if ($dimensions['width'] > getproperty('Image Width')) {
                $this->stringvars['defaultimagewidth'] = getproperty('Image Width');
            }
        }

        if (empty($image['thumbnail_filename'])) {
            $this->stringvars['no_thumbnail'] = 'no thumbnail';
        } else {
            $this->stringvars['thumbnail'] = $image['thumbnail_filename'];
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimagefileoperationsexpanded.tpl");
    }
}


//
//
//
class EditImageCategoriesFormCollapsed extends Template {

    function __construct($filename)
    {
        parent::__construct(str_replace('.', '-', $filename));
        $this->stringvars['filename'] = $filename;
        $this->vars['categorylist'] = new Categorylist(getcategoriesforimage($filename), CATEGORY_IMAGE);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimagecategoriescollapsed.tpl");
    }
}


//
//
//
class EditImageCategoriesFormExpanded extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename));
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('item' => $filename, 'elementtype' => 'image'));
        $this->vars['categoryselection'] = new CategorySelectionForm(true, $this->stringvars['jsid'], CATEGORY_IMAGE);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimagecategoriesexpanded.tpl");
    }
}

//
//
//
class EditImageForm extends Template {

    function __construct($filename) {
        parent::__construct(str_replace('.', '-', $filename), array(), array('admin/includes/javascript/editimageform.js'));
        $this->stringvars['javascript'] = $this->getScripts();
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('filename' => $filename));

        $image = getimage($filename);

        $this->stringvars['filename'] = $filename;
        $this->stringvars['filepath'] = getimagelinkpath($filename, $image['path']);

        $this->vars['image'] = new AdminImage($filename, $image, true);

        if (empty($image['thumbnail_filename'])) {
            $this->stringvars['no_thumbnail'] = 'no thumbnail';
        } else {
            $this->stringvars['thumbnail'] = $image['thumbnail_filename'];
        }

        $this->vars['categorylist'] = new Categorylist(getcategoriesforimage($filename), CATEGORY_IMAGE);

        $this->vars['editdescriptionform'] = new EditImageDescriptionFormCollapsed($filename);
        $this->vars['editcategoriesform'] = new EditImageCategoriesFormCollapsed($filename);
        $this->vars['fileoperationsform'] = new EditImageFileOperationsFormCollapsed($filename);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimageform.tpl");
    }
}


//
//
//
class EditImageFormUsage extends Template {

    function __construct($filename) {
        parent::__construct();

        $pages=pagesforimage($filename);
        $newsitems=newsitemsforimage($filename);

        if ((count($pages) > 0) || (count($newsitems) > 0)) {
            if (count($pages) > 0) {
                $this->stringvars['pagelinks'] = '';
                foreach ($pages as $page) {
                    $this->stringvars['pagelinks'] .= '<a href="admin.php'.makelinkparameters(array("page" => $page)).'" target="_blank" class="smalltext">#'.$page.'</a>. ';
                }
            }
            if (count($newsitems) > 0) {
                $this->stringvars['newsitemlinks'] = '';
                foreach ($newsitems as $newsitem) {
                    $newspage = getpagefornewsitem($newsitem);

                    $linkparams = array();
                    $linkparams['page'] = $newspage;
                    $linkparams['offset'] = getnewsitemoffset($newspage, 1, $newsitem);
                    $linkparams['action'] = 'editcontents';
                    $this->stringvars['newsitemlinks'] .= '<a href="edit/newsedit.php'.makelinkparameters($linkparams).'" target="_blank" class="smalltext">#'.$newsitem.' on page #'.$newspage.'</a>. ';
                }
            }
        }
        else {
            $this->stringvars['not_used'] = 'not used';
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/editimageformusage.tpl");
    }
}



//
//
//
class UnknownImageForm extends Template {

    function __construct($filename, $subpath, $advanced, $searchvalues) {
        parent::__construct();

        $hiddenvars = $searchvalues;
        $hiddenvars['advanced'] = $advanced;
        $hiddenvars['filename'] = $filename;
        $hiddenvars['subpath'] = $subpath;

        $this->stringvars['hiddenvarsdeletefile'] = $this->makehiddenvars(array_merge($hiddenvars, array('action' => 'deleteunknownfile')));
        $this->stringvars['hiddenvarsaddfile'] = $this->makehiddenvars(array_merge($hiddenvars, array('action' => 'addunknownfile')));
        $this->stringvars['filename'] = $filename;
        $this->stringvars['image'] = getimagelinkpath($filename, $subpath);
        $this->stringvars['imagepath'] = getproperty('Image Upload Path').$subpath.'/'.$filename;
        $this->stringvars['imageproperties'] = imageproperties(BASEDIR.'/'.$this->stringvars['imagepath']);

        $dimensions=calculateimagedimensions(BASEDIR.'/'.$this->stringvars['imagepath'], false);
        $this->stringvars['width'] = $dimensions['width'];
        $this->stringvars['height'] = $dimensions['height'];

        // set permissions radio buttons
        $this->vars['permission_granted'] = new RadioButtonForm('', 'permission', PERMISSION_GRANTED, 'Permission granted', false, 'right');
        $this->vars['no_permission'] = new RadioButtonForm('', 'permission', NO_PERMISSION, 'No permission', true, 'right');
        $this->vars['categoryselection']= new CategorySelectionForm(true, '', CATEGORY_IMAGE);
        $this->vars['deletefileconfirmform']= new CheckboxForm('deletefileconfirm', 'deletefileconfirm', 'Confirm delete', false, 'right');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/unknownimageform.tpl");
    }
}


//
// displays a list of editimageforms, depending on display options and page number
//
class ImageList extends Template {

    function __construct($offset, $number, $searchvalues) {
        global $ascdesc, $order, $searchterm, $advanced;

        parent::__construct();

        $filteredfilenames = array();
        $filenames = array();
        $message = '';

        if (!empty($searchterm)) {
            $filteredfilenames = getimagesforsimplesearch($searchterm, $order, $ascdesc);
            $searchresult = ImageList::getrangeoffilteredfilenames($filteredfilenames, $offset, $number, " for search term '$searchterm'.");
            foreach ($searchresult['files'] as $filename) {
                $this->listvars['imageform'][] = new EditImageForm($filename);
            }
            $this->stringvars['searchform'] = '';
        } elseif ($advanced === 'Unknown Image Files') {
            $filteredfilenames = getunknownimages();
            $searchresult = ImageList::getrangeoffilteredfilenames($filteredfilenames, $offset, $number, ' in file system that are unknown.');

            foreach ($searchresult['files'] as $item) {
                $this->listvars['imageform'][] = new UnknownImageForm($item['filename'], $item['subpath'], $advanced, $searchvalues);
            }
            $this->vars['searchform'] = new AdvancedSearchForm($searchvalues, $advanced, $offset, $number, $searchresult['count'], $searchresult['message']);
        } else {
            if ($advanced) {
                if ($advanced === 'Clear search') {
                    $filenames = getsomefilenames($offset, $number, $order, $ascdesc);
                    $noofimages = countimages();
                    $searchresult = array('files' => $filenames, 'count' => $noofimages, 'message' => '');
                    $this->vars['searchform'] = new AdvancedSearchForm($searchvalues, $advanced, $offset, $number, 0, '');
                } else {
                    $filteredimages = ImageList::getfilteredimages($searchvalues, $order, $ascdesc);
                    if ($advanced === 'Missing Image Files') {
                        $filteredfilenames = getmissingimages($filteredimages);
                        $searchresult = ImageList::getrangeoffilteredfilenames($filteredfilenames, $offset, $number, ' with missing file.');
                    } elseif ($advanced === 'Missing Thumbnail Files') {
                        $filteredfilenames = getmissingthumbnails($filteredimages);
                        $searchresult = ImageList::getrangeoffilteredfilenames($filteredfilenames, $offset, $number, ' with missing thumbnail file.');
                    } elseif ($advanced === 'Images missing Thumbnails') {
                        $filteredfilenames = getimagesmissingthumbnails($filteredimages);
                        $searchresult = ImageList::getrangeoffilteredfilenames($filteredfilenames, $offset, $number, ' without thumbnail.');
                    } elseif($advanced === 'Unused Images') {
                        $filteredfilenames = array_values(array_filter($filteredimages, 'imageisunused'));
                        $searchresult = ImageList::getrangeoffilteredfilenames($filteredfilenames, $offset, $number, ' that are unused.');
                    } else {
                        $searchresult = ImageList::getrangeoffilteredfilenames($filteredimages, $offset, $number, '.');
                    }
                    $this->vars['searchform'] = new AdvancedSearchForm($searchvalues, $advanced, $offset, $number, $searchresult['count'], $searchresult['message']);
                }
            } else {
                $filenames = getsomefilenames($offset, $number, $order, $ascdesc);
                $noofimages = countimages();
                $searchresult = array('files' => $filenames, 'count' => $noofimages, 'message' => '');
                $this->stringvars['searchform'] = '';
            }

            foreach ($searchresult['files'] as $filename) {
                $this->listvars['imageform'][] = new EditImageForm($filename);
            }
        }

        $this->vars['navigator'] = new ImagesNavigator($offset, $number, $searchresult['count'], $searchvalues);
    }

    static function getrangeoffilteredfilenames($filteredfilenames, $offset, $number, $messagesuffix) {
        $filenames = array();
        $noofimages = count($filteredfilenames);
        if ($noofimages > 1) {
            $message = "Found $noofimages images$messagesuffix";
        } elseif ($noofimages > 0) {
            $message = "Found $noofimages image$messagesuffix";
        } else {
            $message = "Found no images$messagesuffix";
        }

        for ($i = $offset; $i < ($offset + $number) && $i < count($filteredfilenames); $i++) {
            $filenames[$i - $offset] = $filteredfilenames[$i];
        }
        return array('files' => $filenames, 'count' => $noofimages, 'message' => $message);
    }

    static function getfilteredimages($searchvalues, $order, $ascdesc) {
        return getfilteredimages(
            $searchvalues['filename'],
            $searchvalues['caption'],
            $searchvalues['source'], $searchvalues['sourceblank'],
            $searchvalues['sourcelink'],
            $searchvalues['uploader'],
            $searchvalues['copyright'], $searchvalues['copyrightblank'],
            $searchvalues['selectedcat'], $searchvalues['categoriesblank'],
            $order, $ascdesc
        );
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/imagelist.tpl");
    }
}


//
//
//
class AdminImage extends Template {
    function __construct($filename, $imagedata, $showcaption = true) {
        parent::__construct(str_replace('.', '-', $filename));

        $this->stringvars['imagefile'] = $filename;
        $this->stringvars['timestamp'] = strtotime('now');

        if ($showcaption) {
            $this->vars['caption']= new ImageCaption(getimage($filename));
        }

        $filepath = getimagepath($filename, $imagedata['path']);

        $this->stringvars['elementheight'] = getproperty('Thumbnail Size') + 5 * IMAGECAPTION_LINEHEIGHT;

        $fileexists = file_exists($filepath);

        if ($fileexists) {
            $this->stringvars['image'] = 'image';
            $this->stringvars['imagepath'] = getimagelinkpath($filepath, $imagedata['path']) . '?' . time();
            $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('imagelink' => $this->stringvars['imagepath']));

            $imageproperties = imageproperties($filepath, $imagedata['uploaddate'], $imagedata['imageeditor_id']);
            if (strlen($imageproperties) > 0) {
                $this->stringvars['imageproperties'] = $imageproperties;
            }
        } else {
            $this->stringvars['no_image'] = 'no image';
        }

        if (!empty($imagedata['thumbnail_filename'])) {
            $this->stringvars['thumbnail'] = 'thumbnail';
            $this->stringvars['thumbnailpath'] = getimagelinkpath($imagedata['thumbnail_filename'], $imagedata['path']);
            $thumbnailpath = getimagepath($imagedata['thumbnail_filename'], $imagedata['path']);

            if (file_exists($thumbnailpath)) {
                $thumbnailproperties = imageproperties($thumbnailpath);
                if (strlen($thumbnailproperties) > 0) {
                    $this->stringvars['thumbnailproperties'] = $thumbnailproperties;
                }
            }
        } else {
            $dimensions = array();
            if ($fileexists) {
                $dimensions = calculateimagedimensions($filepath, false);
                $this->stringvars['width'] = $dimensions['width'];
                $this->stringvars['height'] = $dimensions['height'];
            }
            $this->stringvars['no_thumbnail'] = 'no thumbnail';

            if (isset($dimensions['resized']) && $dimensions['resized']) {
                $this->stringvars['resized'] = $dimensions['resized'];
            }
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/adminimage.tpl");
    }

}


//
//
//
class AdvancedSearchForm extends Template {

    function __construct($searchvalues, $advanced, $offset, $imagesperpage, $noofimages, $message = '') {
        global $order, $ascdesc;
        parent::__construct();

        if (!empty($message)) {
            $this->stringvars['message'] = $message;
        }

        $this->stringvars['number'] = $imagesperpage;
        $this->stringvars['filename'] = $searchvalues['filename'];
        $this->stringvars['caption'] = $searchvalues['caption'];
        $this->stringvars['source'] = $searchvalues['source'];
        $this->stringvars['sourcelink'] = $searchvalues['sourcelink'];
        $this->stringvars['copyright'] = $searchvalues['copyright'];

        $this->vars['categoryselection']= new CategorySelectionForm(true, '', CATEGORY_IMAGE, 15, $searchvalues['selectedcat'], false, 'selectedcat');

        $this->vars['categoriesblankform'] =  new CheckboxForm('categoriesblank', 1, 'Search for images without categories', $searchvalues['categoriesblank'], 'right');

        $this->vars['sourceblankform'] =  new CheckboxForm('sourceblank', 1, 'Search for images with blank source', $searchvalues['sourceblank'], 'right');

        $this->vars['copyrightblankform'] =  new CheckboxForm('copyrightblank', 1, 'Search for images with blank copyright', $searchvalues['copyrightblank'], 'right');

        $this->vars['usersselectionform'] = new ImageUsersSelectionForm($searchvalues['uploader']);

        // Add nagivation options
        $hiddenvars = array('advanced' => $advanced, 'number' => $imagesperpage, 'order' => $order, 'ascdesc' => $ascdesc);
        $this->stringvars['hiddenvars'] = $this->makehiddenvars($hiddenvars);
    }

    static function getsearchvalues($searchvars) {
        $result= array();

        $fillvars = !isset($searchvars['advanced']) || ($searchvars['advanced'] !== 'Clear search');

        $result['filename'] = $fillvars && isset($searchvars['filename']) ? $searchvars['filename'] : '';
        $result['caption'] = $fillvars && isset($searchvars['caption']) ? $searchvars['caption'] : '';
        $result['source'] = $fillvars && isset($searchvars['source']) ? $searchvars['source'] : '';
        $result['sourcelink'] = $fillvars && isset($searchvars['sourcelink']) ? $searchvars['sourcelink'] : '';
        $result['copyright'] = $fillvars && isset($searchvars['copyright']) ? $searchvars['copyright'] : '';

        if ($fillvars && isset($searchvars['selectedcat']) && is_array($searchvars['selectedcat']) > 0) {
            $result['selectedcat'] = $searchvars['selectedcat'];
        } else {
            $result['selectedcat'] = array();
        }

        $result['categoriesblank'] = $fillvars && isset($searchvars['categoriesblank']);
        $result['sourceblank'] = $fillvars && isset($searchvars['sourceblank']);
        $result['copyrightblank'] = $fillvars && isset($searchvars['copyrightblank']);
        $result['uploader'] = $fillvars && isset($searchvars['uploader']) ? $searchvars['uploader'] : -1;
        return $result;
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/advancedsearchform.tpl");
    }
}


//
//
//
class ImagesNavigator extends Template {

    function __construct($offset, $number, $noofimages, $searchvalues) {
        global $order, $ascdesc, $searchterm, $advanced;
        parent::__construct();

        $this->stringvars['searchterm'] = $searchterm;
        $this->stringvars['searchtoggle'] = $advanced ? 'Simple Search' : 'Advanced Search';

        $this->stringvars['number'] = $number;

        $this->vars['orderselection'] = new ImageOrderSelectionForm($order);
        $this->vars['ascdescselection'] = new AscDescSelectionForm($ascdesc === 'asc');
        $this->vars['pagemenu'] = ImagesNavigator::getpagemenu($offset, $number, $noofimages, $searchterm, $searchvalues);

        if ($advanced) {
            $searchvalues['advanced'] = $advanced;
        }

        $this->stringvars['hiddenvars'] = $this->makehiddenvars($searchvalues);
    }

    // returns a pagemenu with filter parameters
    static function getpagemenu($offset, $imagesperpage, $noofimages, $searchterm, $searchvalues) {
        global $order, $ascdesc, $advanced;

        $params['number'] = $imagesperpage;
        $params['order'] = $order;
        $params['ascdesc'] = $ascdesc;
        $params['searchterm'] = $searchterm;

        if (isset($advanced)) {
            $params['advanced'] = $advanced;

            if (isset($searchvalues['filename']) && strlen($searchvalues['filename']) > 0) {
                $params['filename'] = $searchvalues['filename'];
            }
            if (isset($searchvalues['caption']) && strlen($searchvalues['caption']) > 0) {
                $params['caption'] = $searchvalues['caption'];
            }
            if (isset($searchvalues['source']) && strlen($searchvalues['source']) > 0) {
                $params['source'] = $searchvalues['source'];
            }
            if (isset($searchvalues['uploader']) && strlen($searchvalues['uploader']) > 0) {
                $params['uploader'] = $searchvalues['uploader'];
            }
            if (isset($searchvalues['copyright']) && strlen($searchvalues['copyright']) > 0) {
                $params['copyright'] = $searchvalues['copyright'];
            }
            if (isset($searchvalues['sourceblank']) && strlen($searchvalues['sourceblank']) > 0) {
                $params['sourceblank'] = 1;
            }
            if (isset($searchvalues['copyrightblank']) && strlen($searchvalues['copyrightblank']) > 0) {
                $params['copyrightblank'] = 1;
            }

            if (isset($searchvalues['selectedcat'])) {
                foreach ($searchvalues['selectedcat'] as $selectedcat) {
                    $params["selectedcat%5B%5D"][] = $selectedcat;
                }
            }

            if (isset($searchvalues['categoriesblank']) && strlen($searchvalues['categoriesblank']) > 0) {
                $params['categoriesblank'] = 1;
            }
        }

        return new PageMenu($offset, $imagesperpage, $noofimages, $params);
    }


    // assigns templates
    function createTemplates() {
        $this->addTemplate("admin/imagelist/imagesnavigator.tpl");
    }
}

//
// for search form
//
class ImageOrderSelectionForm extends Template {

    function __construct($order="") {
        parent::__construct();

        $this->stringvars['optionform_name'] = 'order';
        $this->stringvars['optionform_id'] = 'order';
        $this->stringvars['optionform_label'] = 'Order by: ';
        $this->stringvars['jsid'] = '';
        $this->stringvars['optionform_size'] = 1;
        $this->stringvars['optionform_attributes'] = '';

        $this->listvars['option'][]= new OptionFormOption('filename', $order === 'filename', 'Filename');
        $this->listvars['option'][]= new OptionFormOption('caption', $order === 'caption', 'Caption');
        $this->listvars['option'][]= new OptionFormOption('source', $order === 'source', 'Source');
        $this->listvars['option'][]= new OptionFormOption('uploader', $order === 'uploader', 'Uploader');
        $this->listvars['option'][]= new OptionFormOption('uploaddate', $order === 'uploaddate', 'Upload Date');
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("forms/optionform.tpl");
    }
}


//
// for filterform
//
class ImageUsersSelectionForm  extends Template {

    function __construct($selecteduser) {
        parent::__construct();

        $this->stringvars['optionform_name'] = 'uploader';
        $this->stringvars['optionform_id'] = 'uploader';
        $this->stringvars['optionform_label'] = 'Uploader: ';
        $this->listvars['option'][]= new OptionFormOption(0, $selecteduser == 0, 'Anybody');

        $users = getallusernames();
        foreach ($users as $id => $username) {
            $this->listvars['option'][]= new OptionFormOption($id, $selecteduser == $id, input2html($username));
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate("forms/optionform.tpl");
    }
}


//
// for filterform
//
class AdminImagePage  extends Template
{

    function __construct($message, $error, $offset, $number, $searchvalues) {

        parent::__construct(
            'adminimagepage',
            array('includes/javascript/jquery.js', 'admin/includes/javascript/messageboxes.js'),
            array('admin/includes/javascript/addimageform.js', 'admin/includes/javascript/imagesearchtoggle.js')
        );

        $this->stringvars['javascript']=$this->getScripts();

        $this->stringvars['stylesheet']=getCSSPath("main.css");
        $this->stringvars['stylesheetcolors']= getCSSPath("colors.css");
        $this->stringvars['adminstylesheet']=getCSSPath("admin.css");
        $this->stringvars['headertitle']= title2html(getproperty("Site Name")).' - Webpage building';
        $this->stringvars['scriptlinks']=$this->getjspaths();

        $this->stringvars['pageeditinglink']= "admin.php".makelinkparameters(array("page" => $this->stringvars['page']));

        $this->vars['message'] = new AdminMessage($message, $error);
        $this->vars['addimageform'] = new AddImageFormCollapsed();

        $this->vars['imagelist'] = new ImageList($offset, $number, $searchvalues);
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("admin/imagelist/adminimagepage.tpl");
    }
}

//
// gets file dimensions and upload info as a string
//
function imageproperties($filename, $uploaddate="", $uploader="")
{
    $result="";
    if(file_exists($filename)) {
        $dimensions = getimagedimensions($filename);
        $result.=basename($filename);
        $result.='&nbsp;- '.$dimensions["width"].'&nbsp;x&nbsp;'.$dimensions["height"].'&nbsp;pixel';
        $result.='&nbsp;- '.filesize($filename).'&nbsp;bytes.';
        if($uploaddate || $uploader) {
            $result.='<br />Uploaded&nbsp;'.$uploaddate.' by&nbsp;'.getdisplayname($uploader).'.';
        }
    }
    else
    {
        $result.='<p class="highlight">File <i>'.basename($filename).'</i> not found</p>';
    }
    return $result;
}
?>
