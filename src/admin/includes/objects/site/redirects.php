<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/formatting.php';
require_once BASEDIR.'/includes/objects/template.php';

//
//
//
class SiteRedirects extends Template {

    function __construct() {
        parent::__construct();

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'siteredirects'));

        $redirects = getredirects();

        foreach ($redirects as $id => $redirect) {
            $this->listvars['redirect'][] = new SiteRedirect($id, $redirect['pattern'], $redirect['replacement']);
        }
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/site/redirects.tpl');
    }
}


//
//
//
class SiteRedirect extends Template {

    function __construct($id, $pattern, $replacement) {
        parent::__construct();

        $this->stringvars['actionvars'] = makelinkparameters(array('page' => $this->stringvars['page'], 'action' => 'siteredirects'));
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array('id' => $id));
        $this->stringvars['id'] = $id;
        $this->stringvars['pattern'] = input2html($pattern);
        $this->stringvars['replacement'] = input2html($replacement);
    }

    // assigns templates
    function createTemplates() {
        $this->addTemplate('admin/site/redirect.tpl');
    }
}

?>
