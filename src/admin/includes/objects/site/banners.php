<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/includes/objects/editors/delete.php';
require_once BASEDIR.'/admin/includes/objects/editors/insert.php';
require_once BASEDIR.'/admin/includes/objects/editors/move.php';
require_once BASEDIR.'/admin/includes/objects/forms.php';
require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/includes/objects/page.php';

//
//
//
class SiteBannerEditForm extends Template
{

    function __construct($id, $contents)
    {
        parent::__construct($id);

        $linkparams["page"] = $this->stringvars['page'];
        $linkparams["action"] = "sitebanner";
        $linkparams["postaction"] = "editbanner";
        $this->stringvars['editactionvars'] = makelinkparameters($linkparams);
        $this->stringvars['hiddenvars'] = $this->makehiddenvars(array("bannerid" => $id));

        if($contents['header']) {
            $this->stringvars['header']=input2html($contents['header']);
        } else {
            $this->stringvars['header']="";
        }

        $this->vars['banner'] = new Banner($contents);

        if (!isbannercomplete($contents)) {
            $this->stringvars['incomplete']= "incomplete";
        }

        if(strlen($contents['image'])>0) {
            $this->stringvars['image']=$contents['image'];
        } else {
            $this->stringvars['noimage']="true";
        }

        $this->stringvars['description']=input2html($contents['description']);
        $this->stringvars['link']=$contents['link'];
        $this->stringvars['code']=input2html($contents['code']);

        $this->vars['moveeditor'] = new MoveEditor($this->stringvars['jsid'], 0, $id, 'banner');
        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], 0, $id, 'banner');
        $this->vars['deleteeditor'] = new DeleteEditor($this->stringvars['jsid'], 0, $id, 'banner');

        $this->vars['submitrow']= new SubmitRow("bannerproperties", "Submit Banner Changes", true);
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("admin/site/bannereditform.tpl");
    }
}




//
//
//
class SiteBanners extends Template
{

    function __construct()
    {
        parent::__construct();

        $this->stringvars['displayhiddenvars'] = $this->makehiddenvars(array("postaction" => "displaybanners"));

        $linkparams["page"] = $this->stringvars['page'];
        $linkparams["postaction"] = "displaybanners";
        $linkparams["action"] = "sitebanner";
        $this->stringvars['displayactionvars'] = makelinkparameters($linkparams);

        $this->vars['displaybanners_yes'] = new RadioButtonForm($this->stringvars['jsid'], "toggledisplaybanners", 1, "Yes", getproperty('Display Banners'), "right");
        $this->vars['displaybanners_no'] = new RadioButtonForm($this->stringvars['jsid'], "toggledisplaybanners", 0, "No", !getproperty('Display Banners'), "right");

        $banners = getbanners();
        foreach ($banners as $id => $contents) {
            $this->listvars['editform'][] = new SiteBannerEditForm($id, $contents);
        }

        $this->vars['inserteditor'] = new InsertEditor($this->stringvars['jsid'], 0, null, 'banner');
    }

    // assigns templates
    function createTemplates()
    {
        $this->addTemplate("admin/site/banners.tpl");
    }
}

?>
