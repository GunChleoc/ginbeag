// Copy given text to clipboard
function copy(copyme) {
    $(document).ready(
        function () {
            var tempinput = document.createElement('input');
            document.body.appendChild(tempinput)
            tempinput.value = copyme;
            tempinput.select();
            document.execCommand('copy',false);
            tempinput.remove();
            showprogressbox("Copied '" + copyme + "' to clipboard");
    });
}

$(document).ready(
    function () {

        addlistenersDescriptionCollapsed();
        addlistenersFileOperationsCollapsed();
        addlistenersCategoriesCollapsed();
        addImageOverlayListeners();

        function savedescription() {
            var elements = [
                $("#{JSID}savedescriptionbutton"),
                $("#{JSID}resetdescriptionbutton"),
                $("#{JSID}canceldescriptionbutton"),
                $("input[name={JSID}permission]"),
                $("#{JSID}caption"),
                $("#{JSID}source"),
                $("#{JSID}sourcelink"),
                $("#{JSID}copyright"),
                $("#{JSID}permission")
            ];
            disableElements(elements);

            showprogressbox("Saving Description for "+$("#{JSID}filename").val()+" ...");

            postRequest(
                projectroot+"admin/includes/ajax/imagelist/savedescription.php",
                {
                    filename: $("#{JSID}filename").val(),
                    caption: uni2ent($("#{JSID}caption").val()),
                    source: uni2ent($("#{JSID}source").val()),
                    sourcelink: $("#{JSID}sourcelink").val(),
                    copyright: uni2ent($("#{JSID}copyright").val()),
                    permission: $('input[name={JSID}permission]:checked').val()
                },
                function (xml) {
                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/updateimage.php",
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}image"), html);
                            addImageOverlayListeners();
                        },
                        elements
                    ); // post
                    showmessageXML(xml);
                },
                elements
            ); // post
            postRequest(
                projectroot+"admin/includes/ajax/imagelist/collapsedescription.php",
                {
                    filename: $("#{JSID}filename").val()
                },
                function (html) {
                    setHtmlOrError($("#{JSID}editdescriptioncontents"), html);
                    addlistenersDescriptionCollapsed();
                },
                elements
            ); // post
        } // savedescription

        /**
         * listeners for Descriptions
         */
        function addlistenersDescriptionCollapsed() {
            // editdescriptionbutton
            $("#{JSID}editdescriptionbutton").click(
                function () {
                    var elements = [$("#{JSID}editdescriptionbutton")];
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/expanddescription.php",
                        {
                            filename: $("#{JSID}filename").val(),
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}editdescriptioncontents"), html);
                            addlistenersDescriptionExpanded();
                        },
                        elements
                    ); // post
                }
            ); // editdescriptionbutton
        } // addlistenersDescriptionCollapsed

        function addlistenersDescriptionExpanded() {

            // Form actions on enter key
            $(document).on('keypress',function(e) {
                if (e.which == 13) {
                    $('#{JSID}editdescriptionform > fieldset > fieldset > input').each(function () {
                        if ($(this).is(':focus')) {
                            savedescription();
                            return false;
                        }
                    });
                }
            });

            // save description
            $("#{JSID}savedescriptionbutton").click(
                function () {
                    savedescription();
                }
            ); // save description

            // cancel description
            $("#{JSID}canceldescriptionbutton").click(
                function () {
                    var elements = [
                        $("#{JSID}savedescriptionbutton"),
                        $("#{JSID}resetdescriptionbutton"),
                        $("#{JSID}canceldescriptionbutton"),
                        $("input[name={JSID}permission]"),
                        $("#{JSID}caption"),
                        $("#{JSID}source"),
                        $("#{JSID}sourcelink"),
                        $("#{JSID}copyright"),
                        $("#{JSID}permission")
                    ];
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/collapsedescription.php",
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}editdescriptioncontents"), html);
                            addlistenersDescriptionCollapsed();
                        },
                        elements
                    ); // post
                }
            ); // cancel description
        } // addlistenersDescriptionExpanded

        /**
         * listeners for File Operations
         */
        function addlistenersFileOperationsCollapsed() {
            // fileoperationsbutton
            $("#{JSID}fileoperationsbutton").click(
                function () {
                    var elements = [$("#{JSID}fileoperationsbutton")];
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/expandfileoperations.php",
                        {
                            filename: $("#{JSID}filename").val(),
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}fileoperationscontents"), html);
                            addlistenersFileOperationsExpanded();
                        },
                        elements
                    ); // post
                }
            ); // fileoperationsbutton
        } // addlistenersFileOperationsCollapsed


        function addlistenersFileOperationsExpanded() {
            var elements = [
                $("#{JSID}addthumbnailbutton"),
                $("#{JSID}addthumbnailfile"),
                $("#{JSID}cancelfileoperationsbutton"),
                $("#{JSID}createthumbnail"),
                $("#{JSID}deleteimage"),
                $("#{JSID}deletethumbnail"),
                $("#{JSID}replaceimagebutton"),
                $("#{JSID}replaceimagefile"),
                $("#{JSID}replacethumbnailbutton"),
                $("#{JSID}replacethumbnailfile"),
                $("#{JSID}resizeimage")
            ];

            // replaceimagebutton
            addUploadFileListener('{JSID}replaceimagefile', '#{JSID}replaceimagebutton', 'replaceimage.php', elements)
            addUploadFileListener('{JSID}replacethumbnailfile', '#{JSID}replacethumbnailbutton', 'replacethumbnail.php', elements)
            addUploadFileListener('{JSID}addthumbnailfile', '#{JSID}addthumbnailbutton', 'addthumbnail.php', elements)

            addGenerateImageListener("#{JSID}createthumbnail", "createthumbnail.php", "Creating thumbnail for "+$("#{JSID}filename").val()+" ...", elements);
            addGenerateImageListener("#{JSID}resizeimage", "resizeimage.php", "Resizing image "+$("#{JSID}filename").val()+" ...", elements);

            // delete thumbnail
            $("#{JSID}deletethumbnail").click(
                function () {
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/deletethumbnailconfirm.php",
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (html) {
                            $("#overlay-background").css("display","block");
                            setHtmlOrError($("#overlay-contents"), html);

                            $("#{JSID}deletethumbnailconfirm").click(
                                function () {
                                    var confirmelements = [$("#{JSID}deletethumbnailconfirm")];
                                    disableElements(elements);

                                    showprogressbox("Deleting thumbnail for "+$("#{JSID}filename").val()+" ...");

                                    postRequest(
                                        projectroot+"admin/includes/ajax/imagelist/deletethumbnail.php",
                                        {
                                            filename: $("#{JSID}filename").val()
                                        },
                                        function (xml) {
                                            enableElements(confirmelements);
                                            var error = showmessageXML(xml);

                                            if (!error) {
                                                postRequest(
                                                    projectroot+"admin/includes/ajax/imagelist/collapsefileoperations.php",
                                                    {
                                                        filename: $("#{JSID}filename").val()
                                                    },
                                                    function (html) {
                                                        setHtmlOrError($("#{JSID}fileoperationscontents"), html);
                                                        addlistenersFileOperationsCollapsed();
                                                        postRequest(
                                                            projectroot+"admin/includes/ajax/imagelist/updateimage.php",
                                                            {
                                                                filename: $("#{JSID}filename").val()
                                                            },
                                                            function (html) {
                                                                setHtmlOrError($("#{JSID}image"), html);
                                                                addImageOverlayListeners();
                                                            },
                                                            confirmelements
                                                        ); // post
                                                    },
                                                    confirmelements
                                                ); // post
                                                $("#overlay-contents").html('');
                                                $("#overlay-background").css("display","none");
                                            }
                                        },
                                        confirmelements
                                    ); // post
                                }
                            ); // execute delete thumbnail

                            // cancel delete thumbnail
                            $("#{JSID}deletethumbnailcancel").click(
                                function () {
                                    $("#overlay-contents").html('');
                                    $("#overlay-background").css("display","none");
                                    enableElements(elements);
                                }
                            ); // cancel delete thumbnail
                        },
                        elements
                    ); // post
                }
            ); // delete thumbnail


            // delete image
            $("#{JSID}deleteimage").click(
                function () {
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/deleteimageconfirm.php",
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (html) {
                            $("#overlay-background").css("display","block");
                            setHtmlOrError($("#overlay-contents"), html);

                            $("#{JSID}deleteimageconfirm").click(
                                function () {
                                    var confirmelements = [$("#{JSID}deleteimageconfirm")];
                                    disableElements(elements);

                                    showprogressbox("Deleting image "+$("#{JSID}filename").val()+" ...");

                                    postRequest(
                                        projectroot+"admin/includes/ajax/imagelist/deleteimage.php",
                                        {
                                            filename: $("#{JSID}filename").val()
                                        },
                                        function (xml) {
                                            enableElements(confirmelements);
                                            var error = showmessageXML(xml);

                                            if (!error) {
                                                $("#{JSID}imageform").remove();
                                                $("#overlay-contents").html('');
                                                $("#overlay-background").css("display","none");
                                            }
                                        },
                                        confirmelements
                                    ); // post
                                }
                            ); // execute delete image

                            // cancel delete image
                            $("#{JSID}deleteimagecancel").click(
                                function () {
                                    $("#overlay-contents").html('');
                                    $("#overlay-background").css("display","none");
                                    enableElements(elements);
                                }
                            ); // cancel delete image
                        },
                        elements
                    ); // post
                }
            ); // delete image

            // cancel file operations
            $("#{JSID}cancelfileoperationsbutton").click(
                function () {
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/collapsefileoperations.php",
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}fileoperationscontents"), html);
                            addlistenersFileOperationsCollapsed();
                        },
                        elements
                    ); // post
                    showmessageXML(xml);
                }
            ); // cancel file operations
        } // addlistenersFileOperationsExpanded

        function addGenerateImageListener(buttonname, scriptname, progessmessage, elements) {
            $(buttonname).click(
                function () {
                    disableElements(elements);
                    showprogressbox(progessmessage);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/" + scriptname,
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (xml) {
                            var error = showmessageXML(xml);

                            enableElements(elements);
                            if (!error) {
                                postRequest(
                                    projectroot+"admin/includes/ajax/imagelist/collapsefileoperations.php",
                                    {
                                        filename: $("#{JSID}filename").val()
                                    },
                                    function (html) {
                                        setHtmlOrError($("#{JSID}fileoperationscontents"), html);
                                        addlistenersFileOperationsCollapsed();
                                        postRequest(
                                            projectroot+"admin/includes/ajax/imagelist/updateimage.php",
                                            {
                                                filename: $("#{JSID}filename").val()
                                            },
                                            function (html) {
                                                setHtmlOrError($("#{JSID}image"), html);
                                                addImageOverlayListeners();
                                            },
                                            elements
                                        ); // post
                                    },
                                    elements
                                ); // post
                            }
                        },
                        elements
                    ); // post
                }
            );
        } // addGenerateImageListener

        function addUploadFileListener(fileinput, buttonname, scriptname, elements) {
            $(buttonname).click(
                function () {
                    disableElements(elements);
                    showprogressbox("Uploading file…");

                    var files = document.getElementById(fileinput).files;

                    if (files.length > 0 ) {
                        var formData = new FormData();
                        formData.append("newname", files[0]);
                        formData.append("filename", $("#{JSID}filename").val());

                        var url = projectroot + "admin/includes/ajax/imagelist/" + scriptname;
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            dataType: 'html',
                            success: function(xml) {
                                var error = showmessageXML(xml);
                                if (!error) {
                                    postRequest(
                                        projectroot+"admin/includes/ajax/imagelist/collapsefileoperations.php",
                                        {
                                            filename: $("#{JSID}filename").val()
                                        },
                                        function (html) {
                                            setHtmlOrError($("#{JSID}fileoperationscontents"), html);
                                            addlistenersFileOperationsCollapsed();
                                            postRequest(
                                                projectroot+"admin/includes/ajax/imagelist/updateimage.php",
                                                {
                                                    filename: $("#{JSID}filename").val()
                                                },
                                                function (html) {
                                                    setHtmlOrError($("#{JSID}image"), html);
                                                    addImageOverlayListeners();
                                                },
                                                elements
                                            ); // post
                                        },
                                        elements
                                    ); // post
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var errormessage='Server Error: ' + XMLHttpRequest.status + ' - ' + XMLHttpRequest.statusText+' - URL: '+url;
                                if (errormessage) {
                                    alert(errormessage);
                                }
                                showmessage(errormessage);
                                enableElements(elements);
                            }
                        });
                    } else {
                        alert("Please select a file");
                        enableElements(elements);
                    }
                }
            );
        } // addUploadFileListener


        /**
         * listeners for Categories
         */
        function addlistenersCategoriesCollapsed()
        {
            // editcategoriesbutton
            $("#{JSID}editcategoriesbutton").click(
                function () {
                    var elements = [$("#{JSID}editcategoriesbutton")];
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/expandcategories.php",
                        {
                            filename: $("#{JSID}filename").val(),
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}editcategoriescontents"), html);
                            addlistenersCategoriesExpanded();
                        },
                        elements
                    ); // post
                }
            ); // editcategoriesbutton
        } // addlistenersCategoriesCollapsed

        function addlistenersCategoriesExpanded() {
            var elements = [
                $("#{JSID}addcatbutton"),
                $("#{JSID}removecatbutton"),
                $("#{JSID}selectedcat")
            ];

            // add categories
            $("#{JSID}addcatbutton").click(
                function () {
                    disableElements(elements);
                    showprogressbox("Saving Categories for "+$("#{JSID}filename").val()+" ...");

                    postRequest(
                        projectroot+"admin/includes/ajax/editors/categories/add.php",
                        {
                            elementtype: $("#{JSID}elementtype").val(),
                            item: $("#{JSID}item").val(),
                            selectedcat: $("#{JSID}selectedcat").val()
                        },
                        function (xml) {
                            postRequest(
                                projectroot+"admin/includes/ajax/editors/categories/update.php",
                                {
                                    elementtype: $("#{JSID}elementtype").val(),
                                    item: $("#{JSID}item").val(),
                                },
                                function (html) {
                                    setHtmlOrError($("#{JSID}categorylist"), html);
                                    enableElements(elements);
                                },
                                elements
                            ); // post
                            showmessageXML(xml);
                        },
                        elements
                    ); // post
                }
            ); // add categories


            // remove categories
            $("#{JSID}removecatbutton").click(
                function () {
                    disableElements(elements);
                    showprogressbox("Saving Categories for "+$("#{JSID}filename").val()+" ...");

                    postRequest(
                        projectroot+"admin/includes/ajax/editors/categories/remove.php",
                        {
                            elementtype: $("#{JSID}elementtype").val(),
                            item: $("#{JSID}item").val(),
                            selectedcat: $("#{JSID}selectedcat").val()
                        },
                        function (xml) {
                            postRequest(
                                projectroot+"admin/includes/ajax/editors/categories/update.php",
                                {
                                    elementtype: $("#{JSID}elementtype").val(),
                                    item: $("#{JSID}item").val(),
                                },
                                function (html) {
                                    setHtmlOrError($("#{JSID}categorylist"), html);
                                    enableElements(elements);
                                },
                                elements
                            ); // post
                            showmessageXML(xml);
                        },
                        elements
                    ); // post
                }
            ); // remove categories

            // cancel categories
            $("#{JSID}cancelcategoriesbutton").click(
                function () {
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/collapsecategories.php",
                        {
                            filename: $("#{JSID}filename").val()
                        },
                        function (html) {
                            setHtmlOrError($("#{JSID}editcategoriescontents"), html);
                            addlistenersCategoriesCollapsed();
                        },
                        elements
                    ); // post
                }
            ); // cancel categories
        } // addlistenersCategoriesExpanded

        function addImageOverlayListeners() {
            // Image overlay
            $("#{JSID}imagefile").click(
                function () {
                    $("#overlay-background").css("display","block");
                    $("#overlay-contents").html('<img id="{JSID}imagemodal" src="' + $("#{JSID}imagelink").val() + '" />');

                    $("#overlay-contents").click(
                        function () {
                            $("#overlay-contents").html('');
                            $("#overlay-background").css("display","none");
                        }
                    );
                    $("#overlay-background").click(
                        function () {
                            $("#overlay-contents").html('');
                            $("#overlay-background").css("display","none");
                        }
                    );
                }
            ); // Image overlay
        } // addImageOverlayListeners

        // get usage
        $("#{JSID}showusagebutton").click(
            function () {
                var elements = [$("#{JSID}showusagebutton")];
                disableElements(elements);

                showprogressbox("Fetching Usage Info for "+$("#{JSID}filename").val()+" ...");

                postRequest(
                    projectroot+"admin/includes/ajax/imagelist/getimageusage.php",
                    {
                        filename: $("#{JSID}filename").val()
                    },
                    function (html) {
                        setHtmlOrError($("#{JSID}usage"), html);
                        $("#{JSID}showusagebutton").val('Update info');
                        showmessage("Updated usage info for "+$("#{JSID}filename").val());
                        enableElements(elements);
                    },
                    elements
                ); // post

            }
        ); // get usage
    }
); // document
