$(document).ready(
    function () {

        function savesource() {
            if($("#{JSID}year").val().length!=4) {
                alert("Please enter a 4-digit year!");
            } else if(!$.isNumeric($("#{JSID}year").val())) {
                alert("The year must be a number!");
            } else {

                var elements = [
                    $("#{JSID}savesourcebutton"),
                    $("#{JSID}savesourcereset"),
                    $("#{JSID}author"),
                    $("#{JSID}location"),
                    $("#{JSID}day"),
                    $("#{JSID}month"),
                    $("#{JSID}year"),
                    $("#{JSID}source"),
                    $("#{JSID}sourcelink"),
                    $("#{JSID}toc")
                ]
                disableElements(elements);

                showprogressbox("Saving Source Info for Article ID: "+$("#{JSID}page").val()+" ...");

                postRequest(
                    projectroot+"admin/includes/ajax/articles/savesource.php",
                    {
                        page: $("#{JSID}page").val(),
                        author: uni2ent($("#{JSID}author").val()),
                        location: uni2ent($("#{JSID}location").val()),
                        day: $("#{JSID}day").val(),
                        month: $("#{JSID}month").val(),
                        year: $("#{JSID}year").val(),
                        source: uni2ent($("#{JSID}source").val()),
                        sourcelink: $("#{JSID}sourcelink").val(),
                        toc: $("#{JSID}toc").val()
                    },
                    function (xml) {
                        enableElements(elements);
                        showmessageXML(xml);
                    },
                    elements
                ); // post
            } // year check
        }

        // Save savesource on enter key
        $(document).on('keypress',function(e) {
            if (e.which == 13) {
                $('#{JSID}sourceform > fieldset > input').each(function () {
                    if ($(this).is(':focus')) {
                        savesource();
                        return false;
                    }
                });
            }
        });

        // save source
        $("#{JSID}savesourcebutton").click(
            function () {
                savesource();
            }
        ); // save source
    }
); // document
