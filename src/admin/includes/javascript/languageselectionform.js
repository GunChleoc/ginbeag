$(document).ready(
    function () {

        /**
         * helper for BBCode
         */
        function insertOpenCloseTag(opentag, closetag, input_element)
        {
            var sourcetext = input_element.val();
            var caretstart = input_element.prop('selectionStart');
            var caretend = input_element.prop('selectionEnd');

            var text = sourcetext.substring(0, caretstart);
            var tag = opentag + sourcetext.substring(caretstart, caretend) + closetag;
            text = text + tag;
            text = text + sourcetext.substring(caretend);
            input_element.val(text);

            // Set caret
            var new_position = caretend + opentag.length;
            if (caretstart != caretend) {
                new_position += closetag.length;
            }
            input_element.focus();
            input_element.prop("selectionStart", new_position);
            input_element.prop("selectionEnd", new_position);
        }

        /* languageselectionform listener */
        $("#{JSID}languageselectionform").children().each(
            function (index) {
                $(this).on(
                    "click", function () {
                        if ($(this).attr("value")!=0) {
                            insertOpenCloseTag("[lang="+$(this).attr("value")+"]", "[/lang]", $("#{JSID}"));
                        }
                    }
                );
            }
        ); // languageselectionform

        /* languagebutton listener */
        $("#{JSID}languagebutton").click(
            function () {
                // Misuse name attribute to get the value to insert, to save us a hidden element.
                insertOpenCloseTag("[lang="+$(this).attr("name")+"]", "[/lang]", $("#{JSID}"));

            }
        ); // languagebutton
    }
); // document
