$(document).ready(
    function () {

        // save source
        $("#{JSID}savetocbutton").click(
            function () {

                var elements = [
                    $("#{JSID}savetocbutton"),
                    $("#{JSID}toc")
                ]
                disableElements(elements);

                showprogressbox("Saving TOC level for Article Section: "+$("#{JSID}articlesection").val()+" ...");

                postRequest(
                    projectroot+"admin/includes/ajax/articles/savetoc.php",
                    {
                        page: $("#{JSID}page").val(),
                        item: $("#{JSID}articlesection").val(),
                        toc: $("#{JSID}toc").val()
                    },
                    function (xml) {
                        enableElements(elements);
                        showmessageXML(xml);
                    },
                    elements
                ); // post
            }
        ); // save source
    }
); // document
