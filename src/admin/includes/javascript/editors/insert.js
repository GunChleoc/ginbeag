$(document).ready(
    function () {

        // Listeners

        $("#{JSID}insert").click(
            function () {
                var elements = [
                    $("#{JSID}insert")
                ];
                disableElements(elements);
                showprogressbox("Inserting item ...");

                postRequest(
                    projectroot+"admin/includes/ajax/editors/insert/insert.php",
                    {
                        page: $("#{JSID}page").val(),
                        item: $("#{JSID}item").val(),
                        elementtype: $("#{JSID}elementtype").val(),
                        offset: $("#{JSID}offset").val(),
                        filename: $("#{JSID}filename").val(),
                    },
                    function (newsection) {

                        var error = showmessageXML(newsection, true);
                        if (parseInt(newsection) != newsection) {
                            error = true;
                            alert("Error inserting item");
                            showmessage("Error inserting item");
                            enableElements(elements);
                        }
                        if (!error) {
                            updateElement(newsection, elements);
                        }
                    },
                    elements
                ); // post
            }
        ); // insert

        function updateElement(newsection, elements) {
            switch ($("#{JSID}elementtype").val()) {
                case 'gallery':
                    location.reload(true);
                break;
                case 'newsitem':
                    window.location.replace(projectroot+"admin/edit/newsedit.php?action=editcontents&page=" + $("#{JSID}page").val());
                break;
                default:
                    postRequest(
                        projectroot+"admin/includes/ajax/editors/insert/update.php",
                        {
                            page: $("#{JSID}page").val(),
                            item: newsection,
                            elementtype: $("#{JSID}elementtype").val(),
                            offset: $("#{JSID}offset").val(),
                        },
                        function (html) {
                            var error = showmessageXML(html, true);
                            if (!error) {
                                if ($("#{JSID}moveitem").attr('name') == 'moveitem') {
                                    $("#{JSID}moveitem").after(html);
                                } else {
                                    // This is our first section
                                    $("#{JSID}insertform").after(html);
                                }
                                showmessage("Inserted item");
                            }
                            enableElements(elements);
                        },
                        elements
                    ); // post
            }
        } // updateElement

    }
); // document
