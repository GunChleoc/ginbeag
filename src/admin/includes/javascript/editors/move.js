$(document).ready(
    function () {

        // Init button states

        if ($("#{JSID}offset").val() > 1) {
            showElements([ $("#{JSID}movepreviouspage") ]);
        }

        updateItemButtonStates('{JSID}');

        // Helper functions

        function isGallery() {
            return $("#{JSID}elementtype").val() === 'gallery';
        }

        function hideElements(elements) {
            disableElements(elements);

            elements.forEach(function (element) {
                element.css({ opacity: 0 });
            });
        }
        function showElements(elements) {
            enableElements(elements);

            elements.forEach(function (element) {
                element.css({ opacity: 1 });
            });
        }

        function updateItemButtonStates(jsid) {
            var isfirst = $('#'+jsid+'moveitem').prev().attr('name') !== 'moveitem';
            if (isGallery()) {
                isfirst &= $("#{JSID}offset").val() == 0;
            }

            var islast = $('#'+jsid+'moveitem').next().attr('name') !== 'moveitem';
            if (isGallery()) {
                islast &= $("#{JSID}offset").val() == $("#{JSID}highest-offset").val();
            }

            if (isfirst && islast && $("#{JSID}offset").prop('id') == undefined) {
                hideElements([ $("#"+jsid+"movepositions"), $("#"+jsid+"movelabel") ]);
            } else {
                showElements([ $("#"+jsid+"movepositions"), $("#"+jsid+"movelabel") ]);
            }
            if (isfirst) {
                hideElements([ $("#"+jsid+"moveup"), $("#"+jsid+"movetop") ]);
            } else {
                showElements([ $("#"+jsid+"moveup"), $("#"+jsid+"movetop") ]);
            }
            if (islast) {
                hideElements([ $("#"+jsid+"movedown"), $("#"+jsid+"movebottom") ]);
            } else {
                showElements([ $("#"+jsid+"movedown"), $("#"+jsid+"movebottom") ]);
            }
        }

        function updateAllButtonStates() {
            $("[name='moveitem']").each(function () {
                var jsid = $(this).prop('id').replace(new RegExp('moveitem$'), '');
                updateItemButtonStates(jsid);
            });
        }

        // Up/down scroll animation
        function scrollToItem() {
            $('#admincontentarea').animate({
                scrollTop:
                    $('#admincontentarea').scrollTop()
                    - $('#admincontentarea').offset().top
                    + $("#{JSID}moveitem").offset().top
            }, 500);
        }

        // Left/right move animation
        function dissappearToPage(direction) {
            var margin = (direction === 'up' || direction === 'top') ? -400 : 400;

            $('#{JSID}moveitem').animate(
                {
                    marginLeft: margin
                },
                100,
                function () {
                    $('#{JSID}moveitem').remove();
                    location.reload(true);
                }
            );
        }

        // Section move implementation

        function move(direction, articlepage) {
            var elements = [
                $("#{JSID}movepositions"),
                $("#{JSID}moveup"),
                $("#{JSID}movetop"),
                $("#{JSID}movedown"),
                $("#{JSID}movebottom")
            ];
            disableElements(elements);
            showprogressbox("Moving item ...");

            postRequest(
                projectroot+"admin/includes/ajax/editors/move/move.php",
                {
                    positions: $("#{JSID}movepositions").val(),
                    direction: direction,
                    page: $("#{JSID}page").val(),
                    item: $("#{JSID}item").val(),
                    elementtype: $("#{JSID}elementtype").val(),
                    articlepage: articlepage
                },
                function (xml) {
                    var error = showmessageXML(xml);
                    if (error) {
                        enableElements(elements);
                    } else {
                        if (articlepage) {
                            // Left/right move animation
                            dissappearToPage(direction);
                        } else {
                            // Shift the item and scroll to it
                            var positions = Number.MAX_VALUE;
                            switch (direction) {
                                case 'up':
                                    positions = $("#{JSID}movepositions").val();
                                case 'top':
                                    for (var i = 0; i < positions; i++) {
                                        var target = $("#{JSID}moveitem").prev();
                                        if (target && target.attr('name') == 'moveitem') {
                                            $("#{JSID}moveitem").insertBefore(target);
                                            scrollToItem();
                                            updateAllButtonStates();
                                        } else {
                                            if (isGallery() && i < positions - 1) {
                                                dissappearToPage(direction);
                                            }
                                            return false;
                                        }
                                    }
                                break;
                                case 'down':
                                    positions = $("#{JSID}movepositions").val();
                                case 'bottom':
                                    for (var i = 0; i < positions; i++) {
                                        var target = $("#{JSID}moveitem").next();
                                        if (target && target.attr('name') == 'moveitem') {
                                            $("#{JSID}moveitem").insertAfter(target);
                                            scrollToItem();
                                            updateAllButtonStates();
                                        } else {
                                            if (isGallery() && i < positions - 1) {
                                                dissappearToPage(direction);
                                            }
                                            return false;
                                        }
                                    }
                                break;
                            }
                        }
                    }
                    showmessageXML(xml);
                },
                elements
            );
        }

        // Listeners

        $("#{JSID}moveup").click(
            function () {
                move('up', null);
            }
        );
        $("#{JSID}movedown").click(
            function () {
                move('down', null);
            }
        );
        $("#{JSID}movetop").click(
            function () {
                move('top', null);
            }
        );
        $("#{JSID}movebottom").click(
            function () {
                move('bottom', null);
            }
        );
        $("#{JSID}movepreviouspage").click(
            function () {
                move('up', $("#{JSID}offset").val());
            }
        );
        $("#{JSID}movenextpage").click(
            function () {
                move('down', $("#{JSID}offset").val());
            }
        );
    }
); // document
