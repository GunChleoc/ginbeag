$(document).ready(
    function () {

        function update_categories() {
            postRequest(
                projectroot+"admin/includes/ajax/editors/categories/update.php",
                {
                    elementtype: $("#{JSID}elementtype").val(),
                    item: $("#{JSID}item").val(),
                    page: $("#{JSID}page").val()
                },
                function (html) {
                    setHtmlOrError($("#{JSID}categorylist"), html);
                },
                {}
            ); // post
        }

        // add categories
        $("#{JSID}addcatbutton").click(
            function () {

                var elements = [
                    $("#{JSID}addcatbutton"),
                    $("#{JSID}removecatbutton"),
                    $("#{JSID}selectedcat")
                ];

                disableElements(elements);

                showprogressbox("Saving Categories ...");

                postRequest(
                    projectroot+"admin/includes/ajax/editors/categories/add.php",
                    {
                        elementtype: $("#{JSID}elementtype").val(),
                        item: $("#{JSID}item").val(),
                        page: $("#{JSID}page").val(),
                        selectedcat: $("#{JSID}selectedcat").val()
                    },
                    function (xml) {
                        showmessageXML(xml);
                        update_categories();
                        enableElements(elements);
                    },
                    elements
                ); // post

            }
        ); // add categories


        // remove categories
        $("#{JSID}removecatbutton").click(
            function () {
                var elements = [
                    $("#{JSID}addcatbutton"),
                    $("#{JSID}removecatbutton"),
                    $("#{JSID}selectedcat")
                ];
                disableElements(elements);

                showprogressbox("Saving Categories ...");

                postRequest(
                    projectroot+"admin/includes/ajax/editors/categories/remove.php",
                    {
                        elementtype: $("#{JSID}elementtype").val(),
                        item: $("#{JSID}item").val(),
                        page: $("#{JSID}page").val(),
                        selectedcat: $("#{JSID}selectedcat").val()
                    },
                    function (xml) {
                        showmessageXML(xml);
                        update_categories();
                        enableElements(elements);
                    },
                    elements
                ); // post

            }
        ); // remove categories

    }
); // document
