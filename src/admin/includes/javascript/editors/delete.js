$(document).ready(
    function () {

        // Listeners

        $("#{JSID}delete").click(
            function () {
                var elements = [
                    $("#{JSID}delete")
                ];
                disableElements(elements);

                if ($("#{JSID}deleteconfirmcheckbox").prop('checked')) {
                    $("#{JSID}deleteconfirmcheckbox").prop('checked', false);
                    deleteItem();
                } else {
                    postRequest(
                        projectroot+"admin/includes/ajax/editors/delete/deleteconfirm.php",
                        {
                            jsid: "{JSID}",
                            item: $("#{JSID}item").val(),
                            elementtype: $("#{JSID}elementtype").val(),
                            offset: $("#{JSID}offset").val(),
                        },
                        function (html) {
                            console.log(html);
                            $("#overlay-background").css("display","block");
                            setHtmlOrError($("#overlay-contents"), html);

                            $("#{JSID}deleteconfirm").click(
                                function () {
                                    deleteItem();
                                    $("#overlay-contents").html('');
                                    $("#overlay-background").css("display","none");
                                }
                            ); // execute delete

                            // cancel delete
                            $("#{JSID}deletecancel").click(
                                function () {
                                    $("#overlay-contents").html('');
                                    $("#overlay-background").css("display","none");
                                    enableElements(elements);
                                }
                            ); // cancel delete
                        },
                        elements
                    ); // deleteconfirm
                }
            }
        ); // delete


        function deleteItem() {
            var elements = [
                $("#{JSID}deleteconfirm"),
                $("#{JSID}deletecancel")
            ];
            disableElements(elements);

            showprogressbox("Deleting item " + $("#{JSID}item").val() + " ...");

            postRequest(
                projectroot+"admin/includes/ajax/editors/delete/delete.php",
                {
                    page: $("#{JSID}page").val(),
                    item: $("#{JSID}item").val(),
                    elementtype: $("#{JSID}elementtype").val(),
                    offset: $("#{JSID}offset").val(),
                },
                function (xml) {
                    var error = showmessageXML(xml);
                    if (!error) {
                        $("#{JSID}moveitem").remove();

                        switch ($("#{JSID}elementtype").val()) {
                            case 'gallery':
                                location.reload(true);
                            break;
                            case 'newsitem':
                                window.location.replace(projectroot+"admin/edit/newsedit.php?action=editcontents&page=" + $("#{JSID}page").val() + "&offset=" + Math.max(0, $("#{JSID}offset").val() - 1));
                            break;
                        }
                    }
                    enableElements(elements);
                },
                elements
            ); // post
        }
    }
); // document
