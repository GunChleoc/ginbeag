/**
 * returns text
 */
function savestatusfailedmessage()
{
    return "Failed to save!";
}


$(document).ready(
    function () {

        // only activate buttons if there is a change
        var filenameisedited=false;
        var layoutisedited=false;

        addListenersCollapsed();

        function addListenersCollapsed() {

            // Expand editor
            $("#{JSID}displayimage").click(
                function () {
                    expandImageEditor();
                }
            );

            $("#{JSID}addimage").click(
                function () {
                    expandImageEditor();
                }
            ); // Expand editor
        }

        function expandImageEditor() {
            var elements = [$("#{JSID}addimage")];

            postRequest(
                projectroot+"admin/includes/ajax/editors/image/toggle.php",
                {
                    item: $("#{JSID}item").val(),
                    elementtype: $("#{JSID}elementtype").val(),
                    page: $("#{JSID}page").val(),
                    toggle: 'expand'
                },
                function (html) {
                    setHtmlOrError($("#{JSID}imageeditor"), html);
                    addListenersExpanded();
                },
                elements
            ); // post
        }

        function collapseImageEditor() {
            var elements = [
                $("#{JSID}imagealigncenter"),
                $("#{JSID}imagealignleft"),
                $("#{JSID}imagealignright"),
                $("#{JSID}imagefilename"),
                $("#{JSID}resetfilename"),
                $("#{JSID}resetlayout"),
                $("#{JSID}submitfilename"),
                $("#{JSID}submitlayout"),
                $("#{JSID}usethumbnail")
            ];

            postRequest(
                projectroot+"admin/includes/ajax/editors/image/toggle.php",
                {
                    item: $("#{JSID}item").val(),
                    elementtype: $("#{JSID}elementtype").val(),
                    page: $("#{JSID}page").val(),
                    toggle: 'collapse'
                },
                function (html) {
                    setHtmlOrError($("#{JSID}imageeditor"), html);
                    addListenersCollapsed();
                },
                elements
            ); // post
        }


        function addListenersExpanded() {
            // Collapse editor
            $("#{JSID}closeimageeditor").click(
                function () {
                    collapseImageEditor();
                }
            ); // Collapse editor

            addlistenersFilename();
            addlistenersLayout();
        }

        /**
         * call when something changes in the text
         */
        function setfilenameisedited()
        {
            filenameisedited=true;
            $("#{JSID}submitfilename").val("Save Changes");
            $("#{JSID}submitfilename").css("font-style","normal");
            enableElements([$("#{JSID}submitfilename"),$("#{JSID}resetfilename")]);
        }

        /**
         * call when changes are reset or saved
         */
        function setfilenameisnotedited()
        {
            filenameisedited=false;
            $("#{JSID}submitfilename").val("To change image, type in the box above");
            $("#{JSID}submitfilename").css("font-style","italic");
            disableElements([$("#{JSID}submitfilename"),$("#{JSID}resetfilename")]);
        }

        /**
         * call when something changes in the text
         */
        function setlayoutisedited()
        {
            layoutisedited=true;
            $("#{JSID}submitlayout").val("Save Changes");
            $("#{JSID}submitlayout").css("font-style","normal");
            enableElements([$("#{JSID}submitlayout"),$("#{JSID}resetlayout")]);
        }

        /**
         * call when changes are reset or saved
         */
        function setlayoutisnotedited()
        {
            layoutisedited=false;
            $("#{JSID}submitlayout").val("To change image layout, select a button above");
            $("#{JSID}submitlayout").css("font-style","italic");
            disableElements([$("#{JSID}submitlayout"),$("#{JSID}resetlayout")]);
        }


        /**
         * listeners for filename pane
         */
        function addlistenersFilename()
        {
            setfilenameisnotedited();
            var elements = new Array();
            elements[0] = $("#{JSID}submitfilename");
            elements[1] = $("#{JSID}resetfilename");

            // watch edit state to activate save button
            $("#{JSID}imagefilename").on(
                "input", function () {
                    setfilenameisedited();
                }
            );

            /* save image filename */
            $("#{JSID}submitfilename").click(
                function () {
                    disableElements(elements);
                    showprogressbox("Saving Image File: "+$("#{JSID}imagefilename").val()+" ... ");

                    postRequest(
                        projectroot+"admin/includes/ajax/editors/image/savefilename.php",
                        {
                            imagefilename: $("#{JSID}imagefilename").val(),
                            page: $("#{JSID}page").val(),
                            item: $("#{JSID}item").val(),
                            elementtype: $("#{JSID}elementtype").val()
                        },
                        function (xml) {
                            var error = showmessageXML(xml);
                            if (error != "1") {
                                setfilenameisnotedited();

                                postRequest(
                                    projectroot+"admin/includes/ajax/editors/image/update.php",
                                    {
                                        page: $("#{JSID}page").val(),
                                        item: $("#{JSID}item").val(),
                                        elementtype: $("#{JSID}elementtype").val()
                                    },
                                    function (html) {
                                        setHtmlOrError($("#{JSID}editorimagepane"), html);
                                    },
                                    elements
                                ); // post updateimage.php

                                if ($("#{JSID}imagefilename").val().trim().length < 1) {
                                    $("#{JSID}editorlayoutpane").html("");
                                } else if (!["link", "gallery", "newsitemsynopsis"].includes($("#{JSID}elementtype").val())) {
                                    postRequest(
                                        projectroot+"admin/includes/ajax/editors/image/showlayout.php",
                                        {
                                            page: $("#{JSID}page").val(),
                                            item: $("#{JSID}item").val(),
                                            elementtype: $("#{JSID}elementtype").val()
                                        },
                                        function (html) {
                                            setHtmlOrError($("#{JSID}editorlayoutpane"), html);
                                            addlistenersLayout();
                                        },
                                        elements
                                    ); // post
                                }

                            } // no error
                        },
                        elements
                    ); // post saveimagefilename.php

                }
            ); // submitfilename



            /* reset edit status */
            $("#{JSID}resetfilename").click(
                function () {
                    setfilenameisnotedited();
                }
            );    // resetbutton

        } // addlistenersfilename




        /**
         * listeners for layout pane
         */
        function addlistenersLayout()
        {
            setlayoutisnotedited();

            var elements = [
                $("#{JSID}submitlayout"),
                $("#{JSID}resetlayout")
            ]

            // watch edit state to activate save button
            $("#{JSID}imagealignleft").on(
                "click", function () {
                    setlayoutisedited();
                }
            );

            $("#{JSID}imagealignright").on(
                "click", function () {
                    setlayoutisedited();
                }
            );

            $("#{JSID}imagealigncenter").on(
                "click", function () {
                    setlayoutisedited();
                }
            );

            // watch edit state to activate save button
            $("#{JSID}usethumbnail").on(
                "change", function () {
                    setlayoutisedited();
                }
            );

            /* save image layout */
            $("#{JSID}submitlayout").click(
                function () {

                    disableElements(elements);

                    showprogressbox("Saving Layout. Alignment: " + $('input[name={JSID}imagealign]:checked').val() +
                        ". Use Thumbnail: " + $('#{JSID}usethumbnail').prop('checked') + " ... ");

                    postRequest(
                        projectroot+"admin/includes/ajax/editors/image/savelayout.php",
                        {
                            imagealign: $('input[name={JSID}imagealign]:checked').val(),
                            usethumbnail: $('#{JSID}usethumbnail').prop('checked'),
                            page: $("#{JSID}page").val(),
                            item: $("#{JSID}item").val(),
                            elementtype: $("#{JSID}elementtype").val()
                        },
                        function (xml) {
                            var element=$(xml).find('message');
                            var error = element.attr("error");
                            if(error !="1") {
                                setlayoutisnotedited();
                            } // no error
                            showmessageXML(xml);
                            enableElements(elements);
                        },
                        elements
                    ); // post

                }
            ); // submitlayout

            /* reset edit status */
            $("#{JSID}resetlayout").click(
                function () {
                    setlayoutisnotedited();
                }
            );    // resetbutton

        } // addlistenerslayout

    }
); // document
