function savetitle() {
    $("#{JSID}titlelanguageselectionform").remove();
    alert("Submit");
}

$(document).ready(
    function () {

        function savetitle() {
            var elements = [
                $("#{JSID}savetitlebutton"),
                $("#{JSID}savetitlereset"),
                $("#{JSID}title")
            ];
            disableElements(elements);

            showprogressbox("Saving Title for Item ID: "+$("#{JSID}item").val()+" ...");

            postRequest(
                projectroot+"admin/includes/ajax/editors/title/save.php",
                {
                    item: $("#{JSID}item").val(),
                    title: uni2ent($("#{JSID}title").val()),
                    elementtype: $("#{JSID}elementtype").val(),
                    page: $("#{JSID}page").val()
                },
                function (xml) {
                    showmessageXML(xml);
                    collapseTitleEditor();
                },
                elements
            ); // post
        }

        // Save title on enter key
        $(document).on('keypress',function(e) {
            if (e.which == 13) {
                if ($("#{JSID}title").is(':focus')) {
                    savetitle();
                }
            }
        });

        // expand title
        $("#{JSID}displaytitle").click(
            function () {
                expandTitleEditor();
            }
        ); // expand title

        // expand title
        $("#{JSID}addtitle").click(
            function () {
                expandTitleEditor();
            }
        ); // expand title

        $("#{JSID}savetitlebutton").click(
            function () {
                savetitle();
            }
        ); // save title

        // cancel edit title
        $("#{JSID}savetitlecancel").click(
            function () {
                var elements = [
                    $("#{JSID}savetitlebutton"),
                    $("#{JSID}savetitlereset"),
                    $("#{JSID}title")
                ];
                disableElements(elements);
                collapseTitleEditor();
            }
        ); // cancel edit title


        function collapseTitleEditor() {
            postRequest(
                projectroot+"admin/includes/ajax/editors/title/collapse.php",
                {
                    item: $("#{JSID}item").val(),
                    elementtype: $("#{JSID}elementtype").val(),
                    page: $("#{JSID}page").val()
                },
                function (html) {
                    setHtmlOrError($("#{JSID}titleform"), html);
                    if ($("#{JSID}elementtype").val() == "newsitem") {
                        postRequest(
                            projectroot+"admin/includes/ajax/editors/title/updatetitle.php",
                            {
                                item: $("#{JSID}item").val(),
                                elementtype: $("#{JSID}elementtype").val()
                            },
                            function (html) {
                                setHtmlOrError($("#{JSID}itemheadertitle"), html);
                            },
                            {}
                        ); // post
                    }
                },
                []
            ); // post
        }

        function expandTitleEditor() {
            postRequest(
                projectroot+"admin/includes/ajax/editors/title/expand.php",
                {
                    item: $("#{JSID}item").val(),
                    title: $("#{JSID}title").val().trim(),
                    elementtype: $("#{JSID}elementtype").val(),
                    page: $("#{JSID}page").val()
                },
                function (html) {
                    setHtmlOrError($("#{JSID}titleform"), html);
                    $("#{JSID}title").focus();
                },
                []
            ); // post
        }

    }
); // document
