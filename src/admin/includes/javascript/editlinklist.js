$(document).ready(
    function () {

        function saveproperties() {
            var elements = [
                $("#{JSID}savepropertiesbutton"),
                $("#{JSID}savepropertiesreset"),
                $("#{JSID}link")
            ];
            disableElements(elements);

            showprogressbox("Saving Properties for Link ID : "+$("#{JSID}linkid").val()+" ...");

            postRequest(
                projectroot+"admin/includes/ajax/linklists/savelinkproperties.php",
                {
                    page: $("#{JSID}page").val(),
                    link: $("#{JSID}link").val(),
                    linkid: $("#{JSID}linkid").val()
                },
                function (xml) {
                    showmessageXML(xml);
                    enableElements(elements);
                },
                elements
            ); // post
        }

        // Form actions on enter key
        $(document).on('keypress',function(e) {
            if (e.which == 13) {
                if ($("#{JSID}link").is(':focus')) {
                    saveproperties();
                }
            }
        });

        // save properties
        $("#{JSID}savepropertiesbutton").click(
            function () {
                saveproperties();
            }
        ); // save properties

    }
); // document
