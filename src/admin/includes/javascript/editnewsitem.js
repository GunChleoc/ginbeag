$(document).ready(
    function () {

        function savesource() {
            var elements = [
                $("#{JSID}savesourcebutton"),
                $("#{JSID}savesourcereset"),
                $("#{JSID}contributor"),
                $("#{JSID}location"),
                $("#{JSID}source"),
                $("#{JSID}sourcelink")
            ];
            disableElements(elements);

            showprogressbox("Saving Source Info for Newsitem ID: "+$("#{JSID}newsitem").val()+" ...");

            postRequest(
                projectroot+"admin/includes/ajax/news/savesource.php",
                {
                    newsitem: $("#{JSID}newsitem").val(),
                    page: $("#{JSID}page").val(),
                    source: uni2ent($("#{JSID}source").val()),
                    sourcelink: $("#{JSID}sourcelink").val(),
                    location: uni2ent($("#{JSID}location").val()),
                    contributor: uni2ent($("#{JSID}contributor").val())
                },
                function (xml) {
                    enableElements(elements);
                    showmessageXML(xml);
                },
                elements
            ); // post
        }

        function savedate() {
            if($("#{JSID}year").val().length!=4) {
                alert("Please enter a 4-digit year!");
            } else if(!$.isNumeric($("#{JSID}year").val())) {
                alert("The year must be a number!");
            } else {
                var elements = [
                    $("#{JSID}savedatebutton"),
                    $("#{JSID}savedatereset"),
                    $("#{JSID}day"),
                    $("#{JSID}month"),
                    $("#{JSID}year"),
                    $("#{JSID}hours"),
                    $("#{JSID}minutes"),
                    $("#{JSID}seconds")
                ];
                disableElements(elements);

                showprogressbox("Saving Date for Newsitem ID: "+$("#{JSID}newsitem").val()+" ...");

                postRequest(
                    projectroot+"admin/includes/ajax/news/savedate.php",
                    {
                        newsitem: $("#{JSID}newsitem").val(),
                        page: $("#{JSID}page").val(),
                        day: $("#{JSID}day").val(),
                        month: $("#{JSID}month").val(),
                        year: $("#{JSID}year").val(),
                        hours: $("#{JSID}hours").val(),
                        minutes: $("#{JSID}minutes").val(),
                        seconds: $("#{JSID}seconds").val()
                    },
                    function (xml) {

                        postRequest(
                            projectroot+"admin/includes/ajax/news/updatedate.php",
                            {
                                newsitem: $("#{JSID}newsitem").val()
                            },
                            function (html) {
                                enableElements(elements);
                                setHtmlOrError($("#{JSID}dateheader"), html);
                            },
                            elements
                        ); // post

                        showmessageXML(xml);
                    },
                    elements
                ); // post
            } // year check
        }

        function savepermissions() {
            var elements = [
                $("#{JSID}savepermissionsbutton"),
                $("#{JSID}savepermissionsreset"),
                $("#{JSID}copyright"),
                $("#{JSID}imagecopyright"),
                $("input[name={JSID}permission]")
            ];
            disableElements(elements);

            showprogressbox("Saving Copyright Info for Newsitem ID: "+$("#{JSID}newsitem").val()+" ...");

            postRequest(
                projectroot+"admin/includes/ajax/news/savepermissions.php",
                {
                    newsitem: $("#{JSID}newsitem").val(),
                    page: $("#{JSID}page").val(),
                    copyright: uni2ent($("#{JSID}copyright").val()),
                    imagecopyright: uni2ent($("#{JSID}imagecopyright").val()),
                    permission: $('input[name={JSID}permission]:checked').val()
                },
                function (xml) {
                    enableElements(elements);
                    showmessageXML(xml);
                },
                elements
            ); // post
        }

        // Form actions on enter key
        $(document).on('keypress',function(e) {
            if (e.which == 13) {
                if ($("#{JSID}year").is(':focus')) {
                    savedate();
                } else {
                    $('#{JSID}sourceform > fieldset > input').each(function () {
                        if ($(this).is(':focus')) {
                            savesource();
                            return false;
                        }
                    });
                    $('#{JSID}permissionsform > fieldset > input').each(function () {
                        if ($(this).is(':focus')) {
                            savepermissions();
                            return false;
                        }
                    });
                }
            }
        });

        // publish/unpublish
        $("#{JSID}publishbutton").click(
            function () {
                var elements = new Array();
                elements[0] = $("#{JSID}publishbutton");
                disableElements(elements);

                var publish = $("#{JSID}publishbutton").val() == "Publish Newsitem";

                if (publish) {
                    showprogressbox("Publishing Newsitem ID: "+$("#{JSID}newsitem").val()+" ...");
                } else {
                    showprogressbox("Hiding Newsitem ID: "+$("#{JSID}newsitem").val()+" ...");
                }

                postRequest(
                    projectroot+"admin/includes/ajax/news/publish.php",
                    {
                        newsitem: $("#{JSID}newsitem").val(),
                        page: $("#{JSID}page").val(),
                        publish: publish ? 1 : 0
                    },
                    function (xml) {
                        showmessageXML(xml);
                        enableElements(elements);
                        if (publish) {
                            $("#{JSID}publishbutton").val('Hide Newsitem');
                        } else {
                            $("#{JSID}publishbutton").val('Publish Newsitem');
                        }
                    },
                    elements
                ); // post
            }
        ); // publish/unpublish

        // save permissions
        $("#{JSID}savepermissionsbutton").click(
            function () {
                savepermissions();
            }
        );

        // save source
        $("#{JSID}savesourcebutton").click(
            function () {
                savesource();
            }
        );

        // save date
        $("#{JSID}savedatebutton").click(
            function () {
                savedate();
            }
        );
    }
); // document
