$(document).ready(
    function () {
        addImageListenersCollapsed();

        function addimage() {
            var elements = [$("#addimage")];
            disableElements(elements);

            var imagefiles = document.getElementById("addimagefile").files;

            if (imagefiles.length > 0) {
                var formData = new FormData();
                formData.append("filename", imagefiles[0]);

                var thumbnailfiles = document.getElementById("addimagethumbnail").files;
                if (thumbnailfiles.length > 0) {
                    formData.append("thumbnail", thumbnailfiles[0]);
                }
                $("#addimagethumbnail").val('');

                // Construct filename for loading new image edit form after upload
                var imagefilename = imagefiles[0]['name'];
                var newfilename = $("#addimagenewname").val();
                formData.append("newname", newfilename);
                if (newfilename) {
                    imagefilename = newfilename + imagefilename.substring(imagefilename.lastIndexOf('.'));
                }
                imagefilename = imagefilename.replace(" ", "_")
                $("#addimagefile").val('');

                formData.append("resizeimage", $("#resizeimage").prop('checked') ? 1 : 0);
                formData.append("dontcreatethumbnail", $("#dontcreatethumbnail").prop('checked'));
                formData.append("caption", $("#addimagecaption").val());
                formData.append("source", $("#addimagesource").val());
                formData.append("sourcelink", $("#addimagesourcelink").val());
                formData.append("copyright", $("#addimagecopyright").val());
                formData.append("permission", $("#permission2").prop('checked') ? 2 : 1);
                formData.append("selectedcat", $("#selectedcat").val());

                var url = projectroot + "admin/includes/ajax/imagelist/addimage.php";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'html',
                    success: function(html) {
                        $("#statusmessage").html(html);
                        var error = showmessageXML(html);
                        enableElements(elements);
                        if (!error) {
                            postRequest(
                                projectroot+"admin/includes/ajax/imagelist/editimageform.php",
                                {
                                    filename: imagefilename
                                },
                                function (html) {
                                    $("#imageeditforms").prepend(html);
                                    enableElements(elements);
                                },
                                elements
                            ); // post
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var errormessage = 'Server Error: ' + XMLHttpRequest.status + ' - ' + XMLHttpRequest.statusText + ' - URL: ' + url;
                        if (errormessage) {
                            alert(errormessage);
                        }
                        showmessage(errormessage);
                        enableElements(elements);
                    }
                });
            } else {
                alert("Please select a file");
                enableElements(elements);
            }
        }

        function addImageListenersCollapsed() {
            $("#addimageformexpand").click(
                function () {
                    var elements = [$("#addimageformexpand")];
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/expandaddimageform.php",
                        {},
                        function (html) {
                            setHtmlOrError($("#addimageformcontainer"), html);
                            addImageListenersExpanded();
                            enableElements(elements);
                        },
                        elements
                    ); // post
                }
            );
        }

        function addImageListenersExpanded() {
            // Form actions on enter key
            $(document).on('keypress',function(e) {
                if (e.which == 13) {
                    $('#addimageform > div > fieldset > input').each(function () {
                        if ($(this).is(':focus')) {
                            addimage();
                            return false;
                        }
                    });
                    $('#addimageform > div > fieldset > fieldset > input').each(function () {
                        if ($(this).is(':focus')) {
                            addimage();
                            return false;
                        }
                    });
                }
            });

            $("#addimage").click(
                function () {
                    addimage();
                }
            );

            $("#addimageformcollapse").click(
                function () {
                    var elements = [$("#addimageformcollapse")];
                    disableElements(elements);

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/collapseaddimageform.php",
                        {},
                        function (html) {
                            setHtmlOrError($("#addimageformcontainer"), html);
                            addImageListenersCollapsed();
                            enableElements(elements);
                        },
                        elements
                    ); // post
                }
            );
        }

    }
); // document
