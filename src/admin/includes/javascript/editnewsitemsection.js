$(document).ready(
    function () {

        // Save whether newitemsection is quoted
        $("#{JSID}isquoted").change(
            function () {
                var elements = [ $("#{JSID}isquoted") ]
                disableElements(elements);

                showprogressbox("Saving Quote Toggle for section: "+$("#{JSID}newsitemsection").val()+" ...");

                var isquoted = $("#{JSID}isquoted").prop("checked") ? 1 : 0;

                postRequest(
                    projectroot+"admin/includes/ajax/news/savequoted.php",
                    {
                        page: $("#{JSID}page").val(),
                        newsitemsection: $("#{JSID}newsitemsection").val(),
                        isquoted: isquoted
                    },
                    function (xml) {
                        showmessageXML(xml);

                        // Ensure the UI matches the state in the database
                        postRequest(
                            projectroot+"admin/includes/ajax/news/updatequoted.php",
                            {
                                newsitemsection: $("#{JSID}newsitemsection").val()
                            },
                            function (xml) {
                                var ischecked = $(xml).find('message').text();

                                $("#{JSID}isquoted").prop("checked", ischecked);

                                if (ischecked) {
                                    $("#{JSID}moveitem").addClass("newsquote");
                                } else {
                                    $("#{JSID}moveitem").removeClass("newsquote");
                                }
                            },
                            {}
                        ); // post

                        enableElements(elements);
                    },
                    elements
                ); // post
            }
        ); // save options

    }
); // document
