$(document).ready(
    function () {

        var togglebutton = $("#searchtoggle");

        togglebutton.click(
            function () {
                var label = togglebutton.attr('value');

                if (label == 'Simple Search') {
                    togglebutton.attr('value', 'Advanced Search');
                    $("#searchform").html('');
                } else {
                    togglebutton.attr('value', 'Simple Search');

                    var searchvars = {
                        // Global
                        advanced: 1,
                        page: $("#page").val(),
                        number: $("#number").val(),
                        order: $("#order").val(),
                        ascdesc: $("#ascdesc").val(),
                    }

                    // Fill hidden vars from any previous search
                    $('#imagesnavigator').find('input:hidden').each(function(index) {
                        searchvars[$(this).attr("name")] = $(this).val();
                    });

                    postRequest(
                        projectroot+"admin/includes/ajax/imagelist/advancedsearch.php",
                        searchvars,
                        function (html) {
                            setHtmlOrError($("#searchform"), html);
                        },
                        {}
                    ); // post
                }
            }
        ); // togglebutton

    }
); // document
