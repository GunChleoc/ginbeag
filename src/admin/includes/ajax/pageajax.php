<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/pagesmod.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';

/**
 * Class for handling messaging in Ajax calls with page lock/edit data update handling.
 */
class PageAjax extends Ajax {

    var $page;

    /**
     * Initialize page number & check that user has valid page lock.
     */
    function __construct($page = null) {
        parent::__construct();
        $this->page = $page;

        // Acquire lock
        if ($this->page) {
            $message = getpagelock($this->page);
            if (!empty($message)) {
                $this->produce_error($message);
            }
        }
    }


    /**
     * Print plain text or XML from message. If there was an error in any database query,
     * prints error XML instead.
     *
     * Update edit data for page.
     *
     * @param string $message  The message to print
     *
     * @param boolean $html    Optional. If this is true, print as is without adding
     *                         any XML information.
     *                         If this is false, print
     *                         <message error="0"><![CDATA[' . $message . ']]></message>
     */
    function produce($message, $html = false) {
        if (!($this->error)) {
            updateeditdata($this->page);
        }
        $this->produce_xml($message, $html);
    }
}
?>
