<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/functions/files.php';
require_once BASEDIR.'/admin/functions/imagesmod.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';

$ajax = new Ajax();

$message = '';
$error = false;

if (!isset($_POST['filename'])) {
     $ajax->produce_error('Error adding thumbnail - no filename');
}

$filename = $_POST['filename'];
$fileskey = 'newname';

if (isset($_FILES[$fileskey]) && isset($_FILES[$fileskey]['name'])) {
    $thumbnail = $_FILES[$fileskey]['name'];

    $pathinfo = pathinfo($filename);
    $imageextension = $pathinfo['extension'];
    $extension = pathinfo($thumbnail, PATHINFO_EXTENSION);
    $extensionerror = checkextension($filename, $thumbnail);
    if (!empty($extensionerror)) {
        $message .= $extensionerror;
        $error = true;
    } else {
        $thumbnail = $pathinfo['filename'] . '_thn.' . $extension;
        $errorcode = uploadfile(getproperty("Image Upload Path").getimagesubpath($filename), $fileskey, $thumbnail);
        if ($errorcode == UPLOAD_ERR_OK) {
            addthumbnail($filename, $thumbnail);
        } else {
            $message .= "<br />Error ".$errorcode.": ".fileerrors($errorcode)." ";
            $error = true;
        }
    }
    unset($_FILES);
} else {
    $ajax->produce_error('Please select a file.');
}

if ($error) {
    $ajax->produce_error("Failed to upload thumbnail. $message");
} else {
    $ajax->produce("Added thumbnail for image $filename");
}
?>
