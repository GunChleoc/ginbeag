<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';
require_once BASEDIR.'/functions/imagefiles.php';

$ajax = new Ajax();

if (!isset($_POST['filename'])) {
    $ajax->produce_error('Error deleting thumbnail - no filename');
}

$filename = $_POST['filename'];
$imagedata = getimage($filename);

if (!isset($imagedata['thumbnail_filename'])) {
    $ajax->produce_error("Thumbnail '$filename' does not exist!");
} else {
    $thumbnail = $imagedata['thumbnail_filename'];
    if (!empty($thumbnail)) {
        $success = deletethumbnail($filename);
        if (!$success) {
            $imagedir = getproperty('Image Upload Path').$imagedata['path'];
            $ajax->produce_error("Failed to delete thumbnail file '$thumbnail' from dir '$imagedir'");
        }
    } else {
        $ajax->produce_error('No thumbnail found!');
    }
}

$ajax->produce("Deleted thumbnail for image $filename");
?>
