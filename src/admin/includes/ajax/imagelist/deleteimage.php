<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';
require_once BASEDIR.'/functions/imagefiles.php';
require_once BASEDIR.'/includes/constants.php';

$ajax = new Ajax();

if (!isset($_POST['filename'])) {
     $ajax->produce_error('Error deleting image - no filename');
}

$message = '';
$error = false;

$filename = $_POST['filename'];

$pages=pagesforimage($filename);
$newsitems=newsitemsforimage($filename);
if (empty($pages) && empty($newsitems)) {
    $imagedata = getimage($filename);
    deletemobilethumbnail($imagedata);

    $imagedir = getproperty('Image Upload Path').$imagedata['path'];

    $thumbnail = $imagedata['thumbnail_filename'];
    if (!empty($thumbnail)) {
        if (!file_exists(BASEDIR."/$imagedir/$thumbnail")) {
            $success = true;
        } else {
            $success = deletefile($imagedir, $thumbnail);
        }

        if ($success) {
            deleteimage($imagedir, $filename);
        } else {
            $ajax->produce_error("Failed to delete thumbnail file '$thumbnail' from dir '$imagedir'");
        }
    } else {
        deleteimage($imagedir, $filename);
    }
} else {
    $message = 'Could not delete image, because it is still used.';
    if (!empty($pages)) {
        $message .= ' Pages:';
        foreach ($pages as $key => $page) {
            $message.=' #'.$page.'.';
        }
    }
    if (!empty($newsitems)) {
        $message.=' Newsitems: ';
        foreach ($newsitems as $key => $newsitem) {
            $message.=' #'.$newsitem.' on page #'.getpagefornewsitem($newsitem).'.';
        }
    }
    $ajax->produce_error($message);
}

$ajax->produce("Deleted image $filename");

//
// Delete any thumbnail files from file system first!!!
//
function deleteimage($imagedir, $filename) {

    deletefile($imagedir, $filename);
    if (file_exists($filename)) {
        $ajax->produce_error("Failed to delete image file '$filename' from dir '$imagedir'");
    }

    $result = true;
    if (imageisunused($filename)) {
        $sql = new SQLDeleteStatement(IMAGECATS_TABLE, array('image_filename'), array($filename), 's');
        $result = $result & $sql->run();
        $sql = new SQLDeleteStatement(IMAGES_TABLE, array('image_filename'), array($filename), 's');
        $result = $result & $sql->run();
    } else {
        $result = false;
    }
    return $result;
}
?>
