<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/functions/categoriesmod.php';
require_once BASEDIR.'/admin/functions/files.php';
require_once BASEDIR.'/admin/functions/imagesmod.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';

$ajax = new Ajax();

$message = '';
$error = false;
$success = false;

if (!isset($_FILES['filename'])) {
     $ajax->produce_error('Error adding image - no filename');
}

$filename = $_FILES['filename']['name'];

if (!$filename) {
    $message = 'Please select an image for upload';
    $error = true;
} else {
    $imageuploadpath = getproperty('Image Upload Path');
    $thumbnail = isset($_FILES['thumbnail']) ? $_FILES['thumbnail']['name'] : false;
    $newname = $_POST['newname'];
    $caption = $_POST['caption'];
    $source = $_POST['source'];
    $sourcelink = $_POST['sourcelink'];
    $copyright = $_POST['copyright'];
    $permission = $_POST['permission'];
    $selectedcats = $_POST['selectedcat'];
    $dontcreatethumbnail = $_POST['dontcreatethumbnail'];
    $resizeimage = $_POST['resizeimage'];

    // Make new path for each month to avoid directory that is too full
    $subpath = makeimagesubpath();

    // create path in file system if necessary and set permissions
    $imagedir = BASEDIR.'/'.$imageuploadpath.$subpath;
    if (!file_exists($imagedir)) {
        mkdir($imagedir, 0757);
    }
    $copyindexsuccess = @copy(BASEDIR.'/'.$imageuploadpath.'/index.html', $imagedir.'/index.html');
    $copyindexsuccess = $copyindexsuccess & @copy(BASEDIR.'/'.$imageuploadpath.'/index.php', $imagedir.'/index.php');
    if (!$copyindexsuccess ) {
        $message .= ' SECURITY WARNING: unable to create index files in '.$imagedir.'. Please use FTP to copy these files from <em>'.BASEDIR.'/'.getproperty('Image Upload Path').'</em> for security reasons!';
        $error = true;
    }

    if (!empty($newname)) {
        $filename = $newname . '.' . pathinfo($filename, PATHINFO_EXTENSION);
    }
    $filename = cleanupfilename($filename);
    $filename = str_replace('_thn.', '.', $filename);

    $pathinfo = pathinfo($filename);
    $extension = $pathinfo['extension'];
    $imagename = $pathinfo['filename'];;

    if (strlen($filename) > 40) {
        $imagename = substr($imagename, 0, 40);
        $filename = $imagename . '.' . $extension;
        if (isset($_POST['filename'])) {
            $_POST['filename'] = $filename;
        }
        if (isset($_GET['filename'])) {
            $_GET['filename'] = $filename;
        }
    }

    if (imageexists($filename)) {
        $message .= ' Image already exists: '.$filename.'. ';
        $error = true;
    } else {
        $errorcode = uploadfile($imageuploadpath.$subpath, 'filename', $filename);
        if ($errorcode == UPLOAD_ERR_OK) {
            $success = true;
        } else {
            $message .= " Error $errorcode: ".fileerrors($errorcode).' ';
            $success = false;
            $error = true;
        }
    }
    if ($success) {
        addimage($filename, $subpath, $caption, $source, $sourcelink, $copyright, $permission);
        if (!empty($selectedcats)) {
            addimagecategories($filename, explode(',', $selectedcats));
        }
        $filename=basename($filename);

        if ($resizeimage) {
            $resizesuccess = resizeimagewidth(BASEDIR.'/'.$imageuploadpath.$subpath, $filename);

            if ($resizesuccess) {
                $message .= " Image '$filename' resized successfully.";
            } else {
                $message .= ' Failed to resize image. ';
                $error = true;
            }
        }
        if (!$dontcreatethumbnail) {
            // Mobile thumbnails are created on the fly, we only create a desktop thumbnail here
            // Don't upscale small images
            if (should_have_thumbnail(array('image_filename' => $filename, 'path' => $subpath), getproperty('Thumbnail Size'))) {
                $thsuccess = createthumbnail(BASEDIR.'/'.$imageuploadpath.$subpath, $filename);

                if($thsuccess) {
                    addthumbnail($filename, $imagename.'_thn.'.$extension);
                    $message .= " Thumbnail for '$filename' created successfully. ";
                } else {
                    $message .= ' Failed to create thumbnail. ';
                    $error = true;
                }
            } else {
                $message .= ' No thumbnail needed. ';
            }
        } elseif ($thumbnail) {
            $newthumbname = $imagename.'_thn'.$extension;

            $errorcode = uploadfile($imageuploadpath.$subpath, 'thumbnail', $newthumbname);
            if ($errorcode == UPLOAD_ERR_OK) {
                $thsuccess = true;
            } else {
                $message .= " Error $errorcode: ".fileerrors($errorcode).' ';
                $thsuccess = false;
                $error = true;
            }

            if($thsuccess) {
                addthumbnail($filename, $newthumbname);
                $message="Thumbnail for '$filename' uploaded successfully.";
            } else {
                $message .= ' Failed to upload thumbnail. ';
                $error = true;
            }
        }
    }
    if ($success) {
        $message .= ' Added Image';
    } else {
        $message .= ' Failed to upload image. ';
        $error = true;
    }
}

if ($error) {
    $ajax->produce_error($message);
} else {
    $ajax->produce($message, true);
}
?>
