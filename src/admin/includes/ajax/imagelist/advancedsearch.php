<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';
require_once BASEDIR.'/admin/includes/objects/imagelist.php';
require_once BASEDIR.'/functions/db.php';

$ajax = new Ajax();

$number = max(0, isset($_POST['number']) ? $_POST['number'] : getproperty('Imagelist Images Per Page'));
$page = isset($_POST['page']) ? max(0, $_POST['page']) : 0;
$offset = isset($_POST['offset']) ? max(0, $_POST['offset']) : 0;
$order = isset($_POST['order']) ? $_POST['order'] : 'uploaddate';
$ascdesc = isset($_POST['ascdesc']) ? $_POST['ascdesc'] : 'desc';

$advanced = $_POST['advanced'];
$searchvalues = AdvancedSearchForm::getsearchvalues($_POST);

$editor = new AdvancedSearchForm($searchvalues, $advanced, $offset, $number, 0, '');
$ajax->produce($editor->toHTML(), true);
?>
