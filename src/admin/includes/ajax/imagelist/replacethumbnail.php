<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/functions/files.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/functions/images.php';

$ajax = new Ajax();

if (!isset($_POST['filename'])) {
     $ajax->produce_error('Error replacing thumbnail - no filename');
}

$message = '';
$error = false;

$filename = $_POST['filename'];
$fileskey = 'newname';

if (isset($_FILES[$fileskey]) && isset($_FILES[$fileskey]['name'])) {
    $thumbnail = $_FILES[$fileskey]['name'];
    $imagedata = getimage($filename);
    $thumbnailfilename = $imagedata['thumbnail_filename'];
    $extensionerror = checkextension($filename, $thumbnailfilename);
    if (!empty($extensionerror)) {
        $message = $extensionerror;
        $error = true;
    } else {
        $errorcode = replacefile(getproperty('Image Upload Path').$imagedata['path'], $fileskey, $thumbnailfilename);
        if ($errorcode !== UPLOAD_ERR_OK) {
            $message = "Error $errorcode: " . fileerrors($errorcode) . ". ";
            $error = true;
        }
    }

    unset($_FILES);

    if ($error) {
        $ajax->produce_error("Failed to replace thumbnail. $message");
    }
} else {
    $ajax->produce_error('Please select a file.');
}

$ajax->produce("Replaced thumbnail for image $filename");
?>
