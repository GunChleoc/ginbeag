<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$ajax = new PageAjax($_POST['page']);

$sql = new SQLUpdateStatement(
    NEWSITEMS_TABLE,
    array('source', 'sourcelink', 'location', 'contributor'), array('newsitem_id'),
    array($_POST['source'], $_POST['sourcelink'], $_POST['location'], $_POST['contributor'], $_POST['newsitem']), 'ssssi'
);

if ($sql->run()) {
  $ajax->produce('Saved Source Info for Newsitem ID: ' . $_POST['newsitem']);
} else {
  $ajax->produce_error('Error Saving Source Info for Newsitem ID: ' . $_POST['newsitem']);
}

?>
