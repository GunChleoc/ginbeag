<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$ajax = new PageAjax($_POST['page']);

$success = fakethedate($_POST['newsitem'], $_POST['day'], $_POST['month'], $_POST['year'], $_POST['hours'], $_POST['minutes'], $_POST['seconds']);

if ($success) {
    $ajax->produce('Saved Date for Newsitem ID: ' . $_POST['newsitem']);
} else {
    $ajax->produce_error('Error Saving Date for Newsitem ID: ' . $_POST['newsitem']);
}

function fakethedate($newsitem, $day, $month, $year, $hours, $minutes, $seconds) {
    if (strlen($day) == 1) {
        $day = "0" . $day;
    }
    if (strlen($month) == 1) {
        $month = "0" . $month;
    }
    if (strlen($hours) == 1) {
        $hours = "0" . $hours;
    }
    if (strlen($minutes) == 1) {
        $minutes = "0" . $minutes;
    }
    if (strlen($seconds) == 1) {
        $seconds = "0" . $seconds;
    }
    if (strlen($year) == 4) {
        $date = SQLStatement::setinteger($year) . "-"
        . SQLStatement::setinteger($month) . "-"
        . SQLStatement::setinteger($day) . " "
        . SQLStatement::setinteger($hours) . ":"
        . SQLStatement::setinteger($minutes) . ":"
        . SQLStatement::setinteger($seconds);
        $sql = new SQLUpdateStatement(
            NEWSITEMS_TABLE,
            array('date'), array('newsitem_id'),
            array($date, $newsitem), 'si'
        );
        return $sql->run();
    }
    return false;
}
?>
