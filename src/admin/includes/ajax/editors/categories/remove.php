<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/functions/categoriesmod.php';

if (isset($_POST['page'])) {
    require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
    $page = $_POST['page'];
    $ajax = new PageAjax($page);
} else {
    include_once BASEDIR.'/admin/includes/ajax/ajax.php';
    $page = 0;
    $ajax = new Ajax($page);
}

$item = isset($_POST['item']) ? $_POST['item'] : 0;
$selectedcats = $_POST['selectedcat'];
$elementtype = $_POST['elementtype'];

switch ($elementtype) {
    case 'article':
        if (removearticlecategories($page, $selectedcats)) {
            $ajax->produce("Removed Categories from Page ID: $page.");
        } else {
            $ajax->produce_error("Error removing Categories from Page ID: $page!");
        }
    break;
    case 'newsitem':
        if (removenewsitemcategories($item, $selectedcats)) {
            $ajax->produce("Removed Categories from Newsitem ID: $item.");
        } else {
            $ajax->produce_error("Error removing Categories from Newsitem ID: $item!");
        }
    break;
    case 'image':
        if (removeimagecategories($item, $selectedcats)) {
            $ajax->produce("Removed categories from $item");
        } else {
            $ajax->produce_error("Error removing categories from $item!");
        }
    break;
    default:
        $ajax->produce_error("Unknown element type $elementtype");
}

?>
