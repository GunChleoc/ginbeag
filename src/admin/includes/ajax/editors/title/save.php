<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$success = false;

$item = $_POST['item'];
$title = trim($_POST['title']);

switch ($_POST['elementtype']) {
    case 'articlesection':
        $sql = new SQLUpdateStatement(
            ARTICLESECTIONS_TABLE,
            array('sectiontitle'), array('articlesection_id'),
            array($title, $item), 'si'
        );
        $success = $sql->run();
        break;
    case 'link':
        $sql = new SQLUpdateStatement(
            LINKS_TABLE,
            array('title'), array('link_id'),
            array($title, $item), 'si'
        );
        $success = $sql->run();
        break;
    case 'newsitemsection':
        $sql = new SQLUpdateStatement(
            NEWSITEMSECTIONS_TABLE,
            array('sectiontitle'), array('newsitemsection_id'),
            array($title, $item), 'si'
        );
        $success = $sql->run();
        break;
    case 'newsitem':
        $sql = new SQLUpdateStatement(
            NEWSITEMS_TABLE,
            array('title'), array('newsitem_id'),
            array($title, $item), 'si'
        );
        $success = $sql->run();
        break;
    default:
        $ajax->produce_error("Unknown element type $elementtype");
}

if ($success) {
    $ajax->produce("Updated Title for item: $item");
} else {
    $ajax->produce_error("Error Updating Title for item: $item");
}
?>
