<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$newitem = -1;

$item = isset($_POST['item']) ? $_POST['item'] : null;
$offset = isset($_POST['offset']) ? $_POST['offset'] : 0;
$elementtype = $_POST['elementtype'];

switch ($elementtype) {
    case 'articlesection':
        if (!$offset) {
            $ajax->produce_error("No articlepage defined for section: $item");
        } else {
            // Get all sections on same articlepage
            $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, 'pagenumber', array('articlesection_id'), array($item), 'i');
            $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, 'articlesection_id', array('article_id', 'pagenumber'), array($page, $sql->fetch_value()), 'ii');
            $sql->set_order(array('position' => 'ASC'));
            $items = $sql->fetch_column();

            $itemposition = make_room_for_item($item, $items, ARTICLESECTIONS_TABLE, 'articlesection_id');

            // Now insert the new element
            if ($itemposition >= 0) {
                $sql = new SQLInsertStatement(
                    ARTICLESECTIONS_TABLE,
                    array('article_id', 'pagenumber', 'position', 'imagealign', 'usethumbnail', 'toc_level'),
                    array($page, $offset, $itemposition, 'left', 1, 1),
                    'iiisii'
                );

                $newitem = $sql->insert();
            }
        }
    break;
    case 'gallery':
        $sql = new SQLSelectStatement(GALLERYITEMS_TABLE, 'galleryitem_id', array('page_id'), array($page), 'i');
        $sql->set_order(array('position' => 'ASC'));
        $items = $sql->fetch_column();

        $itemposition = make_room_for_item($item, $items, GALLERYITEMS_TABLE, 'galleryitem_id');

        // Now insert the new element
        if ($itemposition >= 0) {
            $sql = new SQLInsertStatement(
                GALLERYITEMS_TABLE,
                array('page_id', 'image_filename', 'position'),
                array($page, $_POST['filename'], $itemposition),
                'isi'
            );

            $newitem = $sql->insert();
        }
    break;
    case 'link':
        $sql = new SQLSelectStatement(LINKS_TABLE, 'link_id', array('page_id'), array($page), 'i');
        $sql->set_order(array('position' => 'ASC'));
        $items = $sql->fetch_column();

        $itemposition = make_room_for_item($item, $items, LINKS_TABLE, 'link_id');

        // Now insert the new element
        if ($itemposition >= 0) {
            $sql = new SQLInsertStatement(
                LINKS_TABLE,
                array('page_id', 'position'),
                array($page, $itemposition),
                'ii'
            );

            $newitem = $sql->insert();
        }
    break;
    case 'newsitem':
        include_once BASEDIR.'/admin/functions/sessions.php';
        // Newsitems always get inserted without specifying a position, because sorting is automatic by date
        $sql = new SQLInsertStatement(
            NEWSITEMS_TABLE,
            array('page_id', 'date', 'editor_id', 'permission', 'ispublished', 'usethumbnail'),
            array($page, date(DATETIMEFORMAT, strtotime('now')), getsiduser(), NO_PERMISSION, 0, 1),
            'isiiii'
        );
        $newitem = $sql->insert();
    break;
    case 'newsitemsection':
        if (!$offset) {
            $ajax->produce_error("No newsitem defined for section: $item");
        } else {
            $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, array('newsitemsection_id'), array('newsitem_id'), array($offset), 'i');
            $sql->set_order(array('position' => 'ASC'));
            $items = $sql->fetch_column();

            $itemposition = make_room_for_item($item, $items, NEWSITEMSECTIONS_TABLE, 'newsitemsection_id');

            // Now insert the new element
            if ($itemposition >= 0) {
                $sql = new SQLInsertStatement(
                    NEWSITEMSECTIONS_TABLE,
                    array('newsitem_id', 'position', 'imagealign', 'usethumbnail'),
                    array($offset, $itemposition, 'left', 1),
                    'iisi'
                );
                $newitem = $sql->insert();
            }
        }
    break;
    case 'banner':
        $sql = new SQLSelectStatement(BANNERS_TABLE, 'banner_id');
        $sql->set_order(array('position' => 'ASC'));
        $items = $sql->fetch_column();

        $itemposition = make_room_for_item($item, $items, BANNERS_TABLE, 'banner_id');

        // Now insert the new element
        if ($itemposition >= 0) {
            $sql = new SQLInsertStatement(
                BANNERS_TABLE,
                array('position'),
                array($itemposition),
                'i'
            );

            $newitem = $sql->insert();
        }
    break;
    default:
        $ajax->produce_error("Unknown element type $elementtype");
}

if ($newitem >= 0) {
    $ajax->produce($newitem, true);
} else {
    $ajax->produce_error("Error inserting item after item: $item");
}


// Ensure sequential positions and make gap for new item
function make_room_for_item($item, $items, $table, $key) {

    $itemposition = $item ? array_search($item, $items) + 1 : 0;

    $itemcount = count($items);
    if ($itemcount > 0) {
        $values = array();
        for ($i = 0; $i < $itemposition; $i++) {
            array_push($values, array($i, $items[$i]));
        }
        for ($i = $itemposition; $i < $itemcount; $i++) {
            array_push($values, array($i + 1, $items[$i]));
        }

        $sql = new SQLUpdateStatement(
            $table,
            array('position'), array($key),
            array(), 'ii'
        );
        $sql->set_values($values);

        if ($sql->run()) {
            return $itemposition;
        } else {
            return -1;
        }
    }

    return $itemposition;
}

?>
