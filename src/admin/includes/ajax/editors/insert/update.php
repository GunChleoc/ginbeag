<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';

$ajax = new Ajax();

$item = $_POST['item'];
$offset = isset($_POST['offset']) ? $_POST['offset'] : 0;
$elementtype = $_POST['elementtype'];
switch ($elementtype) {
    case 'articlesection':
        include_once BASEDIR.'/admin/functions/pagecontent/articlepagesmod.php';
        include_once BASEDIR.'/admin/includes/objects/edit/articlepage.php';
        include_once BASEDIR.'/functions/pagecontent/articlepages.php';

        $contents = getarticlesectioncontents($item);
        $maxtoclevel = getarticlepagecontents($_POST['page'])['max_toc_level'];
        $printme = new ArticleSectionForm($offset, $item, $contents, $maxtoclevel);
    break;
    case 'gallery':
        // include_once BASEDIR.'/admin/functions/pagecontent/linklistpagesmod.php';
        include_once BASEDIR.'/admin/includes/objects/edit/gallerypage.php';
        // TODO get imagedata & highestoffset
        // $printme = new GalleryImageForm($page, $item, $imagedata, $offset, $highestOffset);
    break;
    case 'link':
        include_once BASEDIR.'/admin/functions/pagecontent/linklistpagesmod.php';
        include_once BASEDIR.'/admin/includes/objects/edit/linklistpage.php';
        $printme = new EditLinkListLinkForm($item, getlinkcontents($item));
    break;
    case 'newsitem':
        include_once BASEDIR.'/admin/includes/objects/edit/newspage.php';
        $printme = new EditNewsItemForms($_POST['page'], $offset);
    break;
    case 'newsitemsection':
        include_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
        include_once BASEDIR.'/admin/includes/objects/edit/newspage.php';
        $printme = new NewsitemSectionForm($offset, $item, getnewsitemsectioncontents($item));
    break;
    case 'banner':
        include_once BASEDIR.'/functions/banners.php';
        include_once BASEDIR.'/admin/includes/objects/site/banners.php';
        $printme = new SiteBannerEditForm($item, getbannercontents($item));
    break;
    default:
        $ajax->produce_error("Unknown element type $elementtype");
}
$ajax->produce($printme->toHTML(), true);

?>
