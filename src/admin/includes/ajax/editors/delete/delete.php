<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$success = false;

$item = isset($_POST['item']) ? $_POST['item'] : 0;
$offset = isset($_POST['offset']) ? $_POST['offset'] : 0;
$elementtype = $_POST['elementtype'];

switch ($elementtype) {
    case 'articlesection':
        $success = deleteitem(ARTICLESECTIONS_TABLE, 'articlesection_id', $item);
    break;
    case 'gallery':
        $success = deleteitem(GALLERYITEMS_TABLE, 'galleryitem_id', $item);
    break;
    case 'link':
        $success = deleteitem(LINKS_TABLE, 'link_id', $item);
    break;
    case 'newsitem':
        $success = deleteitem(NEWSITEMS_TABLE, 'newsitem_id', $item);
    break;
    case 'newsitemsection':
        $success = deleteitem(NEWSITEMSECTIONS_TABLE, 'newsitemsection_id', $item);
    break;
    case 'banner':
        $success = deleteitem(BANNERS_TABLE, 'banner_id', $item);
    break;
    default:
        $ajax->produce_error("Unknown element type $elementtype");
}

if ($success) {
    $ajax->produce("Deleted item $item");
} else {
    $ajax->produce_error("Error deleting item: $item");
}

function deleteitem($table, $column, $item) {
    $sql = new SQLDeleteStatement($table, array($column), array($item), 'i');
    $sql->run();

    // Delete always returns empty. Try fetch to check whether the item is gone.
    $sql = new SQLSelectStatement($table, $column, array($column), array($item), 'i');
    return empty($sql->fetch_value());
}
?>
