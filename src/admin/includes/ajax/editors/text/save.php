<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$item = $_POST['item'];
$elementtype = $_POST['elementtype'];
$text = $_POST['savetext'];

switch ($elementtype) {
    case 'pageintro':
        $sql = new SQLUpdateStatement(
            PAGES_TABLE,
            array('introtext'), array('page_id'),
            array($text, $page), 'si'
        );
        if ($sql->run()) {
            $ajax->produce('Saved synopsis');
        }
    break;
    case 'articlesection':
        $sql = new SQLUpdateStatement(
            ARTICLESECTIONS_TABLE,
            array('text'), array('articlesection_id'),
            array($text, $item), 'si'
        );
        if ($sql->run()) {
            $ajax->produce('Saved article section');
        }
    break;
    case 'link':
        $sql = new SQLUpdateStatement(
            LINKS_TABLE,
            array('description'), array('link_id'),
            array($text, $item), 'si'
        );
        if ($sql->run()) {
            $ajax->produce('Saved link description');
        }
    break;
    case 'newsitemsynopsis':
        $sql = new SQLUpdateStatement(
            NEWSITEMS_TABLE,
            array('synopsis'), array('newsitem_id'),
            array($text, $item), 'si'
        );
        if ($sql->run()) {
            $ajax->produce('Saved newsitem synopsis');
        }
    break;
    case 'newsitemsection':
        $sql = new SQLUpdateStatement(
            NEWSITEMSECTIONS_TABLE,
            array('text'), array('newsitemsection_id'),
            array($text, $item), 'si'
        );
        if ($sql->run()) {
            $ajax->produce('Saved newsitem section text');
        }
    break;
    case 'sitepolicy':
        $sql = new SQLUpdateStatement(
            SPECIALTEXTS_TABLE,
            array('text'), array('id'),
            array(addslashes($text), 'sitepolicy'), 'ss'
        );
        if ($sql->run()) {
            $ajax->produce('Saved sitepolicy text');
        } else {
            $ajax->produce_error("Error saving $elementtype text.");
        }
    break;
    case 'guestbook':
        $sql = new SQLUpdateStatement(
            SPECIALTEXTS_TABLE,
            array('text'), array('id'),
            array(addslashes($text), 'guestbook'), 'ss'
        );
        if ($sql->run()) {
            $ajax->produce('Saved guestbook intro text');
        }
    break;
    case 'contact':
        $sql = new SQLUpdateStatement(
            SPECIALTEXTS_TABLE,
            array('text'), array('id'),
            array(addslashes($text), 'contact'), 'ss'
        );
        if ($sql->run()) {
            $ajax->produce('Saved contact form intro text');
        }
    break;
    default:
        $ajax->produce_error("Unknown elementtype: $elementtype");
}

// Report any database failures/errors
$ajax->produce_error("Error saving $elementtype text.");
?>
