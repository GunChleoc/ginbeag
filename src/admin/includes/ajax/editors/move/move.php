<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/admin/functions/moveitems.php';
require_once BASEDIR.'/includes/constants.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$success = false;

$elementtype = $_POST['elementtype'];
$item = $_POST['item'];
$positions = trim($_POST['positions']);
$direction = trim($_POST['direction']);
$legaldirections = array('up', 'down', 'top', 'bottom');
$numbereddirections = array('up', 'down');
$movetoarticlepage = isset($_POST['articlepage']) && !empty($_POST['articlepage']);

if (in_array($direction, $numbereddirections) && (!(@is_numeric($positions) && @ctype_digit($positions)) || $positions < 1)) {
    $ajax->produce_error('Please insert a positive number!');
}
if (!in_array($direction, $legaldirections)) {
    $ajax->produce_error("Unknown move direction: $direction");
}

switch ($elementtype) {
    case 'articlesection':
        $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, 'pagenumber', array('articlesection_id'), array($item), 'i');

        if ($movetoarticlepage) {
            // Move section to a different articlepage
            if ($_POST['articlepage'] <= 1 && $direction === 'up') {
                $ajax->produce_error("Error Moving item: $item to previous page - we're already on the first page!");
            }
            $success = movearticlesectiontoarticlepage($page, $item, $sql->fetch_value(), $direction);
        } else {
            // Move section within an articlepage
            $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, 'articlesection_id', array('article_id', 'pagenumber'), array($page, $sql->fetch_value()), 'ii');
            $success = move_item(ARTICLESECTIONS_TABLE, 'articlesection_id', $item, $sql, $positions, $direction);
        }
    break;
    case 'gallery':
        $sql = new SQLSelectStatement(GALLERYITEMS_TABLE, 'page_id', array('galleryitem_id'), array($item), 'i');
        $sql = new SQLSelectStatement(GALLERYITEMS_TABLE, 'galleryitem_id', array('page_id'), array($sql->fetch_value()), 'i');
        $success = move_item(GALLERYITEMS_TABLE, 'galleryitem_id', $item, $sql, $positions, $direction);
    break;
    case 'link':
        $sql = new SQLSelectStatement(LINKS_TABLE, 'page_id', array('link_id'), array($item), 'i');
        $sql = new SQLSelectStatement(LINKS_TABLE, 'link_id', array('page_id'), array($sql->fetch_value()), 'i');
        $success = move_item(LINKS_TABLE, 'link_id', $item, $sql, $positions, $direction);
    break;
    case 'menu':
        include_once BASEDIR.'/admin/functions/pagesmod.php';
        $success = movepage($item, $direction, $positions);
    break;
    case 'newsitemsection':
        $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, 'newsitem_id', array('newsitemsection_id'), array($item), 'i');
        $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, array('newsitemsection_id'), array('newsitem_id'), array($sql->fetch_value()), 'i');
        $success = move_item(NEWSITEMSECTIONS_TABLE, 'newsitemsection_id', $item, $sql, $positions, $direction);
    break;
    case 'page':
        // TODO AJAX for page
    break;
    case 'banner':
        $sql = new SQLSelectStatement(BANNERS_TABLE, 'banner_id');
        $success = move_item(BANNERS_TABLE, 'banner_id', $item, $sql, $positions, $direction);
    break;
    default:
        $ajax->produce_error("Unknown element type $elementtype");
}

if ($success) {
    if ($movetoarticlepage) {
        $ajax->produce("Moved item: $item one page $direction");
    } else if (in_array($direction, $numbereddirections)) {
        $ajax->produce("Moved item: $item $direction $positions position(s)");
    } else {
        $ajax->produce("Moved item: $item to the $direction");
    }
} else {
    $ajax->produce_error("Error Moving item: $item");
}


//
// Move section to a different articlepage
// Returns the articlepage the section will be in after the move
//
function movearticlesectiontoarticlepage($page, $articlesection, $pagenumber, $direction) {
    $success = true;

    switch ($direction) {
        case 'up':
            // Collect information about previous page
            $pagenumber--;
            $sql = new SQLSelectStatement(
                ARTICLESECTIONS_TABLE,
                'articlesection_id',
                array('article_id', 'pagenumber'),
                array($page, $pagenumber),
                'ii');
            $sections = $sql->fetch_column();
            array_push($sections, $articlesection);
        break;
        case 'down':
            $pagenumber++;
            // Increment number of pages in articles table on demand
            $sql = new SQLSelectStatement(ARTICLES_TABLE, 'numberofpages', array('page_id'), array($page), 'i');
            $maxpage = $sql->fetch_value();

            if ($maxpage <= $pagenumber) {
                $sql = new SQLUpdateStatement(
                    ARTICLES_TABLE,
                    array('numberofpages'),
                    array('page_id'),
                    array($pagenumber, $page),
                    'ii'
                );
                $success &= $sql->run();
            }
            // Collect information about next page
            $sql = new SQLSelectStatement(
                ARTICLESECTIONS_TABLE,
                'articlesection_id',
                array('article_id', 'pagenumber'),
                array($page, $pagenumber),
                'ii'
            );
            $sections = $sql->fetch_column();
            array_unshift($sections, $articlesection);

        break;
        default:
            return false;
    }

    if ($success) {

        // Update pagenumber for section
        $sql = new SQLUpdateStatement(
            ARTICLESECTIONS_TABLE,
            array('pagenumber'), array('articlesection_id'),
            array($pagenumber, $articlesection), 'ii'
        );
        $success &= $sql->run();

        // Reindex section positions
        $values = array();
        for ($i = 0; $i < count($sections); $i++) {
            array_push($values, array($i, $sections[$i]));
        }

        $sql = new SQLUpdateStatement(
            ARTICLESECTIONS_TABLE,
            array('position'), array('articlesection_id'),
            array(), 'ii'
        );
        $sql->set_values($values);
        $success &= $sql->run();
    }

    return $success;
}

?>
