<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';

$ajax = new Ajax();

$message = '';
switch ($_POST['elementtype']) {
    case 'articlesection':
        include_once BASEDIR.'/admin/functions/pagecontent/articlepagesmod.php';
        $contents = getarticlesectioncontents($_POST['item']);
        break;
    case 'gallery':
        include_once BASEDIR.'/functions/db.php';
        include_once BASEDIR.'/includes/constants.php';
        $sql = new SQLSelectStatement(GALLERYITEMS_TABLE, 'image_filename', array('galleryitem_id'), array($_POST['item']), 'i');
        $contents = array('image_filename' => $sql->fetch_value());
        break;
    case 'link':
        include_once BASEDIR.'/admin/functions/pagecontent/linklistpagesmod.php';
        $contents = getlinkcontents($_POST['item']);
        break;
    case 'newsitemsynopsis':
        include_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
        $contents = getnewsitemsynopsisimage($_POST['item']);
        break;
    case 'newsitemsection':
        include_once BASEDIR.'/admin/functions/pagecontent/newspagesmod.php';
        $contents = getnewsitemsectioncontents($_POST['item']);
        break;
    case 'pageintro':
        include_once BASEDIR.'/admin/functions/pagesmod.php';
        $contents = getpageintroimage($_POST['page']);
        break;
    default:
        $ajax->produce_error('Error toggling image editor. Unknown element type: '. $_POST['elementtype']);
}

switch ($_POST['toggle']) {
    case 'collapse':
        $editor = new ImageEditorCollapsed($_POST['page'], $_POST['item'], $_POST['elementtype'], $contents);
        break;
    case 'expand':
        $editor = new ImageEditorExpanded($_POST['page'], $_POST['item'], $_POST['elementtype'], $contents);
        break;
    default:
        $ajax->produce_error('Unknown toggle type: '. $_POST['toggle']);
}

$ajax->produce($editor->toHTML(), true);

?>
