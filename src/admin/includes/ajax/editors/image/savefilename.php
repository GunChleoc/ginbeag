<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/functions/images.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$imagefilename = isset($_POST['imagefilename']) ? trim($_POST['imagefilename']) : null;
if (empty($imagefilename)) {
    $imagefilename = null;
}

$item = $_POST['item'];
$elementtype = $_POST['elementtype'];

if (!empty($imagefilename) && !imageexists($imagefilename)) {
    $ajax->produce_error("Error saving image $imagefilename - we don't have this image!");
} else {
    switch ($elementtype) {
        case 'articlesection':
            $sql = new SQLUpdateStatement(
                ARTICLESECTIONS_TABLE,
                array('image_filename'), array('articlesection_id'),
                array($imagefilename, $item), 'si'
            );

            if ($sql->run()) {
                if ($imagefilename) {
                    $ajax->produce("Saved section image: $imagefilename");
                } else {
                    $ajax->produce('Removed image from section');
                }
            } else {
                $ajax->produce_error("Error saving section image $imagefilename for page $page and section $item");
            }
            break;
        case 'gallery':
            if (!$imagefilename) {
                $ajax->produce_error('If you want to delete this image, please use the Remove button below!');
            } else {
                $sql = new SQLUpdateStatement(
                    GALLERYITEMS_TABLE,
                    array('image_filename'), array('galleryitem_id'),
                    array($imagefilename, $item), 'si'
                );

                if ($sql->run()) {
                    $ajax->produce("Saved gallery image: $imagefilename");
                } else {
                    $ajax->produce_error("Error saving gallery image $imagefilename for page $page");
                }
            }
            break;
        case 'link':
            $sql = new SQLUpdateStatement(
                LINKS_TABLE,
                array('image_filename'), array('link_id'),
                array($imagefilename, $item), 'si'
            );

            if ($sql->run()) {
                if ($imagefilename) {
                    $ajax->produce("Saved link image: $imagefilename");
                } else {
                    $ajax->produce('Removed image from link');
                }
            } else {
                $ajax->produce_error("Error saving link image $imagefilename for page $page and link $item");
            }
            break;
        case 'newsitemsection':
            $sql = new SQLUpdateStatement(
                NEWSITEMSECTIONS_TABLE,
                array('image_filename'), array('newsitemsection_id'),
                array($imagefilename, $item), 'si'
            );

            if ($sql->run()) {
                if ($imagefilename) {
                    $ajax->produce("Saved section image: $imagefilename");
                } else {
                    $ajax->produce('Removed image from section');
                }
            } else {
                $ajax->produce_error("Error saving section image $imagefilename for page $page and section $item");
            }
            break;
        case 'newsitemsynopsis':
            if (!$imagefilename) {
                $ajax->produce_error('If you want to delete this image, please use the Remove button below!');
            } else {
                $sql = new SQLUpdateStatement(
                    NEWSITEMSYNIMG_TABLE,
                    array('image_filename'), array('newsitemimage_id'),
                    array($imagefilename, $item), 'si'
                );

                if ($sql->run()) {
                    $message = $ajax->produce("Saved newsitem synopsis image: $imagefilename");
                } else {
                    $ajax->produce_error("Error saving newsitem synopsis image $imagefilename for newsitem $item");
                }
            }
            break;
        case 'pageintro':
            $sql = new SQLUpdateStatement(
                PAGES_TABLE,
                array('image_filename'), array('page_id'),
                array($imagefilename, $page), 'si'
            );

            if ($sql->run()) {
                if ($imagefilename) {
                    $ajax->produce("Saved synopsis image: $imagefilename");
                } else {
                    $ajax->produce('Removed image from synopsis');
                }
            } else {
                $ajax->produce_error("Error saving synopsis image $imagefilename for page $page");
            }
            break;
        default:
            $ajax->produce_error("Error saving image: unknown element type: $elementtype");
    }
}
?>
