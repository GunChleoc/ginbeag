<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/ajax.php';
require_once BASEDIR.'/admin/includes/objects/editors/image.php';
require_once BASEDIR.'/functions/db.php';

$ajax = new Ajax();

$layout = '';
$elementtype = $_POST['elementtype'];

switch ($elementtype) {
    case 'pageintro':
        $sql = new SQLSelectStatement(PAGES_TABLE, array('imagealign', 'usethumbnail'), array('page_id'), array($_POST['page']), 'i');
    break;
    case 'articlesection':
        $sql = new SQLSelectStatement(ARTICLESECTIONS_TABLE, array('imagealign', 'usethumbnail'), array('articlesection_id'), array($_POST['item']), 'i');
    break;
    case 'newsitemsection':
        $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, array('imagealign', 'usethumbnail'), array('newsitemsection_id'), array($_POST['item']), 'i');
    break;
    default:
        $ajax->produce_error("Error showing image alignment: Unknown elementtype: $elementtype </br /> for image on page: " . $_POST['page'] . ", item: " . $_POST['item']);
}

$layout = $sql->fetch_row();

if (!empty($db->error_report)) {
    $ajax->produce_error("Error fetching layout data for $elementtype");
} else {
    // Some pages have no imagealign defined, so we don't test whether it has been fetched
    $printme = new ImageEditorLayoutPane($_POST['page'], $_POST['item'], $layout['imagealign'], $layout['usethumbnail']);
    $ajax->produce($printme->toHTML(), true);
}
?>
