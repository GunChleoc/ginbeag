<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(dirname(__DIR__))))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$item = $_POST['item'];
$usethumbnail = $_POST['usethumbnail'] === 'true' ? 1 : 0;

if ($_POST['imagealign'] === 'center' || $_POST['imagealign'] === 'left' || $_POST['imagealign'] === 'right' ) {
    $imagealign = $_POST['imagealign'];
} else {
    $imagealign = 'left';
}

$elementtype = $_POST['elementtype'];
switch ($elementtype) {
    case 'articlesection':
        $sql = new SQLUpdateStatement(
            ARTICLESECTIONS_TABLE,
            array('imagealign', 'usethumbnail'), array('articlesection_id'),
            array($imagealign, $usethumbnail, $item), 'sii'
        );

        if ($sql->run()) {
            $ajax->produce("Saved article section image layout: align $imagealign. Use thumbnail " . $_POST['usethumbnail']);
        } else {
            $ajax->produce_error("Error saving article section image layout: align $imagealign. Use thumbnail ".$_POST['usethumbnail']." for page $page and section $item");
        }
    break;
    case 'newsitemsection':
        $sql = new SQLUpdateStatement(
            NEWSITEMSECTIONS_TABLE,
            array('imagealign', 'usethumbnail'), array('newsitemsection_id'),
            array($imagealign, $usethumbnail, $item), 'sii'
        );

        if ($sql->run()) {
            $ajax->produce("Saved newsitem section image layout: align $imagealign. Use thumbnail " . $_POST['usethumbnail']);
        } else {
            $ajax->produce_error("Error saving newsitem section image layout: align $imagealign. Use thumbnail ".$_POST['usethumbnail']." for page $page and section $item");
        }
    break;
    case 'pageintro':
        $sql = new SQLUpdateStatement(
            PAGES_TABLE,
            array('imagealign', 'usethumbnail'), array('page_id'),
            array($imagealign, $usethumbnail, $page), 'sii'
        );

        if ($sql->run()) {
            $ajax->produce("Saved synopsis image layout: align $imagealign. Use thumbnail " . $_POST['usethumbnail']);
        } else {
            $ajax->produce_error("Error saving synopsis image layout: align $imagealign. Use thumbnail ".$_POST['usethumbnail']." for page $page");
        }
    break;
    default:
        $ajax->produce_error("You can't change the layout of images for element type $elementtype");
}
?>
