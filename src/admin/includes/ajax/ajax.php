<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/admin/functions/sessions.php';

// Stop Database output from destroying returned XML
$db->quiet_mode = true;


/**
 * Class for handling messaging in Ajax calls.
 */
class Ajax {

    var $error;


    /**
     * Initialize error state & check that user has valid session.
     */
    function __construct() {
        $this->error = false;

        checksession();
    }

    /**
     * Set error & print <message error="1"><![CDATA[' . $message . ']]></message>
     */
    function produce_error($message) {
        $this->error = true;
        $this->produce_xml($message, false);
    }

    /**
     * Print plain text or XML from message. If there was an error in any database query,
     * prints error XML instead.
     *
     * @param string $message  The message to print
     *
     * @param boolean $html    Optional. If this is true, print as is without adding
     *                         any XML information.
     *                         If this is false, print
     *                         <message error="0"><![CDATA[' . $message . ']]></message>
     */
    function produce($message, $html = false) {
        $this->produce_xml($message, $html);
    }

    /**
     * Print plain text or XML from message. If there was an error in any database query
     * or $this->error = true, prints error XML instead.
     *
     * @param string $message  The message to print
     *
     * @param boolean $html    If this is true, print as is without adding
     *                         any XML information.
     *                         If this is false, print
     *                         <message error="0"><![CDATA[' . $message . ']]></message>
     */
    protected function produce_xml($message, $html) {
        if (!empty($db->error_report)) {
            set_error();
            $message .= " Error: \n<br>" . $db->error_report;
        }

        if ($this->error) {
            header('Content-type: text/xml; charset=utf-8');
            echo '<?xml version="1.0" encoding="UTF-8"?>';
            print('<message error="1"><![CDATA[' . $message . ']]></message>');
        } elseif ($html) {
            print($message);
        } else {
            header('Content-type: text/xml; charset=utf-8');
            echo '<?xml version="1.0" encoding="UTF-8"?>';
            print('<message error="0"><![CDATA[' . $message . ']]></message>');
        }
        exit;
    }
}
?>
