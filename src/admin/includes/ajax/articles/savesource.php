<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(dirname(dirname(dirname(__DIR__)))));

include BASEDIR.'/admin/includes/legaladminvars.php';
require_once BASEDIR.'/admin/includes/ajax/pageajax.php';
require_once BASEDIR.'/functions/db.php';

$page = $_POST['page'];
$ajax = new PageAjax($page);

$year = $_POST['year'];
if (strlen($year) != 4) {
    $year = "0000";
}
$sql = new SQLUpdateStatement(
    ARTICLES_TABLE,
    array('article_author', 'location', 'day', 'month', 'year', 'source', 'sourcelink', 'max_toc_level'), array('page_id'),
    array($_POST['author'], $_POST['location'], $_POST['day'], $_POST['month'], $year, $_POST['source'], $_POST['sourcelink'], $_POST['toc'], $page), 'ssiiissii'
);

if ($sql->run()) {
    $ajax->produce("Saved Source Info for Article ID: $page");
} else {
    $ajax->produce_error("Error saving Source Info for Article ID: $page");
}
?>
