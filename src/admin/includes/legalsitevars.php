<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

if (!defined('LEGALVARS')) {
    define (
        'LEGALVARS',
        array(
            'action',
            'activate',
            'addpage',
            'addredirect',
            'alllinksonsplashpage',
            'allowroot',
            'allowsimplemenu',
            'aotdpages',
            'ascdesc',
            'backup',
            'bannerid',
            'bannerproperties',
            'block',
            'cancel',
            'changeaccess',
            'changelevel',
            'code',
            'confirmunblock',
            'contact',
            'contactfunction',
            'cookieprefix',
            'countmonth',
            'countyear',
            'createuser',
            'date',
            'datetime',
            'deactivate',
            'defaulttemplate',
            'deleteabort',
            'deleteconfirm',
            'deleteentry',
            'description',
            'display',
            'displayaotd',
            'displaybanners',
            'displaypolicy',
            'displaypotd',
            'domainname',
            'editredirect',
            'email',
            'enableguestbook',
            'filterpermission',
            'floodcontrol',
            'flood_interval',
            'flood_perminute',
            'footermessage',
            'galleryimagesperpage',
            'generate',
            'guestbookperpage',
            'header',
            'holder',
            'id',
            'imagepath',
            'imagesperpage',
            'imagewidth',
            'ip',
            'iscontact',
            'banipallrestricted',
            'keywords',
            'leftimage',
            'leftlink',
            'link',
            'localpath',
            'offset',
            'order',
            'messageid',
            'mathcaptcha',
            'month',
            'month_year',
            'mobilethumbnailsize',
            'newsperpage',
            'page',
            'pageid',
            'pagetype',
            'pagetypesettings',
            'pass',
            'passconfirm',
            'pattern',
            'policytitle',
            'positions',
            'postaction',
            'profile',
            'rebuild',
            'ref',
            'referrer',
            'removepage',
            'renamevariables',
            'replacement',
            'rightimage',
            'rightlink',
            'saveproperties',
            'search',
            'searchpublicuser',
            'searchuser',
            'selectmonth',
            'selectyear',
            'serverprotocol',
            'showsd',
            'sid',
            'signature',
            'sitedescription',
            'sitelanguages',
            'sitename',
            'sitestats',
            'spamwords',
            'spamwords_content',
            'spamwords_subject',
            'spfont',
            'spimage',
            'sptext1',
            'sptext2',
            'submit',
            'structure',
            'thumbnailsize',
            'toggledisplaybanners',
            'type',
            'unbanipallrestricted',
            'unblock',
            'usemathcaptcha',
            'userid',
            'userlevel',
            'username',
            'year_year'
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!in_array($key, LEGALVARS)) {
        header('HTTP/1.0 404 Not Found');
        print('HTTP 404: Sorry, but this page does not exist.');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<br />'$key' not registered with legalsitevars.");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

?>
