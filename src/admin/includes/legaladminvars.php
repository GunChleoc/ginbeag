<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

if (!defined('LEGALVARS')) {
    define (
        'LEGALVARS',
        array(
            'action',
            'addarticlepage',
            'addimage',
            'addlinkdescriptionlanguageselectionform',
            'addlinktitlelanguageselectionform',
            'addnewsitem',
            'addpublicusers',
            'addsub',
            'addsubtext',
            'archivenewsitems',
            'articlepage',
            'articlesection',
            'ascdesc',
            'author',
            'backup',
            'bannerid',
            'cattype',
            'changeaccess',
            'changelevel',
            'changelink',
            'contact',
            'contactfunction',
            'contents',
            'contributor',
            'copyright',
            'create',
            'day',
            'delcat',
            'delcatconfirm',
            'deletelastarticlepage',
            'direction',
            'disablerss',
            'display',
            'displayorder',
            'doarchivenewsitems',
            'done',
            'editcat',
            'editcattext',
            'edittext',
            'elementtype',
            'email',
            'enablerss',
            'executedelete',
            'filename',
            'filter',
            'filterpermission',
            'findnewparent',
            'forgetful',
            'from',
            'fromday',
            'frommonth',
            'fromyear',
            'galleryitemid',
            'generate',
            'holder',
            'hours',
            'image',
            'imagealign',
            'imagecopyright',
            'imageid',
            'imagefilename',
            'ispublishable',
            'isquoted',
            'item',
            'jsid',
            'jump',
            'jumppage',
            'key',
            'link',
            'linkid',
            'location',
            'logout',
            'minutes',
            'm',
            'month',
            'movebottom',
            'movecat',
            'movedown',
            'movefrom',
            'moveid',
            'moveto',
            'movetop',
            'moveup',
            'navlevels',
            'navtitle',
            'navtitlelanguageselectionform',
            'newparent',
            'newsitem',
            'newsitemsection',
            'nodelete',
            'noofimages',
            'oldpass',
            'oldestday',
            'oldestmonth',
            'oldestyear',
            'offset',
            'order',
            'page',
            'pagecontents',
            'pagelanguageselectionform',
            'pagelevels',
            'pageposition',
            'pagetitle',
            'pagetitlelanguageselectionform',
            'params',
            'parentnode',
            'pass',
            'passconfirm',
            'permission',
            'positions',
            'postaction',
            'previewtext',
            'printview',
            'profile',
            'publish',
            'ref',
            'referrer',
            'removeconfirm',
            'removeimage',
            'removepublicusers',
            'restrict',
            'restrictaccess',
            'root',
            'savetext',
            'search',
            'seconds',
            'sectiontitle',
            'selectcattype',
            'selectedcat',
            'selectusers',
            'setdisplayorder',
            'setpermissions',
            'showall',
            'sid',
            'sisters',
            'sitepolicy',
            'sitestats',
            'sortlinks',
            'sortsubpages',
            'source',
            'sourcelink',
            'structure',
            'submit',
            'subpages',
            'superforgetful',
            'text',
            'title',
            'to',
            'toc',
            'today',
            'toggle',
            'tomonth',
            'toyear',
            'type',
            'unlock',
            'usethumbnail',
            'user',
            'userid',
            'year'
        )
    );
}

foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!in_array($key, LEGALVARS)) {
        header('HTTP/1.0 404 Not Found');
        print('HTTP 404: Sorry, but this page does not exist.');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<br />'$key' not registered with legaladminvars.");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

?>
