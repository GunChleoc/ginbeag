<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(__DIR__));

// Check actions and legal vars
$action = isset($_GET['action']) ? $_GET['action'] : '';
unset($_GET['action']);

require_once BASEDIR.'/admin/includes/actions.php';

if (issiteaction($action)) {
    include BASEDIR.'/admin/includes/legalsitevars.php';
} else {
    include BASEDIR.'/admin/includes/legaladminvars.php';
}

require_once BASEDIR.'/admin/includes/objects/adminmain.php';
require_once BASEDIR.'/admin/functions/pagesmod.php';

if (isset($_GET['logout'])) {
    unset($_GET['logout']);
    logout();
}

checksession();

// Contents
if (isset($_GET['jumppage'])) {
    $_GET['page']=$_GET['jumppage'];
    unset($_GET['jumppage']);
}

$page = max(isset($_GET['page']) ? $_GET['page'] : 0, 0);

if (isset($_GET['unlock'])) {
    unlockpage($page);
    unset($_GET['unlock']);
}

// Load site page, edit page mode or page view mode
switch ($action) {
    // Page editing mode
    case 'edit':
        include_once BASEDIR.'/admin/pageedit.php';
    // Site
    case 'site':
    case 'sitestats':
        include_once BASEDIR.'/admin/site/stats.php';
        break;
    // Pages
    case 'sitepagetype':
        include_once BASEDIR.'/admin/site/pagetypes.php';
        break;
    case 'sitepagerestrict':
        include_once BASEDIR.'/admin/site/restrictedpages.php';
        break;
    // Features & Layout
    case 'sitelayout':
        include_once BASEDIR.'/admin/site/layout.php';
        break;
    case 'siteiotd':
        include_once BASEDIR.'/admin/site/iotd.php';
        break;
    case 'siteguest':
        include_once BASEDIR.'/admin/site/guestbookadmin.php';
        break;
    case 'sitepolicy':
        include_once BASEDIR.'/admin/site/policy.php';
        break;
    case 'sitebanner':
        include_once BASEDIR.'/admin/site/bannersadmin.php';
        break;
    // Technical
    case 'sitetech':
        include_once BASEDIR.'/admin/site/technical.php';
        break;
    case 'siteredirects':
        include_once BASEDIR.'/admin/site/redirects.php';
        break;
    case 'siteind':
        include_once BASEDIR.'/admin/site/rebuild.php';
        break;
    // Protection
    case 'sitespam':
        include_once BASEDIR.'/admin/site/antispam.php';
        break;
    case 'sitereferrers':
        include_once BASEDIR.'/admin/site/referrers.php';
        break;
    // Users
    case 'siteuserman':
        include_once BASEDIR.'/admin/site/usermanagement.php';
        break;
    case 'siteuserperm':
        include_once BASEDIR.'/admin/site/userpermissions.php';
        break;
    case 'siteuserlist':
        include_once BASEDIR.'/admin/site/userlist.php';
        break;
    case 'siteusercreate':
        include_once BASEDIR.'/admin/site/createpublicuser.php';
        break;
    case 'siteipban':
        include_once BASEDIR.'/admin/site/ipban.php';
        break;
    case 'siteonline':
        include_once BASEDIR.'/admin/site/whosonline.php';
        break;
    // Page view mode
    default:
        include_once BASEDIR.'/admin/includes/objects/messages.php';
        $admin = new AdminMain($page, $action, new AdminMessage('', false));
        print($admin->toHTML());
}

?>
