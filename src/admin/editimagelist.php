<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(__DIR__));

include BASEDIR.'/admin/includes/legalimagevars.php';
require_once BASEDIR.'/admin/functions/categoriesmod.php';
require_once BASEDIR.'/admin/functions/imagesmod.php';
require_once BASEDIR.'/admin/includes/objects/imagelist.php';
require_once BASEDIR.'/functions/imagefiles.php';

checksession();

$number = max(0, isset($_GET['number']) ? $_GET['number'] : getproperty('Imagelist Images Per Page'));
$page = isset($_GET['page']) ? max(0, $_GET['page']) : 0;
$offset = isset($_GET['offset']) ? max(0, $_GET['offset']) : 0;
$order = isset($_GET['order']) ? $_GET['order'] : 'uploaddate';
$ascdesc = isset($_GET['ascdesc']) ? $_GET['ascdesc'] : 'desc';

$searchterm = isset($_GET['searchterm']) ? trim($_GET['searchterm']) : '';

if (empty($searchterm) && isset($_GET['advanced'])) {
    $advanced = $_GET['advanced'];
    $searchvalues = AdvancedSearchForm::getsearchvalues($_GET);
} else {
    if (!isset($advanced)) $advanced = false;
    $searchvalues = array();
}

$message = '';
$error = false;
$success = false;

$action = isset($_GET['action']) ? $_GET['action'] : '';
if ($action === 'addunknownfile') {
    $filename = $_GET['filename'];

    if (imageexists($filename)) {
        $message = "Image already exists: $filename";
        $error = true;
    } else {
        $caption = isset($_GET['caption']) ? $_GET['caption'] : '';
        $source = isset($_GET['source']) ? $_GET['source'] : '';
        $sourcelink = isset($_GET['sourcelink']) ? $_GET['sourcelink'] : '';
        $copyright = isset($_GET['copyright']) ? $_GET['copyright'] : '';
        $permission = isset($_GET['permission']) ? $_GET['permission'] : NO_PERMISSION;
        $selectedcats = isset($_GET['selectedcat']) ? $_GET['selectedcat'] : array();
        $subpath = isset($_GET['subpath']) ? $_GET['subpath'] : '';

        addimage($filename, $subpath, $caption, $source, $sourcelink, $copyright, $permission);
        addimagecategories($filename, $selectedcats);
        $thumbnail = make_thumbnail_filename($filename);
        if (file_exists(getimagepath($thumbnail, $subpath))) {
            addthumbnail($filename, $thumbnail);
        }
        $message = 'Added Image';
    }
} elseif ($action === 'deleteunknownfile') {
    if (isset($_GET['deletefileconfirm'])) {
        $filename = $_GET['filename'];
        $subpath = isset($_GET['subpath']) ? $_GET['subpath'] : '';

        $success = deletefile(getproperty('Image Upload Path').$subpath, $filename);
        if ($success) {
            $message = "File <em>$filename</em> deleted.";
        } else {
            $message = "Error deleting file <em>$filename</em>.";
            $error = true;
        }
    } else {
        $message = 'File delete not confirmed!';
        $error = true;
    }
}

$adminimagepage = new AdminImagePage($message, $error, $offset, $number, $searchvalues);

print($adminimagepage->toHTML());

?>
