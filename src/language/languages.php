<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';
require_once BASEDIR.'/functions/phpcompatibility.php';

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} elseif (isset($_POST['page'])) {
    $page = $_POST['page'];
}

if (isset($page) && $page > 0) {
    include_once BASEDIR.'/functions/pages.php';
    $language_key=getpagecontents($page)['language'];
} else {
    $language_key=get_site_language();
}

if (file_exists(BASEDIR.'/language/'.$language_key.".php")) {
    include_once BASEDIR.'/language/'.$language_key.".php";
} else {
    $language_key=get_site_language();
    if (file_exists(BASEDIR.'/language/'.$language_key.".php")) {
        include_once BASEDIR.'/language/'.$language_key.".php";
    } else {
        include_once BASEDIR.'/language/en.php';
    }
}
unset($language_key);


/**
 * Get translated string for key string
 *
 * @param string $key Key for fetching the translated string
 *
 * @return the translation for the given key
 */
function getlang($key)
{
    global $lang;
    if (array_key_exists($key, $lang)) {
        return $lang[$key];
    } else {
        return "[".$key."]";
    }
}

/**
 * Get translated string from array of key strings, e.g. to get a specific month name
 *
 * @param string $key   Main key, e.g. 'date_month_short'
 * @param int    $index Sub index, e.g. 1
 *
 * @return The translation for the given key at the given index
 */
function getlangarray($key, $index)
{
    global $lang;
    return $lang[$key][$index];
}

function get_site_language() {
    return explode('=', explode(',', getproperty('Site Languages'))[0])[0];
}


//
// formats date and time
//
function formatdatetime($date) {
    $result = @date(getproperty('Date Time Format'), strtotime($date));
    $result = translateday($result, getproperty('Date Time Format'));
    $result = translatemonth($result, getproperty('Date Time Format'));
    return str_replace(' ', '&nbsp;', $result);;
}

//
// formats a date
//
function formatdate($date) {
    $result = @date(getproperty('Date Format'), strtotime($date));
    $result = translateday($result, getproperty('Date Format'));
    $result = translatemonth($result, getproperty('Date Format'));
    return str_replace(' ', '&nbsp;', $result);
}


//
// helper function for formatdate and formatdatetime
// Only works if date starts with the day of the month
//
function translateday($date, $format) {
    if (str_contains($format, 'F')) {
        $date_time = explode(' ', $date);
        $date_time[0] = lang_date_day_format(intval($date_time[0]));
        return implode(' ', $date_time);
    }
    else { return $date;
    }
}

//
// helper function for formatdate and formatdatetime
//
function translatemonth($date, $format) {
    if (str_contains($format, 'F')) {
        $date = str_replace('January', getlangarray('date_month_format', 1), $date);
        $date = str_replace('February', getlangarray('date_month_format', 2), $date);
        $date = str_replace('March', getlangarray('date_month_format', 3), $date);
        $date = str_replace('April', getlangarray('date_month_format', 4), $date);
        $date = str_replace('May', getlangarray('date_month_format', 5), $date);
        $date = str_replace('June', getlangarray('date_month_format', 6), $date);
        $date = str_replace('July', getlangarray('date_month_format', 7), $date);
        $date = str_replace('August', getlangarray('date_month_format', 8), $date);
        $date = str_replace('September', getlangarray('date_month_format', 9), $date);
        $date = str_replace('October', getlangarray('date_month_format', 10), $date);
        $date = str_replace('November', getlangarray('date_month_format', 11), $date);
        $date = str_replace('December', getlangarray('date_month_format', 12), $date);
    } else {
        $date = str_replace('Jan', getlangarray('date_month_short', 1), $date);
        $date = str_replace('Feb', getlangarray('date_month_short', 2), $date);
        $date = str_replace('Mar', getlangarray('date_month_short', 3), $date);
        $date = str_replace('Apr', getlangarray('date_month_short', 4), $date);
        $date = str_replace('May', getlangarray('date_month_short', 5), $date);
        $date = str_replace('Jun', getlangarray('date_month_short', 6), $date);
        $date = str_replace('Jul', getlangarray('date_month_short', 7), $date);
        $date = str_replace('Aug', getlangarray('date_month_short', 8), $date);
        $date = str_replace('Sep', getlangarray('date_month_short', 9), $date);
        $date = str_replace('Oct', getlangarray('date_month_short', 10), $date);
        $date = str_replace('Nov', getlangarray('date_month_short', 11), $date);
        $date = str_replace('Dec', getlangarray('date_month_short', 12), $date);
    }
    return $date;
}

//
//
//
function makearticledate($day, $month, $year) {
    $result = '';

    if ($year !== '0000') {
        if ($month) {
            if ($day) {
                $result = lang_date_day_format($day) . ' ' . getlangarray('date_month_format', $month) . ' ' . $year;
            } else {
                $result = getlangarray('date_month', $month) . ' ' . $year;
            }
        } else {
            $result = $year;
        }
    }
    return $result;
}
?>
