<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/redirects.php';

// *************************************************************************
// Conversion functions
// *************************************************************************

//
//
//
function striptitletags($title)
{
    $title=stripslashes($title);

    //$title=preg_replace("/&amp;#(.*);/U","&#\\1;",$title); // restore unicode characters
    //$title=str_replace("&amp;nbsp;","&nbsp;",$title); // restore &nbsp;
    //$title=str_replace('"',"&quot;",$title); // quotes
    // strip 'em
    $patterns = array(
    "/\[link\](.*?)\[\/link\]/i",
    "/\[url\](.*?)\[\/url\]/i",
    '/\[url="(.*?)"\](.*?)\[\/url\]/i',
    "/\[url=(.*?)\](.*?)\[\/url\]/i",

    "/\[b\](.*?)\[\/b\]/si",
    "/\[u\](.*?)\[\/u\]/si",
    "/\[i\](.*?)\[\/i\]/si",
    "/\[lang=(.*?)\](.*?)\[\/lang\]/si",
    "/\[color=(.*?)\](.*?)\[\/color\]/si",
    "/\[img\](.*?)\[\/img\]/i",
    );
    $replacements = array(
    "\\1", // link
    "\\1", // url
    "\\2", // url
    "\\2", // url

    "\\1", // b
    "\\1", // u
    "\\1", // i
    "\\2", // lang
    "\\2", // color
    "" // img
    );
    $title = preg_replace($patterns, $replacements, $title);
    $title=str_replace('<', '&lt;', $title);
    $title=str_replace('>', '&gt;', $title);

    // Remove HTML tags
    // todo: allowtags in site properties
    $remove=array();
    array_push($remove, 'a');
    array_push($remove, 'b');
    array_push($remove, 'br');
    array_push($remove, 'caption');
    array_push($remove, 'center');
    array_push($remove, 'dd');
    array_push($remove, 'div');
    array_push($remove, 'dl');
    array_push($remove, 'dt');
    array_push($remove, 'em');
    array_push($remove, 'embed');
    array_push($remove, 'hr');
    array_push($remove, 'i');
    array_push($remove, 'img');
    array_push($remove, 'li');
    array_push($remove, 'object');
    array_push($remove, 'ol');
    array_push($remove, 'p');
    array_push($remove, 'pre');
    array_push($remove, 'span');
    array_push($remove, 'strong');
    array_push($remove, 'sub');
    array_push($remove, 'sup');
    array_push($remove, 'table');
    array_push($remove, 'td');
    array_push($remove, 'th');
    array_push($remove, 'tr');
    array_push($remove, 'ul');

    for($i=0;$i<count($remove);$i++)
    {
        $pattern='/\&lt;'.$remove[$i].'(.*?)\&gt;/';
        $title=preg_replace($pattern, "", $title);
        $pattern='/\&lt;(\/)'.$remove[$i].'(.*?)\&gt;/';
        $title=preg_replace($pattern, "", $title);
    }


    // copyright
    $title = str_replace('((C))', '&copy;', $title);
    return $title;
}

//
//
//
function title2html($title)
{
    if (empty($title)) {
        return $title;
    }

    $title=stripslashes($title);

    $title=str_replace('<', '&lt;', $title);
    $title=str_replace('>', '&gt;', $title);

    // bbcode to html
    $patterns = array(
    "/\[b\](.*?)\[\/b\]/si",
    "/\[u\](.*?)\[\/u\]/si",
    "/\[i\](.*?)\[\/i\]/si",
    "/\[lang=(.*?)\](.*?)\[\/lang\]/si"
    );
    $replacements = array(
    "<b>\\1</b>", // b
    "<u>\\1</u>", // u
    "<i>\\1</i>", // i
    "<span lang=\"\\1\">\\2</span>" // lang
    );
    $title = preg_replace($patterns, $replacements, $title);

    // copyright
    $title = str_replace('((C))', '&copy;', $title);
    return $title;
}

//
// strip shlashes and handle unicode
//
function input2html($text, $multiline = false)
{
    $text=stripslashes($text);
    $text=str_replace('&nbsp;', '&amp;nbsp;', $text);
    $text=str_replace("'", '&#39;', $text);
    $text=str_replace('"', '&#34;', $text);

    // lt and gt
    $text=str_replace('<', '&lt;', $text);
    $text=str_replace('>', '&gt;', $text);

    if ($multiline) {
        $text = preg_replace('/\r\n/', '&#10;', $text);
        $text = preg_replace('/\n/', '&#10;', $text);
    }

    return $text;
}


//
//
//
function text2html($text)
{
    global $_GET;
    # Passing null to parameter #1 ($string) of type string is deprecated
    if (is_null($text) || $text === '') {
        return '';
    }

    $text=stripslashes(stripslashes($text));
    // lt and gt
    $text=str_replace('<', '&lt;', $text);
    $text=str_replace('>', '&gt;', $text);

    // http://de.php.net/manual/en/function.preg-replace.php

    // strip color tags for print view
    if(isset($_GET['printview'])) { $text = preg_replace("/\[color=(.*?)\](.*?)\[\/color\]/si", "\\2", $text);
    } else { $text = preg_replace("/\[color=(.*?)\](.*?)\[\/color\]/si", "<span style=\"color:\\1\">\\2</span>", $text);
    }

    // bbcode to html
    $patterns = array(
    "/\[link\](.*?)\[\/link\]/i",
    "/\[url\](.*?)\[\/url\]/i",
    '/\[url="(.*?)"\](.*?)\[\/url\]/i',
    "/\[url=(.*?)\](.*?)\[\/url\]/i",

    "/\[lang=(.*?)\](.*?)\[\/lang\]/si",
    "/\[img\](.*?)\[\/img\]/i",
    "/\[b\](.*?)\[\/b\]/si",
    "/\[u\](.*?)\[\/u\]/si",
    "/\[i\](.*?)\[\/i\]/si"
    );
    $replacements = array(
    "<a href=\"\\1\" target=\"_blank\">\\1</a>", // link
    "<a href=\"\\1\" target=\"_blank\">\\1</a>", // url
    "<a href=\\1 target=\"_blank\">\\2</a>", // url
    "<a href=\"\\1\" target=\"_blank\">\\2</a>", // url

    "<span lang=\"\\1\">\\2</span>", // lang
    "<img src=\"\\1\">", // img
    "<b>\\1</b>", // b
    "<u>\\1</u>", // u
    "<i>\\1</i>" // i
    );
    $text = preg_replace($patterns, $replacements, $text);


    // parsing nested lists
    while(preg_match("/\[list\](.*?)\[\/list\]/si", $text) || preg_match("/\[list=(.*?)\](.*?)\[\/list\]/si", $text))
    {
        $patterns = array(
        "/\[list\](.*?)\[\/list\]/si",
        "/\[list=(.*?)\](.*?)\[\/list\]/si",
        "/\[\*\](.*?)/s"
        );
        $replacements = array(
        "<ul>\\1</ul>",
        "<ol type=\"\\1\">\\2</ol>",
        "<li>\\1"
        );
        $text = preg_replace($patterns, $replacements, $text);
    }


    // tables
    $patterns = array(
    "/\[tr\](\s+?)\[/si",
    "/\[\/tr\](\s+?)\[/si",

    "/\[td\](\s+?)\[/si",
    "/\[\/td\](\s+?)\[/si",

    "/\[th\](\s+?)\[/si",
    "/\[\/th\](\s+?)\[/si",

    "/\[caption\](\s+?)\[/si",
    "/\[\/caption\](\s+?)\[/si",

    "/\[table\](\s+?)\[/si",
    "/\](\s+?)\[\/table\]/si",
    );
    $replacements = array(
    "[tr][",
    "[/tr][",

    "[td][",
    "[/td][",

    "[th][",
    "[/th][",

    "[caption][",
    "[/caption][",

    "[table][",
    "][/table]",
    );
    $text = preg_replace($patterns, $replacements, $text);

    while(preg_match("/\[table(.*?)\](.*?)\[tr\](.*?)\[td(.*?)\](.*?)\[\/td\](.*?)\[\/tr\](.*?)\[\/table\]/si", $text))
    {
        $patterns = array(
        "/\[table(.*?)\](.*?)\[tr\](.*?)\[td(.*?)\](.*?)\[\/td\](.*?)\[\/tr\](.*?)\[\/table\]/si",
        );
        $replacements = array(
        "[table\\1]\\2[tr]\\3<td\\4>\\5</td>\\6[/tr]\\7[/table]",
        );
        $text = preg_replace($patterns, $replacements, $text);
    }
    $patterns = array(
    "/\[tr\](.*?)\[\/tr\]/si",
    "/\[th(.*?)\](.*?)\[\/th\]/si",
    "/\[caption\](.*?)\[\/caption\]/si",
    "/\[table(.*?)\](.*?)\[\/table\]/si",
    );
    $replacements = array(
    "<tr>\\1</tr>",
    "<th\\1>\\2</th>",
    "<caption>\\1</caption>",
    "<table\\1>\\2</table>",
    );
    $text = preg_replace($patterns, $replacements, $text);

    // Auto URL
    $text = preg_replace("/(\s|^)(https?:\/\/(\S*))(\/?)(\s|$)/", '<a href="\\2\\4">\\3</a>\\5', $text);

    //remove sid from local links
    $serverprotocol = getproperty('Server Protocol');
    $patterns = array(
    "/http(s){0,1}:(.*?)".getproperty("Domain Name")."(.*?)(sid=)(\w*?)(&)/",
    "/http(s){0,1}:(.*?)".getproperty("Domain Name")."(.*?)(\?sid=|&sid=)(\w*?)(\W|\s|$)/",
    "/(".str_replace("/", "\/", getprojectrootlinkpath()).")(index|.*admin.*|.*includes.*|.*functions.*)(.php)(.*)/"
    );
    $replacements = array(
    $serverprotocol.getproperty("Domain Name")."\\3",
    $serverprotocol.getproperty("Domain Name")."\\3\\6",
    "\\4"
    );
    $text = preg_replace($patterns, $replacements, $text);

    // remove target="_blank" from internal links
    $patterns = array(
    "/(\")(\?)(.*)(target=\"_blank\")/"
    );
    $replacements = array(
    "\\1\\2\\3"
    );
    $text = preg_replace($patterns, $replacements, $text);

    // Make sure that we're using the correct protocol for site-internal links
    if (getproperty("Server Protocol") == "https://") {
        $text = str_replace('http://'.getproperty("Domain Name"), getproperty("Server Protocol").getproperty("Domain Name"), $text);
    } else
    {
        $text = str_replace('https://'.getproperty("Domain Name"), getproperty("Server Protocol").getproperty("Domain Name"), $text);
    }

    // restore HTML tags
    // todo: allowtags in site properties
    $preserve=array();
    array_push($preserve, 'a');
    array_push($preserve, 'b');
    array_push($preserve, 'br');
    array_push($preserve, 'caption');
    array_push($preserve, 'center');
    array_push($preserve, 'dd');
    array_push($preserve, 'div');
    array_push($preserve, 'dl');
    array_push($preserve, 'dt');
    array_push($preserve, 'em');
    array_push($preserve, 'embed');
    array_push($preserve, 'hr');
    array_push($preserve, 'i');
    array_push($preserve, 'img');
    array_push($preserve, 'li');
    array_push($preserve, 'object');
    array_push($preserve, 'ol');
    array_push($preserve, 'p');
    array_push($preserve, 'pre');
    array_push($preserve, 'span');
    array_push($preserve, 'strong');
    array_push($preserve, 'sub');
    array_push($preserve, 'sup');
    array_push($preserve, 'table');
    array_push($preserve, 'td');
    array_push($preserve, 'th');
    array_push($preserve, 'tr');
    array_push($preserve, 'ul');

    for($i=0;$i<count($preserve);$i++)
    {
        $pattern='/\&lt;'.$preserve[$i].'(.*?)\&gt;/';
        $text=preg_replace($pattern, "<".$preserve[$i]."\\1>", $text);
        $pattern='/\&lt;(\/)'.$preserve[$i].'(.*?)\&gt;/';
        $text=preg_replace($pattern, "<\\1".$preserve[$i]."\\2>", $text);
    }

    // line break
    $text=nl2br($text);

    // copyright
    $text = str_replace('((C))', '&copy;', $text);

    // Redirects from site config
    $redirects = getredirects();
    $patterns = array();
    $replacements = array();

    foreach ($redirects as $redirect) {
        $patterns[] = trim($redirect['pattern']);
        $replacements[] = trim($redirect['replacement']);
    }
    $text = preg_replace($patterns, $replacements, $text);

    return $text;
}

?>
