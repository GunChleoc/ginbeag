<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

function make_random_string($characters, $minlength, $maxlength) {
    $result = '';
    $maxpos = strlen($characters);

    $randomlength = mt_rand($minlength, $maxlength);

    for ($i = 0; $i < $randomlength; $i++) {
        $position = mt_rand(0, $maxpos - 1);
        $result .= substr($characters, $position, 1);
    }
    return $result;
}

// Create random sessionid
function create_sid() {

    list($usec, $sec) = explode(' ', microtime());
    $seed =  (int) ((float) $sec + ((float) $usec * 100000));

    mt_srand($seed);
    return md5(''.mt_rand());
    return md5(''.random_int(0x0000_0000, 0xffff_ffff));
}
?>
