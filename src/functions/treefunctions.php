<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/publicsessions.php';
require_once BASEDIR.'/includes/objects/elements.php';

if(isset($_GET["sid"])) { $user=getpublicsiduser($_GET["sid"]);
} else { $user=0;
}

$sql = new SQLSelectStatement(
    PAGES_TABLE,
    array(
        'page_id',
        'parent_id',
        'title_navigator',
        'title_page',
        'language',
        'position',
        'pagetype',
        'ispublished'
    )
);
$sql->set_order(array('parent_id' => 'ASC', 'position' => 'ASC'));
$allpages = $sql->fetch_many_rows();

$sql = new SQLSelectStatement(RESTRICTEDPAGES_TABLE, '*');
$sql->set_order(array('page_id' => 'ASC'));
$allrestrictedpages = $sql->fetch_two_columns();

//
//
//
function getpagetype($page)
{
    global $allpages;
    if (ispageknown($page)) {
        return $allpages[$page]['pagetype'];
    }
    return "";
}

//
//
//
function getpagetitle($page)
{
    global $allpages;
    if (ispageknown($page)) {
        return $allpages[$page]['title_page'];
    }
    return "";
}

//
//
//
function getnavtitle($page)
{
    global $allpages;
    if (ispageknown($page)) {
        return $allpages[$page]['title_navigator'];
    }
    return "";
}

//
//
//
function getnavposition($page)
{
    global $allpages;
    if (ispageknown($page)) {
        return $allpages[$page]['position'];
    }
    return 0;
}

//
//
//
function getparent($page)
{
    global $allpages;
    if (ispageknown($page)) {
        return $allpages[$page]['parent_id'];
    }
    return 0;
}

//
//
//
function isrootpage($page)
{
    return getparent($page) == 0;
}

//
//
//
function getrootpages()
{
    return getchildren(0);
}

//
//
//
function getchildren($page)
{
    global $allpages;
    $result=array();
    reset($allpages);
    foreach ($allpages as $key => $checkpage) {
        if ($checkpage['parent_id'] == $page) {
            array_push($result, $key);
        }
    }
    return $result;
}

function getchildren_with_navinfo($page)
{
    global $allpages;
    $result=array();
    reset($allpages);
    foreach ($allpages as $key => $checkpage) {
        if ($checkpage['parent_id'] == $page) {
            $result[$key] = $checkpage;
        }
    }
    return $result;
}

//
//
//
function ispageknown($page)
{
    global $allpages;
    return array_key_exists($page, $allpages);
}

//
//
//
function ispublished($page)
{
    global $allpages;
    return ispageknown($page) && $allpages[$page]['ispublished'] == 1;
}


//
//
//
function displaylinksforpage($page)
{
    return (ispublished($page) && (!ispagerestricted($page) || hasaccesssession($page)));
}

//
//
//
function ispagerestricted($page)
{
    global $allrestrictedpages;
    return array_key_exists($page, $allrestrictedpages);
}

//
//
//
function isthisexactpagerestricted($page)
{
    global $allrestrictedpages;
    return in_array($page, $allrestrictedpages);
}

//
//
//
function getpagerestrictionmaster($page)
{
    global $allrestrictedpages;
    if (array_key_exists($page, $allrestrictedpages)) {
        return $allrestrictedpages[$page];
    }
    return 0;
}

//
//
//
function hasaccesssession($page) {
    global $sid;

    $masterpage = getpagerestrictionmaster($page);

    if ($masterpage > 0) {
        if (empty($sid)) {
            return false;
        }

        $sql = new SQLSelectStatement(PUBLICSESSIONS_TABLE, 'session_user_id', array('session_id'), array($sid), 's');
        $user_id = $sql->fetch_value();

        $sql = new SQLSelectStatement(RESTRICTEDPAGESACCESS_TABLE, 'publicuser_id', array('publicuser_id', 'page_id'), array($user_id, $masterpage), 'ii');
        return $sql->fetch_value();
    }
    return true;
}

function checkpublicsession($page) {
    global $_GET, $sid;
    $isvalid = $sid && ispublicsessionvalid($sid);
    //  $user=getpublicsiduser($_GET["sid"]);
    if (!$sid) {
        $hasaccess = false;
    } else {
        $hasaccess = hasaccesssession($page);
    }

    if(!$isvalid || publictimeout($sid) || !$hasaccess)
    // todo: replace ip check with browser agent check
    //if(!$isvalid || publictimeout($sid) || !checkpublicip($sid) || !$hasaccess)
    {
        if (!$hasaccess) {
            $message=getlang('restricted_nopermission');
        } else {
            $message=getlang('restricted_expired');
        }

        $contenturl = 'login.php'.makelinkparameters($_GET);
        $title = getlang('restricted_pagetitle');
        $header = new HTMLHeader($title, $title, $message, $contenturl, getlang('restricted_pleaselogin'), true);
        print($header->toHTML());

        $footer = new HTMLFooter();
        print($footer->toHTML());
        exit;
    }
}
?>
