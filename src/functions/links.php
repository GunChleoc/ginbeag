<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2024 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/db.php';

// *************************************************************************
// Page element and image functions
// *************************************************************************

// Create URL to site
function getprojectrootlinkpath() {
    $localpath = getproperty('Local Path');
    $domain = getproperty('Domain Name');
    $result = getproperty('Server Protocol').$domain.'/';
    if ($localpath) {
        $result .= $localpath.'/';
    }
    return $result;
}

// Turn $assoc_array into prowser link parameters
function makelinkparameters($assoc_array, $include_sid =true) {
    global $sid;

    $result = '';
    $params = array();
    foreach ($assoc_array as $param => $value) {
        if (is_array($value)) {
            $params[] = "$param=" . implode(',', array_map('urlencode', $value));
        } else {
            $params[] = "$param=" . urlencode($value);
        }
    }
    if ($include_sid && !empty($sid)) {
        $params[] = "sid=$sid";
    }

    if (!empty($params)) {
        $result = '?' . implode('&', $params);
    }
    return $result;
}

// Whether mobile style is to be used
function ismobile() {
    global $_GET;
    return isset($_GET['m']);
}

?>
