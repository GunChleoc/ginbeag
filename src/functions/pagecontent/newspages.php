<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/categories.php';
require_once BASEDIR.'/includes/constants.php';

// Get data for selection of newsitems for page
function getpublishednewsitems($page, $number, $offset) {
    $offset = max(0, $offset);
    $number = max(1, $number);

    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, '*', array('page_id', 'ispublished'), array($page, 1), 'ii');
    $sql->set_order(array('date' => (displaynewestnewsitemfirst($page) ? 'DESC' : 'ASC')));
    $sql->set_limit($number, $offset);
    return $sql->fetch_many_rows();
}

// Query sort order
function displaynewestnewsitemfirst($page) {
    $sql = new SQLSelectStatement(NEWS_TABLE, 'shownewestfirst', array('page_id'), array($page), 'i');
    return $sql->fetch_value();
}

// Total number of published newsitems for page
function countpublishednewsitems($page) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'newsitem_id', array('page_id', 'ispublished'), array($page, 1), 'ii');
    $sql->set_operator('count');
    return $sql->fetch_value();
}

// Get data for given newsitem
function getnewsitemcontents($newsitem) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, '*', array('newsitem_id'), array($newsitem), 'i');
    return $sql->fetch_row();
}

// Get date of oldest newsitem
function getoldestnewsitemdate($page) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'date', array('page_id'), array($page), 'i');
    $sql->set_operator('min');
    return @getdate(strtotime($sql->fetch_value()));
}

// Get date of newest newsitem
function getnewestnewsitemdate($page) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'date', array('page_id'), array($page), 'i');
    $sql->set_operator('max');
    return @getdate(strtotime($sql->fetch_value()));
}

// Get which page the given newsitem is on
function getnewsitempage($newsitem) {
    $sql = new SQLSelectStatement(NEWSITEMS_TABLE, 'page_id', array('newsitem_id'), array($newsitem), 'i');
    return $sql->fetch_value();
}

// Get synopsisimages for newsitem
function getnewsitemsynopsisimages($newsitem) {
    $sql = new SQLSelectStatement(NEWSITEMSYNIMG_TABLE, array('newsitemimage_id', 'image_filename'), array('newsitem_id'), array($newsitem), 'i');
    $sql->set_order(array('position' => 'ASC'));
    return $sql->fetch_two_columns();
}

// Get section ids for newsitem
function getnewsitemsections($newsitem) {
    $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, 'newsitemsection_id', array('newsitem_id'), array($newsitem), 'i');
    $sql->set_order(array('position' => 'ASC'));
    return $sql->fetch_column();
}

// Query whether the given section is quoted
function isnewsitemsectionquoted($newsitemsection) {
    $sql = new SQLSelectStatement(NEWSITEMSECTIONS_QUOTED_TABLE, 'newsitemsection_id', array('newsitemsection_id'), array($newsitemsection), 'i');
    $sql->set_distinct();
    $sql->set_operator('count');
    return $sql->fetch_value() == 1;
}

// Get section contents for newsitem
function getnewsitemsectionswithcontent($newsitem) {
    $sql = new SQLSelectStatement(NEWSITEMSECTIONS_TABLE, '*', array('newsitem_id'), array($newsitem), 'i');
    $sql->set_join('image_filename', IMAGES_TABLE, 'image_filename');
    $sql->set_order(array('position' => 'ASC'));
    return $sql->fetch_many_rows();
}


function get_month_name($number) {
    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December');
    return $months[$number];
}

// Get contents of filtered newsitems
function getfilterednewsitems($page, $selectedcat, $from, $to, $order, $ascdesc, $newsitemsperpage, $offset) {
    $values = array();
    $datatypes = '';

    $from = array_map('intval', $from);
    $to = array_map('intval', $to);

    if (checkdate($from['month'], $from['day'], $from['year']) && checkdate($to['month'], $to['day'], $to['year'])) {
        $date = $from['day'] . ' ' . get_month_name($from['month']) . ' '. $from['year'];
        $fromdate = date(DATETIMEFORMAT, strtotime($date));

        $date = $to['day'] . ' ' . get_month_name($to['month'])  . ' ' . $to['year'] . ' 23:59:59';
        $todate = date(DATETIMEFORMAT, strtotime($date));
        if ($todate < $fromdate) {
            unset($fromdate);
            unset($todate);
        }
    }

    $query = 'SELECT DISTINCTROW * FROM '. NEWSITEMS_TABLE . ' AS items';

    // Filter for categories
    if ($selectedcat != 1) {
        // get all category descendants
        $categories = getcategorydescendants($selectedcat, CATEGORY_NEWS);
        $datatypes = str_pad($datatypes, count($categories) + strlen($datatypes), 'i');
        $values = array_merge($values, $categories);

        $query .= ', '. NEWSITEMCATS_TABLE . ' AS cat';
        $query .= ' WHERE cat.newsitem_id = items.newsitem_id';
        $query .= ' AND cat.category ' . SQLStatement::create_in_question_marks($categories) . ' AND';
    } else {
        $query .= ' WHERE';
    }

    // years
    if (isset($fromdate) && isset($todate)) {
        $query .= ' items.date BETWEEN ? AND ? AND';
        array_push($values, $fromdate);
        array_push($values, $todate);
        $datatypes .= 'ss';
    }

    // get pages to search
    $query .= ' items.page_id = ?';
    $query .= ' AND items.ispublished = ?';
    array_push($values, $page);
    array_push($values, 1);
    $datatypes .= 'si';

    if ($order) {
        switch ($order) {
            case 'title':
                $query .= ' ORDER BY items.title ';
            break;
            case 'date':
                $query .= ' ORDER BY date ';
            break;
            case 'source':
                $query .= ' ORDER BY items.source ';
            break;
        }
        $query .= mb_strtolower($ascdesc, 'UTF-8') === 'desc' ? 'DESC' : 'ASC';
    }

    $sql = new RawSQLStatement($query, $values, $datatypes);
    if ($newsitemsperpage > 0) {
        $sql->set_limit($newsitemsperpage, $offset);
    }
    return $sql->fetch_many_rows();
}
?>
