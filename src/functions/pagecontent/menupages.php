<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

require_once BASEDIR.'/functions/categories.php';
require_once BASEDIR.'/functions/pages.php';
require_once BASEDIR.'/includes/constants.php';

// Get menu page settings
function getmenucontents($page) {
    $sql = new SQLSelectStatement(MENUS_TABLE, '*', array('page_id'), array($page), 'i');
    return $sql->fetch_row();
}


// Depth for links in navigator for menu page
function getmenunavigatordepth($page) {
    $sql = new SQLSelectStatement(MENUS_TABLE, 'navigatordepth', array('page_id'), array($page), 'i');
    return $sql->fetch_value();
}

// Preview for an article in menu contents
function getpagepreviewdata($pages) {
    if (empty($pages)) {
        return array();
    }
    $sql = new SQLSelectStatement(ARTICLES_TABLE, array('page_id', 'article_author', 'source', 'day', 'month', 'year'));
    $sql->add_integer_range_condition('page_id', $pages);
    return $sql->fetch_many_rows();
}


// ***************************  articlemenu ********************************* //

// Article years for filter menu
function getallarticleyears() {
    $sql = new SQLSelectStatement(ARTICLES_TABLE, 'year');
    $sql->set_order(array('year' => 'ASC'));
    $sql->set_distinct();
    return $sql->fetch_column();
}


// Return filtered selection of articles
function getfilteredarticles($page, $selectedcat, $from, $to, $order, $ascdesc, $showhidden)
{
    $values = array();
    $datatypes = '';

    $query = 'SELECT DISTINCTROW art.page_id, page.title_page, page.pagetype, page.language FROM ';
    $query .= ARTICLES_TABLE . ' AS art, ';
    $query .= PAGES_TABLE . ' AS page';

    // Filter for categories
    if ($selectedcat != 1) {
        // get all category descendants
        $categories = getcategorydescendants($selectedcat, CATEGORY_ARTICLE);
        $datatypes = str_pad($datatypes, count($categories) + strlen($datatypes), 'i');
        $values = array_merge($values, $categories);

        $query .= ', ' . ARTICLECATS_TABLE . ' AS cat WHERE cat.page_id = art.page_id';
        $query .= ' AND cat.category ' . SQLStatement::create_in_question_marks($categories) . ' AND';
    } else {
        $query .= ' WHERE';
    }

    $query .= ' page.page_id = art.page_id';

    if (!$showhidden) {
        $query .= ' AND page.ispublished = ?';
        array_push($values, 1);
        $datatypes .= 'i';
    }

    // get pages to search
    $pages = getsubpagesforpagetype($page, 'articlemenu');
    $datatypes = str_pad($datatypes, count($pages) + strlen($datatypes), 'i');
    $values = array_merge($values, $pages);
    $query .= ' AND page.parent_id ' . SQLStatement::create_in_question_marks($pages);

    // Filter for years
    if ($from != 'all' && $to != 'all') {
        $query .= ' AND art.year BETWEEN ? AND ?';
        array_push($values, $from);
        array_push($values, $to);
        $datatypes .= 'ss';
    }

    if ($order) {
        switch ($order) {
            case'title':
                $query .= ' ORDER BY page.title_page ';
            break;
            case 'author':
                $query .= ' ORDER BY art.article_author ';
            break;
            case 'date':
                $query .= ' ORDER BY art.year, art.month, art.day ';
            break;
            case 'source':
                $query .= ' ORDER BY art.source ';
            break;
            case 'editdate':
                $query .= ' ORDER BY page.editdate ';
            break;
        }
        $query .= mb_strtolower($ascdesc, 'UTF-8') === 'desc' ? 'DESC' : 'ASC';
    }

    $sql = new RawSQLStatement($query, $values, $datatypes);
    return $sql->fetch_many_rows();
}
?>
