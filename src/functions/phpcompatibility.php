<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */


// *************************************************************************
// Comparison functions
// *************************************************************************

//
// true if $string ends with $suffix
// Will be available with PHP 8
//
if (!function_exists('str_ends_with')) {
// Keep indentation for CI checks
function str_ends_with($string, $suffix) {
    return !empty($suffix) && substr($string, strlen($string) - strlen($suffix)) === $suffix;
}
}

//
// true if $string starts with $prefix
// Will be available with PHP 8
//
if (!function_exists('str_starts_with')) {
// Keep indentation for CI checks
function str_starts_with($string, $prefix) {
    return !empty($prefix) && substr($string, 0, strlen($prefix)) === $prefix;
}
}

//
// true if string $haystack contains string $needle
// Will be available with PHP 8
//
if (!function_exists('str_contains')) {
// Keep indentation for CI checks
function str_contains($haystack, $needle) {
    return !empty($needle) && strpos($haystack, $needle) >= 0;
}
}

?>
