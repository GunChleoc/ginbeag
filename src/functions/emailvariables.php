<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */


// We need to define legal vars here so that we can access the database, which contains the dynamic variables.
$backtrace = debug_backtrace();
$iscontactpage = false;
$isadmin = false;
foreach ($backtrace as $entry) {
    if (isset($entry['file'])) {
        $iscontactpage = $entry['file'] === BASEDIR.'/contact.php' || $entry['file'] === BASEDIR.'/guestbook.php';
        $isadmin = strpos($entry['file'], '/admin/');
        if ($isadmin) break;
    }
}
if ($iscontactpage) {
    define(
        'LEGALVARS',
        array(
            'cancel',
            'fbclid', // Appended by facebook to track you with
            'm',
            'post',
            'postername',
            'sid',
            'submitpost',
            'token',
            'userid'
        )
    );
} else {
    if ($isadmin) {
        include_once BASEDIR.'/admin/includes/legaladminvars.php';
    } else {
        include_once BASEDIR.'/includes/legalvars.php';
    }
}
require_once BASEDIR.'/functions/db.php';

// initialize anti-spam variable names
$sql = new SQLSelectStatement(ANTISPAM_TABLE, array('property_name', 'property_value'));
$emailvariables = $sql->fetch_two_columns();

// Check legal vars with the dynamic vars
foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (!in_array($key, array_merge(LEGALVARS, $emailvariables))) {
        header('HTTP/1.0 404 Not Found');
        print('HTTP 404: Sorry, but this page does not exist.');
        include_once BASEDIR.'/config.php';
        if (DEBUG) {
            print("<br />'$key' not registered with vars for e-mail contact.");
            include_once(BASEDIR.'/functions/debug.php');
            print(format_backtrace());
        }
        exit;
    }
}

?>
