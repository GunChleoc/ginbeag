<?php
/**
 * An Gineadair Beag is a content management system to run websites with.
 *
 * PHP Version 7
 *
 * Copyright (C) 2005-2022 GunChleoc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @category Ginbeag
 * @package  Ginbeag
 * @author   gunchleoc <fios@foramnagaidhlig.net>
 * @license  https://www.gnu.org/licenses/agpl-3.0.en.html GNU AGPL
 * @link     https://github.com/gunchleoc/ginbeag/
 */

define('BASEDIR', dirname(__FILE__));

include BASEDIR.'/includes/legalvars.php';
include_once BASEDIR.'/functions/db.php';

// anti bot nonsense links
// ********************************* achtung - bot secure ist server-spezifisch!
$testpath = "/".getproperty("Local Path");
if (getproperty("Local Path") == "") {
    $testpath = "";
}

if (!((isset($_SERVER["ORIG_PATH_TRANSLATED"])
    && $_SERVER["ORIG_PATH_TRANSLATED"] == BASEDIR.'/login.php')
    || $_SERVER["PHP_SELF"] == $testpath."/login.php")
) {
    //    print("test: ".$_SERVER["PHP_SELF"]);
    header("HTTP/1.0 404 Not Found");
    print("HTTP 404: Sorry, but this page does not exist.");
    exit;
}

require_once BASEDIR.'/functions/links.php';
require_once BASEDIR.'/functions/publicusers.php';
require_once BASEDIR.'/includes/objects/elements.php';
require_once BASEDIR.'/includes/objects/login.php';
require_once BASEDIR.'/includes/objects/pageelements.php';

if (isset($_POST['user'])) {
    $user=trim($_POST['user']);
    $userid=getpublicuserid($user);

    if (!$userid) {
        $header = new PageHeader(0, getlang("login_pagetitle"), getlang("login_pagetitle"),);
        $loginform = new LoginForm($user, getlang("login_error_username"));
        $footer = new PageFooter();
    } elseif (ispublicuseractive($userid)) {
        $login=publiclogin($user, trim($_POST['pass']));
        if (array_key_exists('sid', $login)) {
            $_GET['sid']= $login['sid'];
            $contenturl='index.php'.makelinkparameters($_GET);

            $header = new HTMLHeader(
                getlang("login_pagetitle"),
                getlang("login_pagetitle"),
                $login['message'],
                $contenturl,
                getlang("login_enter"), true
            );

            $footer = new HTMLFooter();
        } else {
            $header = new PageHeader(0, getlang("login_pagetitle"), getlang("login_pagetitle"), '', $displaytype = 'login');
            $loginform = new LoginForm($user, $login['message']);
            $footer = new PageFooter();
        }
    } else {
        $header = new PageHeader(0, getlang("login_pagetitle"), getlang("login_pagetitle"), '', $displaytype = 'login');
        $loginform = new LoginForm("", getlang("login_error_inactive"));
        $footer = new PageFooter();
    }
} else {
    $header = new PageHeader(0, getlang("login_pagetitle"), getlang("login_pagetitle"), '', $displaytype = 'login');
    $loginform = new LoginForm("");
    $footer = new PageFooter();
}

print($header->toHTML());
if (isset($loginform)) {
    print($loginform->toHTML());
}
print($footer->toHTML());
?>
