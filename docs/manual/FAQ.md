# Frequently Asked Questions (FAQ)

## General Pages FAQ

### Do the "page title" short and page title which shows up on the article page have to be added seperately?
Yes. The short title will be shown in the navigator, and the long title in the page itself.

### Under the "Editing Page" window, the "rename Page" function only changes the "page title" short heading?
No, it changes both. Since both titles are automatically filled out, you don't have to type everything in anew, just make your changes.

### Can an article be placed in between two others in the lineup?
Yes. Go to "Edit Page, then use the Move Up/Move Down/MOve to the Top/Move to the bottom Buttons.

### I created a page in the wrong place. What do I do?
Go on "Edit Page", then scroll down to the "Move somewhere else..." Button. You will get a list of pages where you can move it to.

### Within the text editing page, is there a way to include size of font in the options?
No. I did not give this option on purpose, because I want to control this by stylesheet. This way, all the articles will have the same layout. If you wish a bigger font size in general, I have to code this for the moment. I hope one day I'll get around to giving editing styles functionality, but that is low priority on my list.

### Is there a way to change the color of the title?
No, it's again a stylesheet thing.

### The 30 character short title is what the member sees to look up the article, correct? Is the Page Tree going to be the same size to the member? If not, can it be larger or can we have a longer line then 30 characters?
I thought 30 caracters are enough for the navigation bar on the left. What I should do is to always display the long titles in the page itself. e.g. when you've got a menu page, the short titles will show on the navigation bar on the left, and the long titles on the right.

### When we start adding pages, will they be cloaked until we unveil them?
Yes, all pages are hidden from public view until you publish them with the button on the top left, above "New Page". If the button won't show, the page is makred as an internal page. When you are creating a new page, make sure to set it to "public page", then the publishing process will be 2 mouse-clicks faster. I only set the default to "Internal Page" because I'm paranoid. You can earmark a page for publishing when you go on Edit Page.

### Question for you: in regards to making a page public. The person entering the page would need to both 1. change the setting from "internal" to "public" AND change the button in the tool bar at the end of the procedure from "hidden" to "publish"?
You've got that right!

Or, when you are creating the page, declare it "public" right away, then you only need to use the "Publish" button later.

### When I added the new page for 1987-1999 (menu)...in the Page Tree it came up as 1987-1999 (menu) (article). I did not put in the word \
You didn't add a menu page called 1987-1999, you added an article page called 1987-1999 (menu). The software is telling you correctly that it is an article, so it says 1987-1999 (menu) (article). Just make a new menu page and move the articles over.

### I have to resort tons of pages. What do I do?
If you wish to move more than one page around, go to the next higher menu/articlemenu/linklistmenu page, then edit this page. In "Edit page contents..." you'll find a list of all subpages with move page up/down buttons. This should go faster than editing the individual pages.

## Articles FAQ

### How do I categorize an Article?
Go to "Edit Page", then "Edit Page contents...", then scroll down to the bottom.

If the category you need isn't there, you can go to "Edit Categories" on top of the page.

### I don't see where I can add the text for the article
I admit this is a bit complicated. Go to Edit Page, then Edit Page contents..., then read up on the rest on Page#11 of the guide, especially in the "Editing Article pages" section. We've unfortunately got a terminology mix here: First, the article itself is a page, like a menu or a note is a page. Then, the article itself again contains pages

The general structure of an article is a bit hierarchical, which means you need to do some mouse-clicks to get at the text itself:

- first, you've got the general settings as with all pages, e.g. the titles and were it will show up on the site.

- then, every article has a synposis and some source information like author, location...

- after this, the article contains pages, and the pages contain sections.

### I went under to edit page 1 (because I really don't know where to add my text) and under "add section" which I guess would be to add some more text - but where.
When you use "Add Section", the only thing you do is create an empty section. You can then enter a title, the text, and below that, an image. There's a button that says "Edit Text".

### I can figure out almost everything but how to enter the TEXT of the article.
Find the following things and click on them:

* The "Edit Page"-Link on top of the page
* The "Edit page contents..."-Button below the "Rename Page"-stuff
* The "Page 1"-button to the left of "Edit Pages:"
* The "Add Section"-Button
* The "Edit Text"-Button above the "Section Image"-stuff (I just moved it up there)

### How will the Source Date be displayed?
If you've only got a year, it's only the year, same goes for year and month. If you've e.g. only got a day and nothing else, it will not show up, because that doesn't make sense.

### If the article has two categories (ie. Hidalgo and Politics) can they be catergorized in two places?
Add as many categories as you want. You can select more than one at a time to make this faster. Categories are hierarchical. This means that when you add the category "Hidalgo", the article is automatically in the category "movies" as well.

### There is going to be a problem with "By" being inserted by the program in a couple of cases. If articles do not specify an author name, but to avoid having "By" and nothing else show up on the article heading, I'll have to put something.
"By" only gets added if there is an author. You don't have to add anything by force.

## Newsitems FAQ

### Is there a chance that you can give me a Quote function on the synopsis page?
Just add a quoted section.

There are no [quote] tags. Instead, you can add a whole quoted section. You should see an "Insert Section" and an "Insert Quoted Section" button.

### How do I add Newsitems?
Do not add separate pages for each newsitem. You will only need one newspage, and you add newsitems to that page, which you then publish individually within that page. (The "Publish Newsitem" button you see while editing).

### What can we do (if anything) about that quoted text not fitting in the box when I add a picture to it? I really LOVE that option!!
Use smaller pictures or get a bigger computer screen :-> . Even better yet, don't add the image with the [img] tag, but as a webpage image, then it will resize itself a bit if necessary.

## Categories FAQ

### The categories do not give me the choice I need.
Use the "Edit Categories"-Link in the navigation bar on top of the page. You can add, rename, delete and move categories there.

### Can you tell me how to create a lead category as opposed to a sub-category for this section?
If you wish to enter a new a lead category, add it directly below "All Categories".

### Which category should I assign to my image/article/newsitem?
"All Categories" means all articles ever posted, regardless of if you assigned any categories to them. Don't add that to an article, but rather be as specific as you can, e.g. if someone is searching for Movies, all articles on The Lord of the Rings will be included automatically, so it is better to add "The Lord of the Rings", not "Movies" etc.

### What happens when I delete a category?
If you delete a Category, all elements assigned to that Category (Images, Pages, Newsitems) will automatically be assigned to the parent category instead. e.g. If you delete "The Two Towers", all Articles, Images etc. concerning TTT will then be assigned to "The Lord of the Rings".

## Images FAQ

### How do I add an image?
Click the "Images"-link on top of the page. You can upload images and edit their properties there. Once an image is uploaded, you can use it in your page by pasting the image's filename.

### I don't want to get ahead of myself...but will we be able to categorize like the articles?
You can categorize individual pictures

### Is this where you wanted pics only 175 pixels wide. That's kinda small. Can they be bigger?
Make the picture as big as you want. The 175 pixels are for the thumbnails, not for the picture itself.

### Tell me about the blue frame you have on your pics? How do you add it. Can we choose another color?
The blue frame is only in the image list, so you see it's got a thumbnail. When you look at a page with a picture in it you will see it's got no frame.

### When I use the "browse" button my own "My Pictures" file comes up.
That is correct. Images are uploaded from your personal computer or even a disc. You cannot add pictures from the web, you have to download them to your computer first.

### I have just uploaded a picture, but the page where I'm adding it won't recognize it.
Images filenames are case-sensitive, and you need to add the full name, including the extension.

### How do I rename a picture?
You can only rename images while adding them. Do not use the extension (.jpg etc.) in the new name, this will be added automatically by the software. e.g. if you want to rename the image file from "oldname.jpg" to "newname.jpg", enter "newname". If you enter "newname.jpg", it will become "newname.jpg.jpg".

### How do I rename an image thumbnail?
You can't. Thumbnails are automatically given a filename relative to the image. e.g. if you upload an image called "image1.jpg", and your thumbnail file is "whatever.gif" the thumbnail will automatically be renamed to "image1_thn.gif".

### If I wish to replace an image or thumbnail file, do I have to rename it on my computer first?
This is not necessary. When you are replacing image and thumbnail files, they will be renamed automatically.

### What happens when I delete an image?
If you delete an image, its thumbnail will be deleted with it. If the image is used in a page, the software will refuse to delete it and give you a list of the pages where it is still in use.

### I'd like to add images to an article on a second page. When I add a second page I can not figure out how to add the images. I did add an image to a page 2 of and article but I could not see any image once I viewed the page.
If you want an image on the second page, you'll have to add it to a section. So, create a section first.

### Why can't I delete my Image? Where is the "Delete Image" button?
You can only delete images that are no longer used. If you can't see a "Delete Image" button, you should see a list of pages where the image is used instead. You will first have to remove the image from those pages, before you can delete it.

This may seem like a nuisance, but saves us from lots of annoying red X's.

## Administration FAQ

### How do I edit the Splash Page?
Go to _Administration_. In the _Features & Layout_ section, go to _Site Layout_. Scroll down to the _Splash Page_ section, enter your changes and submit.
