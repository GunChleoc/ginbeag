# Overview - What Is Where?

The screen is split into three frames - the top Navigation Bar, the Page Tree on the left, and the Main Display on the right.

## The Navigation Bar
The Navigation Bar on top of the page is divided into three sections:

* **Page editing**
 In the page editing section, you can create a _new page_ under the selected page, and _edit, preview or delete_ the selected page. To select a page, use the link in the Page tree.

* **General properties**
  _Images_ displays all images of this site. All images are kept in the database to make them searchable for you, and therefore easily reusable. If you wish to add an image to your page, please use this function.

* _Categories_ are for classifying your pages and images. They will be used as options in the search function on both the News and Article pages.

* _Administration_ is for controlling the technical setup of the site and to set some general displaying preferences for it. Please be careful about changing any settings there. You can also visit a statistics section and backup the database there.

* **User functions**
  If you go to _Your Profile_, you can change your e-mail address and password. You can also add yourself to the site's contact form.

  Please _log out_ before you go, so your session will be terminated. Sessions expire after 1 hour of inactivity.


## The Page Tree
Use the Page Tree on the left to navigate through the site's pages.

When you have moved or renamed a page, you will need to use a link or button called "done" in the Main Display for the Page Tree to update.

The Page Tree shows the following information about your page:

* Page Number: Navigation Title (Page Type)

At the top of the page tree, you can shortcut the selection of a page by entering its number and hiting "Go". If you use the "Choose page from list"-link, you will get a full display of all existing pages.

## The Main Display
The Main Display is used for previewing and editing the site's pages.

* **Viewing mode**
  When you select a page on the Page Tree, the Main Display will switch to viewing mode. The pages will be displayed the way they will look when published. Please note that the links in the pages will not select new pages, so when you go to Edit Page, the page which you originally selected on the Page Tree will show up. Some of the links might even be broken, or log you out. In case that happens, just use the "Back"-button of your Browser.

  Below the page title, you find the following text:

  * Please link to this URL only in internal discussion! Public link to this page: link

  Use this to grab links to pages for using them in another page's text. Plase do not us the URL of the previewed pages.

* **Editing mode**
  When you select "Edit Page" in the navigator, the Main Display will switch to editing mode. Some General Settings common to all pages will show up. Please see the editing section for more details.

  When you switch to editing mode, you will automatically acquire an editing lock for the page. This serves to avoid concurrent editing of the same page by two different users. The page lock will only be released when you use the "Done"-Button on the bottom of the page, or when you log out. Please see the editing section for more details.

# Working With Pages

Working with pages involves the following steps:

* Creating the page, selecting where it goes, giving it a name and choosing a page type.
* Adding and editing the page's contents
* Publishing

## Page Types
To keep editing pages as simple as possible, the software relies on the concept of page types to give you a template on what the page can do, and how it will be displayed. There are also restrictions on which types of pages can go where.

### Menu Pages
Menu pages are used to structure the site. Their main function is to provide links to the pages placed inside them, but you can also add an introductory text to them. You can create a menu page only below the site root, making it a top level page, or inside another menu page. This goes with the structuring concept, since it would make no sense to have structuring below a content page, e.g. an article. This structuring concept is used to force you to make the site easily navigable for its visitors.

For certain page types, there are special menu pages: articlemenu pages for article pages, and linklistmenu pages for linklist pages. The difference between a normal menu page and these special menu pages is the way in which they are displayed: articlemenu pages offer a search function, and linklistmenu pages give a summary of the links contained in the linklist pages assigned to them. So, editing these special pages is exactly the same as editing a basic menu page, only the way in which they are displayed is different.

### Article Pages
Article pages are used to convey structured text information. They start with a synopsis, followed by sections. You can split long articles over multiple pages, and each section is assigned to a page. The synopsis will always be on page 1 of the article (obviously). Additional information that you can add to the first page of the article are source, a link to the source, author, location and date. You can add one image to the synopsis and one to each section.

You can also categorize article pages using the categories defined in the Categories function. Doing this will make it easier for your visitors to find the article they are looking for, because it is used in the articlemenu's search function.

Article pages can only be created inside articlemenu pages, and articlemenu pages can only be created inside articlemenu or menu pages, or the site root (making them a top level page).

The page you are viewing right now is an Article page.

### External Pages
External pages are used to link to off-site content. You can give them a name and provide a link, and they will appear in the site's navigator. If a visitor clicks on this link, a new browser window will open with the external site in it. External pages can only be created inside menu pages, or the site root (making them a top level page).

### Gallery Pages
Gallery pages can be used to create image galleries. You can add an optional introductory text, then include images that were added to the site's database with the Images function. The images are automatically distributed over more than one page if necessary.

Gallery pages can only be created inside menu pages.

### Linklist Pages
Linklist pages are used to collect links to other sites. Each link can be furnished with a description and an image, e.g. the other site's logo.

Linklist pages can only be created inside linklistmenu pages, or the site root (making them a top level page), andinklistmenu pages can only be created inside linklistmenu or menu pages, or the site root.

### News Pages
News pages are for displaying text in such a fashion that the newest entry is always on top, and there are usually more than one Newsitem per page (the standard setting is five items per page). You can e.g. use this format to keep track on recent developments concerning your site's subject matter, or to inform your visitors of site updates.

Each Newsitem can contain a synopsis with optionally one or more images. You can also give source information with a link, a location and credit the person who found it.

You can structure a newsitem by adding sections and quoted sections.

You can also categorize Newsitems using the categories defined in the Categories function. Doing this will make it easier for your visitors to find the Newsitem they are looking for, because it is used in the News page's search function.

News pages can only be created inside menu pages, or the site root (making them a top level page).

## Creating A New Page
If you wish to create a new page, select a parent page in the Page Tree, then select "New Page" in the Navigation Bar. You will see the options for creating the page in the Main Display.

* **Will this page be published?**
  Public page: You can publish the page when you finished editing it.
  Internal page : This page can not be published.

  These settings can be changed later when editing the page, so don't worry if you forget to select "Public page". The only exceptions are notes, where this selection is ignored.

* **Create page under root instead**
  The new page will be automatically created under the page you selected. If you wish to create a main page instead, please check this box.

* **Page title (short)**
  The page title that will be displayed in the site's navigator.

* **Page title**
  The title that will be shown on the page itself.

* **Page type**
  How a page is edited, what elements can be in it and how it will be displayed, all depends on the page type. There are also restrictions on which types of pages can go where. For more details, please see the Page Types section.

When you are done adding a page, the software will automatically select the new page.

## Editing A Page
If you wish to edit a page, select the page in the Page Tree, then select "Edit Page" from the Navigation Bar. The page will be locked for editing and the Main Display will switch to editing mode.

### General Settings

When you have submitted a change in this section, sometimes you will see a confirmation page. Click on "Done" to return to the editing page.

In the General Settings display, you have the following options:

* **Will this page be published?**
  Unless the page is a note, which are always internal, you can determine whether it is an internal page or earmarked for publishing. If you mark a published page as "Internal", it will be unpublished and removed from view. Use the "Change Setting"-Button to save your changes.

* **Rename Page**
  Enter the new name for the page, then use the "Rename"-Button to submit your changes.

* **Edit Page contents...**
  This button will take you to the page's contents. What happens there depends on the pagetype, so please read up on this in the appropriate sections.

* **Moving a page**
  _move to the top/move page up/move page down/move to the bottom_ will change the location of the page in relation to its sister pages (sister pages are pages that have the same parent page, e.g. are on the same level in the hierarchy and below the same page). Please feel free to experiment, you can't break anything.

  _Select new parent_ will move the page any place you choose. There are some restrictions on where you can move a page, see more on this in the Page Types section.

* **Done**
This button will update the Page Tree with your changes and release the editing lock. The Main Display will change back to viewing mode.

## Deleting A Page
In order to delete a page, select the page in the Page Tree, then select "Delete Page" from the Navigation Bar. If you delete a page, all subpages will be deleted as well. You will be asked to confirm in the Main Display Window, which will also display the subpages that will be included in the delete.

This cannot be undone!

## Publishing A Page
When you create a new page, it is only visible internally. In order for the page to appear on the website, you need to publish it.

In order to publish a page, select the page on the Page Tree. Unless the page is marked as an internal page, you can see a "Publish Page"-Button in the Main Display. If the page is already published, the button will toggle to "Hide Page", and you can use it to remove the page from general viewing.

News pages are a special case: In addition to the page, you also have to publish each Newsitem individually to make it visible for your site's visitors. The "Publish newsitem" button also toggles, so you can hide published Newsitems.

# News Pages
News pages are for displaying text in such a fashion that the newest entry is always on top, and there are usually more than one Newsitem per page (the standard setting is five items per page). You can e.g. use this format to keep track on recent developments concerning your site's subject matter, or to inform your visitors of site updates.

Each Newsitem can contain a synopsis with optionally one or more images. You can also give source information with a link, a location and credit the person who found it.

You can structure a newsitem by adding sections and quoted sections.

You can also categorize Newsitems using the categories defined in the Categories function. Doing this will make it easier for your visitors to find the Newsitem they are looking for, because it is used in the News page's search function.

News pages can only be created inside menu pages, or the site root (making them a top level page).

## Newsitem Structure
News pages are made up of newsitems. The paging is handled by the software, so you just add a new item, fill it with info and that's it. _You have to publish each news item separately_ though, so they won't show up half-finished. Equally, you can hide each individual item. Of course, for them to show up on the homepage, the news page itself needs to be published as well.

Newsitems have a title, a synopsis text, and, if you wish, one or more images that will appear with the synopsis. These elements appear on the top of the editing page.

Below that, you can optionally add text sections with one image each at most. You can also add quoted section, if you wish to e.g. quote a piece of an article.

Finally, you can enter some source information on the bottom of the page. As source info, you can add source, sourcelink and location same as with the articles, and you can specify "Found by:" if you wish to credit someone for the scoop.

Please categorize all news items for the search function. Note that it isn't necessary to add a category "News", because it's a News page anyway.

You can delete a Newsitem by using the button on the bottom of the page.

You can also archive Newsitems using the Archive button.

In a nutshell, the editing display of a newsitem is organized as follows:

* Add Newsitem - Archive Newsitems...
* Publish/Hide/preview
* The synopsis: Edit Title - Edit Synopsis - Add Image
* The sections: Edit Text - Image properties - Delete/Insert/Insert Quoted
* Source info
* Categories
* Delete Newsitem
* General settings/Done buttons


## Navigating News Items
When you are working on a News page, the most recent item is displayed. If you want to work on an older item, you will have to use the "Goto page:"-links on the top right. You can also jump to a page directly, when you enter the page number and hit "Go".

When you are done editing, hit the "Done" button at the bottom of the page. You can also use the "General Settings" button, if you wish to edit the News page's general properties like e.g. the page name, or its position in the navigator.

## Working with Sections
Newsitems have sections, just like articles. You can add one image per section.

Unlike with the articles, you can't move sections around. Please let me know if you need this function. I decided not to add it for now to keep the cluttering down.

You can insert a section anyplace you please, not just at the end.

If you go on "insert quoted section", the new section will be automatically quoted, and all the sections you might create in the future that are within "begin quote" ... "end quote" will be included in the quote as well.

## Previewing and Publishing
Since you will be usually working on Newsitems in an already published News page, each Newsitem will have to be published separately. You can publish a Newsitem by hitting the "Publish newsitem" button, which then toggles to "Hide newsitem". Only then will the newsitem be visible in the page preview, and simultaneously on the (published) News page itself.

So, in order to preview your Newsitem, you can't just use the "Preview Page" link. There is an extra "Preview Newsitem" link on the side of the "Publish newsitem" button, so you can see your results before publishing a Newsitem.

## Archiving News Items
You can split off part of the News Items into a new page, using the "Archive Newsitems..." button on the top right. Just enter the date up to which you want the items archived. All Newsitems that are from the specified date or older will be moved to a new News page. This page will appear as a subpage of the current News page in the Navigator.

**Caution**: Once the Newsitems have been moved to the new page by the archiving function, they cannot be moved back!
